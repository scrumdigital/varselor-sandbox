<?php
/**
 * Template name: Vendor Overview Templete 	
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

get_header();
?>

	<section id="primary" class="content-area">
		<div class="container">

			<?php

			/* Start the Loop */
			while ( have_posts() ) :  the_post();
?>

				<?php the_content(); ?>
			<?php

				//get_template_part( 'template-parts/content/content', 'page' );

				//If comments are open or we have at least one comment, load up the comment template.
				if ( comments_open() || get_comments_number() ) {
					comments_template();
				}

			endwhile; // End of the loop.
			?>

</div>

	</section><!-- #primary -->

<?php
get_footer();