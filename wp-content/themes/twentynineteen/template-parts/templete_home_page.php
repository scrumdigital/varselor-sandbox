<?php
/**
 * Template name: Front Home Page
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

get_header();
?>

	<section id="primary" class="content-area">
		<main id="main" class="site-main">

			<?php

			/* Start the Loop */
			//while ( have_posts() ) :  the_post();
?>

				<?php //the_content(); ?>
			<?php

				//get_template_part( 'template-parts/content/content', 'page' );

				// If comments are open or we have at least one comment, load up the comment template.
				// if ( comments_open() || get_comments_number() ) {
				// 	comments_template();
				// }

			//endwhile; // End of the loop.
			?>

			<div class="homeBanner-wrap">
				<div class="container">
					<div class="row">
						<div class="col-lg-8">
							<div class="homeBanner-content-wrap">
								<h1 class="homeBanner-title">The Enterprise Technology Marketplace</h1>

								<p> Varselor is a technology platform that helps buyers, from Enterprise to start ups, to discover, engage and procure enterprise solutions, services and experts.<br>The Varselor marketplace is all about facilitating speedy, efficient, and transparent dealings between buyers and enterprise solution provider. </br>Discover the appropriate Enterprise Solution Provider, invite them to bids through the smart procurement tool, compare submitted proposals, and procure the desired solution/service.  </p>
								<!-- <p>
									 <strong>How?</strong><br>
									Business-solution buyers, both from the Enterprise and SME, use the Varselor-marketplace to compare, buy and execute - ERP, CRM, HCM, BI and many more Business-critical Solutions.<br>Software and hardware vendors, ISV's, resellers, independent software consultants, list out their products & services. Access and engage buyers from around the globe and locally - growing and widening the customer-base, getting hassle-free payments.

								</p> -->

								<div class="homeBanner-btn-block">
									<a href="/buyer" class="buyProduct-btn popmake-102">Enterprise Buyers</a>
									<a href="/vendor" class="sellProduct-btn">Enterprise Vendors</a>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="homeBanner-shape"></div>
			</div>

<!-- 			<div class="homeBanner-wrap">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<div class="owl-slider homeBanner-slide-wrap">
								<div class="owl-carousel homeBanner-carousel">
									<div class="item">
										<div class="row">
											<div class="col-lg-8">
												<div class="homeBanner-content-wrap">
													<h1 class="homeBanner-title">The Business-Solutions Marketplace</h1>

													<p class="mb-0"> Connects you to the technology vendors - links Enterprise / SME to the service providers.<br> Choose the right technology for your Business - engage and hire the Experts!<br>Varselor is the marketplace to compare, buy and execute on - ERP, CRM, HCM, BI and many more Business-critical Solutions.</p>
</p>

													<div class="homeBanner-btn-block">
														<a href="/buyer" class="buyProduct-btn">Buy Product </a>
														<a href="/vendor" class="sellProduct-btn"> Sell Product </a>
													</div>
												</div>
											</div>
										</div>
									</div>

									<div class="item">
										<div class="row">
											<div class="col-lg-8">
												<div class="homeBanner-content-wrap">
													<h1 class="homeBanner-title">How?</h1>

													<p> Business-solution buyers, both from the Enterprise and SME, use the Varselor-marketplace to compare, buy and execute - ERP, CRM, HCM, BI and many more Business-critical Solutions.<br>Software and hardware vendors, ISV's, resellers, independent software consultants, list out their products & services. Access and engage buyers from around the globe and locally - growing and widening the customer-base, getting hassle-free payments.</p>

													<div class="homeBanner-btn-block">
														<a href="/buyer" class="buyProduct-btn">Buy Product </a>
														<a href="/vendor" class="sellProduct-btn"> Sell Product </a>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						
					</div>
				</div>

				<div class="homeBanner-shape"></div>
			</div> -->
			<div class="varselor-overview-block">
				<div class="container">
					<div class="row">
						<div class="col-lg-5">
							<div class="overview-left-wrap howDoesWork-section">
								<h2> Key Advantages of Varselor Marketplace </h2>
								<ul>
									<li>Discover enterprise solutions and service offerings from thousands of global and local technology vendors</li>
									<li>Get access to a worldwide network of Varselor-service-providers (for professional Services).</li>
									<li>Leverage world-class IT resources to plug the skills gap on a project basis.</li>
									<li>Reduce procurement lead time and make the right technology decisions with analytical insights and comparison data.</li>
									<li>Make technology sourcing simpler, faster, and more streamlined, including multiple vendors and contract management through the platform.</li>
									<li>Reduce technical debt by tracking redundancies and replacing brittle, monolithic systems with more secure, fluid, configurable solutions.</li>
									<li>Track license/service contracts, expiry dates, and renewals. Capitalize milestones for potential renegotiation.</li>
									<li>Stay tuned with the latest technology trends and roadmaps.</li>
								</ul>

								<div class="separator-border"></div>
							</div>
						</div>

						<div class="col-lg-7">
							<div class="overview-right-wrap">
								<h2>How does it Work?</h2>

								<div class="howDoesWork-section">
									<ol>
									 	<li>Access all the enterprise solutions directly from principle Vendors, Services from Systems integrators/independent consultants, and expert resources listed on the marketplace</li>
										<li>Directly engage with them or post bids through the smart procurement tool.</li>
										<li>Buyers can use advanced procurement analytics to help them make informed decisions with robust metrics and comparison data to procure the best solution and associated service.</li>
									</ol>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="varselor-video">
				<div class="videoWrapper videoWrapper169 js-videoWrapper">
				    <!-- YouTube iframe. -->
				    <!-- note the iframe src is empty by default, the url is in the data-src="" argument -->
				    <!-- also note the arguments on the url, to autoplay video, remove youtube adverts/dodgy links to other videos, and set the interface language -->
				    <iframe class="videoIframe js-videoIframe" src="" frameborder="0" allowTransparency="true" allowfullscreen data-src="https://www.youtube.com/embed/Heqr-Y5xhKg?autoplay=1& modestbranding=1&rel=0&hl=sv"></iframe>
				    <!-- the poster frame - in the form of a button to make it keyboard accessible -->
				    <button class="videoPoster js-videoPoster" style="background-image:url(https://varselor.scrumdigital.com/wp-content/themes/twentynineteen/images/video-bg.jpg);">Play video</button>
				</div>
			</div>

			<div class="ourMain-product-section">
				<div class="container">
					<div class="row">
						<div class="col-lg-9">
							<div class="product-sec-title">
								<h2 class="title-head">Top Enterprise Solutions on offer</h2>
							</div>
							<div class="ourMain-product-des">
								<p>Access all the enterprise solutions directly from Global and local Enterprise solution providers</p>
							</div>

							<div class="separator-border"></div>
						</div>
					</div>
				</div>

				<div class="product-carousel-wrap">
				  <div class="owl-carousel ourMain-product-slider">
					  
				    <div class="item main-product-item"><img src="<?php bloginfo('template_url'); ?>/images/image1.png" class="img-fluid m-auto d-block"></div>
				    <div class="item main-product-item"><img src="<?php bloginfo('template_url'); ?>/images/image2.png" class="img-fluid m-auto d-block"></div>
				    <div class="item main-product-item"><img src="<?php bloginfo('template_url'); ?>/images/image3.png" class="img-fluid m-auto d-block"></div>
				    <div class="item main-product-item"><img src="<?php bloginfo('template_url'); ?>/images/image4.png" class="img-fluid m-auto d-block"></div>
				    <div class="item main-product-item"><img src="<?php bloginfo('template_url'); ?>/images/image5.png" class="img-fluid m-auto d-block"></div>
				    <div class="item main-product-item"><img src="<?php bloginfo('template_url'); ?>/images/image6.png" class="img-fluid m-auto d-block"></div>
					  <div class="item main-product-item"><a href="https://varselor.scrumdigital.com/buyer/agency/Zebra%20Technologies"><img src="<?php bloginfo('template_url'); ?>/images/image23.png" class="img-fluid m-auto d-block"></a></div>
				    <div class="item main-product-item"><img src="<?php bloginfo('template_url'); ?>/images/image7.png" class="img-fluid m-auto d-block"></div>
				    <div class="item main-product-item"><img src="<?php bloginfo('template_url'); ?>/images/image8.png" class="img-fluid m-auto d-block"></div>
				    <div class="item main-product-item"><img src="<?php bloginfo('template_url'); ?>/images/image9.png" class="img-fluid m-auto d-block"></div>
				    <div class="item main-product-item"><img src="<?php bloginfo('template_url'); ?>/images/image10.png" class="img-fluid m-auto d-block"></div>
				    <div class="item main-product-item"><img src="<?php bloginfo('template_url'); ?>/images/image11.png" class="img-fluid m-auto d-block"></div>
				    <div class="item main-product-item"><img src="<?php bloginfo('template_url'); ?>/images/image12.png" class="img-fluid m-auto d-block"></div>
				    <div class="item main-product-item"><img src="<?php bloginfo('template_url'); ?>/images/image13.png" class="img-fluid m-auto d-block"></div>
				    <div class="item main-product-item"><img src="<?php bloginfo('template_url'); ?>/images/image14.png" class="img-fluid m-auto d-block"></div>
				    <div class="item main-product-item"><img src="<?php bloginfo('template_url'); ?>/images/image15.png" class="img-fluid m-auto d-block"></div>
				    <div class="item main-product-item"><img src="<?php bloginfo('template_url'); ?>/images/image16.png" class="img-fluid m-auto d-block"></div>
				    <div class="item main-product-item"><img src="<?php bloginfo('template_url'); ?>/images/image17.png" class="img-fluid m-auto d-block"></div>
				    <div class="item main-product-item"><img src="<?php bloginfo('template_url'); ?>/images/image18.png" class="img-fluid m-auto d-block"></div>
				    <div class="item main-product-item"><img src="<?php bloginfo('template_url'); ?>/images/image19.png" class="img-fluid m-auto d-block"></div>
				    <div class="item main-product-item"><img src="<?php bloginfo('template_url'); ?>/images/image20.png" class="img-fluid m-auto d-block"></div>
				    <div class="item main-product-item"><img src="<?php bloginfo('template_url'); ?>/images/image21.png" class="img-fluid m-auto d-block"></div>
				    <div class="item main-product-item"><img src="<?php bloginfo('template_url'); ?>/images/image22.png" class="img-fluid m-auto d-block"></div>
					  
				  </div>
				</div>
			</div>

			<div class="feed-banner">
				<div class="container">
					<div class="row">
						<div class="col-md-12">
							<img src="<?php bloginfo('template_url'); ?>/images/feed-banner.jpg" class="img-fluid m-auto d-block">
						</div>
					</div>
				</div>
			</div>

			<div class="find-product-service-section">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="find-product-service-title">
								<h2 class="title-head"> Browse by Category </h2>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-lg-3 col-md-4 popmake-102">
							<div class="product-service-item">
								<div class="product-service-icon">
									<h5 class="text-white">ERP</h5>
									<div class="product-separator-border"></div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-4 popmake-102">
							<div class="product-service-item">
								<div class="product-service-icon">
									<h5 class="text-white">CRM</h5>
									<div class="product-separator-border"></div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-4 popmake-102">
							<div class="product-service-item">
								<div class="product-service-icon">
									<h5 class="text-white">HCM</h5>
									<div class="product-separator-border"></div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-4 popmake-102">
							<div class="product-service-item">
								<div class="product-service-icon">
									<h5 class="text-white">BUSINESS INTELIGENCE</h5>
									<div class="product-separator-border"></div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-4 popmake-102">
							<div class="product-service-item">
								<div class="product-service-icon">
									<h5 class="text-white">RPA</h5>
									<div class="product-separator-border"></div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-4 popmake-102">
							<div class="product-service-item">
								<div class="product-service-icon">
									<h5 class="text-white">MOBILITY SOLUTIONS</h5>
									<div class="product-separator-border"></div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-4 popmake-102">
							<div class="product-service-item">
								<div class="product-service-icon">
									<h5 class="text-white">ARTIFICIAL INTELIGENCE</h5>
									<div class="product-separator-border"></div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-4 popmake-102">
							<div class="product-service-item">
								<div class="product-service-icon">
									<h5 class="text-white">CONNECTORS</h5>
									<div class="product-separator-border"></div>
								</div>
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-lg-12 text-center">
							<a href="#" class="btn btn-link">See all Categories</a>
						</div>
					</div>
				</div>
			</div>

			<div class="find-product-service-section">
				<div class="container">
					<div class="row">
						<div class="col-lg-9">
							<div class="product-sec-title">
								<h2 class="title-head">Top Services on offer</h2>
							</div>
							<div class="ourMain-product-des">
								<p>Get access to a worldwide network of Systems Integrators and independent consultants for professional services</p>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-lg-3 col-md-4 popmake-102">
							<div class="product-service-item">
								<div class="product-service-icon">
									<h5 class="text-white">IMPLEMENTATION</h5>
									<div class="product-separator-border"></div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-4 popmake-102">
							<div class="product-service-item">
								<div class="product-service-icon">
									<h5 class="text-white">TRAINING</h5>
									<div class="product-separator-border"></div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-4 popmake-102">
							<div class="product-service-item">
								<div class="product-service-icon">
									<h5 class="text-white">SUPPORT</h5>
									<div class="product-separator-border"></div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-4 popmake-102">
							<div class="product-service-item">
								<div class="product-service-icon">
									<h5 class="text-white">INTEGRATION</h5>
									<div class="product-separator-border"></div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-4 popmake-102">
							<div class="product-service-item">
								<div class="product-service-icon">
									<h5 class="text-white">RFP/RFI MANAGMENT</h5>
									<div class="product-separator-border"></div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-4 popmake-102">
							<div class="product-service-item">
								<div class="product-service-icon">
									<h5 class="text-white">CUSTOMIZATION</h5>
									<div class="product-separator-border"></div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-4 popmake-102">
							<div class="product-service-item">
								<div class="product-service-icon">
									<h5 class="text-white">DEVELOPMENT</h5>
									<div class="product-separator-border"></div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-4 popmake-102">
							<div class="product-service-item">
								<div class="product-service-icon">
									<h5 class="text-white">REPORT WRITING</h5>
									<div class="product-separator-border"></div>
								</div>
							</div>
						</div>						
					</div>
					<div class="row">
						<div class="col-lg-12 text-center">
							<a href="#" class="btn btn-link">Show more</a>
						</div>
					</div>
				</div>
			</div>


			<div class="getInTouch-section">
				<div class="container">
					<div class="row">
						<div class="col-lg-6 order-1 order-md-1">
							<div class="getInTouch-sec-title">
								<h2 class="title-head">Resource as a Service</h2>
							</div>
							<div class="ourMain-product-des">
								<b>Leverage World class IT resources</b>
								<p>Enterprise companies often face challenges finding, training, and retaining top IT resources. <br>Varselor Resource as a Service is a unique solution for hiring elite independent IT resources to plug a specific skills gap on a project basis.</p>
								<div class="howDoesWork-section">
									<ol>
										<li>Submit a job requirement on the platform.</li>
										<li>Team of experts will review your requirements and match the required skills and product expertise with the best fitting resources.</li>
										<li>If for some reason, we do not have a match, then our team will work tirelessly to find the right one.</li>
									</ol>
								</div>
							</div>

							<!-- <div class="getInTouch-btn  popmake-102">
								<a href="#"> Contact Us </a>
							</div> -->
						</div>

						<div class="col-lg-6 order-2 order-md-2 d-none d-sm-block">
							<div class="getInTouch-img">
								<img src="<?php bloginfo('template_url'); ?>/images/getIn-touch.jpg" class="img-fluid m-auto d-block">
							</div>
						</div>
					</div>
				</div>
			</div>

			<div class="find-product-service-section">
				<div class="container">
					<div class="col-lg-12">
							<div class="find-product-service-title">
								<h2 class="title-head">Enterprise resources currently in demand</h2>
							</div>
						</div>

					<div class="row">
						<div class="col-lg-3 col-md-4 popmake-102">
							<div class="product-service-item">
								<div class="product-service-icon">
									<h5 class="text-white">CLOUD COMPUTING</h5>
									<div class="product-separator-border"></div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-4 popmake-102">
							<div class="product-service-item">
								<div class="product-service-icon">
									<h5 class="text-white">TECHNICAL CONSULTANTS</h5>
									<div class="product-separator-border"></div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-4 popmake-102">
							<div class="product-service-item">
								<div class="product-service-icon">
									<h5 class="text-white">FUNCTIONAL CONSULTANTS</h5>
									<div class="product-separator-border"></div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-4 popmake-102">
							<div class="product-service-item">
								<div class="product-service-icon">
									<h5 class="text-white">SOFTWARE DEVELOPERS</h5>
									<div class="product-separator-border"></div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-4 popmake-102">
							<div class="product-service-item">
								<div class="product-service-icon">
									<h5 class="text-white">ANALYTICS</h5>
									<div class="product-separator-border"></div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-4 popmake-102">
							<div class="product-service-item">
								<div class="product-service-icon">
									<h5 class="text-white">BUSINESS INTELIGENCE</h5>
									<div class="product-separator-border"></div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-4 popmake-102">
							<div class="product-service-item">
								<div class="product-service-icon">
									<h5 class="text-white">RPA</h5>
									<div class="product-separator-border"></div>
								</div>
							</div>
						</div>
						<div class="col-lg-3 col-md-4 popmake-102">
							<div class="product-service-item">
								<div class="product-service-icon">
									<h5 class="text-white">CYBERSECURITY</h5>
									<div class="product-separator-border"></div>
								</div>
							</div>
						</div>						
					</div>
					
				</div>
			</div>

			<!-- <div class="benefits-section">
				<div class="container">
					<div class="row">
						<div class="col-lg-6">
							<div class="getInTouch-sec-title">
								<h2 class="title-head"> Special Deals for You </h2>
							</div>
							<div class="ourMain-product-des">
								<p> Follow us - subscribe, to get news on the latest special deals, upcoming events, and much more. </p>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-lg-4 col-md-6">
							<div class="benefits-item">								
								<div class="benefits-content">
									<h4> Odoo </h4>
								</div>
								<div class="benefits-icon">
									<img src="<?php bloginfo('template_url'); ?>/images/odoo-logo.svg" class="img-fluid m-auto d-block" alt="Odoo">
								</div>
								<p>As a special introductory offer, Varselor is offering 30% off the list price on various Odoo solution modules, be it ERP, CRM, HRMS - implementation, training and including all important support.</p>
							</div>
						</div>

						<div class="col-lg-4 col-md-6">
							<div class="benefits-item">								
								<div class="benefits-content">
									<h4> SAP </h4>

								</div> 
								<div class="benefits-icon">
									<img src="<?php bloginfo('template_url'); ?>/images/sap-logo.svg" class="img-fluid m-auto d-block" alt="SAP">
								</div>
								<p>Take advantage of Varselor’s special 30% off on SAP Business One offering – includes software licenses, implementation, training and support.</p>
							</div>
						</div>

						<div class="col-lg-4 col-md-6">
							<div class="benefits-item">								
								 <div class="benefits-content">
									<h4> Zoho </h4>

								</div>
								<div class="benefits-icon">
									<img src="<?php bloginfo('template_url'); ?>/images/zoho-logo.svg" class="img-fluid m-auto d-block" alt="Zoho">
								</div>
								<p>Get 15% special discount if you want to buy subscription licenses, and the same also currently applies to implementation, training and support related services.</p>
							</div>
						</div>
						<div class="col-lg-4 col-md-6">
							<div class="benefits-item">								
								<div class="benefits-content">
									<h4> Sage </h4>

								</div>
								<div class="benefits-icon">
									<img src="<?php bloginfo('template_url'); ?>/images/sage-logo.svg" class="img-fluid m-auto d-block" alt="SAGE">
								</div>
								<p>Enterprise and SME buyers can entitle themselves to a 30% discount on Sage 300 ERP and HR licenses, as well on all of support, implementation and training.</p>
							</div>
						</div>
						<div class="col-lg-4 col-md-6">
							<div class="benefits-item">								
								 <div class="benefits-content">
									<h4> Salesforce </h4>

								</div> 
								<div class="benefits-icon">
									<img src="<?php bloginfo('template_url'); ?>/images/salesforce.svg" class="img-fluid m-auto d-block" alt="Salesforce">
								</div>
								<p>A very appealing discount for SalesForce users - you can maximize your savings by a special 10% subscription based discount, in addition to 25% off on implementation, training and support offering.</p>
							</div>
						</div>
					</div>
				</div>
			</div> -->


			<div class="whyUs-section">
				<div class="container">
					<div class="row">
						<div class="col-lg-6 col-md-12">
							<div class="getInTouch-sec-title">
								<h2 class="title-head">Technology Advisory Services</h2>
							</div>
							<div class="ourMain-product-des">
								<p class=""> Varselor helps businesses undergo digital transformation. <br> Benefit from a global advisory services group, highly skilled and experienced IT professionals, consultants possessing the granular skills to provide strategic advice to facilitate digital transformation. <br>Varselor Advisory key objectives include aligning IT strategies, solution architectures, managing complex digital transformation projects, optimizing technology costs by identifying alternate solutions/technology vendors, and mitigating risks.</p>
</p>
							</div>
						</div>
					</div>

					<!-- <div class="row">
		                <div class="col-4">
		                  <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
		                    <a class="nav-link active" id="v-pills-expertise-tab" data-toggle="pill" href="#v-pills-expertise" role="tab" aria-controls="v-pills-expertise" aria-selected="false"> Follow-us </a>

		                    <a class="nav-link" id="v-pills-marketplace-tab" data-toggle="pill" href="#v-pills-marketplace" role="tab" aria-controls="v-pills-marketplace" aria-selected="true">Odoo</a>

		                    <a class="nav-link" id="v-pills-customers-tab" data-toggle="pill" href="#v-pills-customers" role="tab" aria-controls="v-pills-customers" aria-selected="false"> SAP </a>

		                    <a class="nav-link" id="v-pills-moreOption-tab" data-toggle="pill" href="#v-pills-moreOption" role="tab" aria-controls="v-pills-moreOption" aria-selected="false"> Zoho </a>

		                    <a class="nav-link" id="v-pills-moreOption-tab" data-toggle="pill" href="#v-pills-moreOption1" role="tab" aria-controls="v-pills-moreOption" aria-selected="false"> Sage </a>

		                    <a class="nav-link" id="v-pills-moreOption-tab" data-toggle="pill" href="#v-pills-moreOption2" role="tab" aria-controls="v-pills-moreOption" aria-selected="false"> Salesforce </a>

		                  </div>
		                </div>

		                <div class="col-8">
		                  <div class="tab-content" id="v-pills-tabContent">
		                    <div class="tab-pane fade active show" id="v-pills-expertise" role="tabpanel" aria-labelledby="v-pills-expertise-tab">
			                    <div class="card-wrap">
			                      <div class="card-tab-content">
			                      	<p> subscribe, to get news on the latest special deals, upcoming events, and much more.  </p>
			                      </div>
			                    </div>
		                    </div>
		                    <div class="tab-pane fade" id="v-pills-marketplace" role="tabpanel" aria-labelledby="v-pills-marketplace-tab">
		                    	<div class="card-wrap">
			                      <div class="card-tab-content">
			                      	<p> As a special introductory offer, Varselor is offering 30% off the list price on various Odoo solution modules, be it ERP, CRM, HRMS - implementation, training and including all important support. </p>
			                      </div>
			                    </div>
		                    </div>
		                    <div class="tab-pane fade" id="v-pills-customers" role="tabpanel" aria-labelledby="v-pills-customers-tab">
		                    	<div class="card-wrap">
			                      <div class="card-tab-content">
			                      	<p> Take advantage of Varselor’s special 30% off on SAP Business One offering – includes software licenses, implementation, training and support.</p>
			                      </div>
			                    </div>
		                    </div>
		                    <div class="tab-pane fade" id="v-pills-moreOption" role="tabpanel" aria-labelledby="v-pills-moreOption-tab">
		                    	<div class="card-wrap">
			                      <div class="card-tab-content">
			                      	<p> Get 15% special discount if you want to buy subscription licenses, and the same also currently applies to implementation, training and support related services.</p>
			                      </div>
			                    </div>
		                    </div>
		                    <div class="tab-pane fade" id="v-pills-moreOption1" role="tabpanel" aria-labelledby="v-pills-moreOption1-tab">
		                    	<div class="card-wrap">
			                      <div class="card-tab-content">
			                      	<p> Enterprise and SME buyers can entitle themselves to a 30% discount on Sage 300 ERP and HR licenses, as well on all of support, implementation and training.</p>
			                      </div>
			                    </div>
		                    </div>
		                    <div class="tab-pane fade" id="v-pills-moreOption2" role="tabpanel" aria-labelledby="v-pills-moreOption2-tab">
		                    	<div class="card-wrap">
			                      <div class="card-tab-content">
			                      	<p> A very appealing discount for SalesForce users - you can maximize your savings by a special 10% subscription based discount, in addition to 25% off on implementation, training and support offering.</p>
			                      </div>
			                    </div>
		                    </div>
		                  </div>
		                </div>
		          	</div> -->
	          	</div>
          	</div>

			<div class="testimonial-slider">
				<div class="container">
				    <div class="row">
				        <div class="col-md-12">
				            <div id="testimonial-slider" class="owl-carousel">	               
				                <div class="testimonial-item">
				                	<div class="row">
				                		<div class="col-lg-5 col-md-3">
				                			<div class="testimonial-image cell-left">
						                        <img src="<?php bloginfo('template_url'); ?>/images/client-1.jpg" class="img-fluid m-auto d-block">
						                    </div>
				                		</div>
				                		<div class="col-lg-7 col-md-9">
				                			<div class="testimonial-content quote">
				                				<h2><i class="fa fa-quote-left"></i>What Our Clients Say About us </h2>
				                				<p class="mb-0"> Varselor helped us deploy Sage ERP system. Their consultants provided timely service throughout the process and the project was completed on time. The entire implementation was a pleasant experience. I highly recommend them and any resource that they offer.</p>
				                				<span class="testimonial-name">Rajeev Vijayan</span>
				                        		<span class="testimonial-job">Finance Manager, Manifold Systems International FZC</span>
						                    </div>
				                		</div>
				                	</div>	                    
				                </div>

				                <div class="testimonial-item">
				                	<div class="row">
				                		<div class="col-lg-5 col-md-3">
				                			<div class="testimonial-image cell-left">
						                        <img src="<?php bloginfo('template_url'); ?>/images/client-1.jpg" class="img-fluid m-auto d-block">
						                    </div>
				                		</div>
				                		<div class="col-lg-7 col-md-9">
				                			<div class="testimonial-content quote">
				                				<h2><i class="fa fa-quote-left"></i>What Our Clients Say About us </h2>
				                				<p class="mb-0"> Brilliant product! Even better service! Patient, and organized customer service team, and ever ready to assist on any issues that came up during installation, configuration and usage.</p>
				                				<span class="testimonial-name">Macnell Pinto</span>
				                        		<span class="testimonial-job">PAssistant Engineer, Magnatech Middle East Trading LLC</span>
						                    </div>
				                		</div>
				                	</div>	                    
				                </div>
				                <div class="testimonial-item">
				                	<div class="row">
				                		<div class="col-lg-5 col-md-3">
				                			<div class="testimonial-image cell-left">
						                        <img src="<?php bloginfo('template_url'); ?>/images/client-1.jpg" class="img-fluid m-auto d-block">
						                    </div>
				                		</div>
				                		<div class="col-lg-7 col-md-9">
				                			<div class="testimonial-content quote">
				                				<h2><i class="fa fa-quote-left"></i>What Our Clients Say About us </h2>
				                				<p class="mb-0"> The service was bad with the old Sage partner, who were not able to set up the systems in a good way and were not able to link the modules with the finance. Varselor team really helped us to fix all of our problems and did the best to ease our work in a easy and systematic way. Now we are happy.</p>
				                				<span class="testimonial-name">Ravi Sampath</span>
				                        		<span class="testimonial-job">General Manager,E-Smart Electronics LLC</span>
						                    </div>
				                		</div>
				                	</div>	                    
				                </div>
				            </div>
				        </div>
				    </div>
				</div>
			</div>

          	<!-- <div class="find-product-service-section">
				<div class="container">
					<div class="row">
						<div class="col-lg-12">
							<div class="find-product-service-title">
								<h2 class="title-head"> Top Trending Blogs </h2>
							</div>
						</div>
					</div>

					<div class="row">
						<div class="col-lg-3">
							<div class="product-service-item">
								<div class="product-service-icon">
									<img src="<?php bloginfo('template_url'); ?>/images/software-product.png" class="img-fluid m-auto d-block">
								</div>
								<div class="product-service-content">
									<h4> Implementation</h4>

									<div class="product-separator-border"></div>
								</div>
							</div>
						</div>

						<div class="col-lg-3">
							<div class="product-service-item">
								<div class="product-service-icon">
									<img src="<?php bloginfo('template_url'); ?>/images/software-product.png" class="img-fluid m-auto d-block">
								</div>
								<div class="product-service-content">
									<h4> Software Products </h4>

									<div class="product-separator-border"></div>
								</div>
							</div>
						</div>

						<div class="col-lg-3">
							<div class="product-service-item">
								<div class="product-service-icon">
									<img src="<?php bloginfo('template_url'); ?>/images/software-product.png" class="img-fluid m-auto d-block">
								</div>
								<div class="product-service-content">
									<h4> Software Products </h4>

									<div class="product-separator-border"></div>
								</div>
							</div>
						</div>

						<div class="col-lg-3">
							<div class="product-service-item">
								<div class="product-service-icon">
									<img src="<?php bloginfo('template_url'); ?>/images/software-product.png" class="img-fluid m-auto d-block">
								</div>
								<div class="product-service-content">
									<h4> Software Products </h4>

									<div class="product-separator-border"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
 -->
			<div class="globar-client-section">
				<div class="container">
					<div class="row">
						<div class="col-lg-8 offset-lg-2 p-0">
							<div class="globar-map-img">
								<img src="<?php bloginfo('template_url'); ?>/images/map-icon.png" class="img-fluid m-auto d-block">
							</div>
							<div class="getInTouch-sec-title text-center">
								<h2 class="title-head"></h2>
							</div>
							<div class="ourMain-product-des text-center">
								<h4 class="mb-5 text-white">The Varselor marketplace helps businesses around the globe.</h4>
							</div>
						</div>
						<div class="col-md-12">

							<div class="globar-part-logo">
								<img src="<?php bloginfo('template_url'); ?>/images/zebra-logo.png" class="img-fluid m-auto d-block">
								<img src="<?php bloginfo('template_url'); ?>/images/eurocoffee-logo.png" class="img-fluid m-auto d-block">
								<img src="<?php bloginfo('template_url'); ?>/images/image126.png" class="img-fluid m-auto d-block">
								<img src="<?php bloginfo('template_url'); ?>/images/fujitsu-logo.png" class="img-fluid m-auto d-block">
								<img src="<?php bloginfo('template_url'); ?>/images/rmjm.png" class="img-fluid m-auto d-block">
								<!-- <img src="<?php bloginfo('template_url'); ?>/images/sugarcrm-logo.png" class="img-fluid m-auto d-block"> -->
								<img src="<?php bloginfo('template_url'); ?>/images/sumitomo-logo.png" class="img-fluid m-auto d-block">
							</div>
						</div>
					</div>
	          	</div>
          	</div>



		</main><!-- #main -->
	</section><!-- #primary -->

<?php
get_footer();