<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */
?><!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<link rel="profile" href="https://gmpg.org/xfn/11" />

	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/bootstrap.min.css" />
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/fontawesome.css" type="text/css"/>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/owl.carousel.css" />
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/style.css" type="text/css"/>
	<link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/css/responsive.css" type="text/css"/>
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
	<section class="logo-header <?php if(!is_front_page()) { echo 'innerpage-navbar'; } ?>">
		<div class="container">
			<div class="row d-flex center_align">
				<div class="<?php if(is_front_page()) { echo 'col-lg-2 col-md-12'; } else{ echo 'col-lg-5 col-md-12'; } ?> pl-lg-0">
					<div class="mainmenu-wrapper remove-padding">
						<nav class="core-nav">
							<!-- <div class="nav-header">
								<button class="toggle-bar"><span class="fa fa-bars"></span></button>	
							</div>	 -->				
							<div class="menu-left-wrapper">
								<div class="logo">
									<a href="<?php echo get_site_url(); ?>">
										<img src="/wp-content/uploads/2019/10/1569601612logo-varselor.png" alt="">
									</a>
								</div>

								<?php if(!is_front_page()): ?>
									<div class="wrap-core-nav-list right d-none d-lg-block">
										<ul class="menu menu-left-list core-nav-list">										
											<li class="popmake-102 pum-trigger"><a href="#">For Buyers</a></li>
											<li><a href="/vendor">For Vendors</a></li>
										</ul>
									</div>
								<?php endif; ?>
							</div>

							<button class="navbar-toggle d-lg-none" type="button" data-toggle="collapse" data-target=".mobile-topNav-wrap">
								<span class="sr-only">Toggle navigation</span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
								<span class="icon-bar"></span>
							</button>
						</nav>
					</div>
				</div>
				<div class="<?php if(is_front_page()) { echo 'col-lg-8 col-md-6'; } else{ echo 'col-lg-5'; } ?> pl-lg-0 d-none d-lg-block">
					<div class="search-box menu-search-box">
						<form class="search-form" action="" method="GET">
							<div class="input-group">
							    <input type="text" class="form-control" id="prod_name" name="search" placeholder="Search" required="" autocomplete="off">
						    	<div class="input-group-append">
						      		<button type="submit" class="input-group-text"><i class="fa fa-search" aria-hidden="true"></i></button>
						    	</div>
						  	</div>
						</form>
					</div>
				</div>
				<div class="col-lg-2 col-md-3 pr-lg-0 d-none d-lg-block">
					<div class="helpful-links">
						<div class="helpful-links-inner <?php if(!is_front_page()) { echo 'innerpage-helpfulbtn'; } ?>">
							<a href="#" class="btn free-consult freeConsulting-btn popmake-102">Free Consultations</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>


	<?php if(is_front_page()): ?>

	<div class="topNav-wrap d-none d-lg-block">
		<div class="container">
			<div class="row">
				<div class="col-lg-10 offset-lg-2 pl-lg-0 col-md-12">
					<nav class="navbar secondary-menu-wrap p-0">
						<?php
							wp_nav_menu(
								array(
									'menu'              => 'primary',
				    				'theme_location'    => 'primary',
									'menu_class'     => 'nav navbar-nav',
									'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
								)
							);
						?>
					</nav>
				</div>
			</div>
		</div>
	</div>

	<?php endif; ?>


	<div class="mobile-topNav-wrap d-lg-none collapse">
		<div class="container">
			<div class="row">
				<div class="col-12">
					<div class="search-box menu-search-box">
						<form class="search-form" action="" method="GET">
							<div class="input-group">
							    <input type="text" class="form-control" id="prod_name" name="search" placeholder="Search" required="" autocomplete="off">
						    	<div class="input-group-append">
						      		<button type="submit" class="input-group-text"><i class="fa fa-search" aria-hidden="true"></i></button>
						    	</div>
						  	</div>
						</form>
					</div>
				</div>
				<div class="col-md-12">
					<nav class="navbar mobile-secondary-menu-wrap">
						<?php
							wp_nav_menu(
								array(
									'menu'              => 'primary',
				    				'theme_location'    => 'primary',
									'menu_class'     => 'nav navbar-nav',
									'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
								)
							);
						?>
					</nav>
				</div>

				<div class="col-md-12">
					<div class="mobile-menu-logo">
						<a href="<?php echo get_site_url(); ?>">
							<img src="https://varselor.scrumdigital.com/wp-content/uploads/2019/10/logo-primary-inverted.png" alt=" Logo " class="img-fluid m-auto d-block">
						</a>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div id="content" class="site-content">
