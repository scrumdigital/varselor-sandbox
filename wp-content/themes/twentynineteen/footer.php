<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Nineteen
 * @since 1.0.0
 */

?>

	</div><!-- #content -->

	<!-- <footer id="colophon" class="site-footer">
		<?php //get_template_part( 'template-parts/footer/footer', 'widgets' ); ?>
		
	</footer> --><!-- #colophon -->

<footer class="footer site-footer" id="colophon">
		<div class="container">
			<div class="row">
				<div class="col-md-12 pl-lg-0">
					<div class="footer-logo">
							<a href="<?php echo get_site_url(); ?>" class="logo-link">
								<img src="https://varselor.scrumdigital.com/wp-content/uploads/2019/10/logo-primary-inverted.png" alt="" width="164" height="40">
							</a>
						</div>
				</div>
				<div class="col-md-6 col-lg-4 pl-lg-0">
					<!-- <div class="footer-widget  footer-info-area">
						
						<h4 class="title">
								CONTACT INFORMATION
						</h4>
						<div class="text">
							<p>
								<strong>United States & Canada</strong><br>
								Montreal, CA<br>
								Email: NA@varselor.com<br>
								Phone: +1800 607<br><br>
								<strong>Middle East, Africa & Asia</strong><br> Business Bay, Dubai,
								United Arab Emirates<br>
								Email: MEAA@varselor.com<br>
								Phone: +971 45588720
							</p>
						</div>
					</div> -->
					<?php
						if ( is_active_sidebar( 'sidebar-1' ) ) {
							?>
								<div class="footer-widget  footer-info-area">
									<?php dynamic_sidebar( 'sidebar-1' ); ?>
								</div>
							<?php
						}
					?>
				</div>
				<div class="col-md-6 col-lg-5 pr-lg-0">
					<div class="footer-widget info-link-widget">
						<?php
						if ( is_active_sidebar( 'sidebar-2' ) ) {
							?>
								<div class="footer-widget  footer-info-area">
									<?php dynamic_sidebar( 'sidebar-2' ); ?>
								</div>
							<?php
							}
						?>
						<div class="social-media-wrapper">
							<h4 class="title">
									FIND US ON SOCIAL NETWORKS
							</h4>
							<div class="fotter-social-links">
								<ul>
									<li>
	                                    <a href="https://www.facebook.com/" class="facebook social-item" target="_blank"><i class="fab fa-facebook-f"></i></a>
									</li>

									<li>
										<a href="https://twitter.com/" class="twitter social-item" target="_blank"><i class="fab fa-twitter"></i></a>
									</li>

									<li>
										<a href="https://www.linkedin.com/" class="linkedin social-item" target="_blank"><i class="fab fa-linkedin-in"></i></a>
									</li>

									<li>
										<a href="" class="youtube social-item" target="_blank"><i class="fab fa-youtube"></i></a>
									</li>	                                      
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-6 col-lg-3 pl-lg-0">
					<?php
						if ( is_active_sidebar( 'sidebar-3' ) ) {
							?>
								<div class="footer-widget quick-links info-link-widget">
									<?php dynamic_sidebar( 'sidebar-3' ); ?>
									<div class="copyright-widget">
										<div class="content">
											<p class="mb-0">©2019 All Rights Reserved.</p>
										</div>
									</div>
								</div>
							<?php
						}
					?>
				</div>
			</div>
		</div>
	</footer>

</div><!-- #page -->

<?php wp_footer(); ?>


<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/jquery.min.js" /></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/bootstrap.min.js" /></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/owl.carousel.js" /></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/js/script.js" /></script>
</body>
</html>
