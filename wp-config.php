<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'varselor_wordpress' );

/** MySQL database username */
define( 'DB_USER', 'var_ecomm' );

/** MySQL database password */
define( 'DB_PASSWORD', 'Wel123#come' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'B:7tVEy=5J%$4sUPdf#KWb7o@x)],R:Mx`tHTjRCY2r<Nf0Mbsz((htG2aiq)l6x' );
define( 'SECURE_AUTH_KEY',  '>,MeYHrV]nB7im3OCFwuBG[+f&K3p;}clMG(%2wc4gvzx%s=.N8f64jeUj6JlO87' );
define( 'LOGGED_IN_KEY',    '7Ij*X6S<kCDd6Pp70%m8_>fLLwUtpvE+83]1q6,`D%7$db?o!L@Tw>^gP8=(,c#(' );
define( 'NONCE_KEY',        ',|32wE^duPWmt3_s{|Ysu@vEKL_(q%I7#KK.,zhlMSU+C,`isT4p%-z;|b#I=[/0' );
define( 'AUTH_SALT',        'Ps=xMDbWu#GQne#c8KK@*u+wy+[FFuPh$Z@{A%j!s_O=5r@6G^8J-ddGIIu$,LrT' );
define( 'SECURE_AUTH_SALT', 'aeHmpBl$z^[Lu;XqoOx1;=AO1Y:Z}WfBr$2;^hO{yss-Z><z1&bHz0Zt/Y*lvvK:' );
define( 'LOGGED_IN_SALT',   'Y>N*iyr%RSWmC_#Nh.$h=K:b0{.}NmV;Qm=3{,kkC19kuD8/5%eqTUz_HJPWU`15' );
define( 'NONCE_SALT',       'g#)F?o.gsIQw.v5mQf52w<(nHHg)mfz)B+C..}pumJso_eTz {A_$DUE1duPy!^-' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
