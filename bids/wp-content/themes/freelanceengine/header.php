<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 * @package WordPress
 * @subpackage FreelanceEngine
 * @since FreelanceEngine 1.0
 */
global $current_user;
?><!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->
<head>
    <?php global $user_ID; ?>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1 ,user-scalable=no">
    <title><?php wp_title( '|', true, 'right' ); ?></title>
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
    <?php ae_favicon(); ?>
    <?php
    wp_head();
    if ( function_exists( 'et_render_less_style' ) ) {
        //et_render_less_style();
    }

    ?>
    <link rel="stylesheet" type="text/css" href="<?php echo(get_template_directory_uri())?>/assets/css/fontawesome.css">
</head>

<body <?php body_class(); ?>>

<!-- <div class="fre-wrapper"> -->
<header class="fre-header-wrapper">
    <div class="fre-header-wrap" id="main_header">
      <!-- Logo Header Area Start -->
      <section class="logo-header">
        <div class="container">
            <div class="row d-flex center_align">
               <div class="col-md-12 pl-lg-0 col-lg-4 pl-lg-0">
                  <div class="mainmenu-wrapper remove-padding">
                     <nav class="core-nav">
                        <div class="menu-left-wrapper large-screen-menu">
                           <div class="logo">
                                <a href="<?php echo home_url(); ?>">
                                    <?php fre_logo( 'site_logo' ) ?>
                                </a>
                           </div>
                                <?php
                                    wp_nav_menu(
                                        array(
                                            'menu'              => 'primary',
                                            'theme_location'    => 'et_header_standard',
                                            'menu_class'     => 'menu menu-left-list wrap-core-nav-list right core-nav-list',
                                            'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                                        )
                                    );
                                ?>
                        </div>

                        <button class="navbar-toggle d-lg-none" type="button" data-toggle="collapse" data-target=".mobile-topNav-wrap">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>

                        <!-- <div class="fre-hamburger">
                            <span class="hamburger-menu">
                                    <div class="hamburger hamburger--elastic" tabindex="0" aria-label="Menu" role="button"
                                         aria-controls="navigation">
                                        <div class="hamburger-box">
                                            <div class="hamburger-inner"></div>
                                        </div>
                                    </div>
                                </span>
                        </div> -->
                     </nav>
                  </div>
               </div>
               <div class="col-lg-6 col-sm-12 w-26">
                    <div class="fre-search-wrap search-box menu-search-box">
                        <?php
                        global $wp;
                        $active_profile = '';
                        $active_project = '';
                        $action_link    = '';
                        $input_hint     = '';
                        $current_url    = home_url( add_query_arg( array(), $wp->request ) );
                        $current_url    = $current_url . '/';

                        if ( is_user_logged_in() ) {
                            $user_data = get_userdata( $current_user->ID );
                            $user_role = implode( ', ', $user_data->roles );
                            if ( $user_role == 'freelancer' ) {
                                $active_project = 'active';
                                $action_link    = get_post_type_archive_link( PROJECT );
                                $input_hint     = __( 'Find Projects', ET_DOMAIN );
                            } else if ( $user_role == 'employer' ) {
                                $active_profile = 'active';
                                $action_link    = get_post_type_archive_link( PROFILE );
                                $input_hint     = __( 'Find Freelancers', ET_DOMAIN );
                            } else {
                                $active_profile = 'active';
                                $action_link    = get_post_type_archive_link( PROFILE );
                                $input_hint     = __( 'Find Freelancers', ET_DOMAIN );
                            }
                        } else {
                            $active_profile = 'active';
                            $action_link    = get_post_type_archive_link( PROFILE );
                            $input_hint     = __( 'Find Freelancers', ET_DOMAIN );
                        }

                        if ( $current_url == get_post_type_archive_link( PROJECT ) ) {
                            $active_project = 'active';
                            $active_profile = '';
                            $action_link    = get_post_type_archive_link( PROJECT );
                            $input_hint     = __( 'Find Projects', ET_DOMAIN );
                        } else if ( $current_url == get_post_type_archive_link( PROFILE ) ) {
                            $active_profile = 'active';
                            $active_project = '';
                            $action_link    = get_post_type_archive_link( PROFILE );
                            $input_hint     = __( 'Find Freelancers', ET_DOMAIN );
                        }
                        ?>

                        <form class="fre-form-search" action="<?php echo $action_link; ?>" method="post">
                            <div class="fre-search dropdown">
                                    
                                <input class="fre-search-field" name="keyword"
                                       value="<?php echo isset( $_POST['keyword'] ) ? $_POST['keyword'] : "" ?>" type="text"
                                       placeholder="<?php echo $input_hint; ?>">
                                       <span class="fre-search-dropdown-btn">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                        <!-- <i class="fa fa-caret-down" aria-hidden="true"></i> -->
                                    </span>
                                <!-- <ul class="dropdown-menu fre-search-dropdown">
                                    <li><a class="<?php echo $active_profile; ?>" data-type="profile"
                                           data-action="<?php echo get_post_type_archive_link( PROFILE ); ?>"><?php _e( 'Find Freelancers', ET_DOMAIN ); ?></a>
                                    </li>
                                    <li><a class="<?php echo $active_project; ?>" data-type="project"
                                           data-action="<?php echo get_post_type_archive_link( PROJECT ); ?>"><?php _e( 'Find Projects', ET_DOMAIN ); ?></a>
                                    </li>
                                </ul> -->
                            </div>
                        </form>
                    </div>                    
               </div>
               <div class="col-lg-2 pr-0 w-20 d-none d-lg-block">
                  <div class="helpful-links">
                     <div class="helpful-links-inner">
                        <a href="#" class="btn free-consult freeConsulting-btn">Free Consultations</a>
                     </div>
                  </div>
               </div>
            </div>
        </div>
      </section>
      <!-- Logo Header Area End -->

      <div class="mobile-topNav-wrap d-lg-none collapse">
         <div class="container">
            <div class="row">
               <div class="col-12">
                    <div class="fre-search-wrap search-box menu-search-box mobile-menu-search">
                        <?php
                        global $wp;
                        $active_profile = '';
                        $active_project = '';
                        $action_link    = '';
                        $input_hint     = '';
                        $current_url    = home_url( add_query_arg( array(), $wp->request ) );
                        $current_url    = $current_url . '/';

                        if ( is_user_logged_in() ) {
                            $user_data = get_userdata( $current_user->ID );
                            $user_role = implode( ', ', $user_data->roles );
                            if ( $user_role == 'freelancer' ) {
                                $active_project = 'active';
                                $action_link    = get_post_type_archive_link( PROJECT );
                                $input_hint     = __( 'Find Projects', ET_DOMAIN );
                            } else if ( $user_role == 'employer' ) {
                                $active_profile = 'active';
                                $action_link    = get_post_type_archive_link( PROFILE );
                                $input_hint     = __( 'Find Freelancers', ET_DOMAIN );
                            } else {
                                $active_profile = 'active';
                                $action_link    = get_post_type_archive_link( PROFILE );
                                $input_hint     = __( 'Find Freelancers', ET_DOMAIN );
                            }
                        } else {
                            $active_profile = 'active';
                            $action_link    = get_post_type_archive_link( PROFILE );
                            $input_hint     = __( 'Find Freelancers', ET_DOMAIN );
                        }

                        if ( $current_url == get_post_type_archive_link( PROJECT ) ) {
                            $active_project = 'active';
                            $active_profile = '';
                            $action_link    = get_post_type_archive_link( PROJECT );
                            $input_hint     = __( 'Find Projects', ET_DOMAIN );
                        } else if ( $current_url == get_post_type_archive_link( PROFILE ) ) {
                            $active_profile = 'active';
                            $active_project = '';
                            $action_link    = get_post_type_archive_link( PROFILE );
                            $input_hint     = __( 'Find Freelancers', ET_DOMAIN );
                        }
                        ?>

                        <form class="fre-form-search" action="<?php echo $action_link; ?>" method="post">
                            <div class="fre-search dropdown">
                                    
                                <input class="fre-search-field" name="keyword"
                                       value="<?php echo isset( $_POST['keyword'] ) ? $_POST['keyword'] : "" ?>" type="text"
                                       placeholder="<?php echo $input_hint; ?>">
                                       <span class="fre-search-dropdown-btn">
                                        <i class="fa fa-search" aria-hidden="true"></i>
                                        <!-- <i class="fa fa-caret-down" aria-hidden="true"></i> -->
                                    </span>
                                <!-- <ul class="dropdown-menu fre-search-dropdown">
                                    <li><a class="<?php echo $active_profile; ?>" data-type="profile"
                                           data-action="<?php echo get_post_type_archive_link( PROFILE ); ?>"><?php _e( 'Find Freelancers', ET_DOMAIN ); ?></a>
                                    </li>
                                    <li><a class="<?php echo $active_project; ?>" data-type="project"
                                           data-action="<?php echo get_post_type_archive_link( PROJECT ); ?>"><?php _e( 'Find Projects', ET_DOMAIN ); ?></a>
                                    </li>
                                </ul> -->
                            </div>
                        </form>
                    </div>
               </div>
               <div class="col-md-12">
                  <div class="menu-left-wrapper mobile-menu-wrapper d-lg-none">
                            <?php
                                wp_nav_menu(
                                    array(
                                        'menu'              => 'primary',
                                        'theme_location'    => 'et_header_standard',
                                        'menu_class'     => 'menu menu-left-list wrap-core-nav-list right core-nav-list',
                                        'items_wrap'     => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                                    )
                                );
                            ?>
                    </div>
               </div>
               <div class="col-md-12">
                  <div class="mobile-menu-logo">
                     <a href="">
                     <?php fre_logo( 'site_logo' ) ?>
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </div>
        
    </div>
</header>
<!-- MENU DOOR / END -->

<?php
global $user_ID;
if ( $user_ID ) {
    echo '<script type="data/json"  id="user_id">' . json_encode( array(
            'id' => $user_ID,
            'ID' => $user_ID
        ) ) . '</script>';
}