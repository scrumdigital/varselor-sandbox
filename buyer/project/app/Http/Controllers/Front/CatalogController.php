<?php

namespace App\Http\Controllers\Front;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use App\Models\Category;
use App\Models\Subcategory;
use App\Models\Childcategory;
use App\Models\Product;
use App\Models\Brand;
use App\Models\ProductClick;
use App\Models\Comment;
use App\Models\Reply;
use App\Models\Currency;
use App\Models\Order;
use App\Models\Rating;
use App\Classes\Paginate;
use App\Models\Compare;

use Auth;
use Session;
use Illuminate\Support\Facades\DB;

use Illuminate\Support\Collection;

class CatalogController extends Controller
{

// CATEGORIES SECTOPN

public function categories()
{
    return view('front.categories');
}


// CATEGORIES SECTION ENDS


// -------------------------------- CATEGORY SECTION ----------------------------------------

    public function category(Request $request,$slug)
    {

        $this->code_image();
        $sort = "";
        $cat = Category::where('slug','=',$slug)->first();
        $oldcats = $cat->products()->where('status','=',1)->orderBy('id','desc')->get();
        $cats = (new Collection(Product::filterProducts($oldcats)))->paginate(9);
        // Search By Price

    if(!empty($request->min) || !empty($request->max))
    {
        $min = $request->min;
        $max = $request->max;
        $$oldcats = $cat->products()->where('status','=',1)->whereBetween('price', [$min, $max])->orderBy('price','asc')->get();
        $cats = (new Collection(Product::filterProducts($oldcats)))->sortBy('price')->paginate(9);
        if($request->ajax()){
            return view('front.pagination.category',compact('cat','cats','sort','min','max'));
        }
        return view('front.category',compact('cat','cats','sort','min','max'));
    }

        // Search By Sort

    if( !empty($request->sort) )
    {
        $sort = $request->sort;
        if($sort == "new")
        {
        $oldcats = $cat->products()->where('status','=',1)->orderBy('id','desc')->get();
        $cats = (new Collection(Product::filterProducts($oldcats)))->paginate(9);
        }
        else if($sort == "old")
        {
        $oldcats = $cat->products()->where('status','=',1)->get();
        $cats = (new Collection(Product::filterProducts($oldcats)))->paginate(9);
        }
        else if($sort == "low")
        {
        $oldcats = $cat->products()->where('status','=',1)->orderBy('price','asc')->get();
        $cats = (new Collection(Product::filterProducts($oldcats)))->sortBy('price')->paginate(9);
        }
        else if($sort == "high")
        {
        $oldcats = $cat->products()->where('status','=',1)->orderBy('price','desc')->get();
        $cats = (new Collection(Product::filterProducts($oldcats)))->sortByDesc('price')->paginate(9);
        }
        if($request->ajax()){
            return view('front.pagination.category',compact('cat','cats','sort'));
        }
        
        return view('front.category',compact('cat','cats','sort'));
    }

        // Otherwise Go To Category

        if($request->ajax()){
            return view('front.pagination.category',compact('cat','sort','cats'));
        }    

          $oldCompare = Session::get('compare');
       // dd($oldCompare);
        $compare = new Compare($oldCompare);
        $cproducts = $compare->items;
         //dd($cproducts);
        return view('front.category',compact('cat','sort','cats','cproducts'));
    }

    public function subcategory(Request $request,$slug1,$slug2)
    {
        $this->code_image();
        $sort = "";
        $subcat = Subcategory::where('slug','=',$slug2)->first();
        $oldcats = $subcat->products()->where('status','=',1)->orderBy('id','desc')->get();
        $cats = (new Collection(Product::filterProducts($oldcats)))->paginate(9);

        // Search By Price

    if(!empty($request->min) || !empty($request->max))
    {
        $min = $request->min;
        $max = $request->max;
        $oldcats = $subcat->products()->where('status','=',1)->whereBetween('price', [$min, $max])->orderBy('price','asc')->get();
        $cats = (new Collection(Product::filterProducts($oldcats)))->sortBy('price')->paginate(9);
        if($request->ajax()){
            return view('front.pagination.category',compact('subcat','cats','sort','min','max'));
        }
        return view('front.category',compact('subcat','cats','sort','min','max'));
    }

        // Search By Sort

    if( !empty($request->sort) )
    {
        $sort = $request->sort;
        if($sort == "new")
        {
        $oldcats = $subcat->products()->where('status','=',1)->orderBy('id','desc')->get();
        $cats = (new Collection(Product::filterProducts($oldcats)))->paginate(9);
        }
        else if($sort == "old")
        {
        $oldcats = $subcat->products()->where('status','=',1)->get();
        $cats = (new Collection(Product::filterProducts($oldcats)))->paginate(9);
        }
        else if($sort == "low")
        {
        $oldcats = $subcat->products()->where('status','=',1)->orderBy('price','asc')->get();
        $cats = (new Collection(Product::filterProducts($oldcats)))->sortBy('price')->paginate(9);
        }
        else if($sort == "high")
        {
        $oldcats = $subcat->products()->where('status','=',1)->orderBy('price','desc')->get();
        $cats = (new Collection(Product::filterProducts($oldcats)))->sortByDesc('price')->paginate(9);
        }
        if($request->ajax()){
            return view('front.pagination.category',compact('subcat','cats','sort'));
        }
        
        return view('front.category',compact('subcat','cats','sort'));
    }

        // Otherwise Go To Category

        if($request->ajax()){
            return view('front.pagination.category',compact('subcat','sort','cats'));
        }    
        return view('front.category',compact('subcat','sort','cats'));
    }

    public function childcategory(Request $request,$slug1,$slug2,$slug3)
    {
        $this->code_image();
        $sort = "";
        $childcat = Childcategory::where('slug','=',$slug3)->first();
        $oldcats = $childcat->products()->where('status','=',1)->orderBy('id','desc')->get();
        $cats = (new Collection(Product::filterProducts($oldcats)))->paginate(9);

        // Search By Price

    if(!empty($request->min) || !empty($request->max))
    {
        $min = $request->min;
        $max = $request->max;
        $oldcats = $childcat->products()->where('status','=',1)->whereBetween('price', [$min, $max])->orderBy('price','asc')->get();
        $cats = (new Collection(Product::filterProducts($oldcats)))->sortBy('price')->paginate(9);
        if($request->ajax()){
            return view('front.pagination.category',compact('childcat','cats','sort','min','max'));
        }
        return view('front.category',compact('childcat','cats','sort','min','max'));
    }

        // Search By Sort

    if( !empty($request->sort) )
    {
        $sort = $request->sort;
        if($sort == "new")
        {
        $oldcats = $childcat->products()->where('status','=',1)->orderBy('id','desc')->get();
        $cats = (new Collection(Product::filterProducts($oldcats)))->paginate(9);
        }
        else if($sort == "old")
        {
        $oldcats = $childcat->products()->where('status','=',1)->get();
        $cats = (new Collection(Product::filterProducts($oldcats)))->paginate(9);
        }
        else if($sort == "low")
        {
        $oldcats = $childcat->products()->where('status','=',1)->orderBy('price','asc')->get();
        $cats = (new Collection(Product::filterProducts($oldcats)))->sortBy('price')->paginate(9);
        }
        else if($sort == "high")
        {
        $oldcats = $childcat->products()->where('status','=',1)->orderBy('price','desc')->get();
        $cats = (new Collection(Product::filterProducts($oldcats)))->sortByDesc('price')->paginate(9);
        }
        if($request->ajax()){
            return view('front.pagination.category',compact('childcat','cats','sort'));
        }
        
        return view('front.category',compact('childcat','cats','sort'));
    }

        // Otherwise Go To Category

        if($request->ajax()){
            return view('front.pagination.category',compact('childcat','sort','cats'));
        }    
        return view('front.category',compact('childcat','sort','cats'));
        }



    public function tag(Request $request, $tag)
    {
        $this->code_image();
       $tags = $tag;
       $sort = '';
       $oldcats = Product::where('tags', 'like', '%' . $tags . '%')->where('status','=',1)->orderBy('id','desc')->get();
       $products = (new Collection(Product::filterProducts($oldcats)))->paginate(9);

        // Search By Price

    if(!empty($request->min) || !empty($request->max))
    {
        $min = $request->min;
        $max = $request->max;
       $oldcats = Product::where('tags', 'like', '%' . $tags . '%')->where('status','=',1)->whereBetween('price', [$min, $max])->orderBy('price','asc')->get();
       $products = (new Collection(Product::filterProducts($oldcats)))->sortBy('price')->paginate(9);
        if($request->ajax()){
            return view('front.pagination.tags',compact('products','tags','sort','min','max'));
        }
        return view('front.tags',compact('products','tags','sort','min','max'));
    }

        // Search By Sort

    if( !empty($request->sort) )
    {
        $sort = $request->sort;
        if($sort == "new")
        {
        $oldcats = Product::where('tags', 'like', '%' . $tags . '%')->where('status','=',1)->orderBy('id','desc')->get();
        $products = (new Collection(Product::filterProducts($oldcats)))->paginate(9);
        }
        else if($sort == "old")
        {
        $oldcats = Product::where('tags', 'like', '%' . $tags . '%')->where('status','=',1)->get();
        $products = (new Collection(Product::filterProducts($oldcats)))->paginate(9);
        }
        else if($sort == "low")
        {
        $oldcats = Product::where('tags', 'like', '%' . $tags . '%')->where('status','=',1)->orderBy('price','asc')->get();
        $products = (new Collection(Product::filterProducts($oldcats)))->sortBy('price')->paginate(9);
        }
        else if($sort == "high")
        {
        $oldcats = Product::where('tags', 'like', '%' . $tags . '%')->where('status','=',1)->orderBy('price','desc')->get();
        $products = (new Collection(Product::filterProducts($oldcats)))->sortByDesc('price')->paginate(9);
        }
        if($request->ajax()){
            return view('front.pagination.tags',compact('products','tags','sort'));
        }
        
        return view('front.tags',compact('products','tags','sort'));
    }

        // Otherwise Go To Tags

        if($request->ajax()){
            return view('front.pagination.tags',compact('products','tags','sort'));
        }   
       return view('front.tags', compact('products','tags','sort'));
    }

 public function search(Request $request)
    {   
        //$cat_id = $request->cat_id;
        $sort = ''; 
        $min = $request->min;
        $max = $request->max;
        $sort = $request->sort;

        $products = Product::where('name','=',$request->product)->orderBy('id','desc')->get();
        $category = Category::where('name','=',$request->product)->first();
       
        //$products = (new Collection(Product::filterProducts($oldcats)))->paginate(9);
       $oldCompare = Session::get('compare');
      
        $compare = new Compare($oldCompare);
        $cproducts = $compare->items;
         return view('front.search', compact('products','cat_id','sort','category','cproducts'));

    }

    public  static function getSearchCatProducts($catId)
    {   
        $products = Product::where('category_id','=',$catId)->orderBy('id','desc')->get();
       
         return $products;

    }
    public function search_old(Request $request)
    {
        $this->code_image();
        if(!empty($request->cat_id))
        {

// *********************** CATALOG SEARCH SECTION ******************

       $cat_id = $request->cat_id;
       $sort = '';
        // Search By Sort

    if( !empty($request->sort) )
    {
        $min = $request->min;
        $max = $request->max;
        $sort = $request->sort;
        if($sort == "new")
        {
        $oldcats = Product::whereIn('category_id',  $cat_id)->where('status','=',1)->whereBetween('price', [$min, $max])->orderBy('id','desc')->get();
        $products = (new Collection(Product::filterProducts($oldcats)))->paginate(9);
        }
        else if($sort == "old")
        {
        $oldcats = Product::whereIn('category_id',  $cat_id)->where('status','=',1)->whereBetween('price', [$min, $max])->get();
        $products = (new Collection(Product::filterProducts($oldcats)))->paginate(9);
        }
        else if($sort == "low")
        {
        $oldcats = Product::whereIn('category_id',  $cat_id)->where('status','=',1)->whereBetween('price', [$min, $max])->orderBy('price','asc')->get();
        $products = (new Collection(Product::filterProducts($oldcats)))->sortBy('price')->paginate(9);
        }
        else if($sort == "high")
        {
        $oldcats = Product::whereIn('category_id',  $cat_id)->where('status','=',1)->whereBetween('price', [$min, $max])->orderBy('price','desc')->get();
        $products = (new Collection(Product::filterProducts($oldcats)))->sortByDesc('price')->paginate(9);
        }
        if($request->ajax()){
            return view('front.pagination.search',compact('products','cat_id','sort','min','max'));
        }
        
        return view('front.search',compact('products','cat_id','sort','min','max'));
    }



        // Search By Price

    if(!empty($request->min) || !empty($request->max))
    {
        $min = $request->min;
        $max = $request->max;
       $oldcats = Product::whereIn('category_id',  $cat_id)->where('status','=',1)->whereBetween('price', [$min, $max])->orderBy('price','asc')->get();
       $products = (new Collection(Product::filterProducts($oldcats)))->sortBy('price')->paginate(9);
        if($request->ajax()){
            return view('front.pagination.search',compact('products','cat_id','sort','min','max'));
        }
        return view('front.search',compact('products','cat_id','sort','min','max'));
    }


        // Otherwise Go To Tags

        if($request->ajax()){
            return view('front.pagination.search',compact('products','cat_id','sort'));
        }   
       return view('front.search', compact('products','cat_id','sort'));


// *********************** CATALOG SEARCH SECTION ENDS ******************

        }

        else {

// *********************** NORMAL SEARCH SECTION ******************
       $sort='';
       $category_id = $request->category_id; 
       $search = $request->search;
       if($category_id == 0) {

// SORT SEARCH
    if( !empty($request->sort) )
    {
        $sort = $request->sort;
        if($sort == "new")
        {
        $oldcats = Product::where('name', 'like', '%' . $search . '%')->where('status','=',1)->orderBy('id','desc')->get();
        $products = (new Collection(Product::filterProducts($oldcats)))->paginate(9);
        }
        else if($sort == "old")
        {
        $oldcats =Product::where('name', 'like', '%' . $search . '%')->where('status','=',1)->get();
        $products = (new Collection(Product::filterProducts($oldcats)))->paginate(9);
        }
        else if($sort == "low")
        {
        $oldcats = Product::where('name', 'like', '%' . $search . '%')->where('status','=',1)->orderBy('price','asc')->get();
        $products = (new Collection(Product::filterProducts($oldcats)))->sortBy('price')->paginate(9);
        }
        else if($sort == "high")
        {
        $oldcats = Product::where('name', 'like', '%' . $search . '%')->where('status','=',1)->orderBy('price','desc')->get();
        $products = (new Collection(Product::filterProducts($oldcats)))->sortByDesc('price')->paginate(9);
        }
        if($request->ajax()){
            return view('front.pagination.search',compact('products','cat_id','sort','min','max'));
        }
        
        return view('front.search',compact('products','cat_id','sort','min','max'));
    }
// SORT SEARCH ENDS

        $oldcats = Product::where('status','=',1)->whereRaw('MATCH (name) AGAINST (? IN BOOLEAN MODE)' , array($search))->get();
        //$oldcats = Product::where('name', 'like', '%' . $search . '%')->where('status','=',1)->orderBy('id','desc')->get();
        $products = (new Collection(Product::filterProducts($oldcats)))->paginate(9);
        if($request->ajax()){
            return view('front.pagination.search',compact('products','search','category_id','sort'));
        }   
       return view('front.search',compact('products','search','category_id','sort'));    


       }
       else {

// SORT SEARCH
    if( !empty($request->sort) )
    {
        $sort = $request->sort;
        if($sort == "new")
        {
        $oldcats = PProduct::where('category_id', 'like', '%' . $category_id . '%')->where('name', 'like', '%' . $search . '%')->where('status','=',1)->orderBy('id','desc')->get();
        $products = (new Collection(Product::filterProducts($oldcats)))->paginate(9);
        }
        else if($sort == "old")
        {
        $oldcats = Product::where('category_id', 'like', '%' . $category_id . '%')->where('name', 'like', '%' . $search . '%')->where('status','=',1)->where('status','=',1)->get();
        $products = (new Collection(Product::filterProducts($oldcats)))->paginate(9);
        }
        else if($sort == "low")
        {
        $oldcats = Product::where('category_id', 'like', '%' . $category_id . '%')->where('name', 'like', '%' . $search . '%')->where('status','=',1)->orderBy('price','asc')->get();
        $products = (new Collection(Product::filterProducts($oldcats)))->sortBy('price')->paginate(9);
        }
        else if($sort == "high")
        {
        $oldcats =Product::where('category_id', 'like', '%' . $category_id . '%')->where('name', 'like', '%' . $search . '%')->where('status','=',1)->orderBy('price','desc')->get();
        $products = (new Collection(Product::filterProducts($oldcats)))->sortByDesc('price')->paginate(9);
        }
        if($request->ajax()){
            return view('front.pagination.search',compact('products','search','category_id','sort'));
        }
        
        return view('front.search',compact('products','search','category_id','sort'));
    }
// SORT SEARCH ENDS


        $oldcats = Product::where('category_id', 'like', '%' . $category_id . '%')->where('name', 'like', '%' . $search . '%')->where('status','=',1)->orderBy('id','desc')->get();
        $products = (new Collection(Product::filterProducts($oldcats)))->paginate(9);
        if($request->ajax()){
            return view('front.pagination.search',compact('products','search','category_id','sort'));
        }   
       return view('front.search',compact('products','search','category_id','sort'));   

       }
       

// *********************** NORMAL SEARCH SECTION ENDS ******************

        }

       return view('errors.404');         
    }


// -------------------------------- CATEGORY SECTION ENDS----------------------------------------


// -------------------------------- PRODUCT DETAILS SECTION ----------------------------------------

    public function product($slug)
    {
        $this->code_image();
        //$name = str_replace("-", " ", $slug);
        $productt = Product::where('slug','=',$slug)->first();   
        $productt->views+=1;
        $productt->update(); 
        if (Session::has('currency')) 
        {
            $curr = Currency::find(Session::get('currency'));
        }
        else
        {
            $curr = Currency::where('is_default','=',1)->first();
        }
        $product_click =  new ProductClick;
        $product_click->product_id = $productt->id;
        $product_click->date = Carbon::now()->format('Y-m-d');
        $product_click->save();          

        if($productt->user_id != 0)
        {
            $vendors = Product::where('user_id','=',$productt->user_id)->take(8)->get();
        }
        else 
        {
            $vendors = Product::where('user_id','=',0)->take(8)->get();
        }
        $brand = Brand::findOrFail($productt->brand_id);

        $subCategory = Subcategory::findOrFail($productt->subcategory_id);

        if($productt->childcategory_id != '' || $productt->childcategory_id != NULL)
        {
            $childCategory = Childcategory::findOrFail($productt->childcategory_id);
        }
        else
        {
            $childCategory = array();
        }

        $product_overview = DB::table('product_fields')->select('fieldname','fieldvalue')->where('product_id','=',$productt->id)->get();
        $product_features = DB::table('product_features_list')->where('product_id','=',$productt->id)->get();
        $product_variants = DB::table('product_varient')->where('product_id','=',$productt->id)->get();
        $product_gallery = DB::table('galleries')->where('product_id','=',$productt->id)->get();
        $product_related = DB::table('products')->where('brand_id','=',$productt->brand_id)->get();
      
       // $product_first_module = DB::table('product_features_list')->select('module_name')->where('product_id','=',$productt->id)->first();
      
        $variationDescription = array();

        foreach ($product_variants as $key => $product_variant) {
            if($product_variant->varient_name == 'Description')
            {
                $variation1Description = $product_variant->varient1;
                $variation2Description = $product_variant->varient2;
                $variation3Description = $product_variant->varient3;
            }
            elseif ($product_variant->varient_name == 'Variation Name') 
            {
                $variation1VariationName = $product_variant->varient1;
                $variation2VariationName = $product_variant->varient2;
                $variation3VariationName = $product_variant->varient3;
            }
            elseif ($product_variant->varient_name == 'Price') 
            {
                $variation1Price = $product_variant->varient1;
                $variation2Price = $product_variant->varient2;
                $variation3Price = $product_variant->varient3;
            }
            
        }
        //     echo "<pre>";
        // print_r($variation1VariationName);
        // die();
        $product_variant_getCount = DB::table('product_varient')->where('product_id','=',$productt->id)->first();
        
        if(empty($product_variant_getCount))
        {
            $variationCount = '0';
        }
        else
        {
            $variationCount = $product_variant_getCount->variation_count;
        }

        // echo "<pre>";
        // print_r($variationCount);
        // die();
        // echo "<pre>";
        // print_r($product_variant_count->variation_count);
        // print_r($childCategory);
        $module_name_array = array();
       // dd($product_features);
        if(!empty($product_features)){
        foreach ($product_features as $product_feature) 
        {
    
            $module_name_array[] = $product_feature->module_name;  
        }
            $module_names = array_unique($module_name_array);
        }else{
            $module_names = array();
        }
       $oldCompare = Session::get('compare');
      
        $compare = new Compare($oldCompare);
        $cproducts = $compare->items;
        $downloaded = '0';

        $product_support = DB::table('support_varient')->where('product_id','=',$productt->id)->get();
        $product_support_getCount = DB::table('support_varient')->where('product_id','=',$productt->id)->first();
        
        if(empty($product_support_getCount))
        {
            $supportvariationCount = '0';
        }
        else
        {
            $supportvariationCount = $product_support_getCount->variation_count;
        }

        foreach ($product_support as $product_supports) {

            if($product_supports->varient_name == 'Description')
            {
                $Support1Description = $product_supports->varient1;
                $Support2Description = $product_supports->varient2;
                $Support3Description = $product_supports->varient3;
            }
            elseif ($product_supports->varient_name == 'Variation Name') 
            {
                $Support1VariationName = $product_supports->varient1;
                $Support2VariationName = $product_supports->varient2;
                $Support3VariationName = $product_supports->varient3;
            }
            elseif ($product_supports->varient_name == 'Support Price') 
            {

                
                $Support1Price = $product_supports->varient1;
                $Support2Price = $product_supports->varient2;
                $Support3Price = $product_supports->varient3;
                /*die;*/
            }
            
        }
        $product_impl = DB::table('implementation_varient')->where('product_id','=',$productt->id)->get();
        $product_impl_getCount = DB::table('implementation_varient')->where('product_id','=',$productt->id)->first();
        
        if(empty($product_impl_getCount))
        {
            $implvariationCount = '0';
        }
        else
        {
            $implvariationCount = $product_impl_getCount->variation_count;
        }

        foreach ($product_impl as $product_impls) {

            if($product_impls->varient_name == 'Description')
            {
                $impl1Description = $product_impls->varient1;
                $impl2Description = $product_impls->varient2;
                $impl3Description = $product_impls->varient3;
            }
            elseif ($product_impls->varient_name == 'Variation Name') 
            {
                $impl1VariationName = $product_impls->varient1;
                $impl2VariationName = $product_impls->varient2;
                $impl3VariationName = $product_impls->varient3;
            }
            elseif ($product_impls->varient_name == 'Implementation Price') 
            {

                
                $impl1Price = $product_impls->varient1;
                $impl2Price = $product_impls->varient2;
                $impl3Price = $product_impls->varient3;
                /*die;*/
            }
            
        }
        // echo "<pre>";
        
        // print_r($product_support);
        // die();
        /*dd("$product_support");*/
        if ($productt->category_id == '23') 
        {
            return view('front.offer-profile',compact('productt','curr','vendors','brand','subCategory','childCategory','product_overview','product_features','module_names','product_variants','variationCount','variation1Description','variation2Description','variation3Description','variation1VariationName','variation2VariationName','variation3VariationName','variation1Price','variation2Price','variation3Price','product_gallery', 'product_related','cproducts','downloaded','product_support','supportvariationCount','Support1Description', 'Support2Description', 'Support3Description', 'Support1VariationName', 'Support2VariationName', 'Support3VariationName', 'Support1Price', 'Support2Price', 'Support3Price','product_impl', 'implvariationCount','impl1Description','impl2Description','impl3Description','impl1VariationName','impl2VariationName','impl3VariationName','impl1Price','impl2Price','impl3Price'));
        }
        else
        {   
            return view('front.product',compact('productt','curr','vendors','brand','subCategory','childCategory','product_overview','product_features','module_names','product_variants','variationCount','variation1VariationName','variation2VariationName','variation3VariationName','product_gallery','product_related','cproducts','downloaded','product_support','supportvariationCount','Support1Description', 'Support2Description', 'Support3Description', 'Support1VariationName', 'Support2VariationName', 'Support3VariationName', 'Support1Price', 'Support2Price', 'Support3Price','product_impl', 'implvariationCount','impl1Description','impl2Description','impl3Description','impl1VariationName','impl2VariationName','impl3VariationName','impl1Price','impl2Price','impl3Price'));
        }
        

    }

    public function product_download($slug,$token)
    {
        $this->code_image();
        // echo $slug;
        // die();
        //$name = str_replace("-", " ", $slug);
        // $productt = Product::where('name','=',$slug)->first();  
        $productt = Product::where('slug','=',$slug)->first(); 
        $productt->views+=1;
        $productt->update(); 
        if (Session::has('currency')) 
        {
            $curr = Currency::find(Session::get('currency'));
        }
        else
        {
            $curr = Currency::where('is_default','=',1)->first();
        }
        $product_click =  new ProductClick;
        $product_click->product_id = $productt->id;
        $product_click->date = Carbon::now()->format('Y-m-d');
        $product_click->save();          

        if($productt->user_id != 0)
        {
            $vendors = Product::where('user_id','=',$productt->user_id)->take(8)->get();
        }
        else 
        {
            $vendors = Product::where('user_id','=',0)->take(8)->get();
        }
        $brand = Brand::findOrFail($productt->brand_id);

        $subCategory = Subcategory::findOrFail($productt->subcategory_id);

        if($productt->childcategory_id != '' || $productt->childcategory_id != NULL)
        {
            $childCategory = Childcategory::findOrFail($productt->childcategory_id);
        }
        else
        {
            $childCategory = array();
        }

        $product_overview = DB::table('product_fields')->select('fieldname','fieldvalue')->where('product_id','=',$productt->id)->get();
        $product_features = DB::table('product_features_list')->where('product_id','=',$productt->id)->get();
        $product_variants = DB::table('product_varient')->where('product_id','=',$productt->id)->get();
        $product_gallery = DB::table('galleries')->where('product_id','=',$productt->id)->get();
        $product_related = DB::table('products')->where('brand_id','=',$productt->brand_id)->get();
      
       // $product_first_module = DB::table('product_features_list')->select('module_name')->where('product_id','=',$productt->id)->first();
      
        $variationDescription = array();

        foreach ($product_variants as $key => $product_variant) {
            if($product_variant->varient_name == 'Description')
            {
                $variation1Description = $product_variant->varient1;
                $variation2Description = $product_variant->varient2;
                $variation3Description = $product_variant->varient3;
            }
            elseif ($product_variant->varient_name == 'Variation Name') 
            {
                $variation1VariationName = $product_variant->varient1;
                $variation2VariationName = $product_variant->varient2;
                $variation3VariationName = $product_variant->varient3;
            }
            elseif ($product_variant->varient_name == 'Price') 
            {
                $variation1Price = $product_variant->varient1;
                $variation2Price = $product_variant->varient2;
                $variation3Price = $product_variant->varient3;
            }
            
        }
        //     echo "<pre>";
        // print_r($variation1VariationName);
        // die();
        $product_variant_getCount = DB::table('product_varient')->where('product_id','=',$productt->id)->first();
        
        if(empty($product_variant_getCount))
        {
            $variationCount = '0';
        }
        else
        {
            $variationCount = $product_variant_getCount->variation_count;
        }

        // echo "<pre>";
        // print_r($variationCount);
        // die();
        // echo "<pre>";
        // print_r($product_variant_count->variation_count);
        // print_r($childCategory);
        $module_name_array = array();
       // dd($product_features);
        if(!empty($product_features)){
        foreach ($product_features as $product_feature) 
        {
    
            $module_name_array[] = $product_feature->module_name;  
        }
            $module_names = array_unique($module_name_array);
        }else{
            $module_names = array();
        }
       $oldCompare = Session::get('compare');
      
        $compare = new Compare($oldCompare);
        $cproducts = $compare->items;
        // die();
        $productDownload = DB::table('product_specs_downlaod')->where('productId','=',$productt->id)
                                                            ->where('token','=',$token)->first();

        if(!empty($productDownload))
        {
            $downloaded = '1';
        }
        else
        {
            $downloaded = '0';
        }
        // die();
        if ($productt->category_id == '23') 
        {
            return view('front.offer-profile',compact('productt','curr','vendors','brand','subCategory','childCategory','product_overview','product_features','module_names','product_variants','variationCount','variation1Description','variation2Description','variation3Description','variation1VariationName','variation2VariationName','variation3VariationName','variation1Price','variation2Price','variation3Price','downloaded'));
        }
        else
        {   
            return view('front.product',compact('productt','curr','vendors','brand','subCategory','childCategory','product_overview','product_features','module_names','product_variants','variationCount','variation1VariationName','variation2VariationName','variation3VariationName','downloaded'));
        }
        

    }

    public function affProductRedirect($slug)
    {
        $product = Product::where('slug','=',$slug)->first();
//        $product->views+=1;
//        $product->update();


        return redirect($product->affiliate_link);

    }

    public function quick($id)
    {
        $product = Product::findOrFail($id);   
        if (Session::has('currency')) 
        {
            $curr = Currency::find(Session::get('currency'));
        }
        else
        {
            $curr = Currency::where('is_default','=',1)->first();
        }       
        return view('load.quick',compact('product','curr'));

    }

// -------------------------------- PRODUCT DETAILS SECTION ENDS----------------------------------------

// -------------------------------- PRODUCT COMMENT SECTION ----------------------------------------

    public function comment(Request $request)
    {
        $comment = new Comment;
        $input = $request->all();
        $comment->fill($input)->save();
        $comments = Comment::where('product_id','=',$request->product_id)->get()->count();
    $data[0] = $comment->user->photo ? url('assets/images/users/'.$comment->user->photo):url('assets/images/noimage.png');
        $data[1] = $comment->user->name;
        $data[2] = $comment->created_at->diffForHumans();
        $data[3] = $comment->text;
        $data[4] = $comments;
        $data[5] = route('product.comment.delete',$comment->id);
        $data[6] = route('product.comment.edit',$comment->id);
        $data[7] = route('product.reply',$comment->id);
        $data[8] = $comment->user->id;
        return response()->json($data);
    }

    public function commentedit(Request $request,$id)
    {
        $comment =Comment::findOrFail($id);
        $comment->text = $request->text;
        $comment->update();
        return response()->json($comment->text);
    } 

    public function commentdelete($id)
    {
        $comment =Comment::findOrFail($id);
        if($comment->replies->count() > 0)
        {
            foreach ($comment->replies as $reply) {
                $reply->delete();
            }
        }
        $comment->delete();
    }     




// -------------------------------- PRODUCT COMMENT SECTION ENDS ----------------------------------------

    public  function getModuleSection($name,$productid){

            $module_name = str_replace("-"," ",$name);
            $productid = $productid;
            // echo $productid;
            // die();
            return view('front.getajaxsection',compact('module_name','productid'));
    }

// -------------------------------- PRODUCT REPLY SECTION ----------------------------------------

 
    public function reply(Request $request,$id)
    {
        $reply = new Reply;
        $input = $request->all();
        $input['comment_id'] = $id;
        $reply->fill($input)->save();
        $data[0] = $reply->user->photo ? url('assets/images/users/'.$reply->user->photo):url('assets/images/noimage.png');
        $data[1] = $reply->user->name;
        $data[2] = $reply->created_at->diffForHumans();
        $data[3] = $reply->text;
        $data[4] = route('product.reply.delete',$reply->id);
        $data[5] = route('product.reply.edit',$reply->id);
        return response()->json($data);
    } 

    public function replyedit(Request $request,$id)
    {
        $reply = Reply::findOrFail($id);
        $reply->text = $request->text;
        $reply->update();
        return response()->json($reply->text);
    } 

    public function replydelete($id)
    {
        $reply =Reply::findOrFail($id);
        $reply->delete();
    } 

// -------------------------------- PRODUCT REPLY SECTION ENDS----------------------------------------


// ------------------ Rating SECTION --------------------

    public function reviewsubmit(Request $request)
    {
        $ck = 0;
$orders = Order::where('user_id','=',$request->user_id)->where('status','=','completed')->get();

        foreach($orders as $order)
        {
        $cart = unserialize(bzdecompress(utf8_decode($order->cart)));
            foreach($cart->items as $product)
            {
                if($request->product_id == $product['item']['id'])
                {
                    $ck = 1;
                    break;
                }
            }
        }
        if($ck == 1)
        {
            $user = Auth::guard('web')->user();
            $prev = Rating::where('product_id','=',$request->product_id)->where('user_id','=',$user->id)->get();
            if(count($prev) > 0)
            {
            return response()->json(array('errors' => [ 0 => 'You Have Reviewed Already.' ]));                
            }         
            $Rating = new Rating;
            $Rating->fill($request->all());
            $Rating['review_date'] = date('Y-m-d H:i:s');
            $Rating->save();
            $data[0] = 'Your Rating Submitted Successfully.';
            $data[1] = Rating::rating($request->product_id);
            return response()->json($data);  
        }
        else{
            return response()->json(array('errors' => [ 0 => 'Buy This Product First' ]));
        }
    }


    public function reviews($id){
        $productt = Product::find($id);   
        return view('load.reviews',compact('productt','id'));

    }

// ------------------ Rating SECTION ENDS --------------------



    // Capcha Code Image
    private function  code_image()
    {
        $actual_path = str_replace('project','',base_path());
        $image = imagecreatetruecolor(200, 50);
        $background_color = imagecolorallocate($image, 255, 255, 255);
        imagefilledrectangle($image,0,0,200,50,$background_color);

        $pixel = imagecolorallocate($image, 0,0,255);
        for($i=0;$i<500;$i++)
        {
            imagesetpixel($image,rand()%200,rand()%50,$pixel);
        }

        $font = $actual_path.'assets/front/fonts/NotoSans-Bold.ttf';
        $allowed_letters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        $length = strlen($allowed_letters);
        $letter = $allowed_letters[rand(0, $length-1)];
        $word='';
        //$text_color = imagecolorallocate($image, 8, 186, 239);
        $text_color = imagecolorallocate($image, 0, 0, 0);
        $cap_length=6;// No. of character in image
        for ($i = 0; $i< $cap_length;$i++)
        {
            $letter = $allowed_letters[rand(0, $length-1)];
            imagettftext($image, 25, 1, 35+($i*25), 35, $text_color, $font, $letter);
            $word.=$letter;
        }
        $pixels = imagecolorallocate($image, 8, 186, 239);
        for($i=0;$i<500;$i++)
        {
            imagesetpixel($image,rand()%200,rand()%50,$pixels);
        }
        session(['captcha_string' => $word]);
        imagepng($image, $actual_path."assets/images/capcha_code.png");
    }

    //------------agency dummy 

     public function agency_dummy($brand)
    {
        $this->code_image();
        //dd(
        $brand = DB::table('brand')->where('name','=',$brand)->first();  
        $products = PRODUCT::where('brand_id','=',$brand->id)
                    ->join('subcategories', 'products.subcategory_id', '=', 'subcategories.id')
                    ->select('products.name','products.id','products.slug','products.photo','products.thumbnail','subcategories.id as subcat_id', 'subcategories.name as subcat_name')
                    ->get();
        //dd($products);
        $oldCompare = Session::get('compare');
       // dd($oldCompare);
        $compare = new Compare($oldCompare);
        $cproducts = $compare->items;
        return view('front.agency',compact('brand','products','cproducts'));

    }
        //------------category list------------------// 

     public function categoryList($name)
    {
        $this->code_image();
        if($name == 'Add-on'){
            $slug = $name;
        }else{
            $slug = str_replace("-", " ", $name);
        }
        $category = DB::table('subcategories')->where('name','=',$slug)->first();   
         $products = PRODUCT::where('subcategory_id','=',$category->id)
                    ->join('subcategories', 'products.subcategory_id', '=', 'subcategories.id')
                    ->select('products.name','products.id','products.slug','products.photo','products.thumbnail','subcategories.id as subcat_id', 'subcategories.name as subcat_name')
                    ->get();
        $oldCompare = Session::get('compare');
      
        $compare = new Compare($oldCompare);
        $cproducts = $compare->items;
        $searchBy = DB::table('brand')->get();
        $filterby = 'Brand' ;
        
        return view('front.categoryList',compact('category','products','cproducts','searchBy','filterby'));

    }

     //------------product list for services------------------// 


     public function productList($name)
    {
        $this->code_image();
        if($name == 'Add-on'){
            $slug = $name;
        }else{
            $slug = str_replace("-", " ", $name);
        }
        $category = DB::table('subcategories')->where('name','=',$slug)->first();   
         $products = PRODUCT::select('products.name','products.id','products.slug','products.photo','products.thumbnail')
                    ->where('products.category_id','=','22')
                    ->get();
        $oldCompare = Session::get('compare');
      
        $compare = new Compare($oldCompare);
        $cproducts = $compare->items;
      
        return view('front.productList',compact('category','products','cproducts'));

    }

    //------------brand list for services------------------// 


     public function brandList($name)
    {

        $this->code_image();
         if($name == 'Add-on'){
            $catName = $name;
        }else{
            $catName = str_replace("-", " ", $name);
        }
        $brands = DB::table('brand')->get();
        $oldCompare = Session::get('compare');
      
        $compare = new Compare($oldCompare);
        $cproducts = $compare->items;

         return view('front.brands',compact('catName','brands','cproducts'));

    }

     //------------services List------------------// 


     public function serviceList($catname,$brand)
    {
        $this->code_image();
        if($catname == 'Add-on'){
            $cat_name = $catname;
        }else{
            $cat_name = str_replace("-", " ", $catname);
        }
        $brand_name = str_replace("-", " ", $brand);

        $selcategory = DB::table('subcategories')->select('id')->where('name','=',$cat_name)->first();

        $selbrand = DB::table('brand')->select('id')->where('name','=',$brand_name)->first();
       
        $products = PRODUCT::where('subcategory_id', '=', $selcategory->id)->where('brand_id', '=', $selbrand->id)->select('products.name','products.id','products.slug','products.photo','products.thumbnail')
                    ->get();
        //dd($products);
        $oldCompare = Session::get('compare');
      
        $compare = new Compare($oldCompare);
        $cproducts = $compare->items;
         $searchBy = DB::table('products')->where('category_id','=','22')->get();
        $category = DB::table('subcategories')->select('id','name')->where('name','=',$cat_name)->first();
        $filterby = 'Product' ;
      
      
        return view('front.serviceList',compact('products','cproducts','category','searchBy','filterby'));

    }


    //-----------------------------Service page design-------------//
     public function service_page($slug)
    {
        $this->code_image();
        $productt = Product::where('slug','=',$slug)->first();   
        $productt->views+=1;
        $productt->update(); 
        if (Session::has('currency')) 
        {
            $curr = Currency::find(Session::get('currency'));
        }
        else
        {
            $curr = Currency::where('is_default','=',1)->first();
        }
        $product_click =  new ProductClick;
        $product_click->product_id = $productt->id;
        $product_click->date = Carbon::now()->format('Y-m-d');
        // $product_click->save();          

        if($productt->user_id != 0)
        {
            $vendors = Product::where('user_id','=',$productt->user_id)->take(8)->get();
        }
        else 
        {
            $vendors = Product::where('user_id','=',0)->take(8)->get();
        }
        return view('front.servicePage',compact('productt','curr','vendors'));

    }


}
