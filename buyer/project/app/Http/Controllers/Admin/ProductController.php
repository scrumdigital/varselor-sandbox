<?php

namespace App\Http\Controllers\Admin;

use App\Models\Childcategory;
use App\Models\Subcategory;
use Datatables;
use Carbon\Carbon;
use App\Models\Product;
use App\Models\Category;
use App\Models\Currency;
use App\Models\Gallery;
use App\Models\FixedPrice;
use App\Models\UserSlabPricing;
use App\Models\ModulePricing;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Validator;
use Image;
use DB;

class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    //*** JSON Request
    public function datatables()
    {
        DB::statement(DB::raw('set @rownum=0'));
         $datas = Product::orderBy('id','desc')->get(['products.*',DB::raw('@rownum  := @rownum  + 1 AS rownum')]);

         // $datas = Product::orderBy('id','desc')->get();
         
         //--- Integrating This Collection Into Datatables
         return Datatables::of($datas)
                            ->editColumn('s.no', function(Product $data) {
                                return $data->rownum;
                            })
                            ->editColumn('name', function(Product $data) {
                                $name = strlen(strip_tags($data->name)) > 50 ? substr(strip_tags($data->name),0,50).'...' : strip_tags($data->name);
                                $id = '<small>Product ID: <a href="'.route('front.product', $data->slug).'" target="_blank">'.sprintf("%'.08d",$data->id).'</a></small>';
                                return  $name.'<br>'.$id;
                            })
                            ->editColumn('price', function(Product $data) {
                                $sign = Currency::where('is_default','=',1)->first();
                                $price = $sign->sign.$data->price;
                                return  $price;
                            })
                            ->editColumn('stock', function(Product $data) {
                                $stck = (string)$data->stock;
                                if($stck == "0")
                                return "Out Of Stock";
                                elseif($stck == null)
                                return "Unlimited";
                                else
                                return $data->stock;
                            })
                            ->addColumn('status', function(Product $data) {
                                $class = $data->status == 1 ? 'drop-success' : 'drop-danger';
                                $s = $data->status == 1 ? 'selected' : '';
                                $ns = $data->status == 0 ? 'selected' : '';
                                return '<div class="action-list switch-active-deactive-box">

                                <select class="process select droplinks '.$class.'"><option data-val="1" value="'. route('admin-prod-status',['id1' => $data->id, 'id2' => 1]).'" '.$s.'>Activated</option><<option data-val="0" value="'. route('admin-prod-status',['id1' => $data->id, 'id2' => 0]).'" '.$ns.'>Deactivated</option></select>

                                </div>';
                            })                             
                            ->addColumn('action', function(Product $data) {
                                return '<div class="godropdown"><button class="go-dropdown-toggle"> Actions<span class="fal fa-chevron-down"></span></button><div class="action-list product-list-action" ><a class="btn product-table-btn" href="' . route('admin-prod-edit',$data->id) . '"> <span class="fal fa-edit"></span>Edit</a><a class="btn product-table-btn" href="javascript" class="set-gallery" data-toggle="modal" data-target="#setgallery"><input type="hidden" value="'.$data->id.'"><span class="fal fa-eye"></span> View Gallery</a><a data-href="' . route('admin-prod-feature',$data->id) . '" class="feature btn product-table-btn" data-toggle="modal" data-target="#modal2"> <span class="fal fa-star"></span>Highlight</a><a href="javascript:;" data-href="' . route('admin-prod-delete',$data->id) . '" data-toggle="modal" data-target="#confirm-delete" class="delete btn product-table-btn"><span class="fal fa-trash-alt"></span>Delete</a><a class="btn product-table-btn" href="' . route('admin-prod-copy',$data->id).'" ><span class="fal fa-copy"></span>Copy</a></div></div>';
                            }) 
                            ->rawColumns(['name', 'status', 'action'])
                            ->toJson(); //--- Returning Json Data To Client Side
    }

    //*** JSON Request
    public function deactivedatatables()
    {
         $datas = Product::where('status','=',0)->orderBy('id','desc')->get();
         
         //--- Integrating This Collection Into Datatables
         return Datatables::of($datas)
                            ->editColumn('name', function(Product $data) {
                                $name = strlen(strip_tags($data->name)) > 50 ? substr(strip_tags($data->name),0,50).'...' : strip_tags($data->name);
                                $id = '<small>Product ID: <a href="'.route('front.product', $data->slug).'" target="_blank">'.sprintf("%'.08d",$data->id).'</a></small>';
                                return  $name.'<br>'.$id;
                            })
                            ->editColumn('price', function(Product $data) {
                                $sign = Currency::where('is_default','=',1)->first();
                                $price = $sign->sign.$data->price;
                                return  $price;
                            })
                            ->editColumn('stock', function(Product $data) {
                                $stck = (string)$data->stock;
                                if($stck == "0")
                                return "Out Of Stock";
                                elseif($stck == null)
                                return "Unlimited";
                                else
                                return $data->stock;
                            })
                            ->addColumn('status', function(Product $data) {
                                $class = $data->status == 1 ? 'drop-success' : 'drop-danger';
                                $s = $data->status == 1 ? 'selected' : '';
                                $ns = $data->status == 0 ? 'selected' : '';
                                return '<div class="action-list switch-active-deactive-box"><select class="process select droplinks '.$class.'"><option data-val="1" value="'. route('admin-prod-status',['id1' => $data->id, 'id2' => 1]).'" '.$s.'>Activated</option><<option data-val="0" value="'. route('admin-prod-status',['id1' => $data->id, 'id2' => 0]).'" '.$ns.'>Deactivated</option>/select></div>';
                            })                             
                            ->addColumn('action', function(Product $data) {
                                return '<div class="godropdown"><button class="go-dropdown-toggle"> Actions<span class="fal fa-chevron-down"></span></button><div class="action-list product-list-action" ><a class="btn product-table-btn" href="' . route('admin-prod-edit',$data->id) . '"> <span class="fal fa-edit"></span>Edit</a><a class="btn product-table-btn" href="javascript" class="set-gallery" data-toggle="modal" data-target="#setgallery"><input type="hidden" value="'.$data->id.'"><span class="fal fa-eye"></span> View Gallery</a><a data-href="' . route('admin-prod-feature',$data->id) . '" class="feature btn product-table-btn" data-toggle="modal" data-target="#modal2"> <span class="fal fa-star"></span>Highlight</a><a href="javascript:;" data-href="' . route('admin-prod-delete',$data->id) . '" data-toggle="modal" data-target="#confirm-delete" class="delete btn product-table-btn"><span class="fal fa-trash-alt"></span>Delete</a></div></div>';
                            }) 
                            ->rawColumns(['name', 'status', 'action'])
                            ->toJson(); //--- Returning Json Data To Client Side
    }

    //*** GET Request
    public function index()
    {
        return view('admin.product.index');
    }

    //*** GET Request
    public function deactive()
    {
        return view('admin.product.deactive');
    }

    //*** GET Request
    public function types()
    {
        $cats = Category::all();
       // $productcats = Subcategory::where('category_fields',"=",'22');
        return view('admin.product.types',compact('cats'));
    }

     public static function  getSubcats($catId)
    {
            $subcats = Subcategory::where('category_id',"=",$catId)->get();
            //dd($subcats);
           return $subcats;
    }
     public static function  noOfProducts($subcatId)
    {
            $productcount = Product::where('subcategory_id',"=",$subcatId)->count();
            //dd($productcount);
           return $productcount;
    }
    public static function  getsubcatfields($subcatId)
    {
      $subcatfields = DB::table('category_fields')->where('cat_id','=',$subcatId)->count();
      return $subcatfields;
    }  

    public static function  getsubcatfeatures($subcatId)
    {
      $subcatfields = DB::table('category_fields')->where('cat_id','=',$subcatId)->count();
      return $subcatfields;
    }  



    //*** GET Request
    public function createPhysical()
    {
        $cats = Category::all();
        $sign = Currency::where('is_default','=',1)->first();
        return view('admin.product.create.physical',compact('cats','sign'));
    }

     //*** POST Request
    public function createProduct(Request $request)
    {
       // dd($request->category_id  if(empty($request)) {
        $cats = Category::where('id','=',$request->category_id)->first();
        $sign = Currency::where('is_default','=',1)->first();
         $subcats = $request->subcategory_id;
         $catId = $request->category_id; 
         //dd($request->subcategory_id);
        $subcatfields = DB::table('category_fields')->where('cat_id','=',$request->subcategory_id)->get();
        $brands = DB::table('brand')->get();
        $countries = DB::table('countries')->get();
        $variation_count = '';
        $selectedTab = 'genral';
        $productList=  DB::table('products')->select('name')
                        ->where('category_id','=','22')
                        ->get();
        $subcatname = DB::table('subcategories')
                    ->select('name')
                    ->where('id','=',$subcats)->first();
       $product_name = '';
       $i =0;

        foreach($productList as $products){
            
           $product_name .= $products->name.",";

           $i++;
        }
     // echo $product_name;die;
        $fieldsetdata ='';
        

               foreach($subcatfields as $key=>$value){
                          $inputfieldname = str_replace(" ", "_", $value->fieldname);

                            $fieldsetdata .= '
                                          <div class="row">
                                            <div class="col-lg-3">
                                                        <div class="left-area">
                                                                <h4 class="heading">'.$value->fieldname.' </h4>
                                                                <p class="sub-heading">(In Any Language)</p>
                                                        </div>
                                            </div>
                                            <div class="col-lg-8">';
                                              //echo "fdfd".$value->fieldtype;die;

                                            
                        
                                          if($value->fieldtype == 'Multiselect'){
                                            
                                            $fieldsetdata .="<div id='output'></div>";


                                            $optionArr = explode(",",str_replace(", ",",",$value->fieldvalue)); 
                                            $fieldsetdata .='<select data-placeholder="Choose tags ..." id="'.$value->fieldname.'" name="product_subcat_fields['.$value->fieldname.']" multiple class="chosen-select form-control">';
                                                foreach($optionArr as $options){
                                                    $fieldsetdata .='<option value="'.$options.'"> '.$options.' </option>';
                                                }
                                                $fieldsetdata .=' </select>';
                                                
                                          }
                                          if($value->fieldtype == 'dropdown'){
                                            $optionArr = explode(",",str_replace(", ",",",$value->fieldvalue)); 
                                            $fieldsetdata .='<select name="product_subcat_fields['.$value->fieldname.']">';
                                             $fieldsetdata .='<option >Select '.$value->fieldname.'</option>';
                                                foreach($optionArr as $options){
                                                    $fieldsetdata .='<option value="'.$options.'"> '.$options.' </option>';
                                                }
                                                $fieldsetdata .='</select>';
                                        
                                          }if($value->fieldtype == 'checkbox'){
                                                 $checkListArr = explode(",",$value->fieldvalue); 
                                             foreach($checkListArr as $checkList){
                                                
                                                 $fieldsetdata .= $checkList.' <input type="'.$value->fieldtype.'"  placeholder="" name="product_subcat_fields['.$inputfieldname.'][]" class="checkclick" value="'.$checkList.'" >';
                                                }
                                          }if($value->fieldtype == 'radio'){
                                                 $checkListArr = explode(",",$value->fieldvalue); 
                                             foreach($checkListArr as $checkList){
                                                
                                                 $fieldsetdata .= $checkList.' <input type="'.$value->fieldtype.'"  placeholder="" name="product_subcat_fields['.$inputfieldname.'][]" class="radioclick" value="'.$checkList.'" >';
                                                }
                                          }

                                          if($value->fieldtype == 'text'){
                                             $fieldsetdata .='<input type="'.$value->fieldtype.'" class="form-control" placeholder="" name="product_subcat_fields['.$inputfieldname.']" >';
                                          }
                                          if($value->fieldtype == 'text-area'){
                                             $fieldsetdata .='<textarea class="form-control" rows="3" name="product_subcat_fields['.$inputfieldname.']" ></textarea>';
                                          }
                                        $fieldsetdata .='</div></div>';
                                     
                 }         
//echo $request->subcategory_id;
                    $featureLists=  DB::table('category_features')->where('cat_id','=',$request->subcategory_id)->get();
                     $VendorList=  DB::table('users')->where('status','=','2')->get();
                             
                   $prod = array();
                   // dd($featureLists);
                return view('admin.product.create.add',compact('cats','sign','subcats','fieldsetdata','featureLists','brands','countries','VendorList','selectedTab','prod','variation_count','catId', 'subcats','product_name','productList','subcatname'));
        
    }
     public function addProdFeature(Request $request)
    {
       //echo $request->subcat_id; 
         $featureLists=  DB::table('category_features')->where('cat_id','=',$request->subcat_id)->get();
        return view('admin.product.create.addFeatures',compact('data','featureLists'));
 
    }

    public function getFeatureMod($id){

           // $moduleData =  DB::table('category_features_modules')->where('feature_id','=',$id)->get();
                 $moduleData = DB::table('category_features_modules')->groupBy('module_name')->where('feature_id','=',$id)->get();
       

            return view('admin.product.featureModules',compact('moduleData','id'));
 
    }


       //*** GET Request
    public function status($id1,$id2)
    {
        $data = Product::findOrFail($id1);
        $data->status = $id2;
        $data->update();
    }

    //*** POST Request
    public function uploadUpdate(Request $request,$id)
    {
        //--- Validation Section
        $rules = [
          'image' => 'required',
        ];
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
          return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        }

        $data = Product::findOrFail($id);

        //--- Validation Section Ends
        $image = $request->image;
        list($type, $image) = explode(';', $image);
        list(, $image)      = explode(',', $image);
        $image = base64_decode($image);
        $image_name = time().str_random(8).'.png';
        $path = 'assets/images/products/'.$image_name;
        file_put_contents($path, $image);
                if($data->photo != null)
                {
                    if (file_exists(public_path().'/assets/images/products/'.$data->photo)) {
                        unlink(public_path().'/assets/images/products/'.$data->photo);
                    }
                } 
                        $input['photo'] = $image_name;
         $data->update($input);
                if($data->thumbnail != null)
                {
                    if (file_exists(public_path().'/assets/images/thumbnails/'.$data->thumbnail)) {
                        unlink(public_path().'/assets/images/thumbnails/'.$data->thumbnail);
                    }
                } 

        $img = Image::make(public_path().'/assets/images/products/'.$data->photo)->resize(285, 285);
        $thumbnail = time().str_random(8).'.jpg';
        $img->save(public_path().'/assets/images/thumbnails/'.$thumbnail);
        $data->thumbnail  = $thumbnail;   
        $data->update();
        return response()->json(['status'=>true,'file_name' => $image_name]);
    }

   ////////////check product exist with same name//////////
    public function checkProductExist($name){
      $product_name = str_replace("-", " ", $name);
     $chkexist = Product::where('name','=',$product_name)->first();
     //dd($chkexist);
      if(!empty($chkexist)){
        echo "Product name already Exist";
      }
    }

   ///////////////////////////////////////////////////////////
    //*** POST Request
    public function storeProduct(Request $request)
    {
        //--- Validation Section

        $rules = [
            'photo'      => 'required',
            'file'       => 'mimes:zip',
            'upload_product_sheet' => 'mimes:pdf|max:10000'
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        //--- Validation Section Ends

        //--- Logic Section     
        $input = $request->all();
        

      $chkexist = Product::where('name','=',$request->name)->first();
     //dd($chkexist);
      if(empty($chkexist)){
     
        $data = new Product;


        $sign = Currency::where('is_default','=',1)->first();
       
        // echo "<pre>";
        // print_r($input);
        // die();
        // Check File
        if ($file = $request->file('file'))
        {
            $name = time().$file->getClientOriginalName();
            $file->move('assets/files',$name);
            $input['file'] = $name;
        }

        $image = $request->photo;
        list($type, $image) = explode(';', $image);
        list(, $image)      = explode(',', $image);
        $image = base64_decode($image);
        $image_name = time().str_random(8).'.png';
        $path = 'assets/images/products/'.$image_name;
        file_put_contents($path, $image);
        $input['photo'] = $image_name;


        // Check Physical
       
            // Check Condition
                  // Check Seo
        if (empty($request->seo_check))
        {
            $input['meta_tag'] = null;
            $input['meta_description'] = null;
        }
        else {
            if (!empty($request->meta_tag))
            {
                $input['meta_tag'] = implode(',', $request->meta_tag);
            }
        }

    
        //tags
        if (!empty($request->tags))
        {
            $input['tags'] = implode(',', $request->tags);
        }
        if ($request->file('upload_product_sheet')) 
        {
            $upload_product_sheet = $request->file('upload_product_sheet');
            $upload_product_sheetName = time().'.'.$upload_product_sheet->getClientOriginalExtension();
            $destinationPath = public_path('assets/product_sheets');
            $upload_product_sheet->move($destinationPath, $upload_product_sheetName);

            $data->product_spec_sheet = $upload_product_sheetName;
        }
             $data->country_code = $input['country_code'];
             $data->brand_id = $input['brand_id'];
             $data->additional_information = str_replace("\n","<<ENTER>>",$input['additional_information']);
             $data->price_start_range = $input['price_start_range'];
             $data->overview = str_replace("\n","<<ENTER>>",$input['overview']);
             $data->subcategory_id = $input['subcat_id'];
              $data->category_id = $input['cat_id'];
             $data->user_id = $input['vendor_id'];
             if(!empty($input['product_support'])){
              $data->product_support = $input['product_support'];
             } 
             if(!empty($input['product_implementation'])){
              $data->product_implementation = $input['product_implementation'];
             }
            if(!empty($input['addon_products'])){
              $data->addon_products = $input['addon_products'];
            }
         
        $data->fill($input)->save();
     
        $prod = Product::find($data->id);
        $prod->slug = str_slug($data->name,'-').'-'.strtolower(str_random(3).$data->id.str_random(3));
        // Set Thumbnail
        $img = Image::make(public_path().'/assets/images/products/'.$prod->photo)->resize(285, 285);
        $thumbnail = time().str_random(8).'.jpg';
        $img->save(public_path().'/assets/images/thumbnails/'.$thumbnail);
        $prod->thumbnail  = $thumbnail;
        $prod->update();

        // Add To Gallery If any
        $lastid = $data->id;
        if ($files = $request->file('gallery')){
            foreach ($files as  $key => $file){
                if(in_array($key, $request->galval))
                {
                    $gallery = new Gallery;
                    $name = time().$file->getClientOriginalName();
                    $file->move('assets/images/galleries',$name);
                    $gallery['photo'] = $name;
                    $gallery['product_id'] = $lastid;
                    $gallery->save();
                }
            }
        }
        // echo "<pre>".$prod->category_id;
        // print_r($data->id);
        // die();
   
        //--- Redirect Section        
        $msg = 'New Product Added Successfully.';
        if($prod->category_id == '23')
        {   
            return redirect()->route('admin-prod-create-varient',$data->id);
        }
        else
        {
            return redirect()->route('admin-prod-create-overview',$data->id);
        }
     } else{
          $msg="Product name already exist";
         return response()->json($msg);

     } 
       // return response()->json($msg);
        //--- Redirect Section Ends    
    }

    public function createProOverview($lastid){
         $prod = Product::find($lastid);
        //echo"ewewe"; die;
     //dd($prod);
    // dd($request->category_id  if(empty($request)) {
        $cats = Category::where('id','=',$prod->category_id)->first();
//echo $prod->subcategory_id;
        $sign = Currency::where('is_default','=',1)->first();
       // $subcats = Subcategory::where('id','=',$prod->subcategory_id)->first();
        //dd($subcats);
        $subcatfields = DB::table('category_fields')->where('cat_id','=',$prod->subcategory_id)->get();
        $brands = DB::table('brand')->get();
        $countries = DB::table('countries')->get();
        $subcatname = DB::table('subcategories')
                    ->select('name')
                    ->where('id','=',$prod->subcategory_id)->first();
        $variation_count = '';
        $selectedTab = "overview" ;
        //dd($subcatfields);
        $fieldsetdata ='';
          $subcats = $prod->subcategory_id;
        $catId = $prod->category_id;
               foreach($subcatfields as $key=>$value){

                          $inputfieldname = str_replace(" ", "_", $value->fieldname);

                            $fieldsetdata .= '
                                          <div class="row">
                                            <div class="col-lg-3">
                                                        <div class="left-area">
                                                                <h4 class="heading">'.$value->fieldname.' </h4>
                                                                <p class="sub-heading">(In Any Language)</p>
                                                        </div>
                                            </div>
                                            <div class="col-lg-8">';
                                               //echo "fdfd".$value->fieldtype;die;
                        
                                          if($value->fieldtype == 'Multiselect'){
                                            
                                            $fieldsetdata .="<div id='output'></div>";


                                            $optionArr = explode(",",str_replace(", ",",",$value->fieldvalue)); 
                                            $fieldsetdata .='<select data-placeholder="Choose tags ..." id="product_subcat_fields['.$value->fieldname.']" multiple class="chosen-select form-control">';
                                                foreach($optionArr as $options){
                                                    $fieldsetdata .='<option value="'.$options.'"> '.$options.' </option>';
                                                }
                                                $fieldsetdata .=' </select>';
                                                $fieldsetdata .=' <input type="hidden" name="product_subcat_fields['.$value->fieldname.']" value="">';
                                        
                                          }

                                          if($value->fieldtype == 'dropdown'){
                                            $optionArr = explode(",",str_replace(", ",",",$value->fieldvalue)); 
                                            $fieldsetdata .='<select class="form-control" name="product_subcat_fields['.$value->fieldname.']">';
                                             $fieldsetdata .='<option >Select '.$value->fieldname.'</option>';
                                                foreach($optionArr as $options){
                                                    $fieldsetdata .='<option value="'.$options.'"> '.$options.' </option>';
                                                }
                                                $fieldsetdata .='</select>';
                                        
                                          }if($value->fieldtype == 'checkbox'){
                                            $i = 0;
                                                 $checkListArr = explode(",",$value->fieldvalue); 
                                             foreach($checkListArr as $checkList){

                                                
                                                 $fieldsetdata .= '<label class="checkbox checkbox-primary checkbox-inline-block" for="product_subcat_fields['.$inputfieldname.'][]'.'_'.$i.'"> <input type="'.$value->fieldtype.'"  placeholder="" name="product_subcat_fields['.$inputfieldname.'][]" id="product_subcat_fields['.$inputfieldname.'][]'.'_'.$i.'" class="checkclick" value="'.$checkList.'" ><span>'.$checkList.'</span><span class="checkmark"></span></label>';
                                                 $i++;
                                                }
                                          }if($value->fieldtype == 'radio'){
                                                 $checkListArr = explode(",",$value->fieldvalue); 
                                             foreach($checkListArr as $checkList){
                                            $i = 0;

                                                
                                                 $fieldsetdata .= '<label class="radio radio-primary radio-inline-block" id="'.$checkList.'_'.$i.'"> <input type="'.$value->fieldtype.'"  placeholder="" name="product_subcat_fields['.$inputfieldname.'][]" id="'.$checkList.'_'.$i.'" class="radioclick" value="'.$checkList.'" ><span>'.$checkList.'</span><span class="checkmark"></span></label>';
                                                 $i++;
                                                }
                                          }

                                          if($value->fieldtype == 'text'){
                                             $fieldsetdata .='<input type="'.$value->fieldtype.'" class="form-control" placeholder="" name="product_subcat_fields['.$inputfieldname.']" >';
                                          }
                                          if($value->fieldtype == 'text-area'){
                                             $fieldsetdata .='<textarea class="form-control" rows="3" name="product_subcat_fields['.$inputfieldname.']" ></textarea>';
                                          }
                                        $fieldsetdata .='</div></div>';
                                     
                 }         

                // dd($fieldsetdata);
//echo $request->subcategory_id;
                    $featureLists=  DB::table('category_features')->where('cat_id','=',$prod->subcategory_id)->get();
                     $VendorList=  DB::table('users')->where('status','=','2')->get();
                     $product_name='';
                     $productList =array();
                             
                   
        //dd($fieldsetdata);
                return view('admin.product.create.add',compact('cats','sign','subcats','fieldsetdata','featureLists','brands','countries','VendorList','selectedTab','prod','variation_count','subcats','catId','product_name','productList','subcatname'));
        
    }

    public function storeFeature(Request $request){



       $featureLists=  DB::table('category_features_modules')->where('feature_id','=',$request->feature_id)->get();

         $checkExist = DB::table('product_features_list')
                         ->where('product_id','=',$request->prod_id)
                        ->first();
        if(!empty($checkExist)){
          $delfields= DB::table('product_features_list')->where('product_id','=',$request->prod_id)->delete();
        }
//dd($request->function_val[]);
      foreach($featureLists as $feature){
        

       
            $fields=array('product_id'=>$request->prod_id,'feature_id'=>$request->feature_id,'module_name'=>$feature->module_name,'section_name'=>$feature->section_name,'function_name'=>$feature->function_name,'function_value'=>'no');

           //if(empty($checkExist1)){  
             $insfields= DB::table('product_features_list')->insert($fields);
            //}

          
          
      }

          foreach ($request->function_val as $key => $value) {
             $keArray =  explode("_", $key);

                DB::table('product_features_list')
                // ->where('module_fname',$feature->module_name)
                ->where('module_name',$keArray[0])
                ->where('section_name' ,$keArray[1] )
                ->where('function_name',$keArray[2])
                ->where('feature_id' ,$request->feature_id )
                ->update(['function_value' => $value]);
            }
         $msg = 'New Product Added Successfully.';
          return redirect()->route('admin-prod-create-varient',$request->prod_id);
            
    }

    ///// create producut varient ////////////////////
  public function createProVarient($lastid){


        $prod = Product::find($lastid);
        $cats = Category::where('id','=',$prod->category_id)->first();

        $sign = Currency::where('is_default','=',1)->first();
        $subcats = Subcategory::where('id','=',$prod->subcategory_id)->first();
        //dd($subcats);
        $subcatfields = DB::table('category_fields')->where('cat_id','=',$prod->subcategory_id)->get();
        $brands = DB::table('brand')->get();
        $countries = DB::table('countries')->get();
        $subcatname = DB::table('subcategories')
                    ->select('name')
                    ->where('id','=',$prod->subcategory_id)->first();
        $variation_count = '';
        $selectedTab = "varient" ;
        //dd($subcatfields);
        $fieldsetdata ='';
         $subcats = $prod->subcategory_id;
        $catId = $prod->category_id;
    //     echo "<pre>";
    // print_r($catId);
    // die();
         
               foreach($subcatfields as $key=>$value){

                          $inputfieldname = str_replace(" ", "_", $value->fieldname);

                            $fieldsetdata .= '
                                          <div class="row">
                                            <div class="col-lg-4">
                                                        <div class="left-area">
                                                                <h4 class="heading">'.$value->fieldname.' </h4>
                                                                <p class="sub-heading">(In Any Language)</p>
                                                        </div>
                                            </div>
                                            <div class="col-lg-7">';
                                          if($value->fieldtype == 'dropdown'){
                                            $optionArr = explode(",",str_replace(", ",",",$value->fieldvalue)); 
                                            $fieldsetdata .='<select name="product_subcat_fields['.$value->fieldname.']">';
                                             $fieldsetdata .='<option >Select '.$value->fieldname.'</option>';
                                                foreach($optionArr as $options){
                                                    $fieldsetdata .='<option value="'.$options.'"> '.$options.' </option>';
                                                }
                                                $fieldsetdata .='</select>';
                                        
                                          }if($value->fieldtype == 'checkbox'){
                                                 $checkListArr = explode(",",$value->fieldvalue); 
                                             foreach($checkListArr as $checkList){
                                                
                                                 $fieldsetdata .= $checkList.' <input type="'.$value->fieldtype.'"  placeholder="" name="product_subcat_fields['.$inputfieldname.']" class="checkclick" value="'.$checkList.'" >';
                                                }
                                          }if($value->fieldtype == 'radio'){
                                                 $checkListArr = explode(",",$value->fieldvalue); 
                                             foreach($checkListArr as $checkList){
                                                
                                                 $fieldsetdata .= $checkList.' <input type="'.$value->fieldtype.'"  placeholder="" name="product_subcat_fields['.$inputfieldname.']" class="radioclick" value="'.$checkList.'" >';
                                                }
                                          }

                                          if($value->fieldtype == 'text'){
                                             $fieldsetdata .='<input type="'.$value->fieldtype.'" class="form-control" placeholder="" name="product_subcat_fields['.$inputfieldname.']" >';
                                          }
                                          if($value->fieldtype == 'text-area'){
                                             $fieldsetdata .='<textarea class="form-control" name="product_subcat_fields['.$inputfieldname.']" ></textarea>';
                                          }
                                        $fieldsetdata .='</div></div>';
                                     
                 }         
//echo $request->subcategory_id;
                    $featureLists=  DB::table('category_features')->where('cat_id','=',$prod->subcategory_id)->get();
                     $VendorList=  DB::table('users')->where('status','=','2')->get();
                             $product_name=''; 
                             $productList = array();
                   
        //dd($fieldsetdata);
                return view('admin.product.create.add',compact('cats','sign','subcats','fieldsetdata','featureLists','brands','countries','VendorList','selectedTab','prod','variation_count','subcats','catId','product_name','productList','subcatname'));
        
    }


     ///// create producut varient ////////////////////
  public function createProSVarient($lastid){


        $prod = Product::find($lastid);
        $cats = Category::where('id','=',$prod->category_id)->first();

        $sign = Currency::where('is_default','=',1)->first();
        $subcats = Subcategory::where('id','=',$prod->subcategory_id)->first();
        //dd($subcats);
        $subcatfields = DB::table('category_fields')->where('cat_id','=',$prod->subcategory_id)->get();
        $brands = DB::table('brand')->get();
        $countries = DB::table('countries')->get();
        $subcatname = DB::table('subcategories')
                    ->select('name')
                    ->where('id','=',$prod->subcategory_id)->first();
        $variation_count = '';
        $selectedTab = "Svarient" ;
        //dd($subcatfields);
        $fieldsetdata ='';
         $subcats = $prod->subcategory_id;
        $catId = $prod->category_id;
    //     echo "<pre>";
    // print_r($catId);
    // die();
         
               foreach($subcatfields as $key=>$value){

                          $inputfieldname = str_replace(" ", "_", $value->fieldname);

                            $fieldsetdata .= '
                                          <div class="row">
                                            <div class="col-lg-4">
                                                        <div class="left-area">
                                                                <h4 class="heading">'.$value->fieldname.' </h4>
                                                                <p class="sub-heading">(In Any Language)</p>
                                                        </div>
                                            </div>
                                            <div class="col-lg-7">';
                                          if($value->fieldtype == 'dropdown'){
                                            $optionArr = explode(",",str_replace(", ",",",$value->fieldvalue)); 
                                            $fieldsetdata .='<select name="product_subcat_fields['.$value->fieldname.']">';
                                             $fieldsetdata .='<option >Select '.$value->fieldname.'</option>';
                                                foreach($optionArr as $options){
                                                    $fieldsetdata .='<option value="'.$options.'"> '.$options.' </option>';
                                                }
                                                $fieldsetdata .='</select>';
                                        
                                          }if($value->fieldtype == 'checkbox'){
                                                 $checkListArr = explode(",",$value->fieldvalue); 
                                             foreach($checkListArr as $checkList){
                                                
                                                 $fieldsetdata .= $checkList.' <input type="'.$value->fieldtype.'"  placeholder="" name="product_subcat_fields['.$inputfieldname.']" class="checkclick" value="'.$checkList.'" >';
                                                }
                                          }if($value->fieldtype == 'radio'){
                                                 $checkListArr = explode(",",$value->fieldvalue); 
                                             foreach($checkListArr as $checkList){
                                                
                                                 $fieldsetdata .= $checkList.' <input type="'.$value->fieldtype.'"  placeholder="" name="product_subcat_fields['.$inputfieldname.']" class="radioclick" value="'.$checkList.'" >';
                                                }
                                          }

                                          if($value->fieldtype == 'text'){
                                             $fieldsetdata .='<input type="'.$value->fieldtype.'" class="form-control" placeholder="" name="product_subcat_fields['.$inputfieldname.']" >';
                                          }
                                          if($value->fieldtype == 'text-area'){
                                             $fieldsetdata .='<textarea class="form-control" name="product_subcat_fields['.$inputfieldname.']" ></textarea>';
                                          }
                                        $fieldsetdata .='</div></div>';
                                     
                 }         
//echo $request->subcategory_id;
                    $featureLists=  DB::table('category_features')->where('cat_id','=',$prod->subcategory_id)->get();
                     $VendorList=  DB::table('users')->where('status','=','2')->get();
                             $product_name=''; 
                             $productList = array();
                   
        //dd($fieldsetdata);
                return view('admin.product.create.add',compact('cats','sign','subcats','fieldsetdata','featureLists','brands','countries','VendorList','selectedTab','prod','variation_count','subcats','catId','product_name','productList','subcatname'));
        
    }

        ///// create producut Implementation varient ////////////////////
  public function createProIVarient($lastid){


        $prod = Product::find($lastid);
        $cats = Category::where('id','=',$prod->category_id)->first();

        $sign = Currency::where('is_default','=',1)->first();
        $subcats = Subcategory::where('id','=',$prod->subcategory_id)->first();
        //dd($subcats);
        $subcatfields = DB::table('category_fields')->where('cat_id','=',$prod->subcategory_id)->get();
        $brands = DB::table('brand')->get();
        $countries = DB::table('countries')->get();
        $subcatname = DB::table('subcategories')
                    ->select('name')
                    ->where('id','=',$prod->subcategory_id)->first();
        $variation_count = '';
        $selectedTab = "Ivarient" ;
        //dd($subcatfields);
        $fieldsetdata ='';
         $subcats = $prod->subcategory_id;
        $catId = $prod->category_id;
    //     echo "<pre>";
    // print_r($catId);
    // die();
         
               foreach($subcatfields as $key=>$value){

                          $inputfieldname = str_replace(" ", "_", $value->fieldname);

                            $fieldsetdata .= '
                                          <div class="row">
                                            <div class="col-lg-4">
                                                        <div class="left-area">
                                                                <h4 class="heading">'.$value->fieldname.' </h4>
                                                                <p class="sub-heading">(In Any Language)</p>
                                                        </div>
                                            </div>
                                            <div class="col-lg-7">';
                                          if($value->fieldtype == 'dropdown'){
                                            $optionArr = explode(",",str_replace(", ",",",$value->fieldvalue)); 
                                            $fieldsetdata .='<select name="product_subcat_fields['.$value->fieldname.']">';
                                             $fieldsetdata .='<option >Select '.$value->fieldname.'</option>';
                                                foreach($optionArr as $options){
                                                    $fieldsetdata .='<option value="'.$options.'"> '.$options.' </option>';
                                                }
                                                $fieldsetdata .='</select>';
                                        
                                          }if($value->fieldtype == 'checkbox'){
                                                 $checkListArr = explode(",",$value->fieldvalue); 
                                             foreach($checkListArr as $checkList){
                                                
                                                 $fieldsetdata .= $checkList.' <input type="'.$value->fieldtype.'"  placeholder="" name="product_subcat_fields['.$inputfieldname.']" class="checkclick" value="'.$checkList.'" >';
                                                }
                                          }if($value->fieldtype == 'radio'){
                                                 $checkListArr = explode(",",$value->fieldvalue); 
                                             foreach($checkListArr as $checkList){
                                                
                                                 $fieldsetdata .= $checkList.' <input type="'.$value->fieldtype.'"  placeholder="" name="product_subcat_fields['.$inputfieldname.']" class="radioclick" value="'.$checkList.'" >';
                                                }
                                          }

                                          if($value->fieldtype == 'text'){
                                             $fieldsetdata .='<input type="'.$value->fieldtype.'" class="form-control" placeholder="" name="product_subcat_fields['.$inputfieldname.']" >';
                                          }
                                          if($value->fieldtype == 'text-area'){
                                             $fieldsetdata .='<textarea class="form-control" name="product_subcat_fields['.$inputfieldname.']" ></textarea>';
                                          }
                                        $fieldsetdata .='</div></div>';
                                     
                 }         
//echo $request->subcategory_id;
                    $featureLists=  DB::table('category_features')->where('cat_id','=',$prod->subcategory_id)->get();
                     $VendorList=  DB::table('users')->where('status','=','2')->get();
                             $product_name=''; 
                             $productList = array();
                   
        //dd($fieldsetdata);
                return view('admin.product.create.add',compact('cats','sign','subcats','fieldsetdata','featureLists','brands','countries','VendorList','selectedTab','prod','variation_count','subcats','catId','product_name','productList','subcatname'));
        
    }



    public function storeProOverview(Request $request)
    {
              
       // $data = new Product;
        $input = $request->all();


          // dd($input['product_subcat_fields']);
        //////insert data in product fieldset table
        //$fieldvalue = '';
       
        foreach($input['product_subcat_fields'] as $key=>$val){
          $val123=str_replace("\n", "<<ENTER>>", $val);
            if(is_array($val123)){
                //foreach ($val as $key1 => $value1) {
                    $fieldvalue = implode(",",$val123);
                //}
            }else{
                 $fieldvalue = $val123;
            }
// dd($fieldvalue);
          $checkexist = DB::table('product_fields')
                          ->where('cat_id','=',$input['subcat_id'])
                          ->where('product_id','=',$input['prod_id'])
                           ->where('fieldname','=',$key)
                          ->where('fieldvalue','=',$fieldvalue)->first();
          if(!empty($checkexist)){
             $delfields= DB::table('product_fields')->where('product_id','=',$input['prod_id'])->delete();
          
          }
                      
          $fields=array('cat_id'=>$input['subcat_id'],'product_id'=>$input['prod_id'],'fieldname'=>str_replace(" ","_",$key),'fieldvalue'=>$fieldvalue);
              $insfields= DB::table('product_fields')->insert($fields);
          
        } 

          $msg = 'New Product Added Successfully.';
          return redirect()->route('admin-prod-create-feature',$input['prod_id']);
    }
 
    public function storeVarient(Request $request)
    {
        $input = $request->all();         
            // echo "<pre>";
            // print_r($input['variationCount']);//
            // die();
        //////insert data in product fieldset table
        $product = DB::table('products')->select('product_type','category_id')->where('id','=',$input['prod_id'])->first();
        //dd($input['variant_name']);
        foreach($input['variant_name'] as $key=>$val){
            $varient2 ='';
            $varient3='';

          if(!empty($input['varient2'][$key]) ){
            $varient2 = $input['varient2'][$key];
          }if(!empty($input['varient3'][$key]) ){
            $varient3 = $input['varient3'][$key];
          }
         $checkexist = DB::table('product_varient')
                      ->where('product_id','=',$input['prod_id'])
                      ->where('varient_name','=',$val)
                      ->where('varient1','=',$input['varient1'][$key])
                      ->where('varient2','=',$varient2)
                      ->where('varient3','=',$varient3)
                      ->where('variation_count','=',$input['variationCount'])->first();

         if(!empty($checkexist)){
                  $delFields= DB::table('product_varient')->where('product_id','=',$input['prod_id'])->delete();
       
         }
          $fields=array('product_id'=>$input['prod_id'],'varient_name'=>$val,'varient1'=>$input['varient1'][$key],'varient2'=>$varient2,'varient3'=>$varient3,'variation_count'=>$input['variationCount']);
      
          $insfields= DB::table('product_varient')->insert($fields);
        } 

          $msg = 'New Product Added Successfully.';
        // echo "<pre>";
        // print_r($product->category_id);
        // die();

        //return redirect()->route('admin-prod-index');
          if($product->category_id == '23')
          {
            return redirect()->route('admin-prod-index');
          }
          else
          {
            return redirect()->route('view-pricing',$input['prod_id']);
          }
          
    }

////////////////store support varient///////////////////////////
     public function storeSVarient(Request $request)
    {
        $input = $request->all();         
         foreach($input['variant_name_support'] as $key=>$val){

            $varient2 ='';
            $varient3='';

          if(!empty($input['s_varient2'][$key]) ){
            $varient2 = $input['s_varient2'][$key];
          }if(!empty($input['s_varient3'][$key]) ){
            $varient3 = $input['s_varient3'][$key];
          }
           $checkexist = DB::table('support_varient')
                      ->where('product_id','=',$input['prod_id'])
                      ->where('varient_name','=',$val)
                      ->where('varient1','=',$input['s_varient1'][$key])
                      ->where('varient2','=',$varient2)
                      ->where('varient3','=',$varient3)
                      ->where('variation_count','=',$input['variationCount'])->first();

         if(!empty($checkexist)){
                  $delFields= DB::table('support_varient')->where('product_id','=',$input['prod_id'])->delete();
       
         }
          $fields=array('product_id'=>$input['prod_id'],'varient_name'=>$val,'varient1'=>$input['s_varient1'][$key],'varient2'=>$varient2,
            'varient3'=>$varient3,'variation_count'=>$input['variationCount']);
      
          $insfields= DB::table('support_varient')->insert($fields);
        } 

          $msg = 'New Product Added Successfully.';
      
        return redirect()->route('admin-prod-index');
          
           
    }

////////////////store implementation varients///////////////////////
     public function storeIVarient(Request $request)
    {
       $input = $request->all();         
       $product = DB::table('products')->where('id','=',$input['prod_id'])->first();
        foreach($input['variant_name'] as $key=>$val){
           $varient2 ='';
            $varient3='';

          if(!empty($input['i_varient2'][$key]) ){
            $varient2 = $input['i_varient2'][$key];
          }if(!empty($input['i_varient3'][$key]) ){
            $varient3 = $input['i_varient3'][$key];
          }

           $checkexist = DB::table('implementation_varient')
                      ->where('product_id','=',$input['prod_id'])
                      ->where('varient_name','=',$val)
                      ->where('varient1','=',$input['i_varient1'][$key])
                      ->where('varient2','=',$varient2)
                      ->where('varient3','=',$varient3)
                      ->where('variation_count','=',$input['variationCount'])->first();

         if(!empty($checkexist)){
                  $delFields= DB::table('implementation_varient')->where('product_id','=',$input['prod_id'])->delete();
       
         }
          $fields=array('product_id'=>$input['prod_id'],'varient_name'=>$val,'varient1'=>$input['i_varient1'][$key],'varient2'=>$varient2,
            'varient3'=>$varient3,'variation_count'=>$input['variationCount']);
      
          $insfields= DB::table('implementation_varient')->insert($fields);
        } 

        if($product->product_support == 'yes'){
            return redirect()->route('admin-prod-create-Svarient',$input['prod_id']);
        }else{
            return redirect()->route('admin-prod-index');
        
        }
        
          
    }


    public function createProFeature($lastid){
         $prod = Product::find($lastid);
        //echo"ewewe"; die;
     //dd($prod);
    // dd($request->category_id  if(empty($request)) {
        $cats = Category::where('id','=',$prod->category_id)->first();

        $sign = Currency::where('is_default','=',1)->first();
        $subcats = Subcategory::where('id','=',$prod->subcategory_id)->first();
        //dd($subcats);
        $subcatfields = DB::table('category_fields')->where('cat_id','=',$prod->subcategory_id)->get();
        $brands = DB::table('brand')->get();
        $countries = DB::table('countries')->get();
        $subcatname = DB::table('subcategories')
                    ->select('name')
                    ->where('id','=',$prod->subcategory_id)->first();
        $variation_count = '';
        $selectedTab = "feature" ;
        //dd($subcatfields);
        $fieldsetdata ='';
        $subcats = $prod->subcategory_id;
        $catId = $prod->category_id;
               foreach($subcatfields as $key=>$value){

                          $inputfieldname = str_replace(" ", "_", $value->fieldname);

                            $fieldsetdata .= '
                                          <div class="row">
                                            <div class="col-lg-3">
                                                        <div class="left-area">
                                                                <h4 class="heading">'.$value->fieldname.' </h4>
                                                                <p class="sub-heading">(In Any Language)</p>
                                                        </div>
                                            </div>
                                            <div class="col-lg-8">';
                                          if($value->fieldtype == 'dropdown'){
                                            $optionArr = explode(",",str_replace(", ",",",$value->fieldvalue)); 
                                            $fieldsetdata .='<select class="form-control" name="product_subcat_fields['.$value->fieldname.']">';
                                             $fieldsetdata .='<option >Select '.$value->fieldname.'</option>';
                                                foreach($optionArr as $options){
                                                    $fieldsetdata .='<option value="'.$options.'"> '.$options.' </option>';
                                                }
                                                $fieldsetdata .='</select>';
                                        
                                          }if($value->fieldtype == 'checkbox'){
                                            $i = 0;
                                             $checkListArr = explode(",",$value->fieldvalue); 
                                             foreach($checkListArr as $checkList){
                                                
                                                //  $fieldsetdata .= $checkList.' <input type="'.$value->fieldtype.'"  placeholder="" name="product_subcat_fields['.$inputfieldname.']" class="checkclick" value="'.$checkList.'" >';
                                                // }



                                                $fieldsetdata .= '<label class="checkbox checkbox-primary checkbox-inline-block" for="product_subcat_fields['.$inputfieldname.']'.'_'.$i.'"> <input type="'.$value->fieldtype.'"  placeholder="" name="product_subcat_fields['.$inputfieldname.']" id="product_subcat_fields['.$inputfieldname.']'.'_'.$i.'" class="checkclick" value="'.$checkList.'" ><span>'.$checkList.'</span><span class="checkmark"></span></label>';
                                                 $i++;
                                                }


                                          }if($value->fieldtype == 'radio'){
                                                 $checkListArr = explode(",",$value->fieldvalue); 
                                             foreach($checkListArr as $checkList){
                                                $i = 0;
                                                // $fieldsetdata .= $checkList.' <input type="'.$value->fieldtype.'"  placeholder="" name="product_subcat_fields['.$inputfieldname.']" class="radioclick" value="'.$checkList.'" >';
                                                // }

                                                $fieldsetdata .= '<label class="radio radio-primary radio-inline-block" for="'.$checkList.'_'.$i.'"> <input type="'.$value->fieldtype.'"  placeholder="" name="product_subcat_fields['.$inputfieldname.']" id="'.$checkList.'_'.$i.'" class="radioclick" value="'.$checkList.'" ><span>'.$checkList.'</span><span class="checkmark"></span></label>';
                                                 $i++;
                                                }
                                          }

                                          if($value->fieldtype == 'text'){
                                             $fieldsetdata .='<input type="'.$value->fieldtype.'" class="form-control form-control" placeholder="" name="product_subcat_fields['.$inputfieldname.']" >';
                                          }
                                          if($value->fieldtype == 'text-area'){
                                             $fieldsetdata .='<textarea class="form-control" name="product_subcat_fields['.$inputfieldname.']" ></textarea>';
                                          }
                                        $fieldsetdata .='</div></div>';
                                     
                 }         
//echo $request->subcategory_id;
                    $featureLists=  DB::table('category_features')->where('cat_id','=',$prod->subcategory_id)->get();
                     $VendorList=  DB::table('users')->where('status','=','2')->get();
                             $product_name='';
                             $productList= array();
                   
        //dd($fieldsetdata);
                return view('admin.product.create.add',compact('cats','sign','subcats','fieldsetdata','featureLists','brands','countries','VendorList','selectedTab','prod','variation_count','subcats','catId','product_name','productList','subcatname'));
        
    }

    

    

   
     public function edit($id)
    {
        $cats = Category::all();
        $data = Product::findOrFail($id);
        $sign = Currency::where('is_default','=',1)->first();
         $brands = DB::table('brand')->get();
        $countries = DB::table('countries')->get();
        $selectedTab = "genral" ;
         $featureLists=  DB::table('category_features')->get();
        $VendorList=  DB::table('users')->where('status','=','2')->get();
        $product_fixed_price=DB::table('fixed_price')->where('productId','=',$id)->first();
        $product_user_slab=DB::table('user_slab_price')->where('productId','=',$id)->get();
        $product_modules=DB::table('module_price')->where('productId','=',$id)->get();
        $subcatfields = DB::table('category_fields')->where('cat_id','=',$data->subcategory_id)->get();
        $prodfield = DB::table('product_fields')->where('product_id','=',$id)->get();
        $prodlist = DB::table('products')
                    ->select('name')
                    ->where('category_id','=','22')->get();
        $subcatname = DB::table('subcategories')
                    ->select('name')
                    ->where('id','=',$data->subcategory_id)->first();
                   
                    
        /*if (count($product_fixed_price)==0) {
          echo "YES";
        }
        echo "<pre>";
        print_r($product_fixed_price);
        print_r($product_user_slab);
        print_r($product_modules);
        die;*/
        //$prodfield2 = DB::table('product_fields')->where('product_id','=',$id)->get();
        /*dd($prodfield);*/
       $prfArray = array();
      $testarray = array();
      
        $fieldsetdata ='';
               foreach($subcatfields as $key=>$value){
                $flag = 0;
                $inputfieldname = str_replace(" ", "_", $value->fieldname);
                if (!count($prodfield)==0) {
                foreach ($prodfield as $key => $prodvalues) {
                  if ($inputfieldname == $prodvalues->fieldname) {
                    
                        $flag = 1;
                        $fieldsetdata .= '
                                          <div class="row">
                                            <div class="col-lg-3">
                                                        <div class="left-area">
                                                                <h4 class="heading">'.$value->fieldname.' </h4>

                                                                <p class="sub-heading">(In Any Language)</p>
                                                        </div>
                                            </div>
                                            <div class="col-lg-8">';

                                          if($value->fieldtype == 'Multiselect'){
                                            
                                            $fieldsetdata .="<div id='output'></div>";


                                            $optionArr = explode(",",str_replace(", ",",",$value->fieldvalue));
                                            $fieldsetdata .='<select data-placeholder="Choose tags ..." id="product_subcat_fields['.$value->fieldname.']" multiple class="chosen-select form-control">';
                                                if($prodfield[$key]){
                                                  $selectedvalues = explode(",", str_replace(", ", ",", $prodfield[$key]->fieldvalue));                                                  
                                                }
                                                 foreach($optionArr as $options){
                                                  
                                                 if(!empty( $prodfield[$key]) && in_array($options,$selectedvalues)){
                                                    $fieldsetdata .='<option value="'.$options.'" selected="selected"> '.$options.' </option>';
                                                 }else{
                                                      $fieldsetdata .='<option value="'.$options.'" > '.$options.' </option>';
                                                 
                                                 }
                                                }
                                                $fieldsetdata .=' </select>';
                                                $fieldsetdata .=' <input type="hidden" name="product_subcat_fields['.$value->fieldname.']" value="'.$prodfield[$key]->fieldvalue.'">';
                                        
                                          }

                                         if($value->fieldtype == 'dropdown'){
                                            $optionArr = explode(",",str_replace(", ",",",$value->fieldvalue)); 
                                            $fieldsetdata .='<select class="form-control" name="product_subcat_fields['.$value->fieldname.']">';
                                             $fieldsetdata .='<option >Select '.$value->fieldname.'</option>';
                                                foreach($optionArr as $options){
                                                 if(!empty( $prodfield[$key]) && $prodfield[$key]->fieldvalue==$options){
                                                    $fieldsetdata .='<option value="'.$options.'" selected="selected"> '.$options.' </option>';
                                                 }else{
                                                      $fieldsetdata .='<option value="'.$options.'" > '.$options.' </option>';
                                                 
                                                 }
                                                }
                                                $fieldsetdata .='</select>';
                                        
                                          }if($value->fieldtype == 'checkbox'){
                                                 $checkListArr = explode(",",str_replace(", ",",",$value->fieldvalue)); 
                                            
                                                  if(!empty( $prodfield[$key]) ){
                                                  $prfArray  = explode(",",str_replace(", ",",",$prodfield[$key]->fieldvalue)); 
                                                  $notselected = array_diff($checkListArr, $prfArray);
                                                  }
                                                  foreach ($checkListArr as $checkList) {
                                                      if (!empty($prodfield[$key]) && in_array($checkList, $prfArray)) {
                                                        $fieldsetdata .= ' <label class="checkbox checkbox-primary checkbox-inline-block"><input type="'.$value->fieldtype.'" name="product_subcat_fields['.$inputfieldname.'][]" value="'.$checkList.'" placeholder="" checked class="checkclick"><span>'.$checkList.'</span><span class="checkmark"></span></label>';
                                                      }
                                                      else{
                                                        $fieldsetdata .= ' <label class="checkbox checkbox-primary checkbox-inline-block"><input type="'.$value->fieldtype.'" name="product_subcat_fields['.$inputfieldname.'][]" value="'.$checkList.'" placeholder=""  class="checkclick" ><span>'.$checkList.'</span><span class="checkmark"></span></label>';
                                                      }
                                                    }  
                                                //dd(array_diff($prfArray,$notselected));
                                               
                                          }if($value->fieldtype == 'radio'){
                                                 $checkListArr1 = explode(",",str_replace(", ",",",$value->fieldvalue)); 
                                             foreach($checkListArr1 as $checkList1){
                                                  if(!empty( $prodfield[$key]) && $prodfield[$key]->fieldvalue == $checkList1){
                                              
                                                     $fieldsetdata .= ' <label class="radio radio-primary radio-inline-block"><input type="'.$value->fieldtype.'"  placeholder="" name="product_subcat_fields['.$inputfieldname.'][]" checked class="radioclick" value="'.$checkList1.'" ><span>'.$checkList1.'</span><span class="checkmark"></span></label>';
                                                    }else{

                                                          $fieldsetdata .= ' <label class="radio radio-primary radio-inline-block"><input type="'.$value->fieldtype.'"  placeholder="" name="product_subcat_fields['.$inputfieldname.'][]" class="radioclick" value="'.$checkList1.'" ><span>'.$checkList1.'</span><span class="checkmark"></span></label>';
                                                  
                                                    }
                                                }
                                          }
                                          
                                          if($value->fieldtype == 'text'){
                                             if(!empty( $prodfield[$key]) ){
                                               
                                             $fieldsetdata .='<input type="'.$value->fieldtype.'" class="form-control" placeholder="" name="product_subcat_fields['.$inputfieldname.']" value="'.$prodfield[$key]->fieldvalue.'" >';
                                            }
                                            
                                            else{
                                                  $fieldsetdata .='<input type="'.$value->fieldtype.'" class="form-control" placeholder="" name="product_subcat_fields['.$inputfieldname.']" >';
                                         
                                            }
                                          }

                                          if($value->fieldtype == 'text-area'){
                                             if(!empty( $prodfield[$key]) ){
                                               
                                             $fieldsetdata .='<textarea class="form-control" rows="3" name="product_subcat_fields['.$inputfieldname.']"> '.str_replace("<<ENTER>>","\n",$prodfield[$key]->fieldvalue).'</textarea>';
                                            }
                                            
                                            else{
                                                  $fieldsetdata .='<textarea class="form-control" rows="3" name="product_subcat_fields['.$inputfieldname.']"> '.$prodfield[$key]->fieldvalue.'</textarea>';
                                         
                                            }
                                          }
                                        $fieldsetdata .='</div></div>';

                                   
  
                                     
                      }
                     } 
                    }
                    if ($flag == 0)
                    {
                     $fieldsetdata .= '
                                          <div class="row">
                                            <div class="col-lg-3">
                                                        <div class="left-area">
                                                                <h4 class="heading">'.$value->fieldname.' </h4>

                                                                <p class="sub-heading">(In Any Language)</p>
                                                        </div>
                                            </div>
                                            <div class="col-lg-8">';

                                          if($value->fieldtype == 'Multiselect'){
                                            
                                            $fieldsetdata .="<div id='output'></div>";


                                            $optionArr = explode(",",str_replace(", ",",",$value->fieldvalue));
                                             $fieldsetdata .='<select data-placeholder="Choose tags ..." id="product_subcat_fields['.$value->fieldname.']" multiple class="chosen-select form-control">';

                                                 foreach($optionArr as $options){
                                                  
                                                 
                                                      $fieldsetdata .='<option value="'.$options.'" > '.$options.' </option>';
                                                 
                                                 
                                                }
                                                $fieldsetdata .=' </select>';
                                                $fieldsetdata .=' <input type="hidden" name="product_subcat_fields['.$value->fieldname.']" value="'.$prodfield[$key]->fieldvalue.'">';
                                        
                                          }

                                         if($value->fieldtype == 'dropdown'){
                                            $optionArr = explode(",",str_replace(", ",",",$value->fieldvalue)); 
                                            $fieldsetdata .='<select class="form-control" name="product_subcat_fields['.$value->fieldname.']">';
                                             $fieldsetdata .='<option >Select '.$value->fieldname.'</option>';
                                                foreach($optionArr as $options){
                                                 
                                                      $fieldsetdata .='<option value="'.$options.'" > '.$options.' </option>';
                                                 
                                                 
                                                }
                                                $fieldsetdata .='</select>';
                                        
                                          }if($value->fieldtype == 'checkbox'){
                                                 $checkListArr = explode(",",str_replace(", ",",",$value->fieldvalue)); 
                                                  foreach($checkListArr as $checkList1){
                                                      
                                                         
                                                            $fieldsetdata .= '<label class="checkbox checkbox-primary checkbox-inline-block"><input type="'.$value->fieldtype.'"  placeholder="" name="product_subcat_fields['.$inputfieldname.'][]"  class="checkclick" value="'.$checkList1.'" ><span>'.$checkList1.'</span><span class="checkmark"></span></label>';
                                               
                                                      
                                                
                                                }
                                          }if($value->fieldtype == 'radio'){
                                                 $checkListArr1 = explode(",",str_replace(", ",",",$value->fieldvalue)); 
                                             foreach($checkListArr1 as $checkList1){
                                                  

                                                          $fieldsetdata .= ' <label class="radio radio-primary radio-inline-block"><input type="'.$value->fieldtype.'"  placeholder="" name="product_subcat_fields['.$inputfieldname.'][]"  class="radioclick" value="'.$checkList1.'" ><span>'.$checkList1.'</span><span class="checkmark"></span></label>';
                                                    
                                                  
                                                    
                                                }
                                          }
                                          
                                          if($value->fieldtype == 'text'){
                                             
                                                  $fieldsetdata .='<input type="'.$value->fieldtype.'" class="form-control" placeholder="" name="product_subcat_fields['.$inputfieldname.']" >';
                                         
                                            
                                          }

                                          if($value->fieldtype == 'text-area'){
                                             
                                                  $fieldsetdata .='<textarea class="form-control" rows="3" name="product_subcat_fields['.$inputfieldname.']"> '.$prodfield[$key]->fieldvalue.'</textarea>';
                                         
                                            
                                          }
                                        $fieldsetdata .='</div></div>';
                    }
                 }     // dd($checkListArr);

                //cho     $fieldsetdata;die; 
                  $feature =   DB::table('product_features_list')->where('product_id','=',$id)->first();
                                    if(!empty($feature)){
                                    $featureId = $feature->feature_id;
                                    }
                                    $moduleData =   DB::table('product_features_list')->groupBy('module_name')
                                                    ->where('product_id','=',$id)->get();
                                     //$product_varient = array();
                                     $funcData =   DB::table('product_features_list')->select('function_value')->where('product_id','=',$id)->get();
                                     $product_varient =   DB::table('product_varient')->where('product_id','=',$id)->get();
                                    $varientcount =   DB::table('product_varient')->select('variation_count')->where('product_id','=',$id)->first();
                                    
                                     $product_i_varient =   DB::table('implementation_varient')->where('product_id','=',$id)->get();
                                     $ivarientcount =   DB::table('implementation_varient')->select('variation_count')->where('product_id','=',$id)->first();
                                     
                                     $product_s_varient =   DB::table('support_varient')->where('product_id','=',$id)->get();
                                     $svarientcount =   DB::table('support_varient')->select('variation_count')->where('product_id','=',$id)->first();
                                     
                                           
               $selectedTab ='genral';
       // dd($funcData);
        return view('admin.product.edit.editProduct',compact('cats','data','sign','brands','countries','selectedTab','featureLists','VendorList','fieldsetdata','moduleData','featureId','funcData','product_i_varient','product_s_varient','product_varient','varientcount','ivarientcount','svarientcount','selectedTab','id','product_fixed_price','product_user_slab','product_modules','prodlist','subcatname'));
    }
public function editOtherInfo($tab,$id)
{
  $cats = Category::all();
  $data = Product::findOrFail($id);
  $sign = Currency::where('is_default','=',1)->first();
  $brands = DB::table('brand')->get();
  $countries = DB::table('countries')->get();
  $selectedTab = "feature" ;
  $featureLists=  DB::table('category_features')->get();
  $VendorList=  DB::table('users')->where('status','=','2')->get();
  $subcatfields = DB::table('category_fields')->where('cat_id','=',$data->subcategory_id)->get();
  $prodfield = DB::table('product_fields')->where('product_id','=',$id)->get();
  $product_fixed_price=DB::table('fixed_price')->where('productId','=',$id)->first();
  $product_user_slab=DB::table('user_slab_price')->where('productId','=',$id)->get();
  $product_modules=DB::table('module_price')->where('productId','=',$id)->get();
  $prodlist = DB::table('products')
                    ->select('name')
                    ->where('category_id','=','22')->get();
  $selectedTab = $tab;
  $fieldsetdata ='';
  foreach($subcatfields as $key=>$value){
    $flag = 0;
    $inputfieldname = str_replace(" ", "_", $value->fieldname);
    if (!count($prodfield)==0) {
    foreach ($prodfield as $key => $prodvalues) {
      if ($inputfieldname == $prodvalues->fieldname) {
        $flag = 1;
        $fieldsetdata .= '
          <div class="row">
            <div class="col-lg-3">
              <div class="left-area">
                <h4 class="heading">'.$value->fieldname.' </h4>
                <p class="sub-heading">(In Any Language)</p>
              </div>
            </div>
            <div class="col-lg-8">';
            if($value->fieldtype == 'Multiselect'){
              $fieldsetdata .="<div id='output'></div>";
              $optionArr = explode(",",str_replace(", ",",",$value->fieldvalue));
              $fieldsetdata .='<select data-placeholder="Choose tags ..." id="'.$value->fieldname.'" name="product_subcat_fields['.$value->fieldname.']" multiple class="chosen-select form-control">';
              if($prodfield[$key]){
                $selectedvalues = explode(",", str_replace(", ", ",", $prodfield[$key]->fieldvalue));
              }
              foreach($optionArr as $options){
                if(!empty( $prodfield[$key]) && in_array($options, $selectedvalues)){
                  $fieldsetdata .='<option value="'.$options.'" selected="selected"> '.$options.' </option>';
                  }
                else{
                  $fieldsetdata .='<option value="'.$options.'" > '.$options.' </option>';
                }
              }
              $fieldsetdata .=' </select>';
              $fieldsetdata .=' <input type="hidden" name="product_subcat_fields['.$value->fieldname.']" value="'.$prodfield[$key]->fieldvalue.'">';
            }
            if($value->fieldtype == 'dropdown'){
              $optionArr = explode(",",str_replace(", ",",",$value->fieldvalue)); 
              $fieldsetdata .='<select class="form-control" name="product_subcat_fields['.$value->fieldname.']">';
              $fieldsetdata .='<option >Select '.$value->fieldname.'</option>';
              foreach($optionArr as $options){
                if(!empty( $prodfield[$key]) && $prodfield[$key]->fieldvalue==$options){
                  $fieldsetdata .='<option value="'.$options.'" selected="selected"> '.$options.' </option>';
                }
                else{
                  $fieldsetdata .='<option value="'.$options.'" > '.$options.' </option>';
                }
              }
              $fieldsetdata .='</select>';
            }
            if($value->fieldtype == 'checkbox')
            {
              $checkListArr = explode(",",str_replace(", ",",",$value->fieldvalue)); 
              if(!empty( $prodfield[$key]) ){
                $prfArray  = explode(",",str_replace(", ",",",$prodfield[$key]->fieldvalue)); 
                $notselected = array_diff($checkListArr, $prfArray);
              }  
              foreach ($checkListArr as $checkList) {
                if(!empty( $prodfield[$key] && in_array($checkList, $prfArray)) ){
                  $fieldsetdata .= ' <label class="checkbox checkbox-primary checkbox-inline-block"><input type="'.$value->fieldtype.'" name="product_subcat_fields['.$inputfieldname.'][]" value="'.$checkList.'" placeholder="" checked class="checkclick"><span>'.$checkList.'</span><span class="checkmark"></span></label>';
                }
                else{
                  $fieldsetdata .= ' <label class="checkbox checkbox-primary checkbox-inline-block"><input type="'.$value->fieldtype.'" name="product_subcat_fields['.$inputfieldname.'][]" value="'.$checkList.'" placeholder="" class="checkclick"><span>'.$checkList.'</span><span class="checkmark"></span></label>';
                }
              }
            }
            if($value->fieldtype == 'radio'){
                                                 $checkListArr1 = explode(",",str_replace(", ",",",$value->fieldvalue)); 
                                             foreach($checkListArr1 as $checkList1){
                                                  
                                                  if(!empty( $prodfield[$key]) && $prodfield[$key]->fieldvalue == $checkList1){
                                              
                                                     $fieldsetdata .= ' <label class="radio radio-primary radio-inline-block"><input type="'.$value->fieldtype.'"  placeholder="" name="product_subcat_fields['.$inputfieldname.'][]" checked class="radioclick" value="'.$checkList1.'" ><span>'.$checkList1.'</span><span class="checkmark"></span></label>';
                                                    }else{

                                                          $fieldsetdata .= ' <label class="radio radio-primary radio-inline-block"><input type="'.$value->fieldtype.'"  placeholder="" name="product_subcat_fields['.$inputfieldname.'][]" class="radioclick" value="'.$checkList1.'" ><span>'.$checkList1.'</span><span class="checkmark"></span></label>';
                                                  
                                                    }
                                                }
                                          }
                                          
                                          if($value->fieldtype == 'text'){
                                             if(!empty( $prodfield[$key]) ){
                                               
                                             $fieldsetdata .='<input type="'.$value->fieldtype.'" class="form-control" placeholder="" name="product_subcat_fields['.$inputfieldname.']" value="'.$prodfield[$key]->fieldvalue.'" >';
                                            }
                                            
                                            else{
                                                  $fieldsetdata .='<input type="'.$value->fieldtype.'" class="form-control" placeholder="" name="product_subcat_fields['.$inputfieldname.']" >';
                                         
                                            }
                                          }

                                          if($value->fieldtype == 'text-area'){
                                             if(!empty( $prodfield[$key]) ){
                                               
                                             $fieldsetdata .='<textarea class="form-control" rows="3" name="product_subcat_fields['.$inputfieldname.']"> '.str_replace("<<ENTER>>","\n",$prodfield[$key]->fieldvalue).'</textarea>';
                                            }
                                            
                                            else{
                                                  $fieldsetdata .='<textarea class="form-control" rows="3" name="product_subcat_fields['.$inputfieldname.']"> '.$prodfield[$key]->fieldvalue.'</textarea>';
                                         
                                            }
                                          }
                                        $fieldsetdata .='</div></div>';

                                   
  
                                     
                      }
                      }
                    }
                    if ($flag == 0)
                    {
                     $fieldsetdata .= '
                                          <div class="row">
                                            <div class="col-lg-3">
                                                        <div class="left-area">
                                                                <h4 class="heading">'.$value->fieldname.' </h4>

                                                                <p class="sub-heading">(In Any Language)</p>
                                                        </div>
                                            </div>
                                            <div class="col-lg-8">';

                                          if($value->fieldtype == 'Multiselect'){
                                            
                                            $fieldsetdata .="<div id='output'></div>";


                                            $optionArr = explode(",",str_replace(", ",",",$value->fieldvalue));
                                             $fieldsetdata .='<select data-placeholder="Choose tags ..." id="'.$value->fieldname.'" name="product_subcat_fields['.$value->fieldname.']" multiple class="chosen-select form-control">';

                                                 foreach($optionArr as $options){
                                                  
                                                 
                                                      $fieldsetdata .='<option value="'.$options.'" > '.$options.' </option>';
                                                 
                                                 
                                                }
                                                $fieldsetdata .=' </select>';
                                                $fieldsetdata .=' <input type="hidden" name="product_subcat_fields['.$value->fieldname.']" value="'.$prodfield[$key]->fieldvalue.'">';
                                        
                                          }

                                         if($value->fieldtype == 'dropdown'){
                                            $optionArr = explode(",",str_replace(", ",",",$value->fieldvalue)); 
                                            $fieldsetdata .='<select class="form-control" name="product_subcat_fields['.$value->fieldname.']">';
                                             $fieldsetdata .='<option >Select '.$value->fieldname.'</option>';
                                                foreach($optionArr as $options){
                                                 
                                                      $fieldsetdata .='<option value="'.$options.'" > '.$options.' </option>';
                                                 
                                                 
                                                }
                                                $fieldsetdata .='</select>';
                                        
                                          }if($value->fieldtype == 'checkbox'){
                                                 $checkListArr = explode(",",str_replace(", ",",",$value->fieldvalue)); 
                                                  foreach($checkListArr as $checkList1){
                                                      
                                                         
                                                            $fieldsetdata .= '<label class="checkbox checkbox-primary checkbox-inline-block"><input type="'.$value->fieldtype.'"  placeholder="" name="product_subcat_fields['.$inputfieldname.'][]"  class="checkclick" value="'.$checkList1.'" ><span>'.$checkList1.'</span><span class="checkmark"></span></label>';
                                               
                                                      
                                                
                                                }
                                          }if($value->fieldtype == 'radio'){
                                                 $checkListArr1 = explode(",",str_replace(", ",",",$value->fieldvalue)); 
                                             foreach($checkListArr1 as $checkList1){
                                                  

                                                          $fieldsetdata .= ' <label class="radio radio-primary radio-inline-block"><input type="'.$value->fieldtype.'"  placeholder="" name="product_subcat_fields['.$inputfieldname.'][]"  class="radioclick" value="'.$checkList1.'" ><span>'.$checkList1.'</span><span class="checkmark"></span></label>';
                                                    
                                                  
                                                    
                                                }
                                          }
                                          
                                          if($value->fieldtype == 'text'){
                                             
                                                  $fieldsetdata .='<input type="'.$value->fieldtype.'" class="form-control" placeholder="" name="product_subcat_fields['.$inputfieldname.']" >';
                                         
                                            
                                          }

                                          if($value->fieldtype == 'text-area'){
                                             
                                                  $fieldsetdata .='<textarea class="form-control" rows="3" name="product_subcat_fields['.$inputfieldname.']"> '.$prodfield[$key]->fieldvalue.'</textarea>';
                                         
                                            
                                          }
                                        $fieldsetdata .='</div></div>';
                    }
                 }     // dd($checkListArr);

                //cho     $fieldsetdata;die; 
                  $feature =   DB::table('product_features_list')->where('product_id','=',$id)->first();
                                    if(!empty($feature)){
                                    $featureId = $feature->feature_id;
                                    }
                                    $moduleData =   DB::table('product_features_list')->groupBy('module_name')
                                                    ->where('product_id','=',$id)->get();
                        
                                     $funcData =   DB::table('product_features_list')->select('function_value')->where('product_id','=',$id)->get();
                                      $product_varient =   DB::table('product_varient')->where('product_id','=',$id)->get();
                                    $varientcount =   DB::table('product_varient')->select('variation_count')->where('product_id','=',$id)->first();
                                    
                                     $product_i_varient =   DB::table('implementation_varient')->where('product_id','=',$id)->get();
                                     $ivarientcount =   DB::table('implementation_varient')->select('variation_count')->where('product_id','=',$id)->first();
                                     
                                     $product_s_varient =   DB::table('support_varient')->where('product_id','=',$id)->get();
                                     $svarientcount =   DB::table('support_varient')->select('variation_count')->where('product_id','=',$id)->first();

       $subcatname = DB::table('subcategories')
                    ->select('name')
                    ->where('id','=',$data->subcategory_id)->first();
                                     
        return view('admin.product.edit.editProduct',compact('cats','data','sign','brands','countries','selectedTab','featureLists','VendorList','fieldsetdata','moduleData','featureId','funcData','product_varient','product_i_varient','product_s_varient','varientcount','ivarientcount','svarientcount','selectedTab','id','product_fixed_price','product_modules','product_user_slab','prodlist','subcatname'));
    }



    //*** POST Request
    public function updateGenralInfo(Request $request, $id)
    {

        //--- Validation Section
        $rules = [
               'file'       => 'mimes:zip',
               'upload_product_sheet' => 'mimes:pdf|max:10000'
                ];

        $validator = Validator::make(Input::all(), $rules);
        
        if ($validator->fails()) {
          return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        //--- Validation Section Ends


        //-- Logic Section
        $data = Product::findOrFail($id);
        $sign = Currency::where('is_default','=',1)->first();
        $input = $request->all();
        //dd($input);
        $image = $request->photo;
        $image1 = $request->photo2;
        if ($image != $image1) {
          
        list($type, $image) = explode(';', $image);
        list(, $image)      = explode(',', $image);
        $image = base64_decode($image);
        $image_name = time().str_random(8).'.png';
        $path = 'assets/images/products/'.$image_name;
        file_put_contents($path, $image);
        $input['photo'] = $image_name;
        }
        $img = Image::make(public_path().'/assets/images/products/'.$image_name)->resize(285, 285);
        $thumbnail = time().str_random(8).'.jpg';
        $img->save(public_path().'/assets/images/thumbnails/'.$thumbnail);
        $input['thumbnail']  = $thumbnail;
        


        
      
 // Check File
        if ($file = $request->file('file'))
        {
            $name = time().$file->getClientOriginalName();
            $file->move('assets/files',$name);
            $input['file'] = $name;
        }
 
        // Check Physical
       
            // Check Condition
                  // Check Seo
        if (empty($request->seo_check))
        {
            $input['meta_tag'] = null;
            $input['meta_description'] = null;
        }
        else {
            if (!empty($request->meta_tag))
            {
                $input['meta_tag'] = implode(',', $request->meta_tag);
            }
        }

    
        //tags
        if (!empty($request->tags))
        {
            $input['tags'] = implode(',', $request->tags);
        }
        if ($request->file('upload_product_sheet')) 
        {
            $upload_product_sheet = $request->file('upload_product_sheet');
            $upload_product_sheetName = time().'.'.$upload_product_sheet->getClientOriginalExtension();
            $destinationPath = public_path('assets/product_sheets');
            $upload_product_sheet->move($destinationPath, $upload_product_sheetName);

            $data->product_spec_sheet = $upload_product_sheetName;
        }
             $data->country_code = $input['country_code'];
             $data->brand_id = $input['brand_id'];
            
             $data->user_id = $input['vendor_id'];
             if (!empty($input['product_implementation'])) {
              $data->product_implementation = $input['product_implementation'];
             }
             else{
              $data->product_implementation = 'no';
             }
             if (!empty($input['product_support'])) {
              $data->product_support = $input['product_support'];
             }
             else{
              $data->product_support = 'no';
             }
             if (!empty($input['addon_products'])) {
              $data->addon_products=$input['addon_products'];
             }
             

         

         $data->update($input);
        //-- Logic Section Ends
       
          return redirect()->route('admin-prod-edit-otherInfo',['id'=>$id,'tab'=>'genral']);
        
        //--- Redirect Section Ends    
    }

     public function updateOverview(Request $request,$id)
    {
              
       // $data = new Product;
        $input = $request->all();
//dd($input['product_subcat_fields']);
        // echo "<pre>";
        // print_r($input);
        ///die();
        $data = Product::findOrFail($id);
        $deleteData = DB::table('product_fields')
                            ->where('product_id','=',$id)
                            ->delete();
        foreach($input['product_subcat_fields'] as $key=>$val){
            $val1=str_replace("\n", "<<ENTER>>", $val);
            if(is_array($val1)){
                //foreach ($val as $key1 => $value1) {
                    $fieldvalue = implode(",",$val1);
                //}
            }else{
                 $fieldvalue = $val1;
            }
        
              $fields=array('product_id'=>$input['prod_id'],'fieldname'=>str_replace(" ","_",$key),'fieldvalue'=>$fieldvalue);
              $insfields= DB::table('product_fields')->insert($fields);
               /*$insfields= DB::table('product_fields')
                           ->insert(['fieldvalue' => $fieldvalue, 'fieldname' => $key]) ;*/
                           
            
        } 

          $msg = 'New Product Added Successfully.';
          return redirect()->route('admin-prod-edit-otherInfo',['id'=>$id,'tab'=>'overview']);
        
    }

     public function updateFeature(Request $request,$id)
    {

          //DB::table('product_features_list')->where('product_id','=',$id)->delete();
   // dd($request->function_val);

    $featureLists=  DB::table('product_features_list')->where('product_id','=',$id)->first();
        if(!empty($featureLists)) {
             foreach ($request->function_val as $key => $value) {

              $keyArray = explode("_", $key);
                DB::table('product_features_list')
                ->where('module_name',$keyArray[0])
                ->where('section_name' ,$keyArray[1] )
                ->where('function_name',$keyArray[2])
                ->where('product_id',$id)
                ->where('feature_id' ,$request->featureId )
                ->update(['function_value' => $value]);
            }
          }else{

          $featureLists=  DB::table('category_features_modules')->where('feature_id','=',$request->featureId)->get();
         // dd($featureLists);
              foreach($featureLists as $feature){
               $fields=array('product_id'=>$id,'feature_id'=>$request->featureId,'module_name'=>$feature->module_name,'section_name'=>$feature->section_name,'function_name'=>$feature->function_name,'function_value'=>'no');

             //if(empty($checkExist1)){  
               $insfields= DB::table('product_features_list')->insert($fields);
            
            }

          foreach ($request->function_val as $key => $value) {
             $keArray =  explode("_", $key);

                DB::table('product_features_list')
                // ->where('module_fname',$feature->module_name)
                ->where('module_name',$keArray[0])
                ->where('section_name' ,$keArray[1] )
                ->where('function_name',$keArray[2])
                ->where('feature_id' ,$request->featureId )
                ->update(['function_value' => $value]);
            }
        
        }
     
          $msg = 'New Product Added Successfully.';
          return redirect()->route('admin-prod-edit-otherInfo',['id'=>$id,'tab'=>'feature']);
    }


    public function updateVarient(Request $request,$id)
    {

       $input = $request->all();     
      // dd($input)    ;
      foreach($input['variant_name'] as $key=>$val){
            $varient2 ='';
            $varient3='';

          if(!empty($input['varient2'][$key]) ){
            $varient2 = $input['varient2'][$key];
          }if(!empty($input['varient3'][$key]) ){
            $varient3 = $input['varient3'][$key];
          }
         $checkexist = DB::table('product_varient')
                      ->where('product_id','=',$id)
                      ->where('varient_name','=',$val)
                      ->where('varient1','=',$input['varient1'][$key])
                      ->where('varient2','=',$varient2)
                      ->where('varient3','=',$varient3)
                   ->first();

          $fields=array('product_id'=>$id,'varient_name'=>$val,'varient1'=>$input['varient1'][$key],'varient2'=>$varient2,'varient3'=>$varient3,'variation_count'=>$input['numberOfvar']);
      
             if(!empty($checkexist)){
                      $delFields= DB::table('product_varient')->where('product_id','=',$id)->delete();
           
             }
              
              $insfields= DB::table('product_varient')->insert($fields);
            
        } 
      return redirect()->route('admin-prod-edit-otherInfo',['id'=>$id,'tab'=>'varient']);
        
    }



//////////////update Implementation Varient////////////////////////
    public function updateiVarient(Request $request,$id)
    {

       $input = $request->all();  
        $checkexist = DB::table('implementation_varient')
                      ->where('product_id','=',$id)
                     
                    ->first();   
          if(!empty($checkexist)){
                      $delFields= DB::table('implementation_varient')->where('product_id','=',$id)->delete();
           
             }
       //dd($input)    ;
      foreach($input['i_variant_name'] as $key=>$val){
            $varient2 ='';
            $varient3='';

          if(!empty($input['i_varient2'][$key]) ){
            $varient2 = $input['i_varient2'][$key];
          }if(!empty($input['i_varient3'][$key]) ){
            $varient3 = $input['i_varient3'][$key];
          }
        

          $fields=array('product_id'=>$id,'varient_name'=>$val,'varient1'=>$input['i_varient1'][$key],'varient2'=>$varient2,'varient3'=>$varient3,'variation_count'=>$input['variationCount']);
      
           
              
              $insfields= DB::table('implementation_varient')->insert($fields);
            
        } 
      return redirect()->route('admin-prod-edit-otherInfo',['id'=>$id,'tab'=>'Ivarient']);
        
    }


    //////////////update Support Varient////////////////////////
    public function updatesVarient(Request $request,$id)
    {

       $input = $request->all(); 
       /*echo "<pre>";
       print_r($input);
       die;*/
        $checkexist = DB::table('support_varient')
                      ->where('product_id','=',$id)->first();
          if(!empty($checkexist)){
                      $delFields= DB::table('support_varient')->where('product_id','=',$id)->delete();
           
             }            
       
      foreach($input['variant_name_support'] as $key=>$val){
            $varient2 ='';
            $varient3='';

          if(!empty($input['s_varient2'][$key]) ){
            $varient2 = $input['s_varient2'][$key];
          }if(!empty($input['s_varient3'][$key]) ){
            $varient3 = $input['s_varient3'][$key];
          }
                 

          $fields=array('product_id'=>$id,'varient_name'=>$val,'varient1'=>$input['s_varient1'][$key],'varient2'=>$varient2,'varient3'=>$varient3,'variation_count'=>$input['variationCount']);
      
           
              
            $insfields= DB::table('support_varient')->insert($fields);
            
        } 
      return redirect()->route('admin-prod-edit-otherInfo',['id'=>$id,'tab'=>'Svarient']);
        
    }


    //*** GET Request
    public function feature($id)
    {
            $data = Product::findOrFail($id);
            return view('admin.product.highlight',compact('data'));
    }

    //*** POST Request
    public function featuresubmit(Request $request, $id)
    {
        //-- Logic Section
            $data = Product::findOrFail($id);
            $input = $request->all(); 
            if($request->featured == "")
            {
                $input['featured'] = 0;
            }
            if($request->hot == "")
            {
                $input['hot'] = 0;
            }
            if($request->best == "")
            {
                $input['best'] = 0;
            }
            if($request->top == "")
            {
                $input['top'] = 0;
            }
            if($request->latest == "")
            {
                $input['latest'] = 0;
            }
            if($request->big == "")
            {
                $input['big'] = 0;
            } 
            if($request->trending == "")
            {
                $input['trending'] = 0;
            }    
            if($request->sale == "")
            {
                $input['sale'] = 0;
            }   
            if($request->is_discount == "")
            {
                $input['is_discount'] = 0;
                $input['discount_date'] = null;               
            }  

            $data->update($input);
        //-- Logic Section Ends

        //--- Redirect Section        
        $msg = 'Highlight Updated Successfully.';
        return response()->json($msg);      
        //--- Redirect Section Ends    

    }

    //*** GET Request
    public function destroy($id)
    {

        $data = Product::findOrFail($id);
        if($data->galleries->count() > 0)
        {
            foreach ($data->galleries as $gal) {
                    if (file_exists(public_path().'/assets/images/galleries/'.$gal->photo)) {
                        unlink(public_path().'/assets/images/galleries/'.$gal->photo);
                    }
                $gal->delete();
            }

        }

        if($data->ratings->count() > 0)
        {
            foreach ($data->ratings  as $gal) {
                $gal->delete();
            }
        }
        if($data->wishlists->count() > 0)
        {
            foreach ($data->wishlists as $gal) {
                $gal->delete();
            }
        }
        if($data->clicks->count() > 0)
        {
            foreach ($data->clicks as $gal) {
                $gal->delete();
            }
        }
        if($data->comments->count() > 0)
        {
            foreach ($data->comments as $gal) {
            if($gal->replies->count() > 0)
            {
                foreach ($gal->replies as $key) {
                    $key->delete();
                }
            }
                $gal->delete();
            }
        }


        if (!filter_var($data->photo,FILTER_VALIDATE_URL)){
            if (file_exists(public_path().'/assets/images/products/'.$data->photo)) {
                unlink(public_path().'/assets/images/products/'.$data->photo);
            }
        }

        if (file_exists(public_path().'/assets/images/thumbnails/'.$data->thumbnail) && $data->thumbnail != "") {
            unlink(public_path().'/assets/images/thumbnails/'.$data->thumbnail);
        }

        if($data->file != null){
            if (file_exists(public_path().'/assets/files/'.$data->file)) {
                unlink(public_path().'/assets/files/'.$data->file);
            }
        }
        $data->delete();
        //--- Redirect Section     
        $msg = 'Product Deleted Successfully.';
        return response()->json($msg);      
        //--- Redirect Section Ends    

// PRODUCT DELETE ENDS  
    }

    public function addPricing(Request $request)
    {   

        $input = $request->all();
        $products = Product::find($input['prod_id']);
        $productId = $input['prod_id'];
        
        $module_count = count($input['module_name']);
        $user_slab_count = count($input['user_slab_lower_limit']);

        $fixedPrice = new FixedPrice;
        $fixedPrice->productId = $productId;
        $fixedPrice->variation1 = $input['fixed_price_variation1'];
        $fixedPrice->variation2 = $input['fixed_price_variation2'];
        $fixedPrice->variation3 = $input['fixed_price_variation3'];
        if(isset($input['fixed_pricing_type']))
            {
              $fixedPrice->user_based_pricing = '1';
            }
            else
            {
              $fixedPrice->user_based_pricing = '0';
            }
        // $fixedPrice->lower_limit = $input['fixed_price_lower_limit'];
        // $fixedPrice->upper_limit = $input['fixed_price_upper_limit'];

        $fixedPrice->save();

        for($i=0;$i<$module_count;$i++)
        {
            $modulePricing = new ModulePricing;
            //echo "in module".$i."-------".$input['module_name'][$i];
            $modulePricing->productId = $productId;
            $modulePricing->variation1 = $input['module_price_variation1'][$i];
            $modulePricing->variation2 = $input['module_price_variation2'][$i];
            $modulePricing->variation3 = $input['module_price_variation3'][$i];
            $modulePricing->moduleName = $input['module_name'][$i];
            if(isset($input['pricing_type'][$i]))
            {
              $modulePricing->user_based_pricing = '1';
            }
            else
            {
              $modulePricing->user_based_pricing = '0';
            }

            $modulePricing->save();
            //echo "loop".$i;

        }

        for($i=0;$i<$user_slab_count;$i++)
        {   
            $userSlabPricing = new UserSlabPricing;
            //echo "in user slab----------".$input['user_slab_variation2'][$i];
            $userSlabPricing->productId = $productId;
            $userSlabPricing->variation1 = $input['user_slab_variation1'][$i];
            $userSlabPricing->variation2 = $input['user_slab_variation2'][$i];
            $userSlabPricing->variation3 = $input['user_slab_variation3'][$i];
            $userSlabPricing->lower_limit = $input['user_slab_lower_limit'][$i];
            $userSlabPricing->upper_limit = $input['user_slab_upper_limit'][$i];

            $userSlabPricing->save();
        }

        $msg = 'New Product Added Successfully.';
        //echo $products->product_implementation; die;
      if($products->product_implementation=='yes'){

        return redirect()->route('admin-prod-create-Ivarient',$productId);
        
      }elseif($products->product_support == 'yes' && $products->product_implementation == 'no'){
         return redirect()->route('admin-prod-create-Svarient',$productId);
        

      }else{

       return redirect()->route('admin-prod-index');
      }
    }

    public function editPricing(Request $request)
    {   
        

        $input = $request->all();
        /*echo "<pre>";
        print_r($input);
        die;*/
        $products = Product::find($input['prod_id']);
        $productId = $input['prod_id'];
        
        $module_count = count($input['module_name']);
        $user_slab_count = count($input['user_slab_lower_limit']);

        $fixedPrice = new FixedPrice;
        $fixedPrice->productId = $productId;
        $fixedPrice->variation1 = $input['fixed_price_variation1'];
        $fixedPrice->variation2 = $input['fixed_price_variation2'];
        $fixedPrice->variation3 = $input['fixed_price_variation3'];
        if(isset($input['fixed_pricing_type']))
            {
              $fixedPrice->user_based_pricing = '1';
            }
            else
            {
              $fixedPrice->user_based_pricing = '0';
            }

        $deleteData = DB::table('module_price')
                            ->where('productId','=',$productId)
                            ->delete();
        $deleteData1 = DB::table('fixed_price')
                            ->where('productId','=',$productId)
                            ->delete();
        $deleteData2 = DB::table('user_slab_price')
                            ->where('productId','=',$productId)
                            ->delete();
        // $fixedPrice->lower_limit = $input['fixed_price_lower_limit'];
        // $fixedPrice->upper_limit = $input['fixed_price_upper_limit'];

        $fixedPrice->save();

        for($i=0;$i<$module_count;$i++)
        {
            $modulePricing = new ModulePricing;
            //echo "in module".$i."-------".$input['module_name'][$i];
            $modulePricing->productId = $productId;
            $modulePricing->variation1 = $input['module_price_variation1'][$i];
            $modulePricing->variation2 = $input['module_price_variation2'][$i];
            $modulePricing->variation3 = $input['module_price_variation3'][$i];
            $modulePricing->moduleName = $input['module_name'][$i];
            if($input['pricing_type'][$i] == '1')
            {
              $modulePricing->user_based_pricing = '1';
            }
            else
            {
              $modulePricing->user_based_pricing = '0';
            }

            $modulePricing->save();
            //echo "loop".$i;

        }

        for($i=0;$i<$user_slab_count;$i++)
        {   
            $userSlabPricing = new UserSlabPricing;
            //echo "in user slab----------".$input['user_slab_variation2'][$i];
            $userSlabPricing->productId = $productId;
            $userSlabPricing->variation1 = $input['user_slab_variation1'][$i];
            $userSlabPricing->variation2 = $input['user_slab_variation2'][$i];
            $userSlabPricing->variation3 = $input['user_slab_variation3'][$i];
            $userSlabPricing->lower_limit = $input['user_slab_lower_limit'][$i];
            $userSlabPricing->upper_limit = $input['user_slab_upper_limit'][$i];

            $userSlabPricing->save();
        }

        $msg = 'New Product Added Successfully.';
        //echo $products->product_implementation; die;
      

       return redirect()->route('admin-prod-edit-otherInfo',['id'=>$productId,'tab'=>'price']);
        
    }

    public function viewPricingPage($productId)
    {
        // echo "string";
        // die();
        $prod = Product::find($productId);
        $cats = Category::where('id','=',$prod->category_id)->first();
        $catId= $prod->category_id;
        $sign = Currency::where('is_default','=',1)->first();
        $subcats = Subcategory::where('id','=',$prod->subcategory_id)->first();
        $subcatname = DB::table('subcategories')
                    ->select('name')
                    ->where('id','=',$prod->subcategory_id)->first();
        //dd($subcats);
        $subcatfields = DB::table('category_fields')->where('cat_id','=',$prod->subcategory_id)->get();
        $brands = DB::table('brand')->get();
        $countries = DB::table('countries')->get();
        $product_varient = DB::table('product_varient')->where('product_id','=',$prod->id)->get();

        foreach($product_varient as $product_variant){
            $count_variation = $product_variant->variation_count;
        }
        $variation_count = $count_variation;
        $selectedTab = "price" ;
        //dd($subcatfields);
        $fieldsetdata ='';
               foreach($subcatfields as $key=>$value){

                          $inputfieldname = str_replace(" ", "_", $value->fieldname);

                            $fieldsetdata .= '
                                          <div class="row">
                                            <div class="col-lg-4">
                                                        <div class="left-area">
                                                                <h4 class="heading">'.$value->fieldname.' </h4>
                                                                <p class="sub-heading">(In Any Language)</p>
                                                        </div>
                                            </div>
                                            <div class="col-lg-7">';
                                          if($value->fieldtype == 'dropdown'){
                                            $optionArr = explode(",",str_replace(", ",",",$value->fieldvalue)); 
                                            $fieldsetdata .='<select name="product_subcat_fields['.$value->fieldname.']">';
                                             $fieldsetdata .='<option >Select '.$value->fieldname.'</option>';
                                                foreach($optionArr as $options){
                                                    $fieldsetdata .='<option value="'.$options.'"> '.$options.' </option>';
                                                }
                                                $fieldsetdata .='</select>';
                                        
                                          }if($value->fieldtype == 'checkbox'){
                                                 $checkListArr = explode(",",$value->fieldvalue); 
                                             foreach($checkListArr as $checkList){
                                                
                                                 $fieldsetdata .= $checkList.' <input type="'.$value->fieldtype.'"  placeholder="" name="product_subcat_fields['.$inputfieldname.']" class="checkclick" value="'.$checkList.'" required="">';
                                                }
                                          }if($value->fieldtype == 'radio'){
                                                 $checkListArr = explode(",",$value->fieldvalue); 
                                             foreach($checkListArr as $checkList){
                                                
                                                 $fieldsetdata .= $checkList.' <input type="'.$value->fieldtype.'"  placeholder="" name="product_subcat_fields['.$inputfieldname.']" class="radioclick" value="'.$checkList.'" required="">';
                                                }
                                          }

                                          if($value->fieldtype == 'text'){
                                             $fieldsetdata .='<input type="'.$value->fieldtype.'" class="form-control" placeholder="" name="product_subcat_fields['.$inputfieldname.']" required="">';
                                          }
                                          if($value->fieldtype == 'text-area'){
                                             $fieldsetdata .='<textarea class="form-control" rows="3" name="product_subcat_fields['.$inputfieldname.']" ></textarea>';
                                          }
                                        $fieldsetdata .='</div></div>';
                                     
                                     
                 }         
//echo $request->subcategory_id;
                    $featureLists=  DB::table('category_features')->where('cat_id','=',$prod->subcategory_id)->get();
                     $VendorList=  DB::table('users')->where('status','=','2')->get();
                             
                   $product_name='';
                   $productList=array();
        //dd($fieldsetdata);
                return view('admin.product.create.add',compact('cats','sign','subcats','fieldsetdata','featureLists','brands','countries','VendorList','selectedTab','prod','variation_count','product_name','productList','catId','subcatname'));

    }


      public function storeProdCopy($id)
    {
        $prod = Product::find($id);
        $sign = Currency::where('is_default','=',1)->first();
        $subcatfields = DB::table('category_fields')->where('cat_id','=',$prod->subcategory_id)->get();
        $product_varient = DB::table('product_varient')->where('product_id','=',$prod->id)->get();

       
       // $input = $prod;

       // dd($input);
        $data = new Product;
      
          

        $image = $prod->photo;
       // list($type, $image) = explode(';', $image);
        //list(, $image)      = explode(',', $image);
        $image = base64_decode($image);
        $image_name = time().str_random(8).'.png';
        $path = 'assets/images/products/'.$image_name;
        file_put_contents($path, $image);
       
         $data->name = $prod->name."-copy";
         $data->country_code = $prod->country_code;
         $data->brand_id = $prod->brand_id;
         $data->additional_information = $prod->additional_information;
         $data->price_start_range = $prod->price_start_range;
         $data->overview = $prod->overview;
         $data->subcategory_id = $prod->subcategory_id;
         $data->category_id = $prod->category_id;
         $data->user_id = $prod->user_id;
         $data->product_type = $prod->product_type;
         $data->addon_products = $prod->addon_products;
         $data->photo = $image_name;
         //$data->tumbnail = $image_name;
        //dd($data);
        $data->save();
     
       //echo $data->id; die;
      $prod1 = Product::find($data->id);
        $prod1->slug = str_slug($data->name,'-').'-'.strtolower(str_random(3).$data->id.str_random(3));
        // Set Thumbnail
        //$img = Image::make(public_path().'/assets/images/products/'.$prod1->photo)->resize(285, 285);
        $thumbnail = time().str_random(8).'.jpg';
        //$img->save(public_path().'/assets/images/thumbnails/'.$thumbnail);
        $prod1->thumbnail  = $thumbnail;
        $prod1->update();

        // Add To Gallery If any
        $lastid = $data->id;
      /* $galleryimages = Gallery::where('product_id','=',$id)->get();
        if ($files = $galleryimages){
            foreach ($files as  $key => $file){
               
                    $gallery = new Gallery;
                    $name = time().$file;
                    $file->move('assets/images/galleries',$name);
                    $gallery['photo'] = $name;
                    $gallery['product_id'] = $lastid;
                
            }
        }*/
        

     //////////////Add copied data in fildset table//////////////////////
       $product_fields =  DB::table('product_fields')->where('product_id',"=",$id)->get();

        foreach($product_fields as $key=>$val){
           //dd($val);

          $fields=array('cat_id'=>$prod->subcategory_id,'product_id'=>$lastid,'fieldname'=>$val->fieldname,'fieldvalue'=>$val->fieldvalue);
      
          $insfields= DB::table('product_fields')->insert($fields);
        } 

   /////////////////////// add copied data in features table/////////////////////
     $featureLists=  DB::table('product_features_list')->where('product_id','=',$id)->get();
      foreach($featureLists as $feature){
         $fields=array('product_id'=>$lastid,'feature_id'=>$feature->feature_id,'module_name'=>$feature->module_name,'section_name'=>$feature->section_name,'function_name'=>$feature->function_name,'function_value'=>$feature->function_value);
             $insfields= DB::table('product_features_list')->insert($fields);
           
          
      }

       
        
            return redirect()->route('admin-prod-edit',$lastid);
       
       
       // return response()->json($msg);
        //--- Redirect Section Ends    
    }
}
