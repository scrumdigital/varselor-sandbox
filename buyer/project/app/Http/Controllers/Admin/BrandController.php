<?php

namespace App\Http\Controllers\Admin;

use App\Models\Childcategory;
use App\Models\Subcategory;
use Datatables;
use Carbon\Carbon;
use App\Models\Product;
use App\Models\Category;
use App\Models\Currency;
use App\Models\Gallery;
use App\Models\Brand;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Validator;
use Image;
use DB;

class BrandController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }


    //*** GET Request
    public function brands()
    {   
        $brands = Brand::all();
        return view('admin.brand.brand_view',compact('brands'));
    }

    public function brands_add()
    {
        return view('admin.brand.brand_add');
    }

    public function brands_create(Request $request)
    {
        $brand = new Brand;
        $input = $request->all();
        

        if ($request->file('image')) {
            $image = $request->file('image');
            $imageName = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('assets/brand_images');
            $image->move($destinationPath, $imageName);

            $brand->image = $imageName;
        }

        if ($request->file('brand_specs')) {
            $brand_specs = $request->file('brand_specs');
            $brand_specsName = time().'.'.$brand_specs->getClientOriginalExtension();
            $destinationPath = public_path('assets/brand_specs_sheets');
            $brand_specs->move($destinationPath, $brand_specsName);

            $brand->brand_specs = $brand_specsName;
        }
        // echo "<pre>";
        // print_r($input);
        // die;
        $brand->name = $input['name'];
        $brand->description = $input['description'];
        $brand->information = $input['information'];
        $brand->company_information = $input['company_information'];

        $brand->save();
        return redirect()->route('brands-view');
    }

    public function brands_delete(Request $request, $id)
    {
        $data = Brand::findOrFail($id);

        $data->delete();
        //--- Redirect Section     
        // return Redirect::back()->with('message','Operation Successful !');
        return redirect()->route('brands-view');
    }
    
    public function edit(Request $request, $id)
    {
        $data = Brand::findOrFail($id);
        return view('admin.brand.edit',compact('data','cats','dataa'));
    }

    public function update(Request $request, $id)
    {
        $brand = Brand::findOrFail($id);
        $input = $request->all();
        // echo "<pre>";
        // print_r($input);

        if ($request->file('image')) {
            $image = $request->file('image');
            $imageName = time().'.'.$image->getClientOriginalExtension();
            $destinationPath = public_path('assets/brand_images');
            $image->move($destinationPath, $imageName);

            $brand->image = $imageName;
        }
        if ($request->file('brand_specs')) {
            $brand_specs = $request->file('brand_specs');
            $brand_specsName = time().'.'.$brand_specs->getClientOriginalExtension();
            $destinationPath = public_path('assets/brand_specs_sheets');
            $brand_specs->move($destinationPath, $brand_specsName);

            $brand->brand_specs = $brand_specsName;
        }
        // die;
        $brand->name = $input['name'];
        $brand->description = $input['description'];
        $brand->information = $input['information'];
        $brand->company_information = $input['company_information'];

        $brand->update();
    }
}
