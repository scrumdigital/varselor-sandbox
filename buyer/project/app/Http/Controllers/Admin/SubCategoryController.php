<?php

namespace App\Http\Controllers\Admin;

use Datatables;
use App\Models\Category;
use App\Models\Subcategory;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\DB;
use Validator;

class SubCategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
    }

    //*** JSON Request
    public function datatables()
    {
        DB::statement(DB::raw('set @rownum=0'));
         $datas = Subcategory::orderBy('id','desc')->get(['subcategories.*',DB::raw('@rownum  := @rownum  + 1 AS rownum')]);
         //--- Integrating This Collection Into Datatables
         return Datatables::of($datas)
                            ->addColumn('s.no', function(Subcategory $data) {
                                return $data->rownum;
                            }) 
                            ->addColumn('category', function(Subcategory $data) {
                                return $data->category->name;
                            }) 
                            ->addColumn('status', function(Subcategory $data) {
                                $class = $data->status == 1 ? 'drop-success' : 'drop-danger';
                                $s = $data->status == 1 ? 'selected' : '';
                                $ns = $data->status == 0 ? 'selected' : '';

                                return '<div class="action-list"><select class="process select droplinks '.$class.'"><option data-val="1" value="'. route('admin-subcat-status',['id1' => $data->id, 'id2' => 1]).'" '.$s.'>Activated</option><<option data-val="0" value="'. route('admin-subcat-status',['id1' => $data->id, 'id2' => 0]).'" '.$ns.'>Deactivated</option>/select></div>';
                            }) 
                            ->addColumn('action', function(Subcategory $data) {
                                 $subcatfields = DB::table('category_fields')->where('cat_id','=',$data->id)->first();
                                 if(empty($subcatfields)){
                                    $link = route('admin-subcat-fields',$data->id);
                                    $lable = "<span class='fal fa-plus'></span>Add Fieldset";
                                 }else{
                                    $link = route('admin-subcat-edifields',$data->id);
                                    $lable = "<span class='fal fa-edit'></span>Edit Fieldset";
                                 }

                                  
                                return '<div class="godropdown"><button class="go-dropdown-toggle"> Actions<span class="fal fa-chevron-down"></span></button><div class="action-list product-list-action"><a href="' .$link . '" class="fieldset btn product-table-btn" > '.$lable.'</a><a href="' .route('admin-subcat-features-index',$data->id) . '" class="features btn product-table-btn" ><span class="fal fa-star"></span> Features</a><a data-href="' . route('admin-subcat-edit',$data->id) . '" class="edit btn product-table-btn" data-toggle="modal" data-target="#modal1"> <span class="fal fa-edit"></span>Edit</a><a href="javascript:;" data-href="' . route('admin-subcat-delete',$data->id) . '" data-toggle="modal" data-target="#confirm-delete" class="delete btn product-table-btn"><span class="fal fa-trash-alt"></span> Delete</a></div></div>';
                            }) 



                            ->rawColumns(['status','action'])
                            ->toJson(); //--- Returning Json Data To Client Side
    }

    //*** GET Request
    public function index()
    {
        return view('admin.subcategory.index');
    }

    //*** GET Request
    public function create()
    {
      	$cats = Category::all();
        return view('admin.subcategory.create',compact('cats'));
    }

    //*** POST Request
    public function store(Request $request)
    {
        //--- Validation Section
        $rules = [
            'slug' => 'unique:subcategories|regex:/^[a-zA-Z0-9\s-]+$/'
                 ];
        $customs = [
            'slug.unique' => 'This slug has already been taken.',
            'slug.regex' => 'Slug Must Not Have Any Special Characters.'
                   ];
        $validator = Validator::make(Input::all(), $rules, $customs);
        
        if ($validator->fails()) {
          return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        //--- Validation Section Ends

        //--- Logic Section
        $data = new Subcategory();
        $input = $request->all();
        $data->fill($input)->save();
        //--- Logic Section Ends

        //--- Redirect Section        
        $msg = 'New Data Added Successfully.';
        return response()->json($msg);      
        //--- Redirect Section Ends    
    }

    //*** GET Request
    public function edit($id)
    {
    	$cats = Category::all();
        $data = Subcategory::findOrFail($id);
        return view('admin.subcategory.edit',compact('data','cats'));
    }

     public function features($id)
    {
       // $cats = Category::all();
        $cat = Subcategory::findOrFail($id);
        $data =array();
        //$data = DB::table('category_set_features')->where('cat_id','=',$id)->get();
        return view('admin.subcategory.addfeatures',compact('data','cat'));
    }

    public function storefeatures(Request $request,$id){
       // echo "eewwewe";die;
        $records     = array();
        //$moduleIds   =  array();
        $i=0;  
       
        
        $records1=array('cat_id'=>$id,'feature_name'=>$request->input('feature_name'));
        $feature_id =  DB::table('category_features')->insertGetId($records1);
          foreach($request->input('modulename') as $key=>$value){
            $j = $i+1;
            $sectionString ='';
            $functionString ='';
            foreach($request->input('sectionName'.$j.'') as $section){
                  $sectionString .= $section.",";
              foreach($request->input('functionName'.$j.'') as $funcName){
                  $functionString .= $section. "-". $funcName.",";
              }
            }
            
           //echo  $sectionString;
           //echo $functionString;

           $records=array('cat_id'=>$id,'feature_id'=>$feature_id,"module_name"=>$request->input('modulename')[$i],"section_name"=>$sectionString,"function_name" => $functionString);
              $moduleIds = DB::table('category_features_modules')->insert($records);

            $i++;
          }
          //die();
        $cats = Category::all();
        $data = Subcategory::findOrFail($id);
        $msg = "New data aaded successfully";
        return redirect()->route('admin-subcat-features-index', [$data->id]);
    }

    public function importFeature(Request $request,$id){
       
        $rules = [
            'csvfile'      => 'required|mimes:csv,txt',
        ];

        $validator = Validator::make(Input::all(), $rules);

        if ($validator->fails()) {
            return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        }

        $filename = '';
        if ($file = $request->file('csvfile'))
        {
            $filename = time().'-'.$file->getClientOriginalName();
            $file->move('assets/temp_files',$filename);
        }

       $chekExist = DB::table('category_features')->select('id')->where('feature_name','=',$request->input('feature_name'))->first();
       //dd($chekExist);
       if(empty($chekExist)){
         $records1=array('cat_id'=>$id,'feature_name'=>$request->input('feature_name'));
        $feature_id =  DB::table('category_features')->insertGetId($records1);
      
        $file = fopen(public_path('assets/temp_files/'.$filename),"r");
        $i = 0;
        while (($line = fgetcsv($file)) !== FALSE) {
//dd($line);
              //$data = explode(";", $line[$i]);
            // dd($line[2]);
                 $records=array('cat_id'=>$id,'feature_id'=>$feature_id,"module_name"=>$line[0],"section_name"=>$line[1],"function_name"=>$line[2]);
                 $moduleIds = DB::table('category_features_modules')->insert($records);
          $i++;
        }
        fclose($file);
        //--- Redirect Section
        $cats = Category::all();
        $data = Subcategory::findOrFail($id);
        $msg = "New data aaded successfully";
        return redirect()->route('admin-subcat-features-index', [$data->id]);
      }else{
         $cat = Subcategory::findOrFail($id);
           $data =array();
           $msg ="Feature already exist";
        //$data = DB::table('category_set_features')->where('cat_id','=',$id)->get();
        return view('admin.subcategory.addfeatures',compact('data','cat','msg'));



      }

    } 
     public static function getSectionNameFront($modulename,$productid) {
         $sectionData = array();
         $sectionData[] = DB::table('product_features_list')->select('section_name')->groupBy('section_name')
         ->where('product_id','=',$productid)
         ->where('module_name','=',$modulename)->get();
         //dd($sectionData);
        return $sectionData;
      }

     public static function getSFNameFront($modulename,$sectionname,$productid) {
         $fData = array();
         $fData[] = DB::table('product_features_list')->select('function_name')->where('section_name','=',$sectionname)
         ->where('product_id','=',$productid)->where('module_name','=',$modulename)->get();

         //dd($sectionData);
        return $fData;
      }
       public static function getFValFront($sectionname,$function_name,$productid) {
         $fData = array();
         $fData[] = DB::table('product_features_list')->select('function_value')->where('section_name','=',$sectionname)->where('function_name','=',$function_name)
         ->where('product_id','=',$productid)->first();
         //dd($sectionData);
        return $fData;
      }
    public function viewFeature($id)
    {
        $featureid = $id;
        $featureLists   = DB::table('category_features')->where('id','=',$id)->first();
       
        $moduleData = DB::table('category_features_modules')->groupBy('module_name')->where('feature_id','=',$featureLists->id)->get();
        return view('admin.subcategory.editfeature',compact('data','featureLists','moduleData','featureid'));
    }

    public static function getSectionName($modulename,$featureId) {
         $sectionData = array();
         $sectionData[] = DB::table('category_features_modules')->select('section_name')->groupBy('section_name')
         ->where('feature_id','=',$featureId)
         ->where('module_name','=',$modulename)->get();
         //dd($sectionData);
        return $sectionData;
      }

     public static function getSFName($modulename,$sectionname,$featureId) {
         $fData = array();
         $fData[] = DB::table('category_features_modules')->select('function_name')
         ->where('feature_id','=',$featureId)
         ->where('section_name','=',$sectionname)
          ->where('module_name','=',$modulename)
         ->get();
         //dd($sectionData);
        return $fData;
      }
       public static function getFVal($function_name) {
         $fData = array();
         $fData[] = DB::table('product_features_list')->select('function_value')->where('function_name','=',$function_name)->first();
         //dd($sectionData);
        return $fData;
      }

    public function featureList($id)
    {
        //$cats = Category::all();
        $data = Subcategory::findOrFail($id);
        $featureLists = DB::table('category_features')->where('cat_id','=',$id)->get();
        return view('admin.subcategory.featureIndex',compact('data','featureLists'));
    }

    public function fieldset($id)
    {
       // $cats = Category::all();
        $parentid = DB::table('subcategories')->where('id','=',$id)->get();
        foreach ($parentid as $parentids) {
            $parentcategory = $parentids->category_id;
        }
        
        $subcats = Subcategory::where('id','!=',$id)
                    ->where('category_id','=',$parentcategory)
                    ->get();
        
        $data = Subcategory::findOrFail($id);
        return view('admin.subcategory.fieldset',compact('data','subcats'));
        dd($subcats);
    }
      public function editfieldset($id)
    {
        $cats =  Subcategory::findOrFail($id);
        $data = DB::table('category_fields')->where('cat_id','=',$id)->get();
        return view('admin.subcategory.editfieldset',compact('data','id','cats'));
    }

   
   public function storefieldset(Request $request,$id){
       // echo "eewwewe";die;
        $records = array();
        $i=0;
        
       // dd($request->input('fieldvalue'));
        /////copy data from selected category///////////////////
        if($request->input('subcategory') != ''){
             $data = DB::table('category_fields')->where('cat_id','=',$request->input('subcategory'))->get();
           //echo  $id;
            // dd($data);

              foreach($data as $row) {
                //dd($row);
                $records =array('cat_id'=>$id,'fieldname'=>$row->fieldname,"fieldtype"=>$row->fieldtype,"fieldvalue"=>$row->fieldvalue);
           
                DB::table('category_fields')->insert($records);

              
            }

        }else{
            foreach($request->input('fieldname') as $key=>$value) {
                // $i++;
                $records=array('cat_id'=>$id,'fieldname'=>$request->input('fieldname')[$i],"fieldtype"=>$request->input('fieldtype')[$i],"fieldvalue"=>$request->input('fieldvalue')[$i]);
           
                DB::table('category_fields')->insert($records);

            $i++;
              
            }
        }

          //
             $cats = Category::all();
        $data = Subcategory::findOrFail($id);
        $msg = "New data aaded successfully";
        return redirect()->route('admin-subcat-edifields', [$data->id]);
       // return route('admin-subcat-edifields',$data->id);
    }

    public function updatefieldset(Request $request,$id){
       //echo "eewwewe";die;
        $records = array();
        $i=0;
       DB::table('category_fields')->where('cat_id','=',$id)->delete();
      // $dd($selData);
       //$selData->delete();
        //dd($request->input('fieldname'));
            foreach($request->input('fieldname') as $key=>$value) {
                // $i++;
                $records=array('cat_id'=>$id,'fieldname'=>$request->input('fieldname')[$i],"fieldtype"=>$request->input('fieldtype')[$i],"fieldvalue"=>$request->input('fieldvalue')[$i]);
               // dd($records); 
                DB::table('category_fields')->insert($records);

            $i++;
              
            }

          //
             $cats = Category::all();
        $data = Subcategory::findOrFail($id);
        $msg = "New data aaded successfully";
        return redirect()->route('admin-subcat-edifields', [$data->id]);
       // return route('admin-subcat-edifields',$data->id);
    }

    public function delfieldset($id,$cat_id){

       // 
          DB::table('category_fields')->where('id','=',$id)->delete();
      
       
        $msg = "New data aaded successfully";
        return redirect()->route('admin-subcat-edifields', [$cat_id]);
      
    }
    //*** POST Request
    public function update(Request $request, $id)
    {
        //--- Validation Section
        $rules = [
            'slug' => 'unique:subcategories,slug,'.$id.'|regex:/^[a-zA-Z0-9\s-]+$/'
                 ];
        $customs = [
            'slug.unique' => 'This slug has already been taken.',
            'slug.regex' => 'Slug Must Not Have Any Special Characters.'
                   ];
        $validator = Validator::make(Input::all(), $rules, $customs);
        
        if ($validator->fails()) {
          return response()->json(array('errors' => $validator->getMessageBag()->toArray()));
        }
        //--- Validation Section Ends

        //--- Logic Section
        $data = Subcategory::findOrFail($id);
        $input = $request->all();
        $data->update($input);
        //--- Logic Section Ends

        //--- Redirect Section     
        $msg = 'Data Updated Successfully.';
        return response()->json($msg);      
        //--- Redirect Section Ends            
    }

      //*** GET Request Status
      public function status($id1,$id2)
        {
            $data = Subcategory::findOrFail($id1);
            $data->status = $id2;
            $data->update();
        }

    //*** GET Request
    public function load($id)
    {
        $cat = Category::findOrFail($id);
        return view('load.subcategory',compact('cat'));
    }
 //*** GET Request
    public function loadfields($id)
    {
        //echo "test"; die;
        $subcatfields = DB::table('category_fields')->where('cat_id','=',$id)->get();
        //dd($subcatfields);
        $data ='';
               foreach($subcatfields as $key=>$value){

                          $inputfieldname = str_replace(" ", "_", $value->fieldname);

                            $data .= '
                                          </div><div class="col-lg-4">
                                                        <div class="left-area">
                                                                <h4 class="heading">'.$value->fieldname.' </h4>
                                                                <p class="sub-heading">(In Any Language)</p>
                                                        </div>
                                            </div>
                                            <div class="col-lg-7">';
                                          if($value->fieldtype == 'dropdown'){
                                            $optionArr = explode(",",$value->fieldvalue); 
                                            $data .='<select name="'.$value->fieldname.'">';
                                             $data .='<option >Select '.$value->fieldname.'</option>';
                                                foreach($optionArr as $options){
                                                    $data .='<option value="'.$options.'"> '.$options.' </option>';
                                                }
                                                $data .='</select>';
                                        
                                          }if($value->fieldtype == 'checkbox'){
                                                 $checkListArr = explode(",",$value->fieldvalue); 
                                             foreach($checkListArr as $checkList){
                                                
                                                 $data .= $checkList.' <input type="'.$value->fieldtype.'"  placeholder="" name="'.$inputfieldname.'" class="checkclick" value="'.$checkList.'" required="">';
                                                }
                                          }
                                          else{
                                             $data .='<input type="'.$value->fieldtype.'" class="input-field" placeholder="" name="'.$inputfieldname.'" required="">';
                                          }
                                        $data .='</div>
                                     </div> <div class="row">';
                 }                               
                                               
         return response()->json($data);
    }

    
    
    //*** GET Request Delete
    public function destroy($id)
    {
        $data = Subcategory::findOrFail($id);

        if($data->childs->count()>0)
        {
        //--- Redirect Section     
        $msg = 'Remove the subcategories first !!!!';
        return response()->json($msg);      
        //--- Redirect Section Ends    
        }
        if($data->products->count()>0)
        {
        //--- Redirect Section     
        $msg = 'Remove the products first !!!!';
        return response()->json($msg);      
        //--- Redirect Section Ends    
        }

        
        $data->delete();
        //--- Redirect Section     
        $msg = 'Data Deleted Successfully.';
        return response()->json($msg);      
        //--- Redirect Section Ends     
    }
}
