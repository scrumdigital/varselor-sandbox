<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserSlabPricing extends Model
{
    protected $table = 'user_slab_price';
    
    protected $fillable = ['productId', 'variation1', 'variation2', 'variation3', 'lower_limit', 'upper_limit'];
    public $timestamps = false;
}
