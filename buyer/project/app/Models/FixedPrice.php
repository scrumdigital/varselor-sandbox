<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class FixedPrice extends Model
{
    protected $table = 'fixed_price';
    
    protected $fillable = ['productId', 'variation1', 'variation2', 'variation3', 'lower_limit', 'upper_limit'];
    public $timestamps = false;
}
