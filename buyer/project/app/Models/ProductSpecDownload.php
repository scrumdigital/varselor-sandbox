<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductSpecDownload extends Model
{
    protected $table = 'product_specs_downlaod';
    
    protected $fillable = ['productId', 'email', 'brand_id','token'];
    public $timestamps = false;
}
