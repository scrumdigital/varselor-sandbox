<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = 'brand';
    
    protected $fillable = ['name', 'image', 'description', 'information', 'company_information'];
    public $timestamps = false;
}
