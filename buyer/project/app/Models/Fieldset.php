<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Fieldset extends Model
{
    protected $table = 'category_fields';
    
    protected $fillable = [`cat_id`, `fieldname`, `fieldtype`, `fieldvalue`];
    public $timestamps = false;
}