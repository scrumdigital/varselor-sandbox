<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ModulePricing extends Model
{
    protected $table = 'module_price';
    
    protected $fillable = ['productId', 'moduleName', 'variation1', 'variation2', 'variation3'];
    public $timestamps = false;
}
