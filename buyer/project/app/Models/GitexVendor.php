<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GitexVendor extends Model
{
    protected $table = 'gitex_vendor';
    
    protected $fillable = ['name', 'email', 'phone', 'company_name', 'vendor_type','city','country','message'];
    public $timestamps = false;
}
