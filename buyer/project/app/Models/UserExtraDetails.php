<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserExtraDetails extends Model
{
    protected $table = 'users_extra_details';
    
    protected $fillable = ['user_id', 'extra_details'];
    public $timestamps = false;
}
