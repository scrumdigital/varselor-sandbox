@extends('layouts.front')
@section('content')

						<!-- <h1> Thanks for contacting.</h1>
						<h1> Varselor Team will get back to you</h1> -->
<div class="thank-you-wraper">
	<div class="container">
		<div class="row">
			<div class="col-md-8 offset-md-2">
				
				<div class="thank-youpage">
					<i class="nav-icon i-Yes"></i>
					<h1 class="thank-youTitle text-center">Thanks For Contacting</h1>
				</div>

				<div class="thank-main-content">
					
					<p class="text-center">Thanks a bunch for filling that out. It means a lot to us, just like you do! We really appreciate you giving us a moment of your time today. Thanks for being you.</p>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection


