@extends('layouts.front')
<link href="{{asset('assets/vendor/css/style.css')}}" rel="stylesheet"/>


@section('content')

<!-- @include('includes.admin.form-login') -->
<section class="login-signup">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-lg-6">
        <nav class="comment-log-reg-tabmenu">
          <div class="nav nav-tabs" id="nav-tab" role="tablist">
            <a class="nav-item nav-link login active" id="nav-log-tab" data-toggle="tab" href="#nav-log" role="tab" aria-controls="nav-log" aria-selected="true">
              {{ $langg->lang197 }}
            </a>
            <a class="nav-item nav-link" id="nav-reg-tab" data-toggle="tab" href="#nav-reg" role="tab" aria-controls="nav-reg" aria-selected="false">
              {{ $langg->lang198 }}
            </a>
          </div>
        </nav>
        <div class="tab-content" id="nav-tabContent">
          <div class="tab-pane fade show active" id="nav-log" role="tabpanel" aria-labelledby="nav-log-tab">
                <div class="login-area">
                  <div class="header-area">
                    <h4 class="title">{{ $langg->lang172 }}</h4>
                  </div>
                  <div class="login-form signin-form">
                        @include('includes.admin.form-login')
                    <form action="{{ route('user.login.submit') }}" method="POST">
                      {{ csrf_field() }}
                      <div class="form-input">
                        <input type="email" name="email" placeholder="{{ $langg->lang173 }}" required="">
                        <i class="icofont-user-alt-5"></i>
                      </div>
                      <div class="form-input">
                        <input type="password" class="Password" name="password" placeholder="{{ $langg->lang174 }}" required="">
                        <i class="icofont-ui-password"></i>
                      </div>
                      <div class="form-forgot-pass">
                        <div class="left">
                          <input type="checkbox" name="remember"  id="mrp" {{ old('remember') ? 'checked' : '' }}>
                          <label for="mrp">{{ $langg->lang175 }}</label>
                        </div>
                        <div class="right">
                          <a href="javascript:;" id="show-forgot">
                            {{ $langg->lang176 }}
                          </a>
                        </div>
                      </div>
                      <input type="hidden" name="modal"  value="1">
                      <input class="mauthdata" type="hidden"  value="{{ $langg->lang177 }}">
                      <button type="submit" class="submit-btn">{{ $langg->lang178 }}</button>
                        @if(App\Models\Socialsetting::find(1)->f_check == 1 || App\Models\Socialsetting::find(1)->g_check == 1)
                        <div class="social-area">
                            <h3 class="title">{{ $langg->lang179 }}</h3>
                            <p class="text">{{ $langg->lang180 }}</p>
                            <ul class="social-links">
                              @if(App\Models\Socialsetting::find(1)->f_check == 1)
                              <li>
                                <a href="{{ route('social-provider','facebook') }}"> 
                                  <i class="fab fa-facebook-f"></i>
                                </a>
                              </li>
                              @endif
                              @if(App\Models\Socialsetting::find(1)->g_check == 1)
                              <li>
                                <a href="{{ route('social-provider','google') }}">
                                  <i class="fab fa-google-plus-g"></i>
                                </a>
                              </li>
                              @endif
                            </ul>
                        </div>
                        @endif
                    </form>
                  </div>
                </div>
          </div>
          <div class="tab-pane fade" id="nav-reg" role="tabpanel" aria-labelledby="nav-reg-tab">
                <div class="login-area signup-area">
                    <div class="header-area">
                        <h4 class="title">{{ $langg->lang181 }}</h4>
                    </div>
                    <div class="login-form signup-form">
                       @include('includes.admin.form-login')
                        <form action="{{route('vendor-register-submit')}}" method="POST">
                          {{ csrf_field() }}

                            <div class="form-input">
                                <input type="text" class="User Name" name="name" placeholder="{{ $langg->lang182 }}" required="">
                                <i class="icofont-user-alt-5"></i>
                            </div>

                            <div class="form-input">
                                <input type="email" class="User Name" name="email" placeholder="{{ $langg->lang183 }}" required="">
                                <i class="icofont-email"></i>
                            </div>

                            <div class="form-input">
                                <input type="text" class="User Name" name="phone" placeholder="{{ $langg->lang184 }}" required="">
                                <i class="icofont-phone"></i>
                            </div>

                            <div class="form-input">
                                <input type="text" class="User Name form-control" name="company_name" placeholder="Company" required="">
                                <i class="icofont-phone"></i>
                            </div>

                            <div class="form-input input-group form-selectOption-field">
                              <div class="input-group-prepend">
                                <label class="input-group-text"><i class="icofont-phone"></i></label>
                              </div>
                              <select class="User Name custom-select" name="vendor_type" required="">
                                  <option value="sales">Sales</option>
                                  <option value="consultant">Consultant</option>
                                  <option value="hardware_vendor">Hardware Vendor</option>
                                  <option value="hosting_company">Hosting Company</option>
                                  <option value="marketing_freelancer">Marketing - Freelancer</option>
                                  <option value="system_integrator">System Integrator</option>
                                  <option value="isv">ISV</option>
                              </select>
                            </div>

                            <div class="form-input">
                                <input type="text" class="User Name" name="city" placeholder="City" required="">
                                <i class="icofont-location-pin"></i>
                            </div>

                            <div class="form-input">
                                <input type="text" class="User Name" name="country" placeholder="Country" required="">
                                <i class="icofont-location-pin"></i>
                            </div>

                            <div class="form-input">
                                <input type="password" class="Password" name="password" placeholder="{{ $langg->lang186 }}" required="">
                                <i class="icofont-ui-password"></i>
                            </div>

                            <div class="form-input">
                                <input type="password" class="Password" name="password_confirmation" placeholder="{{ $langg->lang187 }}" required="">
                                <i class="icofont-ui-password"></i>
                            </div>

                            @if($gs->is_capcha == 1)

                            <ul class="captcha-area">
                              <li>
                                <p><img class="codeimg1" src="{{asset("assets/images/capcha_code.png")}}" alt=""> <i class="fas fa-sync-alt pointer refresh_code "></i></p>
                              </li>
                            </ul>

                            <div class="form-input">
                                <input type="text" class="Password" name="codes" placeholder="{{ $langg->lang51 }}" required="">
                                <i class="icofont-refresh"></i>
                            </div>

                            @endif

                            <button type="submit" class="submit-btn">Save Details</button>
                        
                        </form>
                    </div>
                </div>
          </div>
        </div>



      </div>





    </div>
  </div>
</section>


<!-- <div class="container">
  <div class="row">
    <div class="col-md-12">
      <form method="post" action="{{route('vendor-register-submit')}}" >
        {{ csrf_field() }}        

              <div class="row">
                  <div class="col-lg-6">
                    <div class="form-group">
                      <input type="text" class="form-control" name="name" placeholder="{{ $langg->lang182 }}" required="">
                      </div>
                  </div>

                  <div class="col-lg-6">
                    <div class="form-group">
                      <input type="email" class="form-control" name="email" placeholder="{{ $langg->lang183 }}" required="">
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <input type="text" class="form-control" name="phone" placeholder="{{ $langg->lang184 }}" required="">
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                      <input type="text" class="form-control" name="company_name" placeholder="Company" required="">
                    </div>
                  </div>
                  <div class="col-lg-6">
                    <div class="form-group">
                          <select class="form-control" name="vendor_type" required="">
                              <option value="sales">Sales</option>
                              <option value="consultant">Consultant</option>
                              <option value="hardware_vendor">Hardware Vendor</option>
                              <option value="hosting_company">Hosting Company</option>
                              <option value="marketing_freelancer">Marketing - Freelancer</option>
                              <option value="system_integrator">System Integrator</option>
                              <option value="isv">ISV</option>
                          </select>
                        </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <input type="text" class="form-control" name="city" placeholder="City" required="">
                      </div>

                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <input type="text" class="form-control" name="country" placeholder="Country" required="">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <input type="password" class=" form-control" name="password" placeholder="{{ $langg->lang186 }}" required="">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <input type="password" class="form-control" name="password_confirmation" placeholder="{{ $langg->lang187 }}" required="">
                      </div>
                    </div>

                    @if($gs->is_capcha == 1)
                      <div class="col-lg-6">
                        <ul class="captcha-area">
                            <li>
                              <p>
                                <img class="codeimg1" src="{{asset("assets/images/capcha_code.png")}}" alt=""> <i class="fas fa-sync-alt pointer refresh_code "></i>
                              </p>
                                    
                            </li>
                        </ul>
                      </div>

                      <div class="col-lg-6">
                        <div class="form-group">
                          <input type="text" class="form-control " name="codes" placeholder="{{ $langg->lang51 }}" required="">
                        </div>
                      </div>
                    @endif
              </div>
              <button type="submit" class="submit-btn">Save Details</button>
      </form>
    </div>
  </div>
</div> -->
      
                        

@endsection