  @php
                                 $sectionName = SubCategoryController::getSectionNameFront($module_name,$productid);
                                 
                                @endphp 

                                
                                                 
                               @foreach($sectionName as $key=>$val)
                                @foreach(json_decode($val) as $sec)
                    <div class="header-card-section">
                      <div class="row">
                        <div class="col-md-12"><h3 class="header-card">{{$sec->section_name}}</h3></div>
                      </div>
                    </div>

                       @php  $fName = SubCategoryController::getSFNameFront($sec->section_name,$productid); 
                       
                       @endphp

                              @foreach($fName as $key1=>$val1)
                                      @foreach(json_decode($val1) as $fun)
                                     @php  $fvalue = SubCategoryController::getFValFront($fun->function_name,$productid); @endphp
                           
                    <div class="card">
                      <div class="row">
                        <div class="col-md-9"><p class="tab-title">{{htmlspecialchars($fun->function_name)}}</p></div>
                        <div class="col-md-3">
                        <div class="icon-box text-center">
                          @if(!empty($fvalue[0]) && ($fvalue[0]->function_value == "Yes" || $fvalue[0]->function_value == "yes"))
                          <i class="fa fa-check" aria-hidden="true"></i>
                          @elseif (!empty($fvalue[0]) && ($fvalue[0]->function_value == "No" || $fvalue[0]->function_value == "no"))
                          <i class="fa fa-times" aria-hidden="true" style="color:red;"></i>
                          @elseif (!empty($fvalue[0]))
                          <p>{{$fvalue[0]->function_value}}</p>
                          @endif
                        
                        </div>
                        </div>
                      </div>
                    </div>
                    @endforeach
                      @endforeach
                   
                     @endforeach
                      @endforeach