@extends('layouts.front')

@section('content')

<div class="breadcrumb-area">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
       
      </div>
    </div>
  </div>
</div>

<!-- Product Details Area Start -->
<section class="product-details-page">
  <div class="container">
        <div class="row">
	        <div class="col-lg-4 col-md-12 pl-lg-0">
            <div class="agency-profile-left-wrapper">
              <div class="agency-profile-img-rating-wrap">
  	          	<div class="agency-img-wrap">
  	          		<img class="img-fluid" src="{{asset('assets/brand_images/'.$brand->image)}}">
  	          	</div>

                <div class="agency-profile-rating d-none d-md-block">
                  <div class="agency-rating-wrap">
                    <div class="agency-rating-star">
                      <i class="material-icons"> star </i>
                      <i class="material-icons"> star </i>
                      <i class="material-icons"> star </i>
                      <i class="material-icons"> star </i>
                      <i class="material-icons"> star </i>
                    </div>

                    <div class="agency-rating-content">
                      <span class="agency-profile-rate"> 4.8 </span>
                      <span class="agency-profile-totalReview"> (13 Reviews) </span>
                     </div>
                  </div>
                </div>                
              </div>

              <div class="agency-fullProfile-content">
                <div class="agency-name">
                  <h2>{{$brand->name}} </h2>
                </div>

                <div class="agency-actionBtn-wrap">
                  <a href="" class="btn edit agencyContact-btn btn-hover" data-href="{{ route('front.contact') }}" data-toggle="modal" data-target="#modal1" >Contact Me </a>
                  <a href="" class="btn edit agencyQuote-btn btn-hover" data-href="{{ route('front.contact') }}" data-toggle="modal" data-target="#modal1" >Get a Quote </a>
                </div>
                {{-- ADD / EDIT MODAL --}}

                    <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="modal1" aria-hidden="true">
                    
                    <div class="modal-dialog modal-dialog-centered" role="document">
                    <div class="modal-content">
                        
                      <div class="modal-header">
                      <h5 class="modal-title">Get In Touch </h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                      </div>
                      <div class="modal-body">
                        

                        <div class="contact-info-wrap p-0">      
                          
                            <div class="row">
                              
                              <div class="col-md-12">
                                <form method="post" action="{{route('save-contact-detail-for-agency')}}" >
                                {{ csrf_field() }}        

                                        <div class="row">
                                            <div class="col-lg-6">
                                              <div class="form-group">
                                                <input type="text" class="User Name form-control" name="name" placeholder="{{ $langg->lang182 }}" required="">
                                              </div>
                                            </div>

                                            <div class="col-lg-6">
                                              <div class="form-group">
                                                <input type="email" class="User Name form-control" name="email" placeholder="{{ $langg->lang183 }}" required="">
                                              </div>
                                            </div>
                                            <div class="col-lg-6">
                                              <div class="form-group">
                                                <input type="text" class="User Name form-control" name="phone" placeholder="{{ $langg->lang184 }}" required="">
                                              </div>
                                            </div>
                                            <div class="col-lg-6">
                                              <div class="form-group">
                                                <input type="text" class="User Name form-control" name="company_name" placeholder="Company" required="">
                                              </div>
                                            </div>
                                           
                                            <div class="col-lg-6">
                                              <div class="form-group">
                                                <input type="text" class="User Name form-control" name="city" placeholder="City" required="">
                                              </div>

                                            </div>
                                            <div class="col-lg-6">
                                              <div class="form-group">
                                                <input type="text" class="User Name form-control" name="country" placeholder="Country" required="">
                                              </div>
                                            </div>
                                            <div class="col-lg-12">
                                              <div class="form-group">
                                                <textarea rows="4" cols="50" class="User Name form-control" name="message" placeholder="Additional Message" required=""></textarea>
                                              </div>
                                            </div>
                                           

                                        </div>
                                        <input type="hidden" name="current_url" id="current_url" value="{{ $brand->name }}">

                                        <button type="submit" class="submit-btn contactBtn" >Save Details</button>
                                        
                                        </script>
                                </form>
                              </div>
                            </div>
                          </div>
                      </div>
                      <div class="modal-footer">
                      <!-- <button type="button" class="btn btn-secondary close_custom" data-dismiss="modal">{{ __("Close") }}</button> -->
                      </div>
                    </div>
                    </div>
</div>


{{-- ADD / EDIT MODAL ENDS --}}

                <div class="agency-profile-des">
                  <h3 class="agency-des-title"> Description </h3>
                  <p> {{$brand->description}}</div>

                <div class="agency-sameInfo">
                  <h3 class="agency-someInfo-title"> Website </h3>
                  <p> <a href="http://{{$brand->information}}">{{$brand->information}}</a> </p>
                </div>

                <div class="agency-sameInfo-about">
                  <h3 class="agency-aboutInfo-title"> Company Profile </h3>
                  <p> {{$brand->company_information}} </p>
                
                </div>
              </div>
            </div>
  	    	</div>

          <div class="col-lg-8 pr-lg-0">
            <div class="row">
              <div class="col-lg-12">
                <div class="page-main-title">
                  <h2 class="seciton-head"> Products </h2>
                </div>
              </div>
            </div>
            <div class="row">
              @if(!empty($products))
              @foreach($products as $item)
              <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                  <div class="product-listing-grid agencyPro-grid-wrap">
                    <div class="item-img product-listing-img">
                      <a href="{{ url('item/' .$item->slug) }}">
                        <img class="img-fluid m-auto d-block" src="{{asset('assets/images/thumbnails/')}}/{{$item->thumbnail}}" alt="">
                      </a>
                    </div>
                    <div class="info prodct-grid-content agency-pro-grid-content">
                        <h2 class="proVendor-name mt-0"><a href="{{ url('item/' . str_replace(' ','-',$item->name)) }}">@php $position=25; $post = substr($item->name, 0, $position);echo $post; 
                                                  echo "...";@endphp </a></h2>
                        <div class="agency-category-rating">
                          <a href="{{ url('categoryList/' .$item->subcat_name) }}">
                            <h5 class="agency-category">{{$item->subcat_name}}</h5></a>
                          <div class="agency-rating-wrapper d-none d-md-block">
                            <div class="rating-box">
                              <span class="rating-star">
                                <i class="material-icons"> star </i>
                              </span>
                              <span class="rating-number"> 4.8 </span>
                            </div>
                          </div>                  
                        </div>
                        <div class="agency-seeOffer-btn">
                            <a href="{{ url('item/' . str_replace(' ','-',$item->name)) }}" class="seeOfferBtn btn btn-hover"> See Offer</a>
                          </div>
                    </div>
                  </div>
              </div>
              @endforeach
              @endif


@if($brand->brand_specs != '' || $brand->brand_specs != NULL)
{{-- Download MODAL --}}

                  <div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                              <h5 class="modal-title">Download Solution Guide for {{$brand->name}}</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                              </button>
                          </div>
                          <div class="modal-body">
                            <div class="contact-info-wrap p-0">      
                              <div class="row">
                                <div class="col-md-12">
                                  <form method="post" action="{{route('save-brand-download-detail')}}" >
                                  {{ csrf_field() }}        
                                    <div class="row">
                                      <div class="col-lg-3">
                                        <h5>Email </h5>
                                      </div>
                                      <div class="col-lg-9">
                                        <div class="form-group">
                                          <input type="email" class="User Name form-control" name="email" placeholder="{{ $langg->lang183 }}" required="">
                                        </div>
                                      </div>
                                    </div>
                                    <input type="hidden" name="brand_id" id="brand_id" value="{{ $brand->id }}">
                                    <input type="hidden" name="current_url" id="current_url" value="{{ $brand->name }}">
                                    <input type="hidden" name="brand_specs" id="brand_specs" value="{{ $brand->brand_specs }}">
                                    <input type="hidden" name="product_spec_sheet" id="product_spec_sheet" value="{{ asset('assets/brand_specs_sheets/'.$brand->brand_specs) }}">
                                    <button type="submit" class="submit-btn contactBtn" id="wwwwww">Submit</button>
                                  </form>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>


{{-- Download MODAL ENDS --}}             

@endif

           
              
            </div>
          </div>
	        
	    </div>
  
  </div>
<!-- Tranding Item Area End -->
</section>
<!-- Product Details Area End -->




@endsection
@section('scripts')
<script>
$(document).on('click','#wwwwww', function(){
      var url = $('#product_spec_sheet').val();
      var brand_name = $('#current_url').val();
      var a = document.createElement('a');
      a.href = url;
      a.setAttribute('download', brand_name+' - Soltion Guide.pdf');
      a.click();
  })
</script>
<script>
  $(window).load(function(){
   setTimeout(function(){
       $('#modal2').modal('show');
   }, 5000);
});
</script>
@endsection
