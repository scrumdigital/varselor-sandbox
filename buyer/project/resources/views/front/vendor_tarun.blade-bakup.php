@extends('layouts.front')

@section('content')


<div class="page-main-content-banner productlist-banner  buyer-category-banner">
  <div class="container">
    <div class="row align-items-center">
      <div class="col-lg-8 col-md-12 p-lg-0">
        <div class="bannerContent-wrap">
          <h1 class="bannerTitle"> Get Business-Solutions Now </h1>
          <p> Identify solutions, engage technology consultants, pay-by-milestone <br><br>Get competitive bids, work with highly experienced vendors and system integrators, choose “Domain Experts” per your line-of-business.</p>

          <a href="#buyer-product-tab" class="agencyContact-btn btn btn-hover banner-btn"> Find Best Solution </a>
        </div>
      </div>

      <div class="col-lg-4 d-none d-lg-block">
        <div class="banner-right-img">
          <img class="img-fluid m-auto d-block" src="{{asset('assets/images/software-sol-modal.png')}}" alt="">
        </div>
      </div>
    </div>
  </div>
</div>

<div class="container">
  <div class="row">
    <div class="col-md-12">
      <div class="page-title-block">
        <h2 class="buyer-category-page-title"> Browse Our Categories. What are you looking for?</h2> 
      </div>
    </div>
    <div class="col-md-12"> 
      <!-- Nav tabs -->
      <div class="tabs-nav category-tabs-wrap">
        <ul class="nav nav-tabs" role="tablist">
          <li role="presentation" class="active show product-info-tabs"><a href="#buyer-product-tab" aria-controls="buyer-product-tab" role="tab" data-toggle="tab"> <span>Products</span></a></li>
          <li role="presentation" class="product-info-tabs"><a href="#buyer-service-tab" aria-controls="buyer-service-tab" role="tab" data-toggle="tab"> <span>Services</span></a></li>
          <li role="presentation" class="product-info-tabs"><a href="#buyer-addon-tab" aria-controls="buyer-addon-tab" role="tab" data-toggle="tab"> <span>Addon</span></a></li>
        </ul>
      </div>
    </div>
  </div>
</div>
<section class="tabs-contant-section">
    <div class="container">
      <div class="row">
        <div class="col-md-12"> 
        <!-- Tab panes -->
            <div class="tab-content">
              <div role="tabpanel" class="tab-pane active show" id="buyer-product-tab">

                <div class="popular-category-wrapper">
                  <div class="row">
                    
                    @foreach ($productCount as $productCounts)
                      @foreach ($productCounts as $productCountss)
                        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                          <a href="{{route('category.view',$productCountss->subcat_name)}}">
                            <div class="popular-category-item">
                              <div class="popular-category-title">
                                  <h4> {{$productCountss->subcat_name}} <span>({{$productCountss->count}})</span> </h4>
                                <div class="popular-separator-border"></div>
                              </div>
                            </div>
                          </a>
                        </div>
                      @endforeach
                    @endforeach
                    <!-- <div class="col-lg-3">
                      <div class="popular-category-item">
                        <div class="popular-category-title">
                          <h4> Accounting Solutions <span>(55)</span> </h4>

                          <div class="popular-separator-border"></div>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-3">
                      <div class="popular-category-item">
                        <div class="popular-category-title">
                          <h4>Addon <span>(55)</span> </h4>

                          <div class="popular-separator-border"></div>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-3">
                      <div class="popular-category-item">
                        <div class="popular-category-title">
                          <h4> Accounting Solutions <span>(55)</span> </h4>

                          <div class="popular-separator-border"></div>
                        </div>
                      </div>
                    </div>

                    <div class="col-lg-3">
                      <div class="popular-category-item">
                        <div class="popular-category-title">
                          <h4> Accounting Solutions <span>(55)</span> </h4>

                          <div class="popular-separator-border"></div>
                        </div>
                      </div>
                    </div> -->
                  </div>
                </div>

                <!-- <div class="search-box category-tabSearch-box">
                  <form class="search-form" action="" method="GET">
                    <div class="input-group">
                      <input type="hidden" id="category_id" name="category_id" value="0">
                        <input type="text" class="form-control" id="prod_name" name="search" placeholder="Search" value="" required="" autocomplete="off">
                        <div class="input-group-append">
                            <button type="submit" class="input-group-text"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </div>
                      </div>
                  </form>
                </div>

                <h4 class="alpha-section-title"> alphabetical </h4>
                <div class="alpha-card-wrap">
                  <h3 class="alpha-letter"> a </h3>
                  <div class="card">
                    <div class="row">
                      <div class="col-md-8"><p class="tab-title"> Add-ons </p></div>
                      <div class="col-md-3"> <p class="text-center">Find Best Solution </p></div>
                      <div class="col-md-1"> <p class="text-right">214</p> </div>
                    </div>
                  </div>
                </div> -->

              </div>

              <div role="tabpanel" class="tab-pane" id="buyer-service-tab">
                <div class="popular-category-wrapper">
                  <div class="row">
                    @foreach ($serviceCount as $serviceCounts)
                      @foreach ($serviceCounts as $serviceCountss)
                        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                          @if($serviceCountss->subcat_id == 65 || $serviceCountss->subcat_id == 66 || $serviceCountss->subcat_id == 69)
                           <a href="{{route('product.view',$serviceCountss->subcat_name)}}">
                         
                          @else
                          <a href="{{route('category.view',$serviceCountss->subcat_name)}}">
                          @endif
                            <div class="popular-category-item">
                              <div class="popular-category-title">
                                <h4>{{$serviceCountss->subcat_name}} <span>({{$serviceCountss->count}})</span> </h4>

                                <div class="popular-separator-border"></div>
                              </div>
                            </div>
                          </a>
                        </div>
                      @endforeach
                    @endforeach
                  </div>
                </div>

                <!-- <div class="search-box category-tabSearch-box">
                  <form class="search-form" action="" method="GET">
                    <div class="input-group">
                      <input type="hidden" id="category_id" name="category_id" value="0">
                        <input type="text" class="form-control" id="prod_name" name="search" placeholder="Search" value="" required="" autocomplete="off">
                        <div class="input-group-append">
                            <button type="submit" class="input-group-text"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </div>
                      </div>
                  </form>
                </div>

                <h4 class="alpha-section-title"> alphabetical </h4>
                <div class="alpha-card-wrap">
                  <h3 class="alpha-letter"> a </h3>
                  <div class="card">
                    <div class="row">
                      <div class="col-md-8"><p class="tab-title"> Add-ons </p></div>
                      <div class="col-md-3"> <p class="text-center">Find Best Solution </p></div>
                      <div class="col-md-1"> <p class="text-right">214</p> </div>
                    </div>
                  </div>
                </div>-->
              </div> 
              <div role="tabpanel" class="tab-pane" id="buyer-addon-tab">
                <div class="popular-category-wrapper">
                  <div class="row">
                    @foreach ($addOnCount as $addOnCounts)
                      @foreach ($addOnCounts as $addOnCountss)
                        <div class="col-lg-3 col-md-6 col-sm-6 col-12">
                          <a href="{{route('category.view',$addOnCountss->subcat_name)}}">
                            <div class="popular-category-item">
                              <div class="popular-category-title">
                                <h4> {{$addOnCountss->subcat_name}} <span>({{$addOnCountss->count}})</span> </h4>

                                <div class="popular-separator-border"></div>
                              </div>
                            </div>
                          </a>
                        </div>
                      @endforeach
                    @endforeach
                  </div>
                </div>

                <!-- <div class="search-box category-tabSearch-box">
                  <form class="search-form" action="" method="GET">
                    <div class="input-group">
                      <input type="hidden" id="category_id" name="category_id" value="0">
                        <input type="text" class="form-control" id="prod_name" name="search" placeholder="Search" value="" required="" autocomplete="off">
                        <div class="input-group-append">
                            <button type="submit" class="input-group-text"><i class="fa fa-search" aria-hidden="true"></i></button>
                        </div>
                      </div>
                  </form>
                </div>
 -->
                <!-- <h4 class="alpha-section-title"> alphabetical </h4>
                <div class="alpha-card-wrap">
                  <h3 class="alpha-letter"> a </h3>
                  <div class="card">
                    <div class="row">
                      <div class="col-md-8"><p class="tab-title"> Add-ons </p></div>
                      <div class="col-md-3"> <p class="text-center">Find Best Solution </p></div>
                      <div class="col-md-1"> <p class="text-right">214</p> </div>
                    </div>
                  </div>
                </div> -->
              </div>

            </div>
        </div>
      </div>
    </div>
  </section>

<div class="buyer-category-banner bottom-adds-slider mb-5">
    
    <div class="owl-slider buyer-categroy-wrap">
      <div id="carousel" class="owl-carousel buyer-categroy-slider">
        <div class="item">
          <img class="img-fluid add-banners" src="{{asset('assets/images/sales-force.jpg')}}" alt="">
        </div>
        <div class="item">
          <img class="img-fluid add-banners" src="{{asset('assets/images/sap-banner.jpg')}}" alt="">
        </div>
      </div>
    </div>
</div>

@endsection

@section('scripts')
  <script>
    $('.product-info-tabs').on('click', function(){
      $('.product-info-tabs.active').removeClass('active');
      $(this).addClass('active');
    });
  </script>
@endsection