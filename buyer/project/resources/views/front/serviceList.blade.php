@extends('layouts.front')

@section('content')


<div class="page-main-content-banner categroylisting-banner-wrap">    
    <div class="owl-slider buyer-categroy-wrap">
      <div id="carousel" class="owl-carousel categroylisting-slider">
        <div class="item">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-lg-8 col-12">
                <div class="bannerContent-wrap">
                  <h1 class="bannerTitle"> {{$category->name}} </h1>
                  <p> A single platform to connect buyers and sellers of business software. Find, compare and buy the right ERP, CRM, Accounting and HR software solutions. Dedicated and designed for small and medium businesses across multiple industries and geographies. </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="item">
          <img class="img-fluid addBanner-img" src="{{asset('assets/images/sales-force.jpg')}}" alt="">
        </div>
        <div class="item">
          <img class="img-fluid addBanner-img" src="{{asset('assets/images/sap-banner.jpg')}}" alt="">
        </div>
      </div>
    </div>
</div>

<!-- left  filter Area Ends here -->
@include('layouts.searchby')


<!-- Product Details Area Start -->

<!-- Product Details Area Start -->

         


            <div class="row" id="searchresults"> 
             @if(!empty($products))
              @foreach($products as $item)
              <div class="col-lg-3 col-md-6 col-12">
                  <div class="product-listing-grid agencyPro-grid-wrap">
                    <div class="item-img product-listing-img">
                       <a href="{{ url('item/' . $item->slug)}}">
                        <img class="img-fluid m-auto d-block" src="{{asset('assets/images/thumbnails/')}}/{{$item->thumbnail}}" alt="">
                      </a>
                    </div>
                    <div class="info prodct-grid-content agency-pro-grid-content">
                          <h5 class="name productName"><a href="{{ url('item/' . $item->slug) }}"> {{$item->name}}</a></h5>
                        <div class="agency-seeOffer-btn addon-category-button">
                          <a href="javascript:;" class="seeOfferBtn  btn btn-hover add-to-compare" data-href="{{ route('product.compare.add',$item->id) }}" > Add to Comparison</a>
                        </div>
                    </div>
                  </div>
              </div>
              @endforeach
              @else
              <div class="col-lg-3 col-md-4 col-6">
                 No product found
              </div>


              @endif
            </div>
          </div>
          
      </div>
  
  </div>
<!-- Tranding Item Area End -->
</section>
<!-- Product Details Area End -->


@endsection
@section('scripts')
<script type="text/javascript">
  //close filter dropdown inside lateral .cd-filter 
    $('.pro-filter-block h2').on('click', function(){
        //$('.pro-filter-block h2').removeClass('filter-widget-title');
        //$(this).addClass('filter-widget-title');
        $(this).toggleClass('closed').siblings('.pro-filter-content').slideToggle(300);
    }) 

    $('.form-check-input').on('click', function(){
        //$('.pro-filter-block h2').removeClass('filter-widget-title');
  
     var brands = [];
            $.each($("input[name='brands']:checked"), function(){
                brands.push($(this).val());
            });

         var query =  brands.join(",");
         var cat_id = $('#cat_id').val();
         var filterby = $('#filterBy').val();
         //alert(cat_id)
             $.ajax({
           url:"../../searchby/"+filterby+"/"+query+"/"+cat_id,
           method:'GET',
          // data:{query:query},
           //dataType:'json',
           success:function(data)
          {
             var result ='';
         var imageUrl = '{{ URL::asset('assets/images/thumbnails/') }}';
            if(data != ''){
               $.each(data, function(i, e){
                 // alert(e)
                   //var alert(key);

                   $.each(e, function(key,val){ 
                    //alert(val)
                   var id = val['id'];
                   var compareurl = "{{ route('product.compare.add', ['slug' => '"+id "']) }}";

                    result += '<div class="col-lg-4 col-md-6 col-12"> <div class="product-listing-grid agencyPro-grid-wrap"><div class="item-img product-listing-img"><a href="{{ url("item/") }}/'+val['slug']+'"><img class="img-fluid m-auto d-block" src="'+imageUrl+'/'+val['thumbnail']+'"></a> </div><div class="info prodct-grid-content agency-pro-grid-content"><h5 class="name productName"><a href="{{ url("item/") }}/'+val['slug']+'">'+val['name']+'</a></h5><div class="agency-seeOffer-btn addon-category-button"><a href="javascript:;" class="seeOfferBtn  btn btn-hover add-to-compare" data-href="'+compareurl+'" >Add to Comparison</a></div></div></div></div>'
                   });
                
               });
            }else{
                   result += ' <div class="col-lg-3 col-md-4 col-6">No product found</div>'
                   

            }

            //alert(result);

            $('#searchresults').html(result);
          }
       })
  
 })

</script>

@endsection
