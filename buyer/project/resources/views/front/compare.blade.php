@extends('layouts.front')

@section('content')

	<!-- Breadcrumb Area Start -->
	<div class="breadcrumb-area">
		<div class="container">
			<div class="row">
				<div class="col-lg-12">
					<ul class="pages">
						<li>
							<a href="{{ route('front.index') }}">
								{{ $langg->lang17 }}
							</a>
						</li>
						<li>
							<a href="{{ route('product.compare') }}">
								{{ $langg->lang69 }}
							</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
	<!-- Breadcrumb Area End -->

	<!-- Compare Area Start -->

	<section class="compareInner-product-wrapper">
		<div class="container">
			@if(Session::has('compare'))
			<div class="row">
				<div class="col-md-12">
					<div class="com-heading compareInner-mainTitle">
						<h2 class="title">
								{{ $langg->lang70 }}
						</h2>
					</div>
				</div>
				<div class="col-lg-12">
					<div class="compareinner-product-img-wrap">
						<div class="row">
							@foreach($products as $product)
							<div class="col-lg-4">
								<div class="compareInner-proImg-title c{{$product['item']['id']}}">

									<div class="compareinner-pro-remove pro-remove c{{$product['item']['id']}}">
										<i class="fal fa-times-circle compare-remove" data-href="{{ route('product.compare.remove',$product['item']['id']) }}" data-class="c{{$product['item']['id']}}"></i>
									</div>

									<a href="{{ route('front.product', $product['item']['slug']) }}" class="compareInner-proImage">
										<img class="img-fluid m-auto d-block" src="{{ $product['item']['thumbnail'] ? asset('assets/images/thumbnails/'.$product['item']['thumbnail']):asset('assets/images/noimage.png') }}" alt="Compare product['item']">
									</a>

									<a href="{{ route('front.product', $product['item']['slug']) }}" class="compareInner-proTitle">{{ $product['item']['name'] }}</a>
								</div>
							</div>
							@endforeach
						</div>
					</div>
				</div>

				<div class="col-lg-12">
					<div class="compareInner-product-content-wrap">
						<div class="row">
							<div class="col-md-12">
								<div class="compareInner-proContent-title">
									<h4> {{ $langg->lang72 }} </h4>

									<div class="row compareInner-product-content-row">
										@foreach($products as $product)
										<div class="col-md-4">
											<div class="compareInner-proContent">
												<div class="pro-price c{{$product['item']['id']}}">
													{{ App\Models\Product::find($product['item']['id'])->showPrice() }}
												</div>
											</div>
										</div>
										@endforeach
									</div>
								</div>
							</div>

							<div class="col-md-12">
								<div class="compareInner-proContent-title">
									<h4> {{ $langg->lang73 }} </h4>

									<div class="row compareInner-product-content-row">
										@foreach($products as $product)
										<div class="col-md-4">
											<div class="compareInner-proContent">
												<div class="pro-ratting c{{$product['item']['id']}}">
													<div class="empty-stars"></div>
												</div>
											</div>
										</div>
										@endforeach
									</div>
								</div>
							</div>


							<div class="col-md-12">
								<div class="compareInner-proContent-title">
									<h4> {{ $langg->lang74 }} </h4>

									<div class="row compareInner-product-content-row">
										@foreach($products as $product)
										<div class="col-md-4">
											<div class="compareInner-proContent">
												<div class="pro-desc c{{$product['item']['id']}}">
													{{ strip_tags($product['item']['details']) }}
												</div>
											</div>
										</div>
										@endforeach
									</div>
								</div>
							</div>

							<div class="col-md-12">
								<div class="compareInner-proContent-title">
									<h4> {{ $langg->lang75 }} </h4>

									<div class="row compareInner-product-content-row">
										@foreach($products as $product)
										<div class="col-md-4">
											<div class="compareInner-proContent">
												<div class="c{{$product['item']['id']}}">
													@if($product['item']['product_type'] == "affiliate")
														<a href="{{ route('affiliate.product', $product['item']['slug']) }}" class="btn__bg">{{ $langg->lang251 }}</a>
													@else
														<a href="javascript:;" data-href="{{ route('product.cart.add',$product['item']['id']) }}" class="btn__bg add-to-cart btn compareInner-proBtn compareInner-pro-addToCart">{{ $langg->lang75 }}</a>
														<a href="{{ route('product.cart.quickadd',$product['item']['id']) }}" class="btn__bg btn compareInner-proBtn compareInner-pro-buyNow">{{ $langg->lang251 }}</a>
													@endif
												</div>
											</div>
										</div>
										@endforeach
									</div>
								</div>
							</div>
						</div>						
					</div>
				</div>
			</div>
			@endif
		</div>
	</section>
		
	</section>
	<!-- <section class="compare-page">
		<div class="container">

			@if(Session::has('compare'))

			<div class="row">
				<div class="col-lg-12">
					<div class="content">
						<div class="com-heading">
							<h2 class="title">
									{{ $langg->lang70 }}
							</h2>
						</div>
						<div class="compare-page-content-wrap">
							<div class="compare-table table-responsive">
								<table class="table table-bordered mb-0">
									<tbody>
										<tr>

											<td class="first-column top">{{ $langg->lang71 }}</td>
											@foreach($products as $product)
											<td class="product-image-title c{{$product['item']['id']}}">

													<img class="img-fluid" src="{{ $product['item']['thumbnail'] ? asset('assets/images/thumbnails/'.$product['item']['thumbnail']):asset('assets/images/noimage.png') }}" alt="Compare product['item']">

												<a href="{{ route('front.product', $product['item']['slug']) }}">
													<h4 class="title">
															{{ $product['item']['name'] }}
													</h4>
												</a>
											</td>
											@endforeach
										</tr>
										<tr>
											<td class="first-column">{{ $langg->lang72 }}</td>
											@foreach($products as $product)
											<td class="pro-price c{{$product['item']['id']}}">{{ App\Models\Product::find($product['item']['id'])->showPrice() }}</td>
											@endforeach
										</tr>
										<tr>
											<td class="first-column">{{ $langg->lang73 }}</td>
											@foreach($products as $product)
											<td class="pro-ratting c{{$product['item']['id']}}">
                                                <div class="ratings">
                                                    <div class="empty-stars"></div>
                                                    <div class="full-stars" style="width:{{App\Models\Rating::ratings($product['item']['id'])}}%"></div>
                                                </div>
											</td>
											@endforeach
										</tr>

										
										
										
										<tr>
												<td class="first-column">{{ $langg->lang74 }}</td>
												@foreach($products as $product)
												<td class="pro-desc c{{$product['item']['id']}}">
													<p>{{ strip_tags($product['item']['details']) }}</p>
												</td>
												@endforeach
	
											</tr>
											<tr>
													<td class="first-column">{{ $langg->lang75 }}</td>
													@foreach($products as $product)
													<td class="c{{$product['item']['id']}}">
															@if($product['item']['product_type'] == "affiliate")
															<a href="{{ route('affiliate.product', $product['item']['slug']) }}" class="btn__bg">{{ $langg->lang251 }}</a>
															@else														
														
														<a href="javascript:;" data-href="{{ route('product.cart.add',$product['item']['id']) }}" class="btn__bg add-to-cart">{{ $langg->lang75 }}</a>
													<a href="{{ route('product.cart.quickadd',$product['item']['id']) }}" class="btn__bg">{{ $langg->lang251 }}</a>
															@endif
													</td>
													@endforeach
												</tr>
										<tr>
											<td class="first-column">{{ $langg->lang76 }}</td>
											@foreach($products as $product)
											<td class="pro-remove c{{$product['item']['id']}}">
												<i class="far fa-trash-alt compare-remove" data-href="{{ route('product.compare.remove',$product['item']['id']) }}" data-class="c{{$product['item']['id']}}"></i>
											</td>
											@endforeach
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>

			@else

			<div class="page-center">
				<h4 class="text-center">{{ $langg->lang60 }}</h4>              
			</div>

			@endif


		</div>
	</section> -->
	<!-- Compare Area End -->

@endsection

@section('scripts')
<script type="text/javascript">
	
  //close filter dropdown inside lateral .cd-filter 
    $('.compareInner-proContent-title h4').on('click', function(){
        $(this).toggleClass('closed').siblings('.compareInner-product-content-row').slideToggle(300);
    }) 
</script>
@endsection