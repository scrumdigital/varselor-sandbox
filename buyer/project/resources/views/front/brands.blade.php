@extends('layouts.front')

@section('content')


<div class="page-main-content-banner categroylisting-banner-wrap">    
    <div class="owl-slider buyer-categroy-wrap">
      <div id="carousel" class="owl-carousel categroylisting-slider">
        <div class="item">
          <div class="container">
            <div class="row align-items-center">
              <div class="col-lg-8 col-12">
                <div class="bannerContent-wrap">
                  <h1 class="bannerTitle"> {{$catName}} </h1>
                  <p> A single platform to connect buyers and sellers of business software. Find, compare and buy the right ERP, CRM, Accounting and HR software solutions. Dedicated and designed for small and medium businesses across multiple industries and geographies. </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="item">
          <img class="img-fluid addBanner-img" src="{{asset('assets/images/sales-force.jpg')}}" alt="">
        </div>
        <div class="item">
          <img class="img-fluid addBanner-img" src="{{asset('assets/images/sap-banner.jpg')}}" alt="">
        </div>
      </div>
    </div>
</div>



<!-- Product Details Area Start -->
<section class="product-details-page">
  <div class="container">
        <div class="row">
          <div class="col-lg-3 col-md-4 pl-0">
            <div class="product-filter-wrapper">
              <div class="product-filter-title">
                <h2> Filters </h2>
              </div>
              <div class="product-filter-box">
                <div class="product-filter-widget_list pro-filter-block">
                    <h2> Filter 2</h2>
                    <ul class="pro-filter-content cd-filters">
                        <li>
                            <div class="form-group">
                              <input name="color[]" class="form-check-input attribute-input" type="checkbox" id="option1">
                              <label class="form-check-label" for="option1"> Option 1 </label>
                            </div>
                        </li>

                        <li>
                            <div class="form-group">
                              <input name="color[]" class="form-check-input attribute-input" type="checkbox" id="option2">
                              <label class="form-check-label" for="option2"> Option 2 </label>
                            </div>
                        </li>

                        <li>
                            <div class="form-group">
                              <input name="color[]" class="form-check-input attribute-input" type="checkbox" id="option3">
                              <label class="form-check-label" for="option3"> Option 3 </label>
                            </div>
                        </li>

                        <li>
                            <div class="form-group">
                              <input name="color[]" class="form-check-input attribute-input" type="checkbox" id="option4">
                              <label class="form-check-label" for="option4"> Option 4 </label>
                            </div>
                        </li>
                    </ul>
                </div>

                <div class="product-filter-widget_list pro-filter-block">
                    <h2> Filter 3</h2>
                    <ul class="pro-filter-content cd-filters">
                        <li>
                            <div class="form-group">
                              <input name="color[]" class="form-check-input attribute-input" type="checkbox" id="option1">
                              <label class="form-check-label" for="option1"> Option 1 </label>
                            </div>
                        </li>

                        <li>
                            <div class="form-group">
                              <input name="color[]" class="form-check-input attribute-input" type="checkbox" id="option2">
                              <label class="form-check-label" for="option2"> Option 2 </label>
                            </div>
                        </li>

                        <li>
                            <div class="form-group">
                              <input name="color[]" class="form-check-input attribute-input" type="checkbox" id="option3">
                              <label class="form-check-label" for="option3"> Option 3 </label>
                            </div>
                        </li>

                        <li>
                            <div class="form-group">
                              <input name="color[]" class="form-check-input attribute-input" type="checkbox" id="option4">
                              <label class="form-check-label" for="option4"> Option 4 </label>
                            </div>
                        </li>
                    </ul>
                </div>

                <div class="product-filter-widget_list pro-filter-block">
                    <h2> Filter 4</h2>
                    <ul class="pro-filter-content cd-filters">
                        <li>
                            <div class="form-group">
                              <input name="color[]" class="form-check-input attribute-input" type="checkbox" id="option1">
                              <label class="form-check-label" for="option1"> Option 1 </label>
                            </div>
                        </li>

                        <li>
                            <div class="form-group">
                              <input name="color[]" class="form-check-input attribute-input" type="checkbox" id="option2">
                              <label class="form-check-label" for="option2"> Option 2 </label>
                            </div>
                        </li>

                        <li>
                            <div class="form-group">
                              <input name="color[]" class="form-check-input attribute-input" type="checkbox" id="option3">
                              <label class="form-check-label" for="option3"> Option 3 </label>
                            </div>
                        </li>

                        <li>
                            <div class="form-group">
                              <input name="color[]" class="form-check-input attribute-input" type="checkbox" id="option4">
                              <label class="form-check-label" for="option4"> Option 4 </label>
                            </div>
                        </li>
                    </ul>
                </div>
              </div>
            </div>
          </div>


          <div class="col-lg-9 p-lg-0">
            <div class="product-search-box">
              <form class="search-form" method="GET">
                <div class="input-group">
                  <input type="hidden" id="category_id" name="category_id" value="0">
                    <input type="text" class="form-control" id="prod_name" name="search" placeholder="Search" value="" required="" autocomplete="off">
                    <div class="input-group-append">
                        <button type="submit" class="input-group-text"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </div>
                  </div>
              </form>
            </div>


            <div class="row"> 
              @if(!empty($brands))
              @foreach($brands as $item)
              <div class="col-lg-4 col-md-6 col-12">
                  <div class="product-listing-grid agencyPro-grid-wrap">
                    <div class="item-img product-listing-img">
                      <a href="{{route('services.view',[str_replace(' ', '-',$catName), str_replace(' ', '-',$item->name)])}}">
                        <img class="img-fluid m-auto d-block" src="{{asset('assets/brand_images/')}}/{{$item->image}}" alt="">
                      </a>
                    </div>
                    <div class="info prodct-grid-content agency-pro-grid-content">
                          <h5 class="name productName"> <a href="{{route('services.view', [str_replace(' ', '-',$catName), str_replace(' ', '-',$item->name)])}}">
                      {{$item->name}}</a></h5>
                        
                    </div>
                  </div>
              </div>
              @endforeach
              @else
              <div class="col-lg-3 col-md-4 col-6">
                 No product found
              </div>


              @endif
            </div>
          </div>
	        
	    </div>
  
  </div>
<!-- Tranding Item Area End -->
</section>
<!-- Product Details Area End -->


@endsection
@section('scripts')
<script type="text/javascript">
  //close filter dropdown inside lateral .cd-filter 
    $('.pro-filter-block h2').on('click', function(){
        //$('.pro-filter-block h2').removeClass('filter-widget-title');
        //$(this).addClass('filter-widget-title');
        $(this).toggleClass('closed').siblings('.pro-filter-content').slideToggle(300);
    }) 
</script>

@endsection
