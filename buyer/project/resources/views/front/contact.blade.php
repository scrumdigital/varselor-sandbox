@extends('layouts.front')


@section('content')
<div class="contact-banner">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1 class="text-center text-white"> Contact US </h1>
      </div>
    </div>
  </div>
</div>

<div class="contact-info-wrap">      
  <div class="container">
    <div class="row">
      <div class="col-lg-8">
        <h2 class="contact-title"> Get In Touch </h2>
        <p class="mb-5"> Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
      </div>
      <div class="col-md-12">
        <form method="post" action="{{route('save-contact-detail')}}" >
        {{ csrf_field() }}        

                <div class="row">
                    <div class="col-lg-6">
                      <div class="form-group">
                        <input type="text" class="User Name form-control" name="name" placeholder="{{ $langg->lang182 }}" required="">
                      </div>
                    </div>

                    <div class="col-lg-6">
                      <div class="form-group">
                        <input type="email" class="User Name form-control" name="email" placeholder="{{ $langg->lang183 }}" required="">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <input type="text" class="User Name form-control" name="phone" placeholder="{{ $langg->lang184 }}" required="">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <input type="text" class="User Name form-control" name="company_name" placeholder="Company" required="">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                            <select class="User Name form-control" name="vendor_type" required="">
                                <option value="sales">I am a --</option>
                                <option value="sales">Sales</option>
                                <option value="consultant">Consultant</option>
                                <option value="hardware_vendor">Hardware Vendor</option>
                                <option value="hosting_company">Hosting Company</option>
                                <option value="marketing_freelancer">Marketing - Freelancer</option>
                                <option value="system_integrator">System Integrator</option>
                                <option value="isv">ISV</option>
                            </select>

                      </div>

                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <input type="text" class="User Name form-control" name="city" placeholder="City" required="">
                      </div>

                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <input type="text" class="User Name form-control" name="country" placeholder="Country" required="">
                      </div>
                    </div>
                    <div class="col-lg-6">
                      <div class="form-group">
                        <textarea rows="4" cols="50" class="User Name form-control" name="message" placeholder="Additional Message" required=""></textarea>
                      </div>
                    </div>
                   

                </div>
                  

                <button type="submit" class="submit-btn contactBtn">Save Details</button>
        </form>
      </div>
    </div>
  </div>
</div>
                        

@endsection