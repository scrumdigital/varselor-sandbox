@extends('layouts.front')
@section('content')
<div class="breadcrumb-area">
   <div class="container">
      <div class="row">
         <div class="col-lg-12">
            <ul class="pages">
               <li><a href="{{route('front.index')}}"><i class="fa fa-arrow-left" aria-hidden="true"></i>
                  </a>
               </li>

               <li><a href="{{route('front.index')}}">{{$productt->category->name}}</a></li>
               @if(!empty($productt->subcategory))
               <li><a
                  href="{{ route('category.view', $productt->subcategory->name) }}">{{$productt->subcategory->name}}</a>
               </li>
               @endif
               @if(!empty($productt->childcategory))
               <li><a
                  href="{{ route('front.childcat',['slug1' => $productt->category->slug, 'slug2' => $productt->subcategory->slug, 'slug3' => $productt->childcategory->slug]) }}">{{$productt->childcategory->name}}</a>
               </li>
               @endif
               <li><a href="{{ route('front.product', $productt->name) }}">{{$productt->name}}</a>
            </ul>
         </div>
      </div>
   </div>
</div>
<!-- Product Details Area Start -->
<section class="product-details-page">
   <div class="container">
      <div class="row">
         <div class="col-lg-9 col-md-12">
            <!-- <div class="xzoom-container">
               <img class="xzoom5" id="xzoom-magnific" src="{{filter_var($productt->photo, FILTER_VALIDATE_URL) ?$productt->photo:asset('assets/images/products/'.$productt->photo)}}" xoriginal="{{filter_var($productt->photo, FILTER_VALIDATE_URL) ?$productt->photo:asset('assets/images/products/'.$productt->photo)}}" />
               </div> -->
            <div class="single-product-section">
               <div class="single-pro-left-wrap">
                  <div class="product-img-wrap">
                     <img src="{{asset('assets/images/products/'.$productt->photo) }}">
                  </div>
                  <div class="product-price">
                     <p class="price" align="center">
                        <span id="sizeprice" >
                         @if(!empty($productt->price_start_range))
                           {{ $productt->price_start_range }}
                        @endif
                        </span>
                        <!-- <small><del>{{ $productt->showPreviousPrice() }}</del></small> -->
                     </p>
                  </div>
                  <div class="addtocart">
                     <a href="" class="btn btn-block btn-border edit" data-href="{{ route('front.contact') }}" data-toggle="modal" data-target="#modal1" > {{ $langg->lang251 }}</a>
                  </div>
               </div>
               <div class="single-pro-right-area">
                  <div class="product-right-info">
                     <h4 class="product-name-block">{{ $productt->name }}</h4>
                     @if($brand->name!="Others")
                     <div class="cat-logo d-flex">
                        <img class="cat-logo-img" src="{{asset('assets/brand_images/'.$brand->image) }}" height="40" width="40"> 
                        <a href="{{route('brand.name',$brand->name)}}">
                           <h4>{{$brand->name}}</h4>
                        </a>
                     </div>
                     @endif
                     <div class="category_info">
                        @if(!empty($childCategory))
                        <p><span class="cat_title">Category</span>  {{$childCategory->name}}</p>
                        @else
                        <p><span class="cat_title">Category</span>  {{$subCategory->name}}</p>
                        @endif
                     </div>
                     <div class="distributor_info">
                        <h3>Exclusive Distributor: <span class="t-primary">Varselor</span></h3>
                     </div>
                     <div class="short-description">
                        <p>{!! str_replace("<<ENTER>>","<br />",$productt->overview)   !!}</p>
                        <!-- <a href="#">Read More</a> -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-lg-3 col-md-12">
            <ul class="product-side-link">
              
               <li><a href="javascript:;" class="btn btn-block btn-border add-to-compare" data-href="{{ route('product.compare.add',$productt->id) }}">Add to Comparision</a></li>
               <li class="clearfix">
                  <hr>
               </li>
               <li><a href="" class="btn btn-block btn-border edit watch-demo" data-href="{{ route('front.contact') }}" data-toggle="modal" data-target="#modal1" >Watch Demo</a></li>
               <li><a href="" class="btn btn-block btn-border edit schedule-demo" data-href="{{ route('front.contact') }}" data-toggle="modal" data-target="#modal1" >Schedule a Demo</a></li>
            </ul>
         </div>
      </div>
   </div>
{{-- ADD / EDIT MODAL --}}

                    <div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="modal1" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                                <div class="modal-header">
                              <h5 class="modal-title">Get in touch with one of our advisors</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                              </button>
                          </div>
                          <div class="modal-body">
                                  <div class="contact-info-wrap p-0">      
                                  <div class="row">
                                      <div class="col-md-12">
                                        <form method="post" action="{{route('save-contact-detail-for-product')}}" >
                                        {{ csrf_field() }}        
                                        <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                      <input type="text" class="User Name form-control" name="name" placeholder="{{ $langg->lang182 }}" required="">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                  <div class="form-group">
                                                    <input type="email" class="User Name form-control" name="email" placeholder="{{ $langg->lang183 }}" required="">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                      <input type="text" class="User Name form-control" name="phone" placeholder="{{ $langg->lang184 }}" required="">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                      <input type="text" class="User Name form-control" name="company_name" placeholder="Company" required="">
                                                    </div>
                                                </div>
                                                  <div class="col-lg-6">
                                                  <div class="form-group">
                                                      <input type="text" class="User Name form-control" name="city" placeholder="City" required="">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                  <div class="form-group">
                                                      <input type="text" class="User Name form-control" name="country" placeholder="Country" required="">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                  <div class="form-group">
                                                      <textarea rows="4" cols="50" class="User Name form-control" name="message" placeholder="Additional Message" required=""></textarea>
                                                  </div>
                                                </div>
                                            </div>
                                            <input type="hidden" name="current_url" id="current_url" value="{{ $productt->name }}">
                                            <button type="submit" class="submit-btn contactBtn" >Save Details</button>
                                        </form>
                                      </div>
                                  </div>
                                </div>
                            </div>
                          </div>
                      </div>
          </div>


{{-- ADD / EDIT MODAL ENDS --}}

{{-- Download PDF MODAL --}}
      @if($downloaded == '0')
        @if($productt->product_spec_sheet == '' || $productt->product_spec_sheet == NULL)
          <div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                                <div class="modal-header">
                              <h5 class="modal-title">Get in touch with one of our advisors</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                              </button>
                          </div>
                          <div class="modal-body">
                                  <div class="contact-info-wrap p-0">      
                                  <div class="row">
                                      <div class="col-md-12">
                                        <form method="post" action="{{route('save-contact-detail-for-product')}}" >
                                        {{ csrf_field() }}        
                                        <div class="row">
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                      <input type="text" class="User Name form-control" name="name" placeholder="{{ $langg->lang182 }}" required="">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                  <div class="form-group">
                                                    <input type="email" class="User Name form-control" name="email" placeholder="{{ $langg->lang183 }}" required="">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                      <input type="text" class="User Name form-control" name="phone" placeholder="{{ $langg->lang184 }}" required="">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group">
                                                      <input type="text" class="User Name form-control" name="company_name" placeholder="Company" required="">
                                                    </div>
                                                </div>
                                                  <div class="col-lg-6">
                                                  <div class="form-group">
                                                      <input type="text" class="User Name form-control" name="city" placeholder="City" required="">
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                  <div class="form-group">
                                                      <input type="text" class="User Name form-control" name="country" placeholder="Country" required="">
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                  <div class="form-group">
                                                      <textarea rows="4" cols="50" class="User Name form-control" name="message" placeholder="Additional Message" required=""></textarea>
                                                  </div>
                                                </div>
                                            </div>
                                            <input type="hidden" name="current_url" id="current_url" value="{{ $productt->name }}">
                                            <button type="submit" class="submit-btn contactBtn" >Save Details</button>
                                        </form>
                                      </div>
                                  </div>
                                </div>
                            </div>
                          </div>
                      </div>
          </div>
        @else
        <div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2" aria-hidden="true">
                        <div class="modal-dialog modal-dialog-centered" role="document">
                        <div class="modal-content">
                                <div class="modal-header">
                              <h5 class="modal-title">Download Specification Sheet for {{$productt->name}}</h5>
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                              <span aria-hidden="true">&times;</span>
                              </button>
                          </div>
                          <div class="modal-body">
                                  <div class="contact-info-wrap p-0">      
                                  <div class="row">
                                      <div class="col-md-12">
                                        <form method="post" action="{{route('save-download-detail')}}" >
                                        {{ csrf_field() }}        
                                        <div class="row">
                                                <div class="col-lg-3">
                                                    <h5>Email </h5>
                                                </div>
                                                <div class="col-lg-9">
                                                  <div class="form-group">
                                                    <input type="email" class="User Name form-control" name="email" placeholder="{{ $langg->lang183 }}" required="">
                                                    </div>
                                                </div>
                                                
                                            </div>
                                            <input type="hidden" name="product_id" id="product_id" value="{{ $productt->id }}">
                                            <input type="hidden" name="current_url" id="current_url" value="{{ $productt->name }}">
                                            <input type="hidden" name="slug_name" id="slug_name" value="{{ $productt->slug }}">
                                            <input type="hidden" name="spec_sheet" id="spec_sheet" value="{{ $productt->product_spec_sheet }}">
                                            <input type="hidden" name="product_spec_sheet" id="product_spec_sheet" value="{{ asset('assets/product_sheets/'.$productt->product_spec_sheet) }}">
                                            <button type="submit" class="submit-btn contactBtn" id="wwwwww">Submit</button>
                                        </form>
                                      </div>
                                  </div>
                                </div>
                            </div>
                          </div>
                      </div>
          </div>
        @endif  
      @endif
{{-- Download PDF MODAL ENDS--}}

</section>
<div class="container">
   <div class="row">
      <div class="col-md-12">
         <!-- Nav tabs -->
         <div class="tabs-nav singleProduct  singleProduct-tabs-wrap">
            <ul class="nav nav-tabs" role="tablist">
               <li role="presentation" class="active product-info-tabs"><a href="#proOverview-tab" aria-controls="proOverview-tab" role="tab" data-toggle="tab"> <span>OVERVIEW</span></a></li>
               @if(!count($module_names)==0)
               <li role="presentation" class="product-info-tabs"><a href="#proFeature-tab" aria-controls="proFeature-tab" role="tab" data-toggle="tab"> <span>FEATURES</span></a></li>
               @endif
               @if(!empty($variationCount))
               <li role="presentation" class="product-info-tabs"><a href="#proPrice-tab" aria-controls="proPrice-tab" role="tab" data-toggle="tab">  <span>PRICE</span></a></li>
               @endif
               @if(!count($product_support)==0)
               <li role="presentation" class="product-info-tabs"><a href="#proSupport-tab" aria-controls="proSupport-tab" role="tab" data-toggle="tab">  <span>Support</span></a></li>
               @endif
            </ul>
         </div>
      </div>
   </div>
</div>
<section class="tabs-contant-section">
   <div class="container">
      <div class="row">
         <div class="col-md-12">
            <!-- Tab panes -->
            <div class="tab-content">
               <div role="tabpanel" class="tab-pane active show" id="proOverview-tab">
                  <h3>Specifications</h3>
                  @if(!empty($product_overview))
                    @foreach ($product_overview as $productOverview)
                      @if($productOverview->fieldvalue != "NA")
                        @if($productOverview->fieldname == "Demo_Link")
                          <div class="card">
                             <div class="row">
                                <div class="col-md-5">
                                   <p class="tab-title"><?php echo str_replace("_"," ",$productOverview->fieldname);  ?></p>
                                </div>
                                <div class="col-md-7"><a href="{{$productOverview->fieldvalue}}" target="_blank"><?php echo str_replace("<<ENTER>>","<br />",$productOverview->fieldvalue);  ?></a></div>
                             </div>
                          </div>
                          @else
                          <div class="card">
                             <div class="row">
                                <div class="col-md-5">
                                   <p class="tab-title"><?php echo str_replace("_"," ",$productOverview->fieldname);  ?></p>
                                </div>
                                <div class="col-md-7"><?php echo str_replace(",",", ",str_replace("<<ENTER>>","<br />",$productOverview->fieldvalue));  ?></div>
                             </div>
                          </div>
                        @endif
                      @endif
                    @endforeach
                  @endif
                  <div class="row">
                     <div class="col-lg-8 pr-lg-0 text-justify">
                        <h4>Additional Information</h4>
                        {!! str_replace("<<ENTER>>","<br />",$productt->additional_information)   !!}
                        <!-- <ul class="option">
                           <li>Implementation</li>
                           <li>Some Areas of Work</li>
                           <li>Some other Areas</li>
                        </ul> -->
                        <!-- <div class="video-slide">
                           <iframe width="950" height="530" src="{!! $productt->youtube   !!}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                           </div> -->
                        <div class="productSingle-gallery-slider-wrap">
                           <div class="row">
                              <div class="col-md-12">
                                 <div class="productSingle-gallery-carousel owl-carousel">
                                  @if(!empty($productt->youtube))
                                    <div class="item">
                                       <div class="productGallery-item">
                                          <a href="" data-toggle="modal" data-src="{{$productt->youtube }}" data-target="#productVideo-modal" class="video-btn">
                                             <img src="{{asset('assets/images/products/'.$productt->photo) }}" class="img-fluid">
                                          </a>
                                       </div>
                                    </div>
                                    @endif
                                    @if(!empty($product_gallery))
                                       @foreach($product_gallery as $galleryImg)
                                       <div class="item">
                                          <div class="productGallery-item">
                                             <img src="{{asset('assets/images/galleries/')}}/{{$galleryImg->photo}}" style="width: 510px;height: 437px;" class="img-fluid m-auto d-block">
                                          </div>
                                       </div>
                                       @endforeach
                                    @endif
                                   
                                 </div>
                              </div>
                           </div>
                        </div>
                        <div class="recommended-addons-slider-wrap">
                           <div class="row">
                              <div class="col-md-12">
                                 <h2 class="recommended-title"> Complementing Solutions </h2>
                              </div>
                              <div class="col-md-12">
                                 <div class="recommended-carousel owl-carousel">
                                    <!-- <div class="item">
                                       <div class="recommended-addon-pro-wrapper">
                                          <div class="recom-addonPro-img">
                                             <img src="{{asset('assets/images/addonimg.jpg')}}" class="img-fluid" alt="">
                                          </div>
                                          <div class="recom-addonPro-content">
                                             <h3 class="recom-addonPro-title"> WT6000 Wearable Computer </h3>
                                             <span class="recom-addonPro-type"> by Zebra</span>
                                             <p> Achieve operational efficiency, maximum comfort. Maximum durability and increased workforce productivity with the WT6000.... </p>
                                             <div class="recom-addonPro-rating">
                                                <h4> <i class="material-icons">star</i> 4.8 <span class="rating-count"> (16) </span></h4>
                                             </div>
                                             <div class="recom-addonPro-price">
                                                <span class="addonPro-price-text">Starting at:</span>
                                                <span class="addonPro-final-price">$6000</span>
                                             </div>
                                          </div>
                                       </div>
                                    </div> -->
                                     @if(!empty($product_related))
                                       @foreach($product_related as $relateditems)
                                    <div class="item">
                                       <div class="recommended-addon-pro-wrapper">
                                          <div class="recom-addonPro-img">
                                             <img src="{{asset('assets/images/products/'.$relateditems->photo)}}" class="img-fluid" alt="">
                                          </div>
                                          <div class="recom-addonPro-content">
                                             <h3 class="recom-addonPro-title"> @php $position=20; $post = substr($relateditems->name, 0, $position);echo $post; 
                                                  echo "...";@endphp </h3>
                                             <span class="recom-addonPro-type"> {{$brand->name}}</span>
                                             <p> @php $position=100; $post = substr($relateditems->overview, 0, $position);echo $post; 
                                                  echo "...";@endphp</p>
                                             <div class="recom-addonPro-rating">
                                                <h4> <i class="material-icons">star</i> 4.8 <span class="rating-count"> (16) </span></h4>
                                             </div>
                                             <div class="recom-addonPro-price">
                                                <span class="addonPro-price-text">Starting at:</span>
                                                <span class="addonPro-final-price">{{$relateditems->price_start_range}}</span>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    @endforeach
                                    @endif
                                    <!-- <div class="item">
                                       <div class="recommended-addon-pro-wrapper">
                                          <div class="recom-addonPro-img">
                                             <img src="{{asset('assets/images/addon-product.jpg')}}" class="img-fluid" alt="">
                                          </div>
                                          <div class="recom-addonPro-content">
                                             <h3 class="recom-addonPro-title"> Núria Moura </h3>
                                             <span class="recom-addonPro-type"> Freelancer</span>
                                             <p> I will do any salesforce configuration, development, data migration and standout or customer development in salesforce and… </p>
                                             <div class="recom-addonPro-rating">
                                                <h4> <i class="material-icons">star</i> 4.8 <span class="rating-count"> (16) </span></h4>
                                             </div>
                                             <div class="recom-addonPro-price">
                                                <span class="addonPro-price-text">Starting at:</span>
                                                <span class="addonPro-final-price">$4.72</span>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div class="item">
                                       <div class="recommended-addon-pro-wrapper">
                                          <div class="recom-addonPro-img">
                                             <img src="{{asset('assets/images/addon-product.jpg')}}" class="img-fluid" alt="">
                                          </div>
                                          <div class="recom-addonPro-content">
                                             <h3 class="recom-addonPro-title"> Núria Moura </h3>
                                             <span class="recom-addonPro-type"> Freelancer</span>
                                             <p> I will do any salesforce configuration, development, data migration and standout or customer development in salesforce and… </p>
                                             <div class="recom-addonPro-rating">
                                                <h4> <i class="material-icons">star</i> 4.8 <span class="rating-count"> (16) </span></h4>
                                             </div>
                                             <div class="recom-addonPro-price">
                                                <span class="addonPro-price-text">Starting at:</span>
                                                <span class="addonPro-final-price">$4.72</span>
                                             </div>
                                          </div>
                                       </div>
                                    </div -->
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div class="col-lg-4"> </div>
                  </div>
               </div>
               <div role="tabpanel" class="tab-pane" id="proFeature-tab">
             
                                 

                   <div class="row">
                      <div class="col-md-4">
                         <ul class="product-feature-left-tab position-sticky sticky-top">

                           @if(!empty($module_names))

                            @php 
                          $i = 0; 
                            @endphp
                            @foreach ($module_names as $key => $module_name) 
                            @php
                            if($i==0){
                            $class = "active";
                            }else{
                            $class = "";
                            }
                            $rel_module_name = str_replace(" ","-",$module_name);
                            @endphp
                              <li class="nav-link" rel="{{$rel_module_name }}">{{$module_name}} </li>
                            @endforeach
                            @endif
                         </ul>
                      </div>
                      <div class="col-md-8">
                         <div class="tab_container product-feature-right-tab-content">
                            @if(!empty($module_names))
                            @php 
                            $j = 0;
                            @endphp
                            @foreach ($module_names as $key => $module_name) 
                            @php
                            $rel_module_name = str_replace(" ","-",$module_name);
                            @endphp       
                            <h3 class="d_active tab_drawer_heading" rel="{{$rel_module_name}}">{{$module_name}}</h3>
                            <div id="{{$rel_module_name}}" class="tab-content">
                               <!-- <h2>{{$module_name}} content</h2> -->
                               <p>  @php
                                  $sectionName = SubCategoryController::getSectionNameFront($module_name,$productt->id);
                                  @endphp 
                                  @foreach($sectionName as $key=>$val)
                                  @foreach(json_decode($val) as $sec)
                               <div class="header-card-section">
                                  <div class="row">
                                     <div class="col-md-12">
                                        <h3 class="header-card">{{$sec->section_name}}</h3>
                                     </div>
                                  </div>
                               </div>
                               @php  $fName = SubCategoryController::getSFNameFront($module_name,$sec->section_name,$productt->id); @endphp
                               @foreach($fName as $key1=>$val1)
                               @foreach(json_decode($val1) as $fun)
                               @php  $fvalue = SubCategoryController::getFValFront($sec->section_name,$fun->function_name,$productt->id); @endphp
                               <div class="card">
                                  <div class="row">
                                     <div class="col-md-9 col-9">
                                        <p class="tab-title">{{$fun->function_name}}</p>
                                     </div>
                                     <div class="col-md-3 col-3">
                                        <div class="icon-box text-center">
                                           @if(!empty($fvalue[0]) && ($fvalue[0]->function_value == "Yes" || $fvalue[0]->function_value == "yes"))
                                           <i class="fa fa-check" aria-hidden="true"></i>
                                           @elseif (!empty($fvalue[0]) && ($fvalue[0]->function_value == "No" || $fvalue[0]->function_value == "no"))
                                           <i class="fa fa-times" aria-hidden="true" style="color:red;"></i>
                                           @elseif (!empty($fvalue[0]))
                                           <p>{{$fvalue[0]->function_value}}</p>
                                           @endif
                                        </div>
                                     </div>
                                  </div>
                               </div>
                               @endforeach
                               @endforeach
                               @endforeach
                               @endforeach</p>
                            </div>
                            @endforeach

                            @else
                              <div class="col-md-8">
                    
                             
                               Not found
                          
                          </div>
                            @endif
                            <!-- #tab1 -->
                            <!-- #tab4 --> 
                         </div>
                         <!-- .tab_container -->
                      </div>
                   </div>
                   
               </div>
               <div role="tabpanel" class="tab-pane" id="proPrice-tab">
                  <div class="row">
                    @if(!count($product_impl)==0)
                     <div class="col-md-12">
                        <!-- Nav tabs -->
                        <div class="tabs-nav singleProduct-pricing-tabs-wrap">
                           <ul class="nav nav-tabs" role="tablist">
                              <li role="presentation"><a href="#priceConfigurator-tab" class="active" aria-controls="priceConfigurator-tab" role="tab" data-toggle="tab"> <span>Price Configurator Product</span></a></li>
                              
                              <li role="presentation"><a href="#priceImplementation-tab" aria-controls="priceImplementation-tab" role="tab" data-toggle="tab"> <span> Price Implementation Product </span></a></li>
                              
                           </ul>
                        </div>
                      </div>
                      @endif
                      <div class="col-md-12">
                        <div class="tabs-contant-section">
                          <div class="tab-content">
                             <div role="tabpanel" class="tab-pane active show" id="priceConfigurator-tab">
                                <div class="pricing-table productPrice-table-wrap">
                                  <div class="table-responsive">
                                    @if(!empty($variationCount) && $variationCount == 1)
                                      <table class="table" cellspacing="4">
                                        <thead>
                                          <tr class="price-label">
                                            <th></th>
                                            <th><span class="btn btn-block btn-border btn-head">{{$variation1VariationName}}</span></th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                         @if(!empty($product_variants))
                                          @foreach ($product_variants as $product_variant)
                                            @if($product_variant->variation_count == 1 && $product_variant->varient_name !="Variation Name")
                                              <tr>
                                                <th>{{$product_variant->varient_name}}</th>
                                                <td>{{$product_variant->varient1}}</td>
                                              </tr>
                                            @endif
                                          @endforeach
                                         @endif
                                          
                                          @if($variationCount == 1)
                                            <tr class="price-button">
                                              <th></th>
                                              <td><a href="#" class="btn btn-block btn-primary">Add to Cart</a></td>
                                            </tr>
                                          @endif
                                        </tbody>
                                      </table>
                                    @endif

                                    @if($variationCount == 2)
                                      <table class="table">
                                        <thead>
                                          <tr class="price-label">
                                            <th></th>
                                            <th><span class="btn btn-block btn-border btn-head">{{$variation1VariationName}}</span></th>
                                            <th><span class="btn btn-block btn-border btn-head">{{$variation2VariationName}}</span></th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                          @foreach ($product_variants as $product_variant)
                                            @if($product_variant->variation_count == 2 && $product_variant->varient_name !="Variation Name")
                                              <tr>
                                                <th>{{$product_variant->varient_name}}</th>
                                                <td>{{$product_variant->varient1}}</td>
                                                <td>{{$product_variant->varient2}}</td>
                                              </tr>
                                            @endif
                                          @endforeach
                                          
                                          @if($variationCount == 2)
                                            <tr class="price-button">
                                              <th></th>
                                              <td><a href="#" class="btn btn-block btn-primary">Add to Cart</a></td>
                                              <td><a href="#" class="btn btn-block btn-primary">Add to Cart</a></td>
                                            </tr>
                                          @endif
                                        </tbody>
                                      </table>
                                    @endif

                                    @if($variationCount == 3)
                                      <table class="table" cellspacing="10" cellpadding="10" border="0">
                                        <thead>
                                          <tr class="price-label">
                                            <th></th>
                                            <th><span class="btn btn-block btn-border btn-head">{{$variation1VariationName}}</span></th>
                                            <th><span class="btn btn-block btn-border btn-head">{{$variation2VariationName}}</span></th>
                                            <th><span class="btn btn-block btn-border btn-head">{{$variation3VariationName}}</span></th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                          @foreach ($product_variants as $product_variant)
                                            @if($product_variant->variation_count == 3 && $product_variant->varient_name !="Variation Name" )
                                              <tr>
                                                <th>{{$product_variant->varient_name}}</th>
                                                <td>{{$product_variant->varient1}}</td>
                                                <td>{{$product_variant->varient2}}</td>
                                                <td>{{$product_variant->varient3}}</td>
                                              </tr>
                                            @endif
                                          @endforeach

                                          @if($variationCount == 3)
                                            <tr class="price-button">
                                              <th></th>
                                              <td><a href="#" class="btn btn-block btn-primary">Add to Cart</a></td>
                                              <td><a href="#" class="btn btn-block btn-primary">Add to Cart</a></td>
                                              <td><a href="#" class="btn btn-block btn-primary">Add to Cart</a></td>
                                            </tr>
                                          @endif
                                        </tbody>
                                      </table>
                                    @endif 
                                   </div>                   
                                </div>
                             </div>

                <div role="tabpanel" class="tab-pane" id="priceImplementation-tab">
                  <div class="pricing-table productPrice-table-wrap">
                    <div class="table-responsive">                      
                        <table class="table" cellspacing="10" cellpadding="10" border="0">
                          @if(!empty($product_impl) && $implvariationCount == 1)
                            <table class="table" cellspacing="4">
                              <thead>
                                <tr class="price-label">
                                  <th></th>
                                  <th><span class="btn btn-block btn-border btn-head">{{$impl1VariationName}}</span></th>
                                </tr>
                              </thead>
                              <tbody>
                                <th class="text-center">Price</th>
                                <td>{{$impl1Price}}</td>
                                @foreach ($product_impl as $product_impls)
                                  @if($product_impls->variation_count == 1 && $product_impls->varient_name !="Variation Name" && $product_impls->varient_name !="Implementation Price")
                                    <tr>
                                      <th>{{$product_impls->varient_name}}</th>
                                      <td>{{$product_impls->varient1}}</td>
                                    </tr>
                                  @endif
                                @endforeach
                               <tr class="price-button">
                                <th></th>
                                <td><a href="#" class="btn btn-block btn-primary">Add to Cart</a></td>
                               </tr>
                              </tbody>
                            </table>
                          @endif

                          @if(!empty($product_impl) && $implvariationCount == 2)
                            <table class="table">
                              <thead>
                                <tr class="price-label">
                                  <th></th>
                                  <th><span class="btn btn-block btn-border btn-head">{{$impl1VariationName}}</span></th>
                                  <th><span class="btn btn-block btn-border btn-head">{{$impl2VariationName}}</span></th>
                                </tr>
                              </thead>
                              <tbody>
                                <th class="text-center">Price</th>
                                <td>{{$impl1Price}}</td>
                                <td>{{$impl2Price}}</td>
                                @foreach ($product_variants as $product_variant)
                                  @if($product_impls->variation_count == 2 && $product_impls->varient_name !="Variation Name" && $product_impls->varient_name !="Implementation Price")
                                    <tr>
                                      <th>{{$product_variant->varient_name}}</th>
                                      <td>{{$product_variant->varient1}}</td>
                                      <td>{{$product_variant->varient2}}</td>
                                    </tr>
                                  @endif
                                @endforeach
                                
                                
                                  <tr class="price-button">
                                    <th></th>
                                    <td><a href="#" class="btn btn-block btn-primary">Add to Cart</a></td>
                                    <td><a href="#" class="btn btn-block btn-primary">Add to Cart</a></td>
                                  </tr>
                                
                              </tbody>
                            </table>
                      @endif

                      @if(!empty($product_impl) && $implvariationCount == 3)
                        <table class="table" cellspacing="10" cellpadding="10" border="0">
                          <thead>
                            <tr class="price-label">
                              <th></th>
                              <th><span class="btn btn-block btn-border btn-head">{{$impl1VariationName}}</span></th>
                              <th><span class="btn btn-block btn-border btn-head">{{$impl2VariationName}}</span></th>
                              <th><span class="btn btn-block btn-border btn-head">{{$impl3VariationName}}</span></th>
                            </tr>
                          </thead>
                          <tbody>
                            <th class="text-center">Price</th>
                                <td>{{$impl1Price}}</td>
                                <td>{{$impl2Price}}</td>
                                <td>{{$impl3Price}}</td>
                            @foreach ($product_impl as $product_impls)
                              @if($product_impls->variation_count == 3 && $product_impls->varient_name !="Variation Name" && $product_impls->varient_name !="Implementation Price")
                                <tr>
                                  
                                  <td>{{$product_impls->varient1}}</td>
                                  <td>{{$product_impls->varient2}}</td>
                                  <td>{{$product_impls->varient3}}</td>
                                </tr>
                              @endif
                            @endforeach

                            
                              <tr class="price-button">
                                <th></th>
                                <td><a href="#" class="btn btn-block btn-primary">Add to Cart</a></td>
                                <td><a href="#" class="btn btn-block btn-primary">Add to Cart</a></td>
                                <td><a href="#" class="btn btn-block btn-primary">Add to Cart</a></td>
                              </tr>
                            
                          </tbody>
                        </table>
                      @endif
                          
                     </table>

                      
                     </div>                   
                  </div>
                             </div>
                          </div>
                        </div>
                      </div>

                            
                  </div>
               </div>
               <div role="tabpanel" class="tab-pane" id="proSupport-tab">
                  <div class="pricing-table productPrice-table-wrap mt-0">
                    <div class="table-responsive">                      
                        <table class="table" cellspacing="10" cellpadding="10" border="0">
                          @if(!empty($product_support) && $supportvariationCount == 1)
                            <table class="table" cellspacing="4">
                              <thead>
                                <tr class="price-label">
                                  <th></th>
                                  <th><span class="btn btn-block btn-border btn-head">{{$Support1VariationName}}</span></th>
                                </tr>
                              </thead>
                              <tbody>
                                <th class="text-center">Price</th>
                                <td>{{$Support1Price}}</td>
                                @foreach ($product_support as $product_supports)
                                  @if($product_supports->variation_count == 1 && $product_supports->varient_name !="Variation Name" && $product_supports->varient_name !="Support Price")
                                    <tr>
                                      <th>{{$product_supports->varient_name}}</th>
                                      <td>{{$product_supports->varient1}}</td>
                                    </tr>
                                  @endif
                                @endforeach
                               <tr class="price-button">
                                <th></th>
                                <td><a href="#" class="btn btn-block btn-primary">Add to Cart</a></td>
                               </tr>
                              </tbody>
                            </table>
                          @endif

                          @if(!empty($product_support) && $supportvariationCount == 2)
                            <table class="table">
                              <thead>
                                <tr class="price-label">
                                  <th></th>
                                  <th><span class="btn btn-block btn-border btn-head">{{$Support1VariationName}}</span></th>
                                  <th><span class="btn btn-block btn-border btn-head">{{$Support2VariationName}}</span></th>
                                </tr>
                              </thead>
                              <tbody>
                                <th class="text-center">Price</th>
                                <td>{{$Support1Price}}</td>
                                <td>{{$Support2Price}}</td>
                                @foreach ($product_variants as $product_variant)
                                  @if($product_supports->variation_count == 2 && $product_supports->varient_name !="Variation Name" && $product_supports->varient_name !="Support Price")
                                    <tr>
                                      <th>{{$product_variant->varient_name}}</th>
                                      <td>{{$product_variant->varient1}}</td>
                                      <td>{{$product_variant->varient2}}</td>
                                    </tr>
                                  @endif
                                @endforeach
                                
                                
                                  <tr class="price-button">
                                    <th></th>
                                    <td><a href="#" class="btn btn-block btn-primary">Add to Cart</a></td>
                                    <td><a href="#" class="btn btn-block btn-primary">Add to Cart</a></td>
                                  </tr>
                                
                              </tbody>
                            </table>
                      @endif

                      @if(!empty($product_support) && $supportvariationCount == 3)
                        <table class="table" cellspacing="10" cellpadding="10" border="0">
                          <thead>
                            <tr class="price-label">
                              <th></th>
                              <th><span class="btn btn-block btn-border btn-head">{{$Support1VariationName}}</span></th>
                              <th><span class="btn btn-block btn-border btn-head">{{$Support2VariationName}}</span></th>
                              <th><span class="btn btn-block btn-border btn-head">{{$Support3VariationName}}</span></th>
                            </tr>
                          </thead>
                          <tbody>
                            <th class="text-center">Price</th>
                                <td>{{$Support1Price}}</td>
                                <td>{{$Support2Price}}</td>
                                <td>{{$Support3Price}}</td>
                            @foreach ($product_support as $product_supports)
                              @if($product_supports->variation_count == 3 && $product_supports->varient_name !="Variation Name" && $product_supports->varient_name !="Support Price")
                                <tr>
                                  
                                  <td>{{$product_supports->varient1}}</td>
                                  <td>{{$product_supports->varient2}}</td>
                                  <td>{{$product_supports->varient3}}</td>
                                </tr>
                              @endif
                            @endforeach

                            
                              <tr class="price-button">
                                <th></th>
                                <td><a href="#" class="btn btn-block btn-primary">Add to Cart</a></td>
                                <td><a href="#" class="btn btn-block btn-primary">Add to Cart</a></td>
                                <td><a href="#" class="btn btn-block btn-primary">Add to Cart</a></td>
                              </tr>
                            
                          </tbody>
                        </table>
                      @endif
                          
                     </table>

                      
                     </div>                   
                  </div>
                  
               </div>
            </div>
         </div>
      </div>
   </div>
</section>

{{-- MESSAGE MODAL --}}
<div class="message-modal">
   <div class="modal" id="vendorform" tabindex="-1" role="dialog" aria-labelledby="vendorformLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h5 class="modal-title" id="vendorformLabel">{{ $langg->lang118 }}</h5>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <div class="modal-body">
               <div class="container-fluid p-0">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="contact-form">
                           <form id="emailreply1">
                              {{csrf_field()}}
                              <ul>
                                 <li>
                                    <input type="text" class="input-field" id="subj1" name="subject"
                                       placeholder="{{ $langg->lang119}}" required="">
                                 </li>
                                 <li>
                                    <textarea class="input-field textarea" name="message" id="msg1"
                                       placeholder="{{ $langg->lang120 }}" required=""></textarea>
                                 </li>
                              </ul>
                              <button class="submit-btn" id="emlsub" type="submit">{{ $langg->lang118 }}</button>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   {{-- MESSAGE MODAL ENDS --}}
   @if(Auth::guard('web')->check())
   @if($productt->user_id != 0)
   {{-- MESSAGE VENDOR MODAL --}}
   <div class="modal" id="vendorform1" tabindex="-1" role="dialog" aria-labelledby="vendorformLabel1" aria-hidden="true">
      <div class="modal-dialog" role="document">
         <div class="modal-content">
            <div class="modal-header">
               <h5 class="modal-title" id="vendorformLabel1">{{ $langg->lang118 }}</h5>
               <button type="button" class="close" data-dismiss="modal" aria-label="Close">
               <span aria-hidden="true">&times;</span>
               </button>
            </div>
            <div class="modal-body">
               <div class="container-fluid p-0">
                  <div class="row">
                     <div class="col-md-12">
                        <div class="contact-form">
                           <form id="emailreply">
                              {{csrf_field()}}
                              <ul>
                                 <li>
                                    <input type="text" class="input-field" readonly=""
                                       placeholder="Send To {{ $productt->user->shop_name }}" readonly="">
                                 </li>
                                 <li>
                                    <input type="text" class="input-field" id="subj" name="subject"
                                       placeholder="{{ $langg->lang119}}" required="">
                                 </li>
                                 <li>
                                    <textarea class="input-field textarea" name="message" id="msg"
                                       placeholder="{{ $langg->lang120 }}" required=""></textarea>
                                 </li>
                                 <input type="hidden" name="email" value="{{ Auth::guard('web')->user()->email }}">
                                 <input type="hidden" name="name" value="{{ Auth::guard('web')->user()->name }}">
                                 <input type="hidden" name="user_id" value="{{ Auth::guard('web')->user()->id }}">
                                 <input type="hidden" name="vendor_id" value="{{ $productt->user->id }}">
                              </ul>
                              <button class="submit-btn" id="emlsub1" type="submit">{{ $langg->lang118 }}</button>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
   {{-- MESSAGE VENDOR MODAL ENDS --}}
   @endif
   @endif
    
</div>
@endsection
@section('scripts')
<script type="text/javascript">
   $(document).on("submit", "#emailreply1", function () {
     var token = $(this).find('input[name=_token]').val();
     var subject = $(this).find('input[name=subject]').val();
     var message = $(this).find('textarea[name=message]').val();
     $('#subj1').prop('disabled', true);
     $('#msg1').prop('disabled', true);
     $('#emlsub').prop('disabled', true);
     $.ajax({
       type: 'post',
       url: "{{URL::to('/user/admin/user/send/message')}}",
       data: {
         '_token': token,
         'subject': subject,
         'message': message,
       },
       success: function (data) {
         $('#subj1').prop('disabled', false);
         $('#msg1').prop('disabled', false);
         $('#subj1').val('');
         $('#msg1').val('');
         $('#emlsub').prop('disabled', false);
         if(data == 0)
           toastr.error("Oops Something Goes Wrong !!");
         else
           toastr.success("Message Sent !!");
         $('.close').click();
       }
   
     });
     return false;
   });
</script>
<script type="text/javascript">
   $(document).on("submit", "#emailreply", function () {
     var token = $(this).find('input[name=_token]').val();
     var subject = $(this).find('input[name=subject]').val();
     var message = $(this).find('textarea[name=message]').val();
     var email = $(this).find('input[name=email]').val();
     var name = $(this).find('input[name=name]').val();
     var user_id = $(this).find('input[name=user_id]').val();
     var vendor_id = $(this).find('input[name=vendor_id]').val();
     $('#subj').prop('disabled', true);
     $('#msg').prop('disabled', true);
     $('#emlsub').prop('disabled', true);
     $.ajax({
       type: 'post',
       url: "{{URL::to('/vendor/contact')}}",
       data: {
         '_token': token,
         'subject': subject,
         'message': message,
         'email': email,
         'name': name,
         'user_id': user_id,
         'vendor_id': vendor_id
       },
       success: function () {
         $('#subj').prop('disabled', false);
         $('#msg').prop('disabled', false);
         $('#subj').val('');
         $('#msg').val('');
         $('#emlsub').prop('disabled', false);
         toastr.success("{{ $langg->message_sent }}");
         $('.ti-close').click();
       }
     });
     return false;
   });
   
</script>
<script>
   $('.product-info-tabs').on('click', function(){
     $('.product-info-tabs.active').removeClass('active');
     $(this).addClass('active');
   });
   
   $('.product-info-module-section').on('click', function(){
     $('.product-info-module-section.active').removeClass('active');
     $(this).addClass('active');
   });
   
   
 
   
</script>
<!-- Demo Tab Js -->
<script type="text/javascript">
   // tabbed content
    // http://www.entheosweb.com/tutorials/css/tabs.asp
    $(".product-feature-right-tab-content .tab-content").hide();
    $(".product-feature-right-tab-content .tab-content:first").show();
   
   /* if in tab mode */
    $("ul.product-feature-left-tab li").click(function() {
    
      $(".product-feature-right-tab-content .tab-content").hide();
      var activeTab = $(this).attr("rel"); 
      $("#"+activeTab).fadeIn();    
    
      $("ul.product-feature-left-tab li").removeClass("active");
      $(this).addClass("active");
   
    $(".tab_drawer_heading").removeClass("d_active");
    $(".tab_drawer_heading[rel^='"+activeTab+"']").addClass("d_active");
    
    });
   /* if in drawer mode */
   $(".tab_drawer_heading").click(function() {
      
      $(".product-feature-right-tab-content .tab-content").hide();
      var d_activeTab = $(this).attr("rel"); 
      $("#"+d_activeTab).fadeIn();
    
    $(".tab_drawer_heading").removeClass("d_active");
      $(this).addClass("d_active");
    
    $("ul.product-feature-left-tab li").removeClass("active");
    $("ul.product-feature-left-tab li[rel^='"+d_activeTab+"']").addClass("active");
    });
   
   $('ul.product-feature-left-tab li').first().addClass("active");
   //$('.product-feature-right-tab-content .tab-content').first().css("display","block");
   /* Extra class "tab_last" 
     to add border to right side
     of last tab */
   // $('ul.product-feature-left-tab li').last().addClass("tab_last");
</script>
<script>
  $(window).load(function(){
   setTimeout(function(){
       $('#modal2').modal('show');
   }, 5000);
});
</script>

<script>
$(document).on('click','#wwwwww', function(){
   
      var url = $('#product_spec_sheet').val();
      var product_name = $('#current_url').val();
      var a = document.createElement('a');
      a.href = url;
      a.setAttribute('download', product_name+'- Specifications.pdf');
      a.click();
  })

</script>

<!-- Modal -->
<div class="modal fade" id="productVideo-modal" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-body">
         <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
         </button>
        <!-- 16:9 aspect ratio -->
         <div class="embed-responsive embed-responsive-16by9">
           <iframe class="embed-responsive-item" src="" id="embedvideo"  allowscriptaccess="always" allow="autoplay"></iframe>
         </div>        
      </div>
    </div>
  </div>
</div>
@endsection