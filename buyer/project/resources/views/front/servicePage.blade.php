@extends('layouts.front')

@section('content')

<div class="breadcrumb-area">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <ul class="pages">

          <li><a href="{{route('front.index')}}"><i class="fa fa-arrow-left" aria-hidden="true"></i>
</a></li>
          <li><a href="{{route('front.category',$productt->category->slug)}}">{{$productt->category->name}}</a></li>
          @if(!empty($productt->subcategory))
          <li><a
              href="{{ route('front.subcat',['slug1' => $productt->category->slug, 'slug2' => $productt->subcategory->slug]) }}">{{$productt->subcategory->name}}</a>
          </li>
          @endif
          @if(!empty($productt->childcategory))
          <li><a
              href="{{ route('front.childcat',['slug1' => $productt->category->slug, 'slug2' => $productt->subcategory->slug, 'slug3' => $productt->childcategory->slug]) }}">{{$productt->childcategory->name}}</a>
          </li>
          @endif
          <li><a href="{{ route('front.product', $productt->slug) }}">{{$productt->name}}</a>

        </ul>
      </div>
    </div>
  </div>
</div>

<!-- Product Details Area Start -->
<section class="product-details-page">
  <div class="container">
        <div class="row">
	        <div class="col-lg-4 col-md-12 pl-0">
            <div class="agency-profile-left-wrapper">
              <div class="agency-profile-img-rating-wrap">
  	          	<div class="agency-img-wrap">
  	          		<img class="img-fluid" src="{{asset('assets/images/oracle.jpg')}}"/>
  	          	</div>

                <div class="agency-profile-rating">
                  <div class="agency-rating-wrap">
                    <div class="agency-rating-star">
                      <i class="material-icons"> star </i>
                      <i class="material-icons"> star </i>
                      <i class="material-icons"> star </i>
                      <i class="material-icons"> star </i>
                      <i class="material-icons"> star </i>
                    </div>

                    <div class="agency-rating-content">
                      <span class="agency-profile-rate"> 4.8 </span>
                      <span class="agency-profile-totalReview"> (13 Reviews) </span>
                     </div>
                  </div>
                </div>                
              </div>

              <div class="agency-fullProfile-content">
                <div class="agency-name">
                  <h2> Oracle </h2>
                </div>

                <div class="agency-actionBtn-wrap">
                  <a href="" class="agencyContact-btn btn btn-hover"> Contact Me </a>
                  <a href="" class="agencyQuote-btn btn btn-hover"> Get a Quote </a>
                </div>

                <div class="agency-profile-des">
                  <h3 class="agency-des-title"> Description </h3>
                  <p> Nec feugiat in fermentum posuere urna nec tincidunt praesent. Ligula ullamcorper malesuada proin libero nunc consequat interdum. Fermentum odio eu feugiat pretium nibh ipsum consequat nisl. Ornare lectus sit amet est placerat in. Neque volutpat ac tincidunt vitae. Nec feugiat in fermentum posuere urna nec tincidunt praesent. Ligula ullamcorper malesuada proin libero nunc consequat interdum. Fermentum odio eu feugiat pretium nibh ipsum. </p>
                </div>

                <div class="agency-sameInfo">
                  <h3 class="agency-someInfo-title"> Some information </h3>
                  <p> English - Native/Bilingual</p>
                </div>

                <div class="agency-sameInfo-about">
                  <h3 class="agency-aboutInfo-title"> Some information about the Company </h3>
                  <p> Associate - Bachalars Of Computer Science </p>
                  <p> Certificate - Masters of Computer Science </p>
                </div>
              </div>
            </div>
  	    	</div>

          <div class="col-lg-8 pr-0">
            <div class="row">
              <div class="col-lg-12">
                <div class="page-main-title">
                  <h2 class="seciton-head"> Services </h2>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-lg-4 col-md-4 col-6">
                  <div class="product-listing-grid agencyPro-grid-wrap">
                    <div class="item-img product-listing-img">
                      <a href="">
                        <img class="img-fluid m-auto d-block" src="{{asset('assets/images/thumbnails/15679436294FroxW2e.jpg')}}" alt="">
                      </a>
                    </div>
                    <div class="info prodct-grid-content agency-pro-grid-content">
                        <p class="proVendor-name mt-0"> Placeholder Vendor Name Goes Here </p>
                        <div class="agency-category-rating">
                          <h5 class="agency-category">Business Inteligence</h5>
                          <div class="agency-rating-wrapper">
                            <div class="rating-box">
                              <span class="rating-star">
                                <i class="material-icons"> star </i>
                              </span>
                              <span class="rating-number"> 4.8 </span>
                            </div>
                          </div>                  
                        </div>
                        <div class="agency-seeOffer-btn">
                            <a href="" class="seeOfferBtn btn btn-hover"> See Offer</a>
                          </div>
                    </div>
                  </div>
              </div>

              <div class="col-lg-4 col-md-4 col-6">
                  <div class="product-listing-grid agencyPro-grid-wrap">
                    <div class="item-img product-listing-img">
                      <a href="">
                        <img class="img-fluid m-auto d-block" src="{{asset('assets/images/thumbnails/15679436294FroxW2e.jpg')}}" alt="">
                      </a>
                    </div>
                    <div class="info prodct-grid-content agency-pro-grid-content">
                        <p class="proVendor-name mt-0"> Placeholder Vendor Name Goes Here </p>
                        <div class="agency-category-rating">
                          <h5 class="agency-category">Business Inteligence</h5>
                          <div class="agency-rating-wrapper">
                            <div class="rating-box">
                              <span class="rating-star">
                                <i class="material-icons"> star </i>
                              </span>
                              <span class="rating-number"> 4.8 </span>
                            </div>
                          </div>                  
                        </div>
                        <div class="agency-seeOffer-btn">
                            <a href="" class="seeOfferBtn btn btn-hover"> See Offer</a>
                          </div>
                    </div>
                  </div>
              </div>

              <div class="col-lg-4 col-md-4 col-6">
                  <div class="product-listing-grid agencyPro-grid-wrap">
                    <div class="item-img product-listing-img">
                      <a href="">
                        <img class="img-fluid m-auto d-block" src="{{asset('assets/images/thumbnails/15679436294FroxW2e.jpg')}}" alt="">
                      </a>
                    </div>
                    <div class="info prodct-grid-content agency-pro-grid-content">
                        <p class="proVendor-name mt-0"> Placeholder Vendor Name Goes Here </p>
                        <div class="agency-category-rating">
                          <h5 class="agency-category">Business Inteligence</h5>
                          <div class="agency-rating-wrapper">
                            <div class="rating-box">
                              <span class="rating-star">
                                <i class="material-icons"> star </i>
                              </span>
                              <span class="rating-number"> 4.8 </span>
                            </div>
                          </div>                  
                        </div>
                        <div class="agency-seeOffer-btn">
                            <a href="" class="seeOfferBtn btn btn-hover"> See Offer</a>
                          </div>
                    </div>
                  </div>
              </div>

              <div class="col-lg-4 col-md-4 col-6">
                  <div class="product-listing-grid agencyPro-grid-wrap">
                    <div class="item-img product-listing-img">
                      <a href="">
                        <img class="img-fluid m-auto d-block" src="{{asset('assets/images/thumbnails/15679436294FroxW2e.jpg')}}" alt="">
                      </a>
                    </div>
                    <div class="info prodct-grid-content agency-pro-grid-content">
                        <p class="proVendor-name mt-0"> Placeholder Vendor Name Goes Here </p>
                        <div class="agency-category-rating">
                          <h5 class="agency-category">Business Inteligence</h5>
                          <div class="agency-rating-wrapper">
                            <div class="rating-box">
                              <span class="rating-star">
                                <i class="material-icons"> star </i>
                              </span>
                              <span class="rating-number"> 4.8 </span>
                            </div>
                          </div>                  
                        </div>
                        <div class="agency-seeOffer-btn">
                            <a href="" class="seeOfferBtn btn btn-hover"> See Offer</a>
                          </div>
                    </div>
                  </div>
              </div>

              <div class="col-lg-4 col-md-4 col-6">
                  <div class="product-listing-grid agencyPro-grid-wrap">
                    <div class="item-img product-listing-img">
                      <a href="">
                        <img class="img-fluid m-auto d-block" src="{{asset('assets/images/thumbnails/15679436294FroxW2e.jpg')}}" alt="">
                      </a>
                    </div>
                    <div class="info prodct-grid-content agency-pro-grid-content">
                        <p class="proVendor-name mt-0"> Placeholder Vendor Name Goes Here </p>
                        <div class="agency-category-rating">
                          <h5 class="agency-category">Business Inteligence</h5>
                          <div class="agency-rating-wrapper">
                            <div class="rating-box">
                              <span class="rating-star">
                                <i class="material-icons"> star </i>
                              </span>
                              <span class="rating-number"> 4.8 </span>
                            </div>
                          </div>                  
                        </div>
                        <div class="agency-seeOffer-btn">
                            <a href="" class="seeOfferBtn btn btn-hover"> See Offer</a>
                          </div>
                    </div>
                  </div>
              </div>

              <div class="col-lg-4 col-md-4 col-6">
                  <div class="product-listing-grid agencyPro-grid-wrap">
                    <div class="item-img product-listing-img">
                      <a href="">
                        <img class="img-fluid m-auto d-block" src="{{asset('assets/images/thumbnails/15679436294FroxW2e.jpg')}}" alt="">
                      </a>
                    </div>
                    <div class="info prodct-grid-content agency-pro-grid-content">
                        <p class="proVendor-name mt-0"> Placeholder Vendor Name Goes Here </p>
                        <div class="agency-category-rating">
                          <h5 class="agency-category">Business Inteligence</h5>
                          <div class="agency-rating-wrapper">
                            <div class="rating-box">
                              <span class="rating-star">
                                <i class="material-icons"> star </i>
                              </span>
                              <span class="rating-number"> 4.8 </span>
                            </div>
                          </div>                  
                        </div>
                        <div class="agency-seeOffer-btn">
                            <a href="" class="seeOfferBtn btn btn-hover"> See Offer</a>
                          </div>
                    </div>
                  </div>
              </div>

              <div class="col-lg-4 col-md-4 col-6">
                  <div class="product-listing-grid agencyPro-grid-wrap">
                    <div class="item-img product-listing-img">
                      <a href="">
                        <img class="img-fluid m-auto d-block" src="{{asset('assets/images/thumbnails/15679436294FroxW2e.jpg')}}" alt="">
                      </a>
                    </div>
                    <div class="info prodct-grid-content agency-pro-grid-content">
                        <p class="proVendor-name mt-0"> Placeholder Vendor Name Goes Here </p>
                        <div class="agency-category-rating">
                          <h5 class="agency-category">Business Inteligence</h5>
                          <div class="agency-rating-wrapper">
                            <div class="rating-box">
                              <span class="rating-star">
                                <i class="material-icons"> star </i>
                              </span>
                              <span class="rating-number"> 4.8 </span>
                            </div>
                          </div>                  
                        </div>
                        <div class="agency-seeOffer-btn">
                            <a href="" class="seeOfferBtn btn btn-hover"> See Offer</a>
                          </div>
                    </div>
                  </div>
              </div>

              <div class="col-lg-4 col-md-4 col-6">
                  <div class="product-listing-grid agencyPro-grid-wrap">
                    <div class="item-img product-listing-img">
                      <a href="">
                        <img class="img-fluid m-auto d-block" src="{{asset('assets/images/thumbnails/15679436294FroxW2e.jpg')}}" alt="">
                      </a>
                    </div>
                    <div class="info prodct-grid-content agency-pro-grid-content">
                        <p class="proVendor-name mt-0"> Placeholder Vendor Name Goes Here </p>
                        <div class="agency-category-rating">
                          <h5 class="agency-category">Business Inteligence</h5>
                          <div class="agency-rating-wrapper">
                            <div class="rating-box">
                              <span class="rating-star">
                                <i class="material-icons"> star </i>
                              </span>
                              <span class="rating-number"> 4.8 </span>
                            </div>
                          </div>                  
                        </div>
                        <div class="agency-seeOffer-btn">
                            <a href="" class="seeOfferBtn btn btn-hover"> See Offer</a>
                          </div>
                    </div>
                  </div>
              </div>

              <div class="col-lg-4 col-md-4 col-6">
                  <div class="product-listing-grid agencyPro-grid-wrap">
                    <div class="item-img product-listing-img">
                      <a href="">
                        <img class="img-fluid m-auto d-block" src="{{asset('assets/images/thumbnails/15679436294FroxW2e.jpg')}}" alt="">
                      </a>
                    </div>
                    <div class="info prodct-grid-content agency-pro-grid-content">
                        <p class="proVendor-name mt-0"> Placeholder Vendor Name Goes Here </p>
                        <div class="agency-category-rating">
                          <h5 class="agency-category">Business Inteligence</h5>
                          <div class="agency-rating-wrapper">
                            <div class="rating-box">
                              <span class="rating-star">
                                <i class="material-icons"> star </i>
                              </span>
                              <span class="rating-number"> 4.8 </span>
                            </div>
                          </div>                  
                        </div>
                        <div class="agency-seeOffer-btn">
                            <a href="" class="seeOfferBtn btn btn-hover"> See Offer</a>
                          </div>
                    </div>
                  </div>
              </div>
            </div>
          </div>
	        
	    </div>
  
  </div>
<!-- Tranding Item Area End -->
</section>
<!-- Product Details Area End -->




@endsection
