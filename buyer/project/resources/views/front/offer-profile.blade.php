@extends('layouts.front')
@section('content')
<section class="offer-page-tab d-none d-md-block">
   <div class="container">
      <div class="row">
         <div class="col-xs-12 ">
            <nav>
               <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                  <a class="nav-item nav-link active" id="nav-home-tab" href="#overview">Overview</a>
                  <a class="nav-item nav-link" id="nav-profile-tab" href="#nav-profile">Description</a>
                  <a class="nav-item nav-link" id="nav-contact-tab" href="#about-seller">About The Seller</a>
                  <a class="nav-item nav-link" id="nav-about-tab" href="#compare-packages">Compare Packages</a>
                  <!-- <a class="nav-item nav-link" id="nav-about-tab" href="#recommendations">Recommendations</a>
                     <a class="nav-item nav-link" id="nav-about-tab" href="#other-services">Other Services</a>
                     <a class="nav-item nav-link" id="nav-about-tab" href="#reviews" >Reviews</a> -->
               </div>
            </nav>
         </div>
      </div>
   </div>
</section>
<!-- offer Area Start -->
<section class="offer-area-page">
   <div class="container">
      <div class="row">
         <div class="col-lg-8">
            <!-- Breadcrumb Area Start -->
            <div class="breadcrumb-area">
               <div class="container">
                 <div class="row">
                     <ul class="pages">
               
                       <li><a href="{{route('front.index')}}"><i class="fa fa-arrow-left" aria-hidden="true"></i>
                       </a></li>
                       <li><a href="#">Services</a></li>
                       <li><a href="#">Support</a>
                       </li>
                       <li><a href="#">Provider List</a>
                       </li>
                       <li><a href="#">Nuri Moura Microsoft Dynamics GP Support</a>
               
                     </ul>
                 </div>
               </div>
            </div>
            <!-- Breadcrumb Area End -->
            <div class="single-offer-section">
               <div class="single-offer-right-area" id="overview">
                  <div class="offer-right-info">
                     <h4 class="offer-name-block"><span>Support:</span> {{$productt->name}}</h4>
                     <div class="offer-logo d-flex">
                        <img class="offer-logo-img" src="{{asset('assets/images/products/'.$productt->provider_pic) }}" height="40" width="40">
                        <h4 class="primary">{{$productt->provider_type}} </h4>
                        |  
                        <h4>{{$productt->provide_name}}</h4>
                        |
                        <h4>4.8 <span class="d-none d-md-inline-block"><i class="material-icons">star</i>
                           <i class="material-icons">star</i>
                           <i class="material-icons">star</i>
                           <i class="material-icons">star</i>
                           <i class="material-icons">star</i></span>
                        </h4>
                     </div>
                     <div class="offer_info" id="about-service">
                        <h3>About this Service</h3>
                     </div>
                     <div class="short-description">
                        <p>{{$productt->overview}}</p>
                     </div>
                     <div class="productSingle-gallery-slider-wrap">
                        <div class="row">
                           <div class="col-md-12">
                              <div class="productSingle-gallery-carousel owl-carousel">
                                 <div class="item">
                                    <div class="productGallery-item">
                                       <img src="{{asset('assets/images/product-gallery.jpg')}}" class="img-fluid m-auto d-block">
                                    </div>
                                 </div>
                                 <div class="item">
                                    <div class="productGallery-item">
                                       <img src="{{asset('assets/images/product-gallery.jpg')}}" class="img-fluid m-auto d-block">
                                    </div>
                                 </div>
                                 <div class="item">
                                    <div class="productGallery-item">
                                       <img src="{{asset('assets/images/product-gallery.jpg')}}" class="img-fluid m-auto d-block">
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <!-- <div class="offer-video-slide">
                        <video width="100%" height="530" controls>
                          <source src="mov_bbb.mp4" type="video/mp4">
                          <source src="mov_bbb.ogg" type="video/ogg">
                        </video>
                        </div>  -->
                     <div class="about-seller" id="about-seller">
                        <div class="offer_info">
                           <h3>About the Seller</h3>
                        </div>
                        <div class="row">
                           <div class="col-lg-3 col-md-4 col-4">
                              <img class="offer-logo-img mb-5 img-fluid" src="{{asset('assets/images/products/'.$productt->provider_pic) }}" alt="user">
                           </div>
                           <div class="col-lg-9 col-md-8 col-8">
                              <h3>{{$productt->provide_name}} <span class="primary">(Online)</span></h3>
                              <h4>{{$productt->provider_type}}</h4>
                              <p class="reating d-none d-md-block">
                                 <i class="material-icons">star</i>
                                 <i class="material-icons">star</i>
                                 <i class="material-icons">star</i>
                                 <i class="material-icons">star</i>
                                 <i class="material-icons">star</i>
                                 4.8 <span style="font-family: 'Gotham Rounded Book', sans-serif;">(13 Reviews)</span>
                              </p>
                              <a href="#" class="btn btn-border btn-contact">Contact Me</a>
                           </div>
                        </div>
                        <div class="card">
                           <div class="row">
                              <div class="col-lg-3 col-md-6 col-6">
                                 <span class="from">From:</span>
                                 <h5>{{$productt->provider_from}}</h5>
                              </div>
                              <div class="col-lg-3 col-md-6 col-6">
                                 <span class="from">{{$productt->provider_membersince}}</span>
                                 <h5>MAY 2019</h5>
                              </div>
                              <div class="col-lg-3 col-md-6 col-6">
                                 <span class="from">Avg. Response Time</span>
                                 <h5>8 HOURS</h5>
                              </div>
                              <div class="col-lg-3 col-md-6 text-right col-6">
                                 <span class="from">Recent Delivery</span>
                                 <h5>6 DAYS</h5>
                              </div>
                           </div>
                        </div>
                        <div class="card">
                           <p>{{$productt->provider_description}}</p>
                        </div>
                     </div>
                  </div>
                  <div class="offer_info" id="compare-packages">
                     <h3>Compare Packages</h3>
                  </div>
                  <div class="offer-table table-responsive">
                     <table class="table table-condensed border">
                        <tbody>
                           <tr>
                              <td>Package</td>
                              @if($variationCount == 1)
                              <td>
                                 <h2>${{$variation1Price}}</h2>
                                 <h3>{{$variation1VariationName}}</h3>
                                 <!-- <h5>SERVICE 1 + SERVICE 2 + SERVICE 3</h5> -->
                                 <p>{{$variation1Description}}</p>
                              </td>
                              @elseif($variationCount == 2)
                              <td>
                                 <h2>${{$variation1Price}}</h2>
                                 <h3>{{$variation1VariationName}}</h3>
                                 <!-- <h5>SERVICE 1 + SERVICE 2 + SERVICE 3</h5> -->
                                 <p>{{$variation1Description}}</p>
                              </td>
                              <td>
                                 <h2>${{$variation2Price}}</h2>
                                 <h3>{{$variation2VariationName}}</h3>
                                 <!-- <h5>SERVICE 1 + SERVICE 2 + SERVICE 3</h5> -->
                                 <p>{{$variation2Description}}</p>
                              </td>
                              @elseif($variationCount == 3)
                              <td>
                                 <h2>${{$variation1Price}}</h2>
                                 <h3>{{$variation1VariationName}}</h3>
                                 <!-- <h5>SERVICE 1 + SERVICE 2 + SERVICE 3</h5> -->
                                 <p>{{$variation1Description}}</p>
                              </td>
                              <td>
                                 <h2>${{$variation2Price}}</h2>
                                 <h3>{{$variation2VariationName}}</h3>
                                 <!-- <h5>SERVICE 1 + SERVICE 2 + SERVICE 3</h5> -->
                                 <p>{{$variation2Description}}</p>
                              </td>
                              <td>
                                 <h2>${{$variation3Price}}</h2>
                                 <h3>{{$variation3VariationName}}</h3>
                                 <!-- <h5>SERVICE 1 + SERVICE 2 + SERVICE 3</h5> -->
                                 <p>{{$variation3Description}}</p>
                              </td>
                              @endif
                           </tr>
                           @foreach($product_variants as $product_variant)
                           @if($product_variant->variation_count == 1)
                           @if($product_variant->varient_name != 'Description')
                           <tr>
                              <td>{{$product_variant->varient_name}}</td>
                              <td>
                                 <p>{{$product_variant->varient1}}</p>
                              </td>
                           </tr>
                           @endif
                           @elseif($product_variant->variation_count == 2)
                           @if($product_variant->varient_name != 'Description')
                           <tr>
                              <td>{{$product_variant->varient_name}}</td>
                              <td>
                                 <p>{{$product_variant->varient1}}</p>
                              </td>
                              <td>
                                 <p>{{$product_variant->varient2}}</p>
                              </td>
                           </tr>
                           @endif
                           @elseif($product_variant->variation_count == 3)
                           @if($product_variant->varient_name != 'Description')
                           <tr>
                              <td>{{$product_variant->varient_name}}</td>
                              <td>
                                 <p>{{$product_variant->varient1}}</p>
                              </td>
                              <td>
                                 <p>{{$product_variant->varient2}}</p>
                              </td>
                              <td>
                                 <p>{{$product_variant->varient3}}</p>
                              </td>
                           </tr>
                           @endif
                           @endif
                           @endforeach
                           <tr>
                              <td> </td>
                              @if($variationCount == 1)
                              <td><a class="btn btn-success btn-block" href="#"> Select</a></td>
                              @elseif($variationCount == 2)
                              <td><a class="btn btn-success btn-block" href="#"> Select</a></td>
                              <td><a class="btn btn-success btn-block" href="#"> Select</a></td>
                              @elseif($variationCount == 3)
                              <td><a class="btn btn-success btn-block" href="#"> Select</a></td>
                              <td><a class="btn btn-success btn-block" href="#"> Select</a></td>
                              <td><a class="btn btn-success btn-block" href="#"> Select</a></td>
                              @endif
                           </tr>
                        </tbody>
                     </table>
                  </div>
                  <div class="recommended-addons-slider-wrap">
                     <div class="row">
                        <div class="col-md-12">
                           <h2 class="recommended-title"> Recommended Addons </h2>
                        </div>
                        <div class="col-md-12">
                           <div class="recommended-carousel owl-carousel">
                              <div class="item">
                                 <div class="recommended-addon-pro-wrapper">
                                    <div class="recom-addonPro-img">
                                       <img src="{{asset('assets/images/addon-product.jpg')}}" class="img-fluid" alt="">
                                    </div>
                                    <div class="recom-addonPro-content">
                                       <h3 class="recom-addonPro-title"> Núria Moura </h3>
                                       <span class="recom-addonPro-type"> Freelancer</span>
                                       <p> I will do any salesforce configuration, development, data migration and standout or customer development in salesforce and… </p>
                                       <div class="recom-addonPro-rating">
                                          <h4> <i class="material-icons">star</i> 4.8 <span class="rating-count"> (16) </span></h4>
                                       </div>
                                       <div class="recom-addonPro-price">
                                          <span class="addonPro-price-text">Starting at:</span>
                                          <span class="addonPro-final-price">$4.72</span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="item">
                                 <div class="recommended-addon-pro-wrapper">
                                    <div class="recom-addonPro-img">
                                       <img src="{{asset('assets/images/addon-product.jpg')}}" class="img-fluid" alt="">
                                    </div>
                                    <div class="recom-addonPro-content">
                                       <h3 class="recom-addonPro-title"> Núria Moura </h3>
                                       <span class="recom-addonPro-type"> Freelancer</span>
                                       <p> I will do any salesforce configuration, development, data migration and standout or customer development in salesforce and… </p>
                                       <div class="recom-addonPro-rating">
                                          <h4> <i class="material-icons">star</i> 4.8 <span class="rating-count"> (16) </span></h4>
                                       </div>
                                       <div class="recom-addonPro-price">
                                          <span class="addonPro-price-text">Starting at:</span>
                                          <span class="addonPro-final-price">$4.72</span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="item">
                                 <div class="recommended-addon-pro-wrapper">
                                    <div class="recom-addonPro-img">
                                       <img src="{{asset('assets/images/addon-product.jpg')}}" class="img-fluid" alt="">
                                    </div>
                                    <div class="recom-addonPro-content">
                                       <h3 class="recom-addonPro-title"> Núria Moura </h3>
                                       <span class="recom-addonPro-type"> Freelancer</span>
                                       <p> I will do any salesforce configuration, development, data migration and standout or customer development in salesforce and… </p>
                                       <div class="recom-addonPro-rating">
                                          <h4> <i class="material-icons">star</i> 4.8 <span class="rating-count"> (16) </span></h4>
                                       </div>
                                       <div class="recom-addonPro-price">
                                          <span class="addonPro-price-text">Starting at:</span>
                                          <span class="addonPro-final-price">$4.72</span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="item">
                                 <div class="recommended-addon-pro-wrapper">
                                    <div class="recom-addonPro-img">
                                       <img src="{{asset('assets/images/addon-product.jpg')}}" class="img-fluid" alt="">
                                    </div>
                                    <div class="recom-addonPro-content">
                                       <h3 class="recom-addonPro-title"> Núria Moura </h3>
                                       <span class="recom-addonPro-type"> Freelancer</span>
                                       <p> I will do any salesforce configuration, development, data migration and standout or customer development in salesforce and… </p>
                                       <div class="recom-addonPro-rating">
                                          <h4> <i class="material-icons">star</i> 4.8 <span class="rating-count"> (16) </span></h4>
                                       </div>
                                       <div class="recom-addonPro-price">
                                          <span class="addonPro-price-text">Starting at:</span>
                                          <span class="addonPro-final-price">$4.72</span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="recommended-addons-slider-wrap otherServices-slider">
                     <div class="row">
                        <div class="col-md-12">
                           <h2 class="recommended-title"> Other Services </h2>
                        </div>
                        <div class="col-md-12">
                           <div class="recommended-carousel owl-carousel">
                              <div class="item">
                                 <div class="recommended-addon-pro-wrapper">
                                    <div class="recom-addonPro-img">
                                       <img src="{{asset('assets/images/addon-product.jpg')}}" class="img-fluid" alt="">
                                    </div>
                                    <div class="recom-addonPro-content">
                                       <h3 class="recom-addonPro-title"> Núria Moura </h3>
                                       <span class="recom-addonPro-type"> Freelancer</span>
                                       <p> I will do any salesforce configuration, development, data migration and standout or customer development in salesforce and… </p>
                                       <div class="recom-addonPro-rating">
                                          <h4> <i class="material-icons">star</i> 4.8 <span class="rating-count"> (16) </span></h4>
                                       </div>
                                       <div class="recom-addonPro-price">
                                          <span class="addonPro-price-text">Starting at:</span>
                                          <span class="addonPro-final-price">$4.72</span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="item">
                                 <div class="recommended-addon-pro-wrapper">
                                    <div class="recom-addonPro-img">
                                       <img src="{{asset('assets/images/addon-product.jpg')}}" class="img-fluid" alt="">
                                    </div>
                                    <div class="recom-addonPro-content">
                                       <h3 class="recom-addonPro-title"> Núria Moura </h3>
                                       <span class="recom-addonPro-type"> Freelancer</span>
                                       <p> I will do any salesforce configuration, development, data migration and standout or customer development in salesforce and… </p>
                                       <div class="recom-addonPro-rating">
                                          <h4> <i class="material-icons">star</i> 4.8 <span class="rating-count"> (16) </span></h4>
                                       </div>
                                       <div class="recom-addonPro-price">
                                          <span class="addonPro-price-text">Starting at:</span>
                                          <span class="addonPro-final-price">$4.72</span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="item">
                                 <div class="recommended-addon-pro-wrapper">
                                    <div class="recom-addonPro-img">
                                       <img src="{{asset('assets/images/addon-product.jpg')}}" class="img-fluid" alt="">
                                    </div>
                                    <div class="recom-addonPro-content">
                                       <h3 class="recom-addonPro-title"> Núria Moura </h3>
                                       <span class="recom-addonPro-type"> Freelancer</span>
                                       <p> I will do any salesforce configuration, development, data migration and standout or customer development in salesforce and… </p>
                                       <div class="recom-addonPro-rating">
                                          <h4> <i class="material-icons">star</i> 4.8 <span class="rating-count"> (16) </span></h4>
                                       </div>
                                       <div class="recom-addonPro-price">
                                          <span class="addonPro-price-text">Starting at:</span>
                                          <span class="addonPro-final-price">$4.72</span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div class="item">
                                 <div class="recommended-addon-pro-wrapper">
                                    <div class="recom-addonPro-img">
                                       <img src="{{asset('assets/images/addon-product.jpg')}}" class="img-fluid" alt="">
                                    </div>
                                    <div class="recom-addonPro-content">
                                       <h3 class="recom-addonPro-title"> Núria Moura </h3>
                                       <span class="recom-addonPro-type"> Freelancer</span>
                                       <p> I will do any salesforce configuration, development, data migration and standout or customer development in salesforce and… </p>
                                       <div class="recom-addonPro-rating">
                                          <h4> <i class="material-icons">star</i> 4.8 <span class="rating-count"> (16) </span></h4>
                                       </div>
                                       <div class="recom-addonPro-price">
                                          <span class="addonPro-price-text">Starting at:</span>
                                          <span class="addonPro-final-price">$4.72</span>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="service-product-review-wrap d-none d-md-block">
                     <h3 class="">Reviews <i class="material-icons">star</i> <span>4.8</span> <span class="service-product-review-count">(16)</span> </h3>
                     <div class="service-pro-rating-type">
                        <div class="rating-type-item">
                           <h5>Seller commmunication level </h5>
                           <div class="serviePro-type-rate"> <span><i class="material-icons">star</i> 4.8 </span> </div>
                        </div>
                        <div class="rating-type-item">
                           <h5> Recommend to a friend </h5>
                           <div class="serviePro-type-rate"> <span><i class="material-icons">star</i> 4.8 </span> </div>
                        </div>
                        <div class="rating-type-item">
                           <h5> Service as described </h5>
                           <div class="serviePro-type-rate"> <span><i class="material-icons">star</i> 4.8 </span> </div>
                        </div>
                     </div>
                  </div>
                  <div class="service-product-rate-review-block d-none d-md-block">
                     <div class="reviewer-imgAndName-wrap">
                        <div class="reviewer-img">
                           <img src="{{asset('assets/images/woman.jpg')}}" alt="Image" class="img-fluid">
                        </div>
                        <div class="reviewer-name-rating">
                           <span class="revieweName"> Núria Moura </span>
                           <span> | </span>
                           <span><i class="material-icons">star</i></span>
                        </div>
                     </div>
                     <div class="reviewer-content">
                        <p> Nec feugiat in fermentum posuere urna nec tincidunt praesent. Ligula ullamcorper malesuada proin libero nunc consequat interdum. Fermentum odio eu feugiat pretium nibh ipsum consequat nisl. Ornare lectus sit amet est placerat in. Neque volutpat ac tincidunt vitae. Nec feugiat in fermentum posuere urna nec tincidunt praesent. Ligula ullamcorper malesuada proin libero nunc consequat interdum. Fermentum odio eu feugiat pretium nibh ipsum. </p>
                        <p class="reviewTime"> 2 Week ago </p>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="col-lg-4 col-md-12 d-none d-md-block">
            <div class="side-tabbar">
               <nav>
                  <div class="nav nav-tabs nav-fill" id="nav-tab" role="tablist">
                     @if($variationCount == 1)
                     <a class="nav-item nav-link" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">{{$variation1VariationName}}</a>
                     @elseif($variationCount == 2)
                     <a class="nav-item nav-link" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">{{$variation1VariationName}}</a>
                     <a class="nav-item nav-link active" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">{{$variation2VariationName}}</a>
                     @elseif($variationCount == 3)
                     <a class="nav-item nav-link" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">{{$variation1VariationName}}</a>
                     <a class="nav-item nav-link active" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">{{$variation2VariationName}}</a>
                     <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab" aria-controls="nav-contact" aria-selected="false">{{$variation3VariationName}}</a>
                     @endif
                  </div>
               </nav>
               <div class="tab-content py-3 px-3 px-sm-0" id="nav-tabContent">
                  @if($variationCount == 1)
                  <div class="tab-pane fade" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                     <h3>${{$variation1Price}}</h3>
                     <p>{{$variation1Description}}</p>
                     <h5>3 DAYS DELIVERY</h5>
                     <ul class="product-side-link">
                        <li><a href="javascript:;" class="btn btn-block btn-border mb-3">Continue (${{$variation1Price}})</a></li>
                        <li><a href="#compare-packages" class="btn btn-block btn-border mb-3">Compare Packages</a></li>
                        <li><a href="javascript:;" class="btn btn-block btn-border mb-3">Contact Seller</a></li>
                        <li><a href="javascript:;" class="btn btn-block btn-border">Get A Quote</a></li>
                     </ul>
                  </div>
                  @elseif($variationCount == 2)
                  <div class="tab-pane fade" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                     <h3>${{$variation1Price}}</h3>
                     <p>{{$variation1Description}}</p>
                     <h5>3 DAYS DELIVERY</h5>
                     <ul class="product-side-link">
                        <li><a href="javascript:;" class="btn btn-block btn-border mb-3">Continue (${{$variation1Price}})</a></li>
                        <li><a href="#compare-packages" class="btn btn-block btn-border mb-3">Compare Packages</a></li>
                        <li><a href="javascript:;" class="btn btn-block btn-border mb-3">Contact Seller</a></li>
                        <li><a href="javascript:;" class="btn btn-block btn-border">Get A Quote</a></li>
                     </ul>
                  </div>
                  <div class="tab-pane fade show active" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                     <h3>${{$variation2Price}}</h3>
                     <p>{{$variation2Description}}</p>
                     <h5>3 DAYS DELIVERY</h5>
                     <ul class="product-side-link">
                        <li><a href="javascript:;" class="btn btn-block btn-border mb-3">Continue (${{$variation2Price}})</a></li>
                        <li><a href="#compare-packages" class="btn btn-block btn-border mb-3">Compare Packages</a></li>
                        <li><a href="javascript:;" class="btn btn-block btn-border mb-3">Contact Seller</a></li>
                        <li><a href="javascript:;" class="btn btn-block btn-border">Get A Quote</a></li>
                     </ul>
                  </div>
                  @elseif($variationCount == 3)
                  <div class="tab-pane fade" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                     <h3>${{$variation1Price}}</h3>
                     <p>{{$variation1Description}}</p>
                     <h5>3 DAYS DELIVERY</h5>
                     <ul class="product-side-link">
                        <li><a href="javascript:;" class="btn btn-block btn-border mb-3">Continue (${{$variation1Price}})</a></li>
                        <li><a href="#compare-packages" class="btn btn-block btn-border mb-3">Compare Packages</a></li>
                        <li><a href="javascript:;" class="btn btn-block btn-border mb-3">Contact Seller</a></li>
                        <li><a href="javascript:;" class="btn btn-block btn-border">Get A Quote</a></li>
                     </ul>
                  </div>
                  <div class="tab-pane fade show active" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                     <h3>${{$variation2Price}}</h3>
                     <p>{{$variation2Description}}</p>
                     <h5>3 DAYS DELIVERY</h5>
                     <ul class="product-side-link">
                        <li><a href="javascript:;" class="btn btn-block btn-border mb-3">Continue (${{$variation2Price}})</a></li>
                        <li><a href="#compare-packages" class="btn btn-block btn-border mb-3">Compare Packages</a></li>
                        <li><a href="javascript:;" class="btn btn-block btn-border mb-3">Contact Seller</a></li>
                        <li><a href="javascript:;" class="btn btn-block btn-border">Get A Quote</a></li>
                     </ul>
                  </div>
                  <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                     <h3>${{$variation3Price}}</h3>
                     <p>{{$variation3Description}}</p>
                     <h5>3 DAYS DELIVERY</h5>
                     <ul class="product-side-link">
                        <li><a href="javascript:;" class="btn btn-block btn-border mb-3">Continue (${{$variation2Price}})</a></li>
                        <li><a href="#compare-packages" class="btn btn-block btn-border mb-3">Compare Packages</a></li>
                        <li><a href="javascript:;" class="btn btn-block btn-border mb-3">Contact Seller</a></li>
                        <li><a href="javascript:;" class="btn btn-block btn-border">Get A Quote</a></li>
                     </ul>
                  </div>
                  @endif
               </div>
            </div>
         </div>
      </div>
   </div>
</section>
<!-- faq Area End-->
@endsection