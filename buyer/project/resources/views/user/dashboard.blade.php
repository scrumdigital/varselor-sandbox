@extends('layouts.front')
@section('content')

<div class="thank-you-wraper">
	<div class="container">
		<div class="row">
			<div class="col-md-8 offset-md-2">
				
				<div class="thank-youpage">
					<i class="nav-icon i-Yes"></i>
					<h1 class="thank-youTitle text-center">Thanks For Signing-up With Us</h1>
				</div>

				<div class="thank-main-content">
					
					<p class="text-center">One of Varselor's advisors will get back to you shortly to complete the onboarding process.</p>
				</div>
			</div>
		</div>
	</div>
</div>



@endsection