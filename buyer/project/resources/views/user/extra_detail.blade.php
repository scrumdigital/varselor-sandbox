@extends('layouts.front')
@extends('layouts.vendorLink')
@section('content')

<div class="breadcrumb-area">
    <div class="container">
        <div class="row py-3">
            <div class="col-lg-12">
                <ul class="pages">   

                    @if($data->vendor_type == 'sales')
                        <form method="post" action="{{route('user-extra-detail-submit', $data->id)}}" class="extra_details_class">
                        {{ csrf_field() }}

                        <div class="form-group">
                            <label for="interested_in">Interested In</label>
                            <select class="input-text form-control" name="interested_in" id="interested_in">
                                <option>Software Sales</option>
                                <option>Software Marketing</option>
                                <option>Inside Sales</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="sale_work_history">Work History</label>
                            <input type="text" id="sale_work_history" class="form-control" name="sale_work_history" value="" />
                        </div>
                        <div class="form-group">
                            <label for="sale_certifications">Certifications</label>
                            <input type="text" id="sale_certifications" class="form-control" name="sale_certifications" value="" />
                        </div>
                        <div class="form-group">
                            <label for="sale_education">Education</label>
                            <input type="text" id="sale_education" class="form-control" name="sale_education" value="" />
                        </div>
                        <div class="form-group">
                            <label for="sale_skills">Skills</label>
                            <input type="text" id="sale_skills" class="form-control" name="sale_skills" value="" />
                        </div>
                        <div class="form-group">
                            <label for="sale_product_specialization">Product Specialization</label>
                            <input type="text" id="sale_product_specialization" class="form-control" name="sale_product_specialization" value="" />
                        </div>
                        <div class="form-group">
                            <label for="sale_product_specialization">Product Specialization</label>
                            <input type="text" id="sale_product_specialization" class="form-control" name="sale_product_specialization" value="" />
                        </div>
                        <div class="form-group">
                            <label for="sale_availability">Avaibility</label>
                            <select class="input-text form-control" name="sale_availability" id="sale_availability">
                                <option>Full time</option>
                                <option>Part time</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="sale_no_of_hour">No of hours/week</label>
                            <input type="text" id="sale_no_of_hour" class="form-control" name="sale_no_of_hour" value="" />
                        </div>
                        <div class="form-group">
                            <label for="sale_hourly_rate">Hourly rate</label>
                            <input type="text" id="sale_hourly_rate" class="form-control" name="sale_hourly_rate" value="" />
                        </div>
                        <div class="form-group">
                            <label for="sale_language">Preferred language</label>
                            <input type="text" id="sale_language" class="form-control" name="sale_language" value="" />
                        </div>
                        <div class="form-group">
                            <label for="sale_industry_experience">Industry Experience</label>
                            <input type="text" id="sale_industry_experience" class="form-control" name="sale_industry_experience" value="" />
                        </div>

                    @elseif($data->vendor_type == 'consultant')
                        <form method="post" action="{{route('user-extra-detail-submit', $data->id)}}" class="extra_details_class">
                        {{ csrf_field() }}

                            <div class="form-group">
                                <label for="consultant_type">Type of Consultant</label>
                                <select class="input-text form-control" name="consultant_type" id="consultant_type">
                                    <option>Functional</option>
                                    <option>Technical</option>
                                    <option>Developer</option>
                                    <option>Techno Functional</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="consultant_work_history">Work History</label>
                                <input type="text" id="consultant_work_history" class="form-control" name="consultant_work_history" value="" />
                            </div>
                            <div class="form-group">
                                <label for="consultant_certifications">Certifications</label>
                                <input type="text" id="consultant_certifications" class="form-control" name="consultant_certifications" value="" />
                            </div>
                            <div class="form-group">
                                <label for="consultant_education">Education</label>
                                <input type="text" id="consultant_education" class="form-control" name="consultant_education" value="" />
                            </div>
                            <div class="form-group">
                                <label for="consultant_skills">Skills</label>
                                <input type="text" id="consultant_skills" class="form-control" name="consultant_skills" value="" />
                            </div>
                            <div class="form-group">
                                <label for="consultant_product_specialization">Product Specialization</label>
                                <input type="text" id="consultant_product_specialization" class="form-control" name="consultant_product_specialization" value="" />
                            </div>
                            <div class="form-group">
                                <label for="consultant_availability">Avaibility</label>
                                <select class="input-text form-control" name="consultant_availability" id="consultant_availability">
                                    <option>Full time</option>
                                    <option>Part time</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="consultant_no_of_hour">No of hours/week</label>
                                <input type="text" id="consultant_no_of_hour" class="form-control" name="consultant_no_of_hour" value="" />
                            </div>
                            <div class="form-group">
                                <label for="consultant_hourly_rate">Hourly rate</label>
                                <input type="text" id="consultant_hourly_rate" class="form-control" name="consultant_hourly_rate" value="" />
                            </div>
                            <div class="form-group">
                                <label for="consultant_language">Preferred language</label>
                                <input type="text" id="consultant_language" class="form-control" name="consultant_language" value="" />
                            </div>
                            <div class="form-group">
                                <label for="consultant_industry_experience">Industry Experience</label>
                                <input type="text" id="consultant_industry_experience" class="form-control" name="consultant_industry_experience" value="" />
                            </div>

                    @elseif($data->vendor_type == 'hardware_vendor')
                        <form method="post" action="{{route('user-extra-detail-submit', $data->id)}}" class="extra_details_class">
                        {{ csrf_field() }}

                            <div class="form-group">
                                <label for="hardware_vendor_sell_redirectly">Do you sell directly of through resellers?</label>
                                <select class="input-text form-control" name="hardware_vendor_specialization" id="hardware_vendor_specialization">
                                    <option>Category 1</option>
                                    <option>Category 2</option>
                                    <option>Category 3</option>
                                    <option>Category 4</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="hardware_vendor_specialization">What Product Category do you specilaize in?</label>
                                <select class="input-text form-control" name="hardware_vendor_sell_redirectly" id="hardware_vendor_sell_redirectly">
                                    <option>Seller 1</option>
                                    <option>Seller 2</option>
                                    <option>Seller 3</option>
                                    <option>Seller 4</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="hardware_key_customer">Key customers using the products?</label>
                                <input type="text" id="hardware_key_customer" class="form-control" name="hardware_key_customer" value="" />
                            </div>
                            <div class="form-group">
                                <label for="hardware_establishment">Year of establishment</label>
                                <input type="text" id="hardware_establishment" class="form-control" name="hardware_establishment" value="" />
                            </div>

                    @elseif($data->vendor_type == 'hosting_company')
                        <form method="post" action="{{route('user-extra-detail-submit', $data->id)}}" class="extra_details_class">
                        {{ csrf_field() }}

                            <div class="form-group">
                                <label for="hosting_company_development_capability">Development capaibilities</label>
                                <select class="input-text form-control" name="hosting_company_development_capability" id="hosting_company_development_capability">
                                    <option>Capability 1</option>
                                    <option>Capability 2</option>
                                    <option>Capability 3</option>
                                    <option>Capability 4</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="hosting_company_partnership_status">Partnership status with multiple vendors</label>
                                <select class="input-text form-control" name="hosting_company_partnership_status" id="hosting_company_partnership_status">
                                    <option>Status 1</option>
                                    <option>Status 2</option>
                                    <option>Status 3</option>
                                    <option>Status 4</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="hosting_company_availability_start">Avaibility to start project </label>
                                <input type="text" id="hosting_company_availability_start" class="form-control" name="hosting_company_availability_start" value="" />
                            </div>
                            <div class="form-group">
                                <label for="hosting_company_language">Preferred language</label>
                                <select class="input-text form-control" name="hosting_company_language" id="hosting_company_language">
                                    <option>Language 1</option>
                                    <option>Language 2</option>
                                    <option>Language 3</option>
                                    <option>Language 4</option>
                                </select>
                            </div>
                            
                            <div class="form-group">
                                <label for="hosting_company_hourly_rate">Hourly rate</label>
                                <input type="text" id="hosting_company_hourly_rate" class="form-control" name="hosting_company_hourly_rate" value="" />
                            </div>
                            <div class="form-group">
                                <label for="hosting_company_year_establishment">Year of establishment</label>
                                <input type="text" id="hosting_company_year_establishment" class="form-control" name="hosting_company_year_establishment" value="" />
                            </div>

                    @elseif($data->vendor_type == 'marketing_freelancer')

                        <form method="post" action="{{route('user-extra-detail-submit', $data->id)}}" class="extra_details_class">
                        {{ csrf_field() }}

                            <div class="form-group">
                                <label for="marketing_freelancer_work_history">Work History</label>
                                <input type="text" id="marketing_freelancer_work_history" class="form-control" name="marketing_freelancer_work_history" value="" />
                            </div>
                            <div class="form-group">
                                <label for="marketing_freelancer_certifications">Certifications</label>
                                <input type="text" id="marketing_freelancer_certifications" class="form-control" name="marketing_freelancer_certifications" value="" />
                            </div>
                            <div class="form-group">
                                <label for="marketing_freelancer_education">Education</label>
                                <input type="text" id="marketing_freelancer_education" class="form-control" name="marketing_freelancer_education" value="" />
                            </div>
                            <div class="form-group">   
                                <label for="marketing_freelancer_skills">Skills</label>
                                <input type="text" id="marketing_freelancer_skills" class="form-control" name="marketing_freelancer_skills" value="" />
                            </div>
                            <div class="form-group">
                                <label for="marketing_freelancer_product_specialization">Product Specialization</label>
                                <input type="text" id="marketing_freelancer_product_specialization" class="form-control" name="marketing_freelancer_product_specialization" value="" />
                            </div>
                            <div class="form-group">
                                <label for="marketing_freelancer_product_specialization">Product Specialization</label>
                                <input type="text" id="marketing_freelancer_product_specialization" class="form-control" name="marketing_freelancer_product_specialization" value="" />
                            </div>
                            <div class="form-group">
                                <label for="marketing_freelancer_availability">Avaibility</label>
                                <select class="input-text form-control" name="marketing_freelancer_availability" id="marketing_freelancer_availability">
                                    <option>Full time</option>
                                    <option>Part time</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="marketing_freelancer_no_of_hour">No of hours/week</label>
                                <input type="text" id="marketing_freelancer_no_of_hour" class="form-control" name="marketing_freelancer_no_of_hour" value="" />
                            </div>
                            <div class="form-group">
                                <label for="marketing_freelancer_hourly_rate">Hourly rate</label>
                                <input type="text" id="marketing_freelancer_hourly_rate" class="form-control" name="marketing_freelancer_hourly_rate" value="" />
                            </div>
                            <div class="form-group">
                                <label for="marketing_freelancer_language">Preferred language</label>
                                <input type="text" id="marketing_freelancer_language" class="form-control" name="marketing_freelancer_language" value="" />
                            </div>
                            <div class="form-group">
                                <label for="marketing_freelancer_industry_experience">Industry Experience</label>
                                <input type="text" id="marketing_freelancer_industry_experience" class="form-control" name="marketing_freelancer_industry_experience" value="" />
                            </div>

                    @elseif($data->vendor_type == 'system_integrator')
                        <form method="post" action="{{route('user-extra-detail-submit', $data->id)}}" class="extra_details_class">
                        {{ csrf_field() }}

                            <div class="form-group">
                                <label for="system_integrator_onsite_availability">Are you available for onsite work?</label>
                                <select class="input-text form-control" name="system_integrator_onsite_availability" id="system_integrator_onsite_availability">
                                    <option>Yes</option>
                                    <option>No</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="system_integrator_industry_specialization">What industries do you specialize in?</label>
                                <div id="specialize"></div>
                                <select class="input-text form-control chosen-specialize" id="system_integrator_industry_specialization" multiple="true">
                                    <option>Industry 1</option>
                                    <option>Industry 2</option>
                                    <option>Industry 3</option>
                                    <option>Industry 4</option>
                                </select>
                                <input type="hidden" name="system_integrator_industry_specialization" value="">
                            </div>
                            <div class="form-group">
                                <label for="system_integrator_vendor_partnership">Vendors you have an active partnership</label>
                                <div id="specialize"></div>
                                <select class="input-text form-control chosen-specialize" id="system_integrator_vendor_partnership" multiple="true">
                                    <option>Vendor 1</option>
                                    <option>Vendor 2</option>
                                    <option>Vendor 3</option>
                                    <option>Vendor 4</option>
                                </select>
                                <input type="hidden" name="system_integrator_vendor_partnership" value="">
                            </div>
                            <div class="form-group">
                                <label for="system_integrator_partnership_type">Please specify the partnership type</label>
                                <div id="specialize"></div>
                                <select class="input-text form-control chosen-specialize" id="system_integrator_partnership_type" multiple="true">
                                    <option>Authorized</option>
                                    <option>Reseller</option>
                                    <option>SI</option>
                                </select>
                                <input type="hidden" name="system_integrator_partnership_type" value="">
                            </div>
                            <div class="form-group">
                                <label for="system_integrator_awrd_list">Please list your awards and recognitions from these Vendors</label>
                                <input type="text" id="system_integrator_awrd_list" class="form-control" name="system_integrator_awrd_list" value="" />
                            </div>
                            <div class="form-group">
                                <label for="system_integrator_certified">Please list the no of certified cosultants for each product</label>
                                <input type="text" id="system_integrator_certified" class="form-control" name="system_integrator_certified" value="" />
                            </div>
                            <div class="form-group">
                                <label for="system_integrator_hourly_rate">Your preferred Hourly rate for Consulting</label>
                                <input type="text" id="system_integrator_hourly_rate" class="form-control" name="system_integrator_hourly_rate" value="" />
                            </div>
                            <div class="form-group">
                                <label for="system_integrator_year_establishment">Company year of establishment</label>
                                <input type="text" id="system_integrator_year_establishment" class="form-control" name="system_integrator_year_establishment" value="" />
                            </div>
                            <div class="form-group">
                                <label for="system_integrator_operating_hour">What are your standard operating hours?</label>
                                <input type="text" id="system_integrator_operating_hour" class="form-control" name="system_integrator_operating_hour" value="" />
                            </div>
                            <div class="form-group">
                                <label for="system_integrator_timezone">What timezone do you prefer to operate in?</label>
                                <input type="text" id="system_integrator_timezone" class="form-control" name="system_integrator_timezone" value="" />
                            </div>
                            <div class="form-group">
                                <label for="system_integrator_response_time">Average response time to customer query?</label>
                                <input type="text" id="system_integrator_response_time" class="form-control" name="system_integrator_response_time" value="" />
                            </div>
                            <div class="form-group">
                                <label for="system_integrator_language">Preferred language</label>
                                <input type="text" id="system_integrator_language" class="form-control" name="system_integrator_language" value="" />
                            </div>

                    @else
                        <form method="post" action="{{route('user-extra-detail-submit', $data->id)}}" class="extra_details_class">
                        {{ csrf_field() }}

                            <div class="form-group">
                                <label for="isv_onsite_availability">Are you available for onsite work? </label>
                                <select class="input-text form-control" name="isv_onsite_availability" id="isv_onsite_availability">
                                    <option>Yes</option>
                                    <option>No</option>
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="isv_industry_specialization">What industries do you specialize in? </label>
                                <div id="specialize"></div>
                                <select class="input-text form-control chosen-specialize" id="isv_industry_specialization" multiple="true">
                                    <option>Industry 1</option>
                                    <option>Industry 2</option>
                                    <option>Industry 3</option>
                                    <option>Industry 4</option>
                                </select>
                                <input type="hidden" name="isv_industry_specialization" value="">
                            </div>

                            <div class="form-group">
                                <label for="isv_vendor_partnership">Vendors you have an active partnership </label>
                                <div id="partnership"></div>
                                <select class="input-text form-control chosen-partnership" id="isv_vendor_partnership" multiple="true">
                                    <option>Vendor 1</option>
                                    <option>Vendor 2</option>
                                    <option>Vendor 3</option>
                                    <option>Vendor 4</option>
                                </select>
                                <input type="hidden" name="isv_vendor_partnership" value="">
                            </div>
                            <div class="form-group">
                                <label for="isv_partnership_type">Please specify the partnership type </label>
                                <div id="isv_partnership"></div>
                                <select class="input-text form-control chosen-isv_partnership" id="isv_partnership_type" multiple="true">
                                    <option>Authorized</option>
                                    <option>Reseller</option>
                                    <option>SI</option>
                                </select>
                                <input type="hidden" name="isv_partnership_type" value="">
                            </div>
                            <div class="form-group">
                                <label for="isv_awrd_list">Please list your awards and recognitions from these Vendors </label>
                                <input type="text" class="form-control" id="isv_awrd_list" name="isv_awrd_list" value="" />
                            </div>
                            <div class="form-group">
                                <label for="isv_certified">Please list the no of certified cosultants for each product </label>
                                <input type="text" class="form-control" id="isv_certified" name="isv_certified" value="" />
                            </div>

                            <div class="form-group">
                                <label for="isv_hourly_rate">Your preferred Hourly rate for Consulting </label>
                                <input type="text" class="form-control" id="isv_hourly_rate" name="isv_hourly_rate" value="" />
                            </div>

                            <div class="form-group">
                                <label for="isv_year_establishment">Company year of establishment </label>
                                <input type="text" class="form-control" id="isv_year_establishment" name="isv_year_establishment" value="" />
                            </div>

                            <div class="form-group">
                                <label for="isv_operating_hour">What are your standard operating hours? </label>
                                <input type="text" class="form-control" id="isv_operating_hour" name="isv_operating_hour" value="" />
                            </div>

                            <div class="form-group">
                                <label for="isv_timezone">What timezone do you prefer to operate in? </label>
                                <input type="text" class="form-control" id="isv_timezone" name="isv_timezone" value="" />
                            </div>

                            <div class="form-group">
                                <label for="isv_response_time">Average response time to customer query? </label>
                                <input type="text" class="form-control" id="isv_response_time" name="isv_response_time" value="" />
                            </div>

                            <div class="form-group">
                                <label for="isv_language">Preferred language </label>
                                <input type="text" class="form-control" id="isv_language" name="isv_language" value="" />
                            </div>

                    @endif
                    <input type="hidden" name="user_id" id="user_id" value="{{ $data->id }}">
                    <input type="hidden" name="vendor_type" id="vendor_type" value="{{ $data->vendor_type }}">
                    <button type="submit" class="submit-btn btn-primary float-right mt-3 mb-3">Save Details</button>
                </form>
                </ul>
            </div>
        </div>
    </div>
</div>


<?php

?>
@endsection
@section('scripts')
<script src="{{asset('assets/admin/js/chosen.jquery.js')}}"></script>
<script type="text/javascript">
    
/**** Choose Multiple Option ******/
document.getElementById('specialize').innerHTML = location.search;
$(".chosen-specialize").chosen();

document.getElementById('isv_partnership').innerHTML = location.search;
$(".chosen-isv_partnership").chosen();

document.getElementById('partnership').innerHTML = location.search;
$(".chosen-partnership").chosen();

$('#isv_industry_specialization').change(function(event){
    if(event.target == this){
        var value = $(this).val();
        $("input[name='isv_industry_specialization']").val(value);
     }
});

$('#isv_vendor_partnership').change(function(event){
    if(event.target == this){
        var value = $(this).val();
        $("input[name='isv_vendor_partnership']").val(value);
     }
});

$('#isv_partnership_type').change(function(event){
    if(event.target == this){
        var value = $(this).val();
        $("input[name='isv_partnership_type']").val(value);
     }
});

</script>

<script>

$('#system_integrator_industry_specialization').change(function(event){
    if(event.target == this){
        var value = $(this).val();
        $("input[name='system_integrator_industry_specialization']").val(value);
     }
});

$('#system_integrator_vendor_partnership').change(function(event){
    if(event.target == this){
        var value = $(this).val();
        $("input[name='system_integrator_vendor_partnership']").val(value);
     }
});

$('#system_integrator_partnership_type').change(function(event){
    if(event.target == this){
        var value = $(this).val();
        $("input[name='system_integrator_partnership_type']").val(value);
     }
});


</script>
@endsection


