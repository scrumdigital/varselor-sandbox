<!DOCTYPE html>
<html lang="en">
   <head>
      <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge" />
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      @if(isset($page->meta_tag) && isset($page->meta_description))
      <meta name="keywords" content="{{ $page->meta_tag }}">
      <meta name="description" content="{{ $page->meta_description }}">
      <title>{{$gs->title}}</title>
      @elseif(isset($blog->meta_tag) && isset($blog->meta_description))
      <meta name="keywords" content="{{ $blog->meta_tag }}">
      <meta name="description" content="{{ $blog->meta_description }}">
      <title>{{$gs->title}}</title>
      @elseif(isset($productt))
      <meta name="keywords" content="{{ !empty($productt->meta_tag) ? implode(',', $productt->meta_tag ): '' }}">
      <meta property="og:title" content="{{$productt->name}}" />
      <meta property="og:description" content="{{ $productt->meta_description != null ? $productt->meta_description : strip_tags($productt->description) }}" />
      <meta property="og:image" content="{{asset('assets/images/'.$productt->photo)}}" />
      <meta name="author" content="GeniusOcean">
      <title>{{$productt->name."-"}}{{$gs->title}}</title>
      @else
      <meta name="keywords" content="{{ $seo->meta_keys }}">
      <meta name="author" content="GeniusOcean">
      <title>{{$gs->title}}</title>
      @endif
      <!-- favicon -->
      <link rel="stylesheet" href="{{asset('assets/admin/css/fontawesome.css')}}">
        <!-- icofont -->
        <link rel="stylesheet" href="{{asset('assets/admin/css/icofont.min.css')}}">
      <link rel="icon"  type="image/x-icon" href="{{asset('assets/images/'.$gs->favicon)}}"/>
      <!-- bootstrap -->
      <link rel="stylesheet" href="{{asset('assets/front/css/bootstrap.min.css')}}">
      <!-- Plugin css -->
      <link rel="stylesheet" href="{{asset('assets/front/css/plugin.css')}}">
      <link rel="stylesheet" href="{{asset('assets/front/css/animate.css')}}">
      <link rel="stylesheet" href="{{asset('assets/front/css/toastr.css')}}">
      <!-- jQuery Ui Css-->
      <link rel="stylesheet" href="{{asset('assets/front/jquery-ui/jquery-ui.min.css')}}">
      <link rel="stylesheet" href="{{asset('assets/front/jquery-ui/jquery-ui.structure.min.css')}}">
      <link rel="stylesheet" href="{{asset('assets/front/jquery-ui/jquery-ui.theme.min.css')}}">
      <!-- Compare section css -->
      <link rel="stylesheet" href="{{asset('assets/front/css/w3.css')}}">
      <link rel="stylesheet" href="{{asset('assets/front/css/test.css')}}">
      <link rel="stylesheet" href="{{asset('assets/front/css/Compare.css')}}">
      <!-- Compare section -->
      @if($langg->rtl == "1")
      <!-- stylesheet -->
      <link rel="stylesheet" href="{{asset('assets/front/css/rtl/style.css')}}">
      <link rel="stylesheet" href="{{asset('assets/front/css/rtl/custom.css')}}">
      <link rel="stylesheet" href="{{asset('assets/front/css/common.css')}}">
      <!-- responsive -->
      <link rel="stylesheet" href="{{asset('assets/front/css/rtl/responsive.css')}}">
      <link rel="stylesheet" href="{{asset('assets/front/css/common-responsive.css')}}">
      @else 
      <!-- stylesheet -->
      <link rel="stylesheet" href="{{asset('assets/front/css/owl.carousel.min.css')}}">
      <link rel="stylesheet" href="{{asset('assets/front/css/style.css')}}">
      <link rel="stylesheet" href="{{asset('assets/front/css/custom.css')}}">
      <link rel="stylesheet" href="{{asset('assets/front/css/common.css')}}">
      <link rel="stylesheet" href="{{asset('assets/front/css/new-style.css')}}">
      <!-- responsive -->
      <link rel="stylesheet" href="{{asset('assets/front/css/responsive.css')}}">
      <link rel="stylesheet" href="{{asset('assets/front/css/common-responsive.css')}}">
      @endif
      <!--Updated CSS-->
      <link rel="stylesheet" href="{{ asset('assets/front/css/styles.php?color='.str_replace('#','',$gs->colors).'&amp;'.'header_color='.str_replace('#','',$gs->header_color).'&amp;'.'footer_color='.str_replace('#','',$gs->footer_color).'&amp;'.'copyright_color='.str_replace('#','',$gs->copyright_color).'&amp;'.'menu_color='.str_replace('#','',$gs->menu_color).'&amp;'.'menu_hover_color='.str_replace('#','',$gs->menu_hover_color)) }}">
      @yield('styles')
   </head>
   <body>
      @if($gs->is_loader == 1)
      <div class="preloader" id="preloader" style="background: url({{asset('assets/images/'.$gs->loader)}}) no-repeat scroll center center #FFF;"></div>
      @endif
      @if($gs->is_popup== 1)
      @if(isset($visited))
      <div style="display:none">
         <img src="{{asset('assets/images/'.$gs->popup_background)}}">
      </div>
      <!--  Starting of subscribe-pre-loader Area   -->
      <div class="subscribe-preloader-wrap" id="subscriptionForm" style="display: none;">
         <div class="subscribePreloader__thumb" style="background-image: url({{asset('assets/images/'.$gs->popup_background)}});">
            <span class="preload-close"><i class="fas fa-times"></i></span>
            <div class="subscribePreloader__text text-center">
               <h1>{{$gs->popup_title}}</h1>
               <p>{{$gs->popup_text}}</p>
               <form action="{{route('front.subscribe')}}" id="subscribeform" method="POST">
                  {{csrf_field()}}
                  <div class="form-group">
                     <input type="email" name="email"  placeholder="{{ $langg->lang741 }}" required="">
                     <button id="sub-btn" type="submit">{{ $langg->lang742 }}</button>
                  </div>
               </form>
            </div>
         </div>
      </div>
      <!--  Ending of subscribe-pre-loader Area   -->
      @endif
      @endif
      <!-- Logo Header Area Start -->
      <section class="logo-header">
         <div class="container">
            <div class="row d-flex center_align">
               <div class="col-md-12 pl-lg-0 col-lg-4 pl-lg-0">
                  <!-- <div class="logo">
                     <a href="{{ route('front.index') }}">
                     	<img src="{{asset('assets/images/'.$gs->logo)}}" alt="">
                     </a>
                     </div> -->
                  <div class="mainmenu-wrapper remove-padding">
                     <nav class="core-nav">
                        <div class="menu-left-wrapper">
                           <div class="logo">
                              <!-- <a href="{{ route('front.index') }}"> -->
                              <a href="https://varselor.scrumdigital.com">
                              <img src="{{asset('assets/images/'.$gs->logo)}}" alt="">
                              </a>
                           </div>
                           <ul class="menu menu-left-list wrap-core-nav-list right">
                              <li class="active"><a href="{{ route('front.index') }}">Buyers</a></li>
                              <li><a href="https://varselor.scrumdigital.com/vendor">Vendors</a></li>
                           </ul>
                        </div>
                        <button class="navbar-toggle d-lg-none" type="button" data-toggle="collapse" data-target=".mobile-topNav-wrap">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                     </nav>
                  </div>
               </div>
               <div class="col-lg-6 col-sm-12 w-26  d-none d-lg-block">
                  <div class="search-box menu-search-box">
                     <form class="search-form" action="{{ route('front.search') }}" method="GET">
                        <div class="input-group">
                           <input type="text" class="form-control" id="search_text" name="product" placeholder="Search" value="{{ isset($_GET['search']) ? $_GET['search'] : ''}}" required="" autocomplete="off">
                           <div class="input-group-append">
                              <button type="submit" class="input-group-text"><i class="fa fa-search" aria-hidden="true"></i></button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
               <div class="col-lg-2 pr-0 w-20 d-none d-lg-block">
                  <div class="helpful-links">
                     <div class="helpful-links-inner">
                        <a href="#" class="btn free-consult freeConsulting-btn">Free Consultations</a>
                        <!-- <li class="my-dropdown">
                           <a href="javascript:;" class="cart carticon">
                           	<div class="icon">
                           		<i class="icofont-cart"></i>
                           		<p class="resp">{{ $langg->lang3 }} <span class="cart-quantity" id="cart-count">{{ Session::has('cart') ? count(Session::get('cart')->items) : '0' }}</span></p>
                           		
                           	</div>
                           	
                           
                           </a>
                           <div class="my-dropdown-menu" id="cart-items">
                           	@include('load.cart')
                           </div>
                           </li> -->
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <!-- Logo Header Area End -->
      <div class="mobile-topNav-wrap d-lg-none collapse">
         <div class="container">
            <div class="row">
               <div class="col-12">
                  <div class="search-box menu-search-box">
                     <form class="search-form" action="{{ route('front.search') }}" method="GET">
                        <div class="input-group">
                           <input type="hidden" id="category_id" name="category_id" value="{{ isset($_GET['category_id']) ? $_GET['category_id'] : '0'}}">
                           <input type="text" class="form-control" id="prod_name" name="search" placeholder="Search" value="{{ isset($_GET['search']) ? $_GET['search'] : ''}}" required="" autocomplete="off">
                           <div class="input-group-append">
                              <button type="submit" class="input-group-text"><i class="fa fa-search" aria-hidden="true"></i></button>
                           </div>
                        </div>
                     </form>
                  </div>
               </div>
               <div class="col-md-12">
                  <nav class="navbar mobile-secondary-menu-wrap">
                     <div class="menu-primary-container">
                        <ul id="menu-primary" class="nav navbar-nav">
                           <li class="">
                              <a href="{{ route('front.index') }}">For Buyers</a>
                           </li>
                           <li >
                              <a href="https://varselor.scrumdigital.com/vendor/">For Vendor</a>
                           </li>
                        </ul>
                     </div>
                  </nav>
               </div>
               <div class="col-md-12">
                  <div class="mobile-menu-logo">
                     <a href="">
                     <img src="{{asset('assets/images/logo-primary-inverted.png')}}" alt=" Logo " class="img-fluid m-auto d-block">
                     </a>
                  </div>
               </div>
            </div>
         </div>
      </div>
      @yield('content')
      <!-- Footer Area Start -->
      <footer class="footer" id="footer">
         <div class="container">
            <div class="row">
               <div class="col-md-12 pl-lg-0">
                  <div class="footer-logo">
                     <a href="https://varselor.scrumdigital.com" class="logo-link">
                     <img src="{{asset('assets/images/logo-primary-inverted.png')}}" alt="" width="164" height="40">
                     </a>
                  </div>
               </div>
               <div class="col-md-6 col-lg-4 pl-lg-0">
                  <div class="footer-widget  footer-info-area">
                     <h4 class="title">
                        United States & Canada
                     </h4>
                     <div class="text">
                        <p>
                           {!! $gs->footer !!}
                        </p>
                     </div>
                  </div>
               </div>
               <div class="col-md-6 col-lg-5 pr-lg-0">
                  <div class="footer-widget  footer-info-area">
                     <h4 class="title">
                        Middle East, Africa & Asia
                     </h4>
                     <div class="text">
                        <p>
                           {!! $gs->address2 !!}
                        </p>
                     </div>
                  </div>
                  <div class="footer-widget info-link-widget social-media-wrapper">
                     <h4 class="title">
                        FIND US ON SOCIAL NETWORKS
                     </h4>
                     <div class="fotter-social-links">
                        <ul>
                           @if(App\Models\Socialsetting::find(1)->f_status == 1)
                           <li>
                              <a href="{{ App\Models\Socialsetting::find(1)->facebook }}" class="facebook" target="_blank">
                              <i class="fab fa-facebook"></i>
                              </a>
                           </li>
                           @endif
                           @if(App\Models\Socialsetting::find(1)->t_status == 1)
                           <li>
                              <a href="{{ App\Models\Socialsetting::find(1)->twitter }}" class="twitter" target="_blank">
                              <i class="fab fa-twitter"></i>
                              </a>
                           </li>
                           @endif
                           @if(App\Models\Socialsetting::find(1)->l_status == 1)
                           <li>
                              <a href="{{ App\Models\Socialsetting::find(1)->linkedin }}" class="linkedin" target="_blank">
                              <i class="fab fa-linkedin-in"></i>
                              </a>
                           </li>
                           @endif
                           @if(App\Models\Socialsetting::find(1)->d_status == 1)
                           <li>
                              <a href="{{ App\Models\Socialsetting::find(1)->youtube }}" class="youtube" target="_blank">
                              <i class="fab fa-youtube"></i>
                              </a>
                           </li>
                           @endif
                        </ul>
                     </div>
                     <!-- <div class="privacy-links">
                        <ul class="link-list">
                        
                        	@foreach(DB::table('pages')->where('footer','=',1)->get() as $data)
                        	<li>
                        		<a href="{{ route('front.page',$data->slug) }}">
                        			{{ $data->title }}
                        		</a>
                        	</li>
                        	@endforeach
                        
                        </ul>
                        </div> -->
                  </div>
               </div>
               <div class="col-md-6 col-lg-3 pl-lg-0">
                  <div class="footer-widget quick-links info-link-widget">
                     <h4 class="title">
                        QUICK ACCESS<!-- {{ $langg->lang21 }} -->
                     </h4>
                     <ul class="link-list">
                        <li>
                           <a href="{{ route('front.index') }}"> Buyers </a>
                        </li>
                        <li>
                           <a href="https://varselor.scrumdigital.com/vendor"> Vendors </a>
                        </li>
                        <li>
                           <a href="#">Privacy & Policy</a>
                        </li>
                        <li>
                           <a href="#"> Terms & Condition </a>
                        </li>
                     </ul>
                     <div class="copyright-widget">
                        <div class="content">
                           <p class="mb-0">{!! $gs->copyright !!}</p>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <!-- <div class="copy-bg">
            <div class="container">
            	<div class="row">
            		<div class="col-lg-12">
            				<div class="content">
            					
            			</div>
            		</div>
            	</div>
            </div>
            </div> -->
      </footer>
      <!-- Footer Area End -->
      <!-- Back to Top Start -->
      <div class="bottomtotop">
         <i class="fas fa-chevron-right"></i>
      </div>
      <!-- Back to Top End -->
      <!-- LOGIN MODAL -->
      <div class="modal fade" id="comment-log-reg" tabindex="-1" role="dialog" aria-labelledby="comment-log-reg-Title" aria-hidden="true">
         <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body">
                  <nav class="comment-log-reg-tabmenu">
                     <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <a class="nav-item nav-link login active" id="nav-log-tab" data-toggle="tab" href="#nav-log" role="tab" aria-controls="nav-log" aria-selected="true">
                        {{ $langg->lang197 }}
                        </a>
                        <a class="nav-item nav-link" id="nav-reg-tab" data-toggle="tab" href="#nav-reg" role="tab" aria-controls="nav-reg" aria-selected="false">
                        {{ $langg->lang198 }}
                        </a>
                     </div>
                  </nav>
                  <div class="tab-content" id="nav-tabContent">
                     <div class="tab-pane fade show active" id="nav-log" role="tabpanel" aria-labelledby="nav-log-tab">
                        <div class="login-area">
                           <div class="header-area">
                              <h4 class="title">{{ $langg->lang172 }}</h4>
                           </div>
                           <div class="login-form signin-form">
                              @include('includes.admin.form-login')
                              <form class="mloginform" action="{{ route('user.login.submit') }}" method="POST">
                                 {{ csrf_field() }}
                                 <div class="form-input">
                                    <input type="email" name="email" placeholder="{{ $langg->lang173 }}" required="">
                                    <i class="icofont-user-alt-5"></i>
                                 </div>
                                 <div class="form-input">
                                    <input type="password" class="Password" name="password" placeholder="{{ $langg->lang174 }}" required="">
                                    <i class="icofont-ui-password"></i>
                                 </div>
                                 <div class="form-forgot-pass">
                                    <div class="left">
                                       <input type="checkbox" name="remember"  id="mrp" {{ old('remember') ? 'checked' : '' }}>
                                       <label for="mrp">{{ $langg->lang175 }}</label>
                                    </div>
                                    <div class="right">
                                       <a href="javascript:;" id="show-forgot">
                                       {{ $langg->lang176 }}
                                       </a>
                                    </div>
                                 </div>
                                 <input type="hidden" name="modal"  value="1">
                                 <input class="mauthdata" type="hidden"  value="{{ $langg->lang177 }}">
                                 <button type="submit" class="submit-btn">{{ $langg->lang178 }}</button>
                                 @if(App\Models\Socialsetting::find(1)->f_check == 1 || App\Models\Socialsetting::find(1)->g_check == 1)
                                 <div class="social-area">
                                    <h3 class="title">{{ $langg->lang179 }}</h3>
                                    <p class="text">{{ $langg->lang180 }}</p>
                                    <ul class="social-links">
                                       @if(App\Models\Socialsetting::find(1)->f_check == 1)
                                       <li>
                                          <a href="{{ route('social-provider','facebook') }}"> 
                                          <i class="fab fa-facebook-f"></i>
                                          </a>
                                       </li>
                                       @endif
                                       @if(App\Models\Socialsetting::find(1)->g_check == 1)
                                       <li>
                                          <a href="{{ route('social-provider','google') }}">
                                          <i class="fab fa-google-plus-g"></i>
                                          </a>
                                       </li>
                                       @endif
                                    </ul>
                                 </div>
                                 @endif
                              </form>
                           </div>
                        </div>
                     </div>
                     <div class="tab-pane fade" id="nav-reg" role="tabpanel" aria-labelledby="nav-reg-tab">
                        <div class="login-area signup-area">
                           <div class="header-area">
                              <h4 class="title">{{ $langg->lang181 }}</h4>
                           </div>
                           <div class="login-form signup-form">
                              @include('includes.admin.form-login')
                              <form class="mregisterform" action="{{route('user-register-submit')}}" method="POST">
                                 {{ csrf_field() }}
                                 <div class="form-input">
                                    <input type="text" class="User Name" name="name" placeholder="{{ $langg->lang182 }}" required="">
                                    <i class="icofont-user-alt-5"></i>
                                 </div>
                                 <div class="form-input">
                                    <input type="email" class="User Name" name="email" placeholder="{{ $langg->lang183 }}" required="">
                                    <i class="icofont-email"></i>
                                 </div>
                                 <div class="form-input">
                                    <input type="text" class="User Name" name="phone" placeholder="{{ $langg->lang184 }}" required="">
                                    <i class="icofont-phone"></i>
                                 </div>
                                 <div class="form-input">
                                    <input type="text" class="User Name" name="address" placeholder="{{ $langg->lang185 }}" required="">
                                    <i class="icofont-location-pin"></i>
                                 </div>
                                 <div class="form-input">
                                    <input type="password" class="Password" name="password" placeholder="{{ $langg->lang186 }}" required="">
                                    <i class="icofont-ui-password"></i>
                                 </div>
                                 <div class="form-input">
                                    <input type="password" class="Password" name="password_confirmation" placeholder="{{ $langg->lang187 }}" required="">
                                    <i class="icofont-ui-password"></i>
                                 </div>
                                 @if($gs->is_capcha == 1)
                                 <ul class="captcha-area">
                                    <li>
                                       <p><img class="codeimg1" src="{{asset("assets/images/capcha_code.png")}}" alt=""> <i class="fas fa-sync-alt pointer refresh_code "></i></p>
                                    </li>
                                 </ul>
                                 <div class="form-input">
                                    <input type="text" class="Password" name="codes" placeholder="{{ $langg->lang51 }}" required="">
                                    <i class="icofont-refresh"></i>
                                 </div>
                                 @endif
                                 <input class="mprocessdata" type="hidden"  value="{{ $langg->lang188 }}">
                                 <button type="submit" class="submit-btn">{{ $langg->lang189 }}</button>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- LOGIN MODAL ENDS -->
      <!-- VENDOR LOGIN MODAL -->
      <div class="modal fade" id="vendor-login" tabindex="-1" role="dialog" aria-labelledby="vendor-login-Title" aria-hidden="true">
         <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" style="transition: .5s;" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body">
                  <nav class="comment-log-reg-tabmenu">
                     <div class="nav nav-tabs" id="nav-tab1" role="tablist">
                        <a class="nav-item nav-link login active" id="nav-log-tab1" data-toggle="tab" href="#nav-log1" role="tab" aria-controls="nav-log" aria-selected="true">
                        {{ $langg->lang234 }}
                        </a>
                        <a class="nav-item nav-link" id="nav-reg-tab1" data-toggle="tab" href="#nav-reg1" role="tab" aria-controls="nav-reg" aria-selected="false">
                        {{ $langg->lang235 }}
                        </a>
                     </div>
                  </nav>
                  <div class="tab-content" id="nav-tabContent">
                     <div class="tab-pane fade show active" id="nav-log1" role="tabpanel" aria-labelledby="nav-log-tab">
                        <div class="login-area">
                           <div class="login-form signin-form">
                              @include('includes.admin.form-login')
                              <form class="mloginform" action="{{ route('user.login.submit') }}" method="POST">
                                 {{ csrf_field() }}
                                 <div class="form-input">
                                    <input type="email" name="email" placeholder="{{ $langg->lang173 }}" required="">
                                    <i class="icofont-user-alt-5"></i>
                                 </div>
                                 <div class="form-input">
                                    <input type="password" class="Password" name="password" placeholder="{{ $langg->lang174 }}" required="">
                                    <i class="icofont-ui-password"></i>
                                 </div>
                                 <div class="form-forgot-pass">
                                    <div class="left">
                                       <input type="checkbox" name="remember"  id="mrp1" {{ old('remember') ? 'checked' : '' }}>
                                       <label for="mrp1">{{ $langg->lang175 }}</label>
                                    </div>
                                    <div class="right">
                                       <a href="javascript:;" id="show-forgot1">
                                       {{ $langg->lang176 }}
                                       </a>
                                    </div>
                                 </div>
                                 <input type="hidden" name="modal"  value="1">
                                 <input type="hidden" name="vendor"  value="1">
                                 <input class="mauthdata" type="hidden"  value="{{ $langg->lang177 }}">
                                 <button type="submit" class="submit-btn">{{ $langg->lang178 }}</button>
                                 @if(App\Models\Socialsetting::find(1)->f_check == 1 || App\Models\Socialsetting::find(1)->g_check == 1)
                                 <div class="social-area">
                                    <h3 class="title">{{ $langg->lang179 }}</h3>
                                    <p class="text">{{ $langg->lang180 }}</p>
                                    <ul class="social-links">
                                       @if(App\Models\Socialsetting::find(1)->f_check == 1)
                                       <li>
                                          <a href="{{ route('social-provider','facebook') }}"> 
                                          <i class="fab fa-facebook-f"></i>
                                          </a>
                                       </li>
                                       @endif
                                       @if(App\Models\Socialsetting::find(1)->g_check == 1)
                                       <li>
                                          <a href="{{ route('social-provider','google') }}">
                                          <i class="fab fa-google-plus-g"></i>
                                          </a>
                                       </li>
                                       @endif
                                    </ul>
                                 </div>
                                 @endif
                              </form>
                           </div>
                        </div>
                     </div>
                     <div class="tab-pane fade" id="nav-reg1" role="tabpanel" aria-labelledby="nav-reg-tab">
                        <div class="login-area signup-area">
                           <div class="login-form signup-form">
                              @include('includes.admin.form-login')
                              <form class="mregisterform" action="{{route('user-register-submit')}}" method="POST">
                                 {{ csrf_field() }}
                                 <div class="row">
                                    <div class="col-lg-6">
                                       <div class="form-input">
                                          <input type="text" class="User Name" name="name" placeholder="{{ $langg->lang182 }}" required="">
                                          <i class="icofont-user-alt-5"></i>
                                       </div>
                                    </div>
                                    <div class="col-lg-6">
                                       <div class="form-input">
                                          <input type="email" class="User Name" name="email" placeholder="{{ $langg->lang183 }}" required="">
                                          <i class="icofont-email"></i>
                                       </div>
                                    </div>
                                    <div class="col-lg-6">
                                       <div class="form-input">
                                          <input type="text" class="User Name" name="phone" placeholder="{{ $langg->lang184 }}" required="">
                                          <i class="icofont-phone"></i>
                                       </div>
                                    </div>
                                    <div class="col-lg-6">
                                       <div class="form-input">
                                          <input type="text" class="User Name" name="address" placeholder="{{ $langg->lang185 }}" required="">
                                          <i class="icofont-location-pin"></i>
                                       </div>
                                    </div>
                                    <div class="col-lg-6">
                                       <div class="form-input">
                                          <input type="text" class="User Name" name="shop_name" placeholder="{{ $langg->lang238 }}" required="">
                                          <i class="icofont-cart-alt"></i>
                                       </div>
                                    </div>
                                    <div class="col-lg-6">
                                       <div class="form-input">
                                          <input type="text" class="User Name" name="owner_name" placeholder="{{ $langg->lang239 }}" required="">
                                          <i class="icofont-cart"></i>
                                       </div>
                                    </div>
                                    <div class="col-lg-6">
                                       <div class="form-input">
                                          <input type="text" class="User Name" name="shop_number" placeholder="{{ $langg->lang240 }}" required="">
                                          <i class="icofont-shopping-cart"></i>
                                       </div>
                                    </div>
                                    <div class="col-lg-6">
                                       <div class="form-input">
                                          <input type="text" class="User Name" name="shop_address" placeholder="{{ $langg->lang241 }}" required="">
                                          <i class="icofont-opencart"></i>
                                       </div>
                                    </div>
                                    <div class="col-lg-6">
                                       <div class="form-input">
                                          <input type="text" class="User Name" name="reg_number" placeholder="{{ $langg->lang242 }}" required="">
                                          <i class="icofont-ui-cart"></i>
                                       </div>
                                    </div>
                                    <div class="col-lg-6">
                                       <div class="form-input">
                                          <input type="text" class="User Name" name="shop_message" placeholder="{{ $langg->lang243 }}" required="">
                                          <i class="icofont-envelope"></i>
                                       </div>
                                    </div>
                                    <div class="col-lg-6">
                                       <div class="form-input">
                                          <input type="password" class="Password" name="password" placeholder="{{ $langg->lang186 }}" required="">
                                          <i class="icofont-ui-password"></i>
                                       </div>
                                    </div>
                                    <div class="col-lg-6">
                                       <div class="form-input">
                                          <input type="password" class="Password" name="password_confirmation" placeholder="{{ $langg->lang187 }}" required="">
                                          <i class="icofont-ui-password"></i>
                                       </div>
                                    </div>
                                    @if($gs->is_capcha == 1)
                                    <div class="col-lg-6">
                                       <ul class="captcha-area">
                                          <li>
                                             <p>
                                                <img class="codeimg1" src="{{asset("assets/images/capcha_code.png")}}" alt=""> <i class="fas fa-sync-alt pointer refresh_code "></i>
                                             </p>
                                          </li>
                                       </ul>
                                    </div>
                                    <div class="col-lg-6">
                                       <div class="form-input">
                                          <input type="text" class="Password" name="codes" placeholder="{{ $langg->lang51 }}" required="">
                                          <i class="icofont-refresh"></i>
                                       </div>
                                    </div>
                                    @endif
                                 </div>
                           </div>
                           <input type="hidden" name="vendor"  value="1">
                           <input class="mprocessdata" type="hidden"  value="{{ $langg->lang188 }}">
                           <button type="submit" class="submit-btn">{{ $langg->lang189 }}</button>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      </div>
      <!-- VENDOR LOGIN MODAL ENDS -->
      <!-- FORGOT MODAL -->
      <div class="modal fade" id="forgot-modal" tabindex="-1" role="dialog" aria-labelledby="comment-log-reg-Title" aria-hidden="true">
         <div class="modal-dialog modal-dialog-scrollable modal-dialog-centered" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body">
                  <div class="login-area">
                     <div class="header-area forgot-passwor-area">
                        <h4 class="title">{{ $langg->lang191 }}  </h4>
                        <p class="text">{{ $langg->lang192 }} </p>
                     </div>
                     <div class="login-form">
                        @include('includes.admin.form-login')
                        <form id="mforgotform" action="{{route('user-forgot-submit')}}" method="POST">
                           {{ csrf_field() }}
                           <div class="form-input">
                              <input type="email" name="email" class="User Name" placeholder="{{ $langg->lang193 }}" required="">
                              <i class="icofont-user-alt-5"></i>
                           </div>
                           <div class="to-login-page">
                              <a href="javascript:;" id="show-login">
                              {{ $langg->lang194 }}
                              </a>
                           </div>
                           <input class="fauthdata" type="hidden"  value="{{ $langg->lang195 }}">
                           <button type="submit" class="submit-btn">{{ $langg->lang196 }}</button>
                        </form>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- FORGOT MODAL ENDS -->
      <!-- Product Quick View Modal -->
      <div class="modal fade" id="quickview" tabindex="-1" role="dialog"  aria-hidden="true">
         <div class="modal-dialog quickview-modal modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
               <div class="submit-loader">
                  <img src="{{asset('assets/images/'.$gs->loader)}}" alt="">
               </div>
               <div class="modal-header">
                  <h5 class="modal-title">{{ $langg->lang199 }}</h5>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body">
                  <div class="container quick-view-modal">
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Product Quick View Modal -->
      <!-- Order Tracking modal Start-->
      <div class="modal fade" id="track-order-modal" tabindex="-1" role="dialog" aria-labelledby="order-tracking-modal" aria-hidden="true">
         <div class="modal-dialog modal-dialog-scrollable modal-lg" role="document">
            <div class="modal-content">
               <div class="modal-header">
                  <h6 class="modal-title"> <b>Order Tracking</b> </h6>
                  <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                  </button>
               </div>
               <div class="modal-body">
                  <div class="order-tracking-content">
                     <form id="track-form" class="track-form">
                        {{ csrf_field() }}
                        <input type="text" id="track-code" placeholder="Get Trackng Code" required="">
                        <button type="submit" class="mybtn1">View Tracking</button>
                        <a href="#"  data-toggle="modal" data-target="#order-tracking-modal"></a>
                     </form>
                  </div>
                  <div>
                     <div class="submit-loader d-none">
                        <img src="{{asset('assets/images/'.$gs->loader)}}" alt="">
                     </div>
                     <div id="track-order">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <!-- Order Tracking modal End -->
      <!-- Compare section -->
      @if(Session::has('compare'))
      @if(!empty($cproducts))
      <div class="comparePanle" style="display: block">
         <div class=" product-compare-wrapper container">
            <div class="row align-items-center">
               <div class="comparePan col-md-9">
                  @foreach($cproducts as $compareList)
                  <div  class="compare-product-item pro-remove c{{$compareList['item']['id']}}">
                     <div class="compare-product-box">
                        <a class=" w3-closebtn cursor compare-remove" data-href="{{ route('product.compare.remove',$compareList['item']['id']) }}" data-class="c{{$compareList['item']['id']}}"> <span class="fal fa-times"></span> </a>
                        <!-- <img src="{{ $compareList['item']['thumbnail'] ? asset('assets/images/thumbnails/'.$compareList['item']['thumbnail']):asset('assets/images/noimage.png') }}" alt="image" style="height:100px;"/> -->
                        
                        <a href="{{ url('item/' . $compareList['item']['slug']  )}}" class="compare-proTitle">
                           <span id="' + data['name'] + '" class="titleMargin1">
                           {{ $compareList['item']['name'] }} </span>
                        </a>
                     </div>
                  </div>
                  @endforeach
               </div>
               <div class="col-md-3 text-right">
                  <a href="{{ route('product.compare') }}" class="notActive cmprBtn pro-compare-btn" >Compare Products</a>
               </div>
            </div>
         </div>
      </div>
      @endif
      @endif
      <!-- <div class="w3-container  w3-center">
         <div class="w3-row w3-card-4 w3-grey w3-border comparePanle w3-margin-top" >
            <div class="w3-row">
               <div class="w3-col l9 m8 s6 w3-margin-top">
                  <h4>Added for comparison</h4>
               </div>
               <div class="w3-col l3 m4 s6 w3-margin-top">
                  &nbsp;
                  <a href="{{ route('product.compare') }}" class="w3-btn w3-round-small w3-white w3-border notActive cmprBtn" >Compare</a>
               </div>
            </div>
            <div class=" titleMargin w3-container comparePan">
               @if(!empty($cproducts))
               @foreach($cproducts as $compareList)
               <div  class="relPos titleMargin w3-margin-bottom  w3-col l3 m4 s4 pro-remove c{{$compareList['item']['id']}}">
                  <div class="w3-white titleMargin">
                     <a class=" w3-closebtn cursor compare-remove" data-href="{{ route('product.compare.remove',$compareList['item']['id']) }}" data-class="c{{$compareList['item']['id']}}">&times</a>
                     <img src="{{ $compareList['item']['thumbnail'] ? asset('assets/images/thumbnails/'.$compareList['item']['thumbnail']):asset('assets/images/noimage.png') }}" alt="image" style="height:100px;"/></a>
                     <p id="' + data['name'] + '" class="titleMargin1"><a href="{{ url('item/' . $compareList['item']['slug']  )}}">{{ $compareList['item']['name'] }}</a></p>
                  </div>
               </div>
               @endforeach
               @endif
            </div>
         </div>
      </div> -->
      <!-- Compare section end -->
      <!--  warning model  -->
      <div id="WarningModal" class="w3-modal">
         <div class="w3-modal-content warningModal">
            <header class="w3-container w3-teal">
               <h3><span>&#x26a0;</span>Error</h3>
            </header>
            <div class="w3-container">
               <h4>Maximum of Three products are allowed for comparision</h4>
            </div>
            <footer class="w3-container w3-right-align">
               <button id="warningModalClose" onclick="document.getElementById('id01').style.display='none'" class="w3-btn w3-hexagonBlue w3-margin-bottom  ">Ok</button>
            </footer>
         </div>
      </div>
      <!--  end of warning model  -->
      <script type="text/javascript">
         var mainurl = "{{url('/')}}";
         var gs      = {!! json_encode($gs) !!};
         var langg    = {!! json_encode($langg) !!};
      </script>
      <!-- jquery -->
      <script src="{{asset('assets/front/js/jquery.js')}}"></script>
      <script src="{{asset('assets/front/jquery-ui/jquery-ui.min.js')}}"></script>
      <!-- popper -->
      <script src="{{asset('assets/front/js/popper.min.js')}}"></script>
      <!-- bootstrap -->
      <script src="{{asset('assets/front/js/bootstrap.min.js')}}"></script>
      <!-- plugin js-->
      <script src="{{asset('assets/front/js/plugin.js')}}"></script>
      <script src="{{asset('assets/front/js/xzoom.min.js')}}"></script>
      <script src="{{asset('assets/front/js/jquery.hammer.min.js')}}"></script>
      <script src="{{asset('assets/front/js/setup.js')}}"></script>
      <script src="{{asset('assets/front/js/owl.carousel.min.js')}}"></script>
      <script src="{{asset('assets/front/js/toastr.js')}}"></script>
      <!-- main -->
      <script src="{{asset('assets/front/js/main.js')}}"></script>
      <!-- custom -->
      <script src="{{asset('assets/front/js/custom.js')}}"></script>
      <!-- Typeahead js for search -->
      <script src="{{asset('assets/front/js/typeahead.js')}}"></script>  
      <script type="text/javascript">
         $('#track-form').on('submit',function(e){
             e.preventDefault();
             var code = $('#track-code').val();
             $('.submit-loader').removeClass('d-none');
             $('#track-order').load('{{ url("order/track/") }}/'+code,function(response, status, xhr){
            if(status == "success")
            {
                  $('.submit-loader').addClass('d-none');
            }
         	});
         });
         
         ////////////typeahead js for search box//////////////////
         
         $('#search_text').typeahead({
                 source: function (query, result) {
                     $.ajax({
                         url: "{{url('/')}}/autocomplete/"+query,
                         dataType: "json",
                         type: "get",
                         success: function (data) {
                             result($.map(data, function (item) {
                                return item;
                             }));
                         }
                     });
                 },
                 select: function () {
                    var searchVal = this.$menu.find('.active').data('value');
                   // var builderurl='';
                    var valArray = searchVal.split(': ');
                    $('#search_text').val(valArray[1]);
                 
                 }
             });
         
         
      </script>
      {!! $seo->google_analytics !!}
      @if($gs->is_talkto == 1)
      <!--Start of Tawk.to Script-->
      {!! $gs->talkto !!}
      <!--End of Tawk.to Script-->
      @endif
      @yield('scripts')
   </body>
</html>