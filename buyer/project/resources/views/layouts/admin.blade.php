<!doctype html>
<html lang="en" dir="ltr">
    
<head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="author" content="GeniusOcean">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Title -->
        <title>{{$gs->title}}</title>
        <!-- favicon -->
        <link rel="icon"  type="image/x-icon" href="{{asset('assets/images/'.$gs->favicon)}}"/>
        <!-- Bootstrap -->
        <!-- <link href="{{asset('assets/admin/css/bootstrap.min.css')}}" rel="stylesheet" /> -->
        <!-- Fontawesome -->
        <link rel="stylesheet" href="{{asset('assets/admin/css/fontawesome.css')}}">
        <!-- icofont -->
        <link rel="stylesheet" href="{{asset('assets/admin/css/icofont.min.css')}}">
        <!-- Sidemenu Css -->
        <!-- <link href="{{asset('assets/admin/plugins/fullside-menu/css/dark-side-style.css')}}" rel="stylesheet" /> -->
        <!-- <link href="{{asset('assets/admin/plugins/fullside-menu/waves.min.css')}}" rel="stylesheet" /> -->

        <!-- <link href="{{asset('assets/admin/css/plugin.css')}}" rel="stylesheet" /> -->

        <link href="{{asset('assets/admin/css/jquery.tagit.css')}}" rel="stylesheet" />     
        <link rel="stylesheet" href="{{ asset('assets/admin/css/bootstrap-coloroicker.css') }}">
        <!-- Main Css -->

        <!-- stylesheet -->
        @if(DB::table('admin_languages')->where('is_default','=',1)->first()->rtl == 1)

        <link href="{{asset('assets/admin/css/rtl/style.css')}}" rel="stylesheet"/>
        <link href="{{asset('assets/admin/css/rtl/custom.css')}}" rel="stylesheet"/>
        <link href="{{asset('assets/admin/css/rtl/responsive.css')}}" rel="stylesheet" />   
        <link href="{{asset('assets/admin/css/rtl/common.css')}}" rel="stylesheet" />
    
        @else 
        
        <link href="{{asset('assets/admin/css/style.css')}}" rel="stylesheet"/>
        <link href="{{asset('assets/admin/css/custom.css')}}" rel="stylesheet"/>
        <link href="{{asset('assets/admin/css/responsive.css')}}" rel="stylesheet" />
        <link href="{{asset('assets/admin/css/common.css')}}" rel="stylesheet" />
        <link href="{{asset('assets/admin/css/lite-purple.min.css')}}" rel="stylesheet" />
        <link href="{{asset('assets/admin/css/perfect-scrollbar.css')}}" rel="stylesheet" />
        @endif      

        @yield('styles')

    </head>
    <body>
        <div class="page">
            <div class="page-main app-admin-wrap layout-sidebar-large clearfix">
                <!-- Header Menu Area Start -->
                <div class="main-header">
                    <div class="logo">
                        <img src="{{asset('assets/images/varselor.png')}}" alt="Varselor">
                    </div>

                    <div class="menu-toggle">
                        <div></div>
                                <div></div>
                        <div></div>
                    </div>

                    <div class="d-flex align-items-center">
                        <div class="search-bar">
                            <input type="text" placeholder="Search">
                            <i class="search-icon text-muted i-Magnifi-Glass1"></i>
                        </div>
                    </div>

                    <div style="margin: auto"></div>

                    <div class="header-part-right">
                        <!-- Full screen toggle -->
                        <i class="i-Full-Screen header-icon d-none d-sm-inline-block" data-fullscreen></i>
                        <!-- Grid menu Dropdown -->
                        <div class="dropdown widget_dropdown">
                            <i class="i-Safe-Box text-muted header-icon" role="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></i>
                            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                <div class="menu-icon-grid">
                                    <a href="#"><i class="i-Shop-4"></i> Home</a>
                                    <a href="#"><i class="i-Library"></i> UI Kits</a>
                                    <a href="#"><i class="i-Drop"></i> Apps</a>
                                    <a href="#"><i class="i-File-Clipboard-File--Text"></i> Forms</a>
                                    <a href="#"><i class="i-Checked-User"></i> Sessions</a>
                                    <a href="#"><i class="i-Ambulance"></i> Support</a>
                                </div>
                            </div>
                        </div>
                        <!-- Notificaiton -->
                        <div class="dropdown">
                            <div class="badge-top-container" role="button" id="dropdownNotification" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <span class="badge badge-primary">3</span>
                                <i class="i-Bell text-muted header-icon"></i>
                            </div>
                            <!-- Notification dropdown -->
                            <div class="dropdown-menu dropdown-menu-right notification-dropdown rtl-ps-none" aria-labelledby="dropdownNotification" data-perfect-scrollbar data-suppress-scroll-x="true">
                                <div class="dropdown-item d-flex">
                                    <div class="notification-icon">
                                        <i class="i-Speach-Bubble-6 text-primary mr-1"></i>
                                    </div>
                                    <div class="notification-details flex-grow-1">
                                        <p class="m-0 d-flex align-items-center">
                                            <span>New message</span>
                                            <span class="badge badge-pill badge-primary ml-1 mr-1">new</span>
                                            <span class="flex-grow-1"></span>
                                            <span class="text-small text-muted ml-auto">10 sec ago</span>
                                        </p>
                                        <p class="text-small text-muted m-0">James: Hey! are you busy?</p>
                                    </div>
                                </div>
                                <div class="dropdown-item d-flex">
                                    <div class="notification-icon">
                                        <i class="i-Receipt-3 text-success mr-1"></i>
                                    </div>
                                    <div class="notification-details flex-grow-1">
                                        <p class="m-0 d-flex align-items-center">
                                            <span>New order received</span>
                                            <span class="badge badge-pill badge-success ml-1 mr-1">new</span>
                                            <span class="flex-grow-1"></span>
                                            <span class="text-small text-muted ml-auto">2 hours ago</span>
                                        </p>
                                        <p class="text-small text-muted m-0">1 Headphone, 3 iPhone x</p>
                                    </div>
                                </div>
                                <div class="dropdown-item d-flex">
                                    <div class="notification-icon">
                                        <i class="i-Empty-Box text-danger mr-1"></i>
                                    </div>
                                    <div class="notification-details flex-grow-1">
                                        <p class="m-0 d-flex align-items-center">
                                            <span>Product out of stock</span>
                                            <span class="badge badge-pill badge-danger ml-1 mr-1">3</span>
                                            <span class="flex-grow-1"></span>
                                            <span class="text-small text-muted ml-auto">10 hours ago</span>
                                        </p>
                                        <p class="text-small text-muted m-0">Headphone E67, R98, XL90, Q77</p>
                                    </div>
                                </div>
                                <div class="dropdown-item d-flex">
                                    <div class="notification-icon">
                                        <i class="i-Data-Power text-success mr-1"></i>
                                    </div>
                                    <div class="notification-details flex-grow-1">
                                        <p class="m-0 d-flex align-items-center">
                                            <span>Server Up!</span>
                                            <span class="badge badge-pill badge-success ml-1 mr-1">3</span>
                                            <span class="flex-grow-1"></span>
                                            <span class="text-small text-muted ml-auto">14 hours ago</span>
                                        </p>
                                        <p class="text-small text-muted m-0">Server rebooted successfully</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- Notificaiton End -->

                        <!-- User avatar dropdown -->
                        <div class="dropdown">
                            <div  class="user col align-self-end">
                                <!-- <img src="{{asset('assets/images/1.jpg')}}" id="userDropdown" alt="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> -->

                                <img src="{{ Auth::guard('admin')->user()->photo ? asset('assets/images/admins/'.Auth::guard('admin')->user()->photo ):asset('assets/images/noimage.png') }}" alt="" id="userDropdown" alt="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="userDropdown">
                                    <div class="dropdown-header">
                                        <i class="i-Lock-User mr-1"></i> Welcome Admin
                                    </div>

                                    <a href="{{ route('admin.profile') }}" class="dropdown-item"> {{ __('Edit Profile') }}</a>
                                    <a href="{{ route('admin.password') }}" class="dropdown-item">{{ __('Change Password') }}</a>
                                    <a href="{{ route('admin.logout') }}" class="dropdown-item"> {{ __('Logout') }}</a>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- Header Menu Area End -->
                <div class="rightsidebar-wrapper">
                    <!-- Side Menu Area Start -->
                    <div class="side-content-wrap">
                        <div class="sidebar-left open rtl-ps-none" data-perfect-scrollbar data-suppress-scroll-x="true">
                            <ul class="navigation-left">
                                <li class="nav-item">
                                    <a class="nav-item-hold" href="{{route('admin.dashboard') }}">
                                        <i class="nav-icon i-File-Horizontal-Text"></i>
                                        <span class="nav-text">{{ __('Dashboard') }}</span>
                                    </a>
                                    <div class="triangle"></div>
                                </li>

                                <li class="nav-item" data-item="orderManagement">
                                    <a class="nav-item-hold accordion-toggle wave-effect" href="#">
                                        <i class="nav-icon i-Library"></i>
                                        <span class="nav-text">Order Management</span>
                                    </a>
                                    <div class="triangle"></div>
                                </li>


                                <li class="nav-item" data-item="productManagement">
                                    <a class="nav-item-hold" href="#">
                                        <i class="nav-icon i-Suitcase"></i>
                                        <span class="nav-text">Product Management</span>
                                    </a>
                                    <div class="triangle"></div>
                                </li>
                                <li class="nav-item" data-item="vendorManagement">
                                    <a class="nav-item-hold" href="#">
                                        <i class="nav-icon i-Computer-Secure"></i>
                                        <span class="nav-text">Vendor Management</span>
                                    </a>
                                    <div class="triangle"></div>
                                </li>


                                <li class="nav-item" data-item="customerManagement">
                                    <a class="nav-item-hold" href="#">
                                        <i class="nav-icon i-File-Clipboard-File--Text"></i>
                                        <span class="nav-text">Customer Management</span>
                                    </a>
                                    <div class="triangle"></div>
                                </li>

                                <li class="nav-item" data-item="contentManagement">
                                    <a class="nav-item-hold" href="#">
                                        <i class="nav-icon i-Bar-Chart-5"></i>
                                        <span class="nav-text">Content Management</span>
                                    </a>
                                    <div class="triangle"></div>
                                </li>


                                <li class="nav-item" data-item="paymentManagement">
                                    <a class="nav-item-hold" href="#">
                                        <i class="nav-icon i-Administrator"></i>
                                        <span class="nav-text">Payment Management</span>
                                    </a>
                                    <div class="triangle"></div>
                                </li>
                                @if(Auth::guard('admin')->user()->IsAdmin())
                                <li class="nav-item" data-item="userManagement">
                                    <a class="nav-item-hold" href="">
                                        <i class="nav-icon i-Double-Tap"></i>
                                        <span class="nav-text">User Management</span>
                                    </a>
                                    <div class="triangle"></div>
                                </li>
                                @endif

                                @if(Auth::guard('admin')->user()->IsAdmin())
                                <!-- <li class="nav-item" data-item="systemActivation">
                                    <a class="nav-item-hold" href="">
                                        <i class="nav-icon i-Double-Tap"></i>
                                        <span class="nav-text"> {{ __('System Activation') }} </span>
                                    </a>
                                    <div class="triangle"></div>
                                </li> -->
                                @endif
                            </ul>
                        </div>

                        <div class="sidebar-left-secondary rtl-ps-none" data-perfect-scrollbar data-suppress-scroll-x="true">
                            <!-- Submenu Dashboards -->
                            <div class="submenu-area" data-parent="orderManagement">
                                <ul class="childNav" data-parent="orderManagement">
                                    <li class="nav-item ">
                                        <a class="" href="{{route('admin-order-index')}}">
                                            <i class="nav-icon i-Clock-3"></i>
                                            <span class="item-name">{{ __('All Orders') }}</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{route('admin-order-pending')}}"  class="">
                                            <i class="nav-icon i-Clock-4"></i>
                                            <span class="item-name">{{ __('Pending Orders') }}</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="" href="{{route('admin-order-processing')}}">
                                            <i class="nav-icon i-Over-Time"></i>
                                            <span class="item-name">{{ __('Processing Orders') }}</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="" href="{{route('admin-order-completed')}}">
                                            <i class="nav-icon i-Clock"></i>
                                            <span class="item-name">{{ __('Completed Orders') }}</span>
                                        </a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="" href="{{route('admin-order-declined')}}">
                                            <i class="nav-icon i-Clock"></i>
                                            <span class="item-name">{{ __('Declined Orders') }}</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                            <div class="submenu-area" data-parent="productManagement">
                                <ul class="childNav" data-parent="productManagement">
                                    <li class="nav-item">
                                        <a class="" href="{{ route('brands-view') }}">
                                            <i class="nav-icon i-File-Clipboard-Text--Image"></i>
                                            <span class="item-name">Manage Brands</span>
                                        </a>
                                    </li>

                                    <li class="nav-item dropdown-sidemenu">
                                        <a>
                                            <i class="nav-icon i-Receipt"></i>
                                            <span class="item-name"> {{ __('Products') }} </span>
                                            <i class="dd-arrow i-Arrow-Down"></i>
                                        </a>
                                        <ul class="submenu">
                                            <li>
                                                <a class="" href="{{ route('admin-prod-types') }}">
                                                    <i class="nav-icon i-Receipt"></i>
                                                    <span class="item-name"> {{ __('Add New Product') }} </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="" href="{{ route('admin-prod-index') }}">
                                                    <i class="nav-icon i-Receipt-4"></i>
                                                    <span class="item-name">{{ __('All Products') }}</span>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="" href="{{ route('admin-prod-deactive') }}">
                                                    <i class="nav-icon i-Receipt-4"></i>
                                                    <span class="item-name">{{ __('Deactivated Product') }}</span>
                                                </a>
                                            </li>
                                            <li></li>
                                        </ul>
                                    </li>

                                    <!-- <li class="nav-item">
                                        <a class="" href="{{ route('admin-prod-import') }}">
                                            <i class="nav-icon i-File-Clipboard-Text--Image"></i>
                                            <span class="item-name"> {{ __('Bulk Product Upload') }} </span>
                                        </a>
                                    </li> -->

                                    <li class="nav-item dropdown-sidemenu">
                                        <a>
                                            <i class="nav-icon i-Receipt"></i>
                                            <span class="item-name"> {{ __('Manage Categories') }} </span>
                                            <i class="dd-arrow i-Arrow-Down"></i>
                                        </a>
                                        <ul class="submenu">
                                            <li>
                                                <a class="" href="{{ route('admin-cat-index') }}">
                                                    <i class="nav-icon i-Receipt"></i>
                                                    <span class="item-name"> {{ __('Main Category') }} </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="" href="{{ route('admin-subcat-index') }}">
                                                    <i class="nav-icon i-Receipt-4"></i>
                                                    <span class="item-name"> {{ __('Sub Category') }} </span>
                                                </a>
                                            </li>
                                            <!-- <li>
                                                <a class="" href="{{ route('admin-childcat-index') }}">
                                                    <i class="nav-icon i-Receipt-4"></i>
                                                    <span class="item-name">{{ __('Child Category') }}</span>
                                                </a>
                                            </li> -->
                                            <!-- <li></li> -->
                                        </ul>
                                    </li>

                                    <!-- <li class="nav-item dropdown-sidemenu">
                                        <a>
                                            <i class="nav-icon i-Receipt"></i>
                                            <span class="item-name"> {{ __('Affiliate Products') }} </span>
                                            <i class="dd-arrow i-Arrow-Down"></i>
                                        </a>
                                        <ul class="submenu">
                                            <li>
                                                <a class="" href="{{ route('admin-import-create') }}">
                                                    <i class="nav-icon i-Receipt"></i>
                                                    <span class="item-name"> {{ __('Add Affiliate Product') }} </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="" href="{{ route('admin-import-index') }}">
                                                    <i class="nav-icon i-Receipt-4"></i>
                                                    <span class="item-name"> {{ __('All Affiliate Products') }} </span>
                                                </a>
                                            </li>
                                            <li></li>
                                        </ul>
                                    </li> -->

                                    <li class="nav-item dropdown-sidemenu">
                                        <a>
                                            <i class="nav-icon i-Receipt"></i>
                                            <span class="item-name"> {{ __('Product Discussion') }} </span>
                                            <i class="dd-arrow i-Arrow-Down"></i>
                                        </a>
                                        <ul class="submenu">
                                            <li>
                                                <a class="" href="{{ route('admin-rating-index') }}">
                                                    <i class="nav-icon i-Receipt"></i>
                                                    <span class="item-name"> {{ __('Product Reviews') }} </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="" href="{{ route('admin-comment-index') }}">
                                                    <i class="nav-icon i-Receipt-4"></i>
                                                    <span class="item-name"> {{ __('Comments') }} </span>
                                                </a>
                                            </li>
                                            <li></li>
                                        </ul>
                                    </li>

                                    <!-- <li class="nav-item">
                                        <a class="" href="{{ route('admin-prod-import') }}">
                                            <i class="nav-icon i-File-Clipboard-Text--Image"></i>
                                            <span class="item-name"> {{ __('Brand') }} </span>
                                        </a>
                                    </li> -->
                                     @if(Auth::guard('admin')->user()->IsAdmin())

                                    <li class="nav-item dropdown-sidemenu">
                                        <a>
                                            <i class="nav-icon i-Receipt"></i>
                                            <span class="item-name"> Others </span>
                                            <i class="dd-arrow i-Arrow-Down"></i>
                                        </a>
                                        <ul class="submenu">
                                            <li>
                                                <a class="" href="{{ route('admin-shipping-index') }}">
                                                    <i class="nav-icon i-Receipt"></i>
                                                    <span class="item-name"> {{ __('Shipping Methods') }} </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="" href="{{ route('admin-package-index') }}">
                                                    <i class="nav-icon i-Receipt-4"></i>
                                                    <span class="item-name"> {{ __('Packagings') }} </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="" href="{{ route('admin-pick-index') }}">
                                                    <i class="nav-icon i-Receipt"></i>
                                                    <span class="item-name">{{ __('Pickup Locations') }} </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="" href="{{ route('admin-gs-affilate') }}">
                                                    <i class="nav-icon i-Receipt-4"></i>
                                                    <span class="item-name">{{__('Affiliate Information')}}</span>
                                                </a>
                                            </li>
                                            <li></li>
                                        </ul>
                                    </li>
                                    @endif
                                </ul>
                            </div>

                            <div class="submenu-area" data-parent="vendorManagement">
                                <ul class="childNav" data-parent="vendorManagement">
                                    <li class="nav-item ">
                                        <a class="" href="{{ route('admin-vendor-index') }}">
                                            <i class="nav-icon i-Clock-3"></i>
                                            <span class="item-name">{{ __('Vendors List') }}</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ route('admin-vendor-withdraw-index') }}"  class="">
                                            <i class="nav-icon i-Clock-4"></i>
                                            <span class="item-name">{{ __('Withdraws') }}</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="" href="{{ route('admin-vendor-subs') }}">
                                            <i class="nav-icon i-Over-Time"></i>
                                            <span class="item-name">{{ __('Vendor Subscriptions') }}</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="" href="{{ route('admin-vendor-color') }}">
                                            <i class="nav-icon i-Clock"></i>
                                            <span class="item-name">{{ __('Default Background') }}</span>
                                        </a>
                                    </li>

                                    <li class="nav-item">
                                        <a class="" href="{{ route('admin-subscription-index') }}">
                                            <i class="nav-icon i-Clock"></i>
                                            <span class="item-name">{{ __('Vendor Subscription Plans') }}</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                            <div class="submenu-area" data-parent="customerManagement">
                                <ul class="childNav" data-parent="customerManagement">
                                    <li class="nav-item ">
                                        <a class="" href="{{ route('admin-vendor-index') }}">
                                            <i class="nav-icon i-Clock-3"></i>
                                            <span class="item-name">{{ __('Customers List') }}</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{ route('admin-vendor-withdraw-index') }}"  class="">
                                            <i class="nav-icon i-Clock-4"></i>
                                            <span class="item-name">{{ __('Withdraws') }}</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="" href="{{ route('admin-vendor-subs') }}">
                                            <i class="nav-icon i-Over-Time"></i>
                                            <span class="item-name">{{ __('Customer Default Image') }}</span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="" href="{{ route('admin-vendor-color') }}">
                                            <i class="nav-icon i-Clock"></i>
                                            <span class="item-name">{{ __('Subscribers') }}</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>

                            <div class="submenu-area" data-parent="contentManagement">
                                <ul class="childNav" data-parent="contentManagement">

                                    <li class="nav-item dropdown-sidemenu">
                                        <a>
                                            <i class="nav-icon i-Receipt"></i>
                                            <span class="item-name"> {{ __('Messages') }} </span>
                                            <i class="dd-arrow i-Arrow-Down"></i>
                                        </a>
                                        <ul class="submenu">
                                            <li>
                                                <a class="" href="{{ route('admin-message-index') }}">
                                                    <i class="nav-icon i-Receipt"></i>
                                                    <span class="item-name"> {{ __('Tickets') }} </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="" href="{{ route('admin-message-dispute') }}">
                                                    <i class="nav-icon i-Receipt-4"></i>
                                                    <span class="item-name">{{ __('Disputes') }}</span>
                                                </a>
                                            </li>
                                            <li></li>
                                        </ul>
                                    </li>

                                    @if(Auth::guard('admin')->user()->IsAdmin())
                                    <li class="nav-item dropdown-sidemenu">
                                        <a>
                                            <i class="nav-icon i-Receipt"></i>
                                            <span class="item-name"> {{ __('General Settings') }} </span>
                                            <i class="dd-arrow i-Arrow-Down"></i>
                                        </a>
                                        <ul class="submenu">
                                            <li>
                                                <a class="" href="{{ route('admin-gs-popup') }}">
                                                    <i class="nav-icon i-Receipt"></i>
                                                    <span class="item-name"> {{ __('Popup Banner') }} </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="" href="{{ route('admin-gs-error-banner') }}">
                                                    <i class="nav-icon i-Receipt-4"></i>
                                                    <span class="item-name"> {{ __('Error Banner') }} </span>
                                                </a>
                                            </li>
                                            <li></li>
                                        </ul>
                                    </li>
                                    @endif

                                    <li class="nav-item dropdown-sidemenu">
                                        <a>
                                            <i class="nav-icon i-Receipt"></i>
                                            <span class="item-name"> {{ __('Home Page Settings') }} </span>
                                            <i class="dd-arrow i-Arrow-Down"></i>
                                        </a>
                                        <ul class="submenu">
                                            <li>
                                                <a class="" href="{{ route('admin-review-index') }}">
                                                    <i class="nav-icon i-Receipt"></i>
                                                    <span class="item-name"> {{ __('Reviews') }} </span>
                                                </a>
                                            </li>
                                            <li></li>
                                        </ul>
                                    </li>

                                    <li class="nav-item dropdown-sidemenu">
                                        <a>
                                            <i class="nav-icon i-Receipt"></i>
                                            <span class="item-name"> {{ __('Email Settings') }}</span>
                                            <i class="dd-arrow i-Arrow-Down"></i>
                                        </a>
                                        <ul class="submenu">
                                            <li>
                                                <a class="" href="{{route('admin-mail-index')}}">
                                                    <i class="nav-icon i-Receipt"></i>
                                                    <span class="item-name"> {{ __('Email Template') }} </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="" href="{{route('admin-mail-config')}}">
                                                    <i class="nav-icon i-Receipt-4"></i>
                                                    <span class="item-name"> {{ __('Email Configurations') }} </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="" href="{{route('admin-group-show')}}">
                                                    <i class="nav-icon i-Receipt-4"></i>
                                                    <span class="item-name"> {{ __('Group Email') }} </span>
                                                </a>
                                            </li>
                                            <li></li>
                                        </ul>
                                    </li>

                                    <li class="nav-item dropdown-sidemenu">
                                        <a>
                                            <i class="nav-icon i-Receipt"></i>
                                            <span class="item-name"> {{ __('Social Settings') }} </span>
                                            <i class="dd-arrow i-Arrow-Down"></i>
                                        </a>
                                        <ul class="submenu">
                                            <li>
                                                <a class="" href="{{route('admin-social-index')}}">
                                                    <i class="nav-icon i-Receipt"></i>
                                                    <span class="item-name"> {{ __('Social Links') }} </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="" href="{{route('admin-social-facebook')}}">
                                                    <i class="nav-icon i-Receipt-4"></i>
                                                    <span class="item-name"> {{ __('Facebook Login') }} </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="" href="{{route('admin-social-google')}}">
                                                    <i class="nav-icon i-Receipt"></i>
                                                    <span class="item-name"> {{ __('Google Login') }} </span>
                                                </a>
                                            </li>
                                            <li></li>
                                        </ul>
                                    </li>

                                    <li class="nav-item dropdown-sidemenu">
                                        <a>
                                            <i class="nav-icon i-Receipt"></i>
                                            <span class="item-name"> {{ __('Language Settings') }} </span>
                                            <i class="dd-arrow i-Arrow-Down"></i>
                                        </a>
                                        <ul class="submenu">
                                            <li>
                                                <a class="" href="{{route('admin-lang-index')}}">
                                                    <i class="nav-icon i-Receipt"></i>
                                                    <span class="item-name"> {{ __('Website Language') }} </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="" href="{{route('admin-tlang-index')}}">
                                                    <i class="nav-icon i-Receipt-4"></i>
                                                    <span class="item-name"> {{ __('Admin Panel Language') }} </span>
                                                </a>
                                            </li>
                                            <li></li>
                                        </ul>
                                    </li>

                                    <li class="nav-item dropdown-sidemenu">
                                        <a>
                                            <i class="nav-icon i-Receipt"></i>
                                            <span class="item-name"> {{ __('SEO Tools') }} </span>
                                            <i class="dd-arrow i-Arrow-Down"></i>
                                        </a>
                                        <ul class="submenu">
                                            <li>
                                                <a class="" href="{{ route('admin-prod-popular',30) }}">
                                                    <i class="nav-icon i-Receipt"></i>
                                                    <span class="item-name"> {{ __('Popular Products') }} </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="" href="{{ route('admin-seotool-analytics') }}">
                                                    <i class="nav-icon i-Receipt-4"></i>
                                                    <span class="item-name"> {{ __('Google Analytics') }} </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a class="" href="{{ route('admin-seotool-keywords') }}">
                                                    <i class="nav-icon i-Receipt-4"></i>
                                                    <span class="item-name"> {{ __('Website Meta Keywords') }} </span>
                                                </a>
                                            </li>
                                            <li></li>
                                        </ul>
                                    </li>
                                </ul>
                            </div>

                            <div class="submenu-area" data-parent="paymentManagement">
                                <ul class="childNav" data-parent="paymentManagement">
                                    <li class="nav-item ">
                                        <a class="" href="{{ route('admin-coupon-index') }}">
                                            <i class="nav-icon i-Clock-3"></i>
                                            <span class="item-name">{{ __('Set Coupons') }}</span>
                                        </a>
                                    </li>

                                    @if(Auth::guard('admin')->user()->IsAdmin())
                                    <li class="nav-item">
                                        <a href="{{route('admin-gs-payments')}}"  class="">
                                            <i class="nav-icon i-Clock-4"></i>
                                            <span class="item-name"> {{__('Payment Information')}} </span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="" href="{{route('admin-payment-index')}}">
                                            <i class="nav-icon i-Over-Time"></i>
                                            <span class="item-name"> {{ __('Payment Gateways') }} </span>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="" href="{{route('admin-currency-index')}}">
                                            <i class="nav-icon i-Clock"></i>
                                            <span class="item-name"> {{ __('Currencies') }} </span>
                                        </a>
                                    </li>
                                    @endif
                                </ul>
                            </div>

                            @if(Auth::guard('admin')->user()->IsAdmin())
                            <div class="submenu-area" data-parent="userManagement">
                                <ul class="childNav" data-parent="userManagement">
                                    <li class="nav-item">
                                        <a href="{{ route('admin-staff-index') }}"  class="">
                                            <i class="nav-icon i-Clock-4"></i>
                                            <span class="item-name"> {{ __('Manage Staffs') }} </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            @endif

                            @if(Auth::guard('admin')->user()->IsAdmin())
                            <div class="submenu-area" data-parent="systemActivation">
                                <ul class="childNav" data-parent="systemActivation">
                                    <li class="nav-item">
                                        <a href="{{route('admin-activation-form')}}"  class="">
                                            <i class="nav-icon i-Clock-4"></i>
                                            <span class="item-name"> {{ __('Activation') }} </span>
                                        </a>
                                    </li>

                                    <li class="nav-item">
                                        <a href="{{route('admin-generate-backup')}}"  class="">
                                            <i class="nav-icon i-Clock-4"></i>
                                            <span class="item-name"> {{ __('Generate Backup') }} </span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            @endif

                        </div>
                        <div class="sidebar-overlay"></div>
                    </div>                    
                </div>

                <div class="main-content-wrap sidenav-open d-flex flex-column">
                	<div class="main-content">
                		<!-- Main Content Area Start -->
                    	@yield('content')
                    	<!-- Main Content Area End -->
                	</div>
                </div>
            </div>
        </div>

   <!-- ============ Search UI Start ============= -->
  <div class="search-ui">
        <div class="search-header">
            <img src="{{asset('assets/images/varselor.png')}}" alt="" class="logo">
            <button class="search-close btn btn-icon bg-transparent float-right mt-2">
                <i class="i-Close-Window text-22 text-muted"></i>
            </button>
        </div>

        <input type="text" placeholder="Type here" class="search-input w-100" autofocus>

        <!-- <div class="search-title">
            <span class="text-muted">Search results</span>
        </div> -->

        <!-- <div class="search-results list-horizontal">
            <div class="list-item col-md-12 p-0">
                <div class="card o-hidden flex-row mb-4 d-flex">
                    <div class="list-thumb d-flex">
                        <img src="{{asset('assets/images/products/headphone-1.jpg')}}" alt="">
                    </div>
                    <div class="flex-grow-1 pl-2 d-flex">
                        <div class="card-body align-self-center d-flex flex-column justify-content-between align-items-lg-center flex-lg-row">
                            <a href="" class="w-40 w-sm-100">
                                <div class="item-title">Headphone 1</div>
                            </a>
                            <p class="m-0 text-muted text-small w-15 w-sm-100">Gadget</p>
                            <p class="m-0 text-muted text-small w-15 w-sm-100">
                                $300
                                <del class="text-secondary">$400</del>
                            </p>
                            <p class="m-0 text-muted text-small w-15 w-sm-100 d-none d-lg-block item-badges">
                                <span class="badge badge-danger">Sale</span>
                            </p>
                        </div>

                    </div>
                </div>
            </div>
            <div class="list-item col-md-12 p-0">
                <div class="card o-hidden flex-row mb-4 d-flex">
                    <div class="list-thumb d-flex">
                        <img src="{{asset('assets/images/products/headphone-2.jpg')}}" alt="">
                    </div>
                    <div class="flex-grow-1 pl-2 d-flex">
                        <div class="card-body align-self-center d-flex flex-column justify-content-between align-items-lg-center flex-lg-row">
                            <a href="" class="w-40 w-sm-100">
                                <div class="item-title">Headphone 1</div>
                            </a>
                            <p class="m-0 text-muted text-small w-15 w-sm-100">Gadget</p>
                            <p class="m-0 text-muted text-small w-15 w-sm-100">
                                $300
                                <del class="text-secondary">$400</del>
                            </p>
                            <p class="m-0 text-muted text-small w-15 w-sm-100 d-none d-lg-block item-badges">
                                <span class="badge badge-primary">New</span>
                            </p>
                        </div>

                    </div>
                </div>
            </div>
            <div class="list-item col-md-12 p-0">
                <div class="card o-hidden flex-row mb-4 d-flex">
                    <div class="list-thumb d-flex">
                        <img src="{{asset('assets/images/products/headphone-3.jpg')}}" alt="">
                    </div>
                    <div class="flex-grow-1 pl-2 d-flex">
                        <div class="card-body align-self-center d-flex flex-column justify-content-between align-items-lg-center flex-lg-row">
                            <a href="" class="w-40 w-sm-100">
                                <div class="item-title">Headphone 1</div>
                            </a>
                            <p class="m-0 text-muted text-small w-15 w-sm-100">Gadget</p>
                            <p class="m-0 text-muted text-small w-15 w-sm-100">
                                $300
                                <del class="text-secondary">$400</del>
                            </p>
                            <p class="m-0 text-muted text-small w-15 w-sm-100 d-none d-lg-block item-badges">
                                <span class="badge badge-primary">New</span>
                            </p>
                        </div>

                    </div>
                </div>
            </div>
            <div class="list-item col-md-12 p-0">
                <div class="card o-hidden flex-row mb-4 d-flex">
                    <div class="list-thumb d-flex">
                        <img src="{{asset('assets/images/products/headphone-4.jpg')}}" alt="">
                    </div>
                    <div class="flex-grow-1 pl-2 d-flex">
                        <div class="card-body align-self-center d-flex flex-column justify-content-between align-items-lg-center flex-lg-row">
                            <a href="" class="w-40 w-sm-100">
                                <div class="item-title">Headphone 1</div>
                            </a>
                            <p class="m-0 text-muted text-small w-15 w-sm-100">Gadget</p>
                            <p class="m-0 text-muted text-small w-15 w-sm-100">
                                $300
                                <del class="text-secondary">$400</del>
                            </p>
                            <p class="m-0 text-muted text-small w-15 w-sm-100 d-none d-lg-block item-badges">
                                <span class="badge badge-primary">New</span>
                            </p>
                        </div>

                    </div>
                </div>
            </div>
        </div> -->
        <!-- <div class="col-md-12 mt-5 text-center">
            <nav aria-label="Page navigation example">
                <ul class="pagination d-inline-flex">
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </li>
                    <li class="page-item"><a class="page-link" href="#">1</a></li>
                    <li class="page-item"><a class="page-link" href="#">2</a></li>
                    <li class="page-item"><a class="page-link" href="#">3</a></li>
                    <li class="page-item">
                        <a class="page-link" href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                            <span class="sr-only">Next</span>
                        </a>
                    </li>
                </ul>
            </nav>
        </div> -->
    </div>
    <!-- ============ Search UI End ============= -->
            
        <script type="text/javascript">
          var mainurl = "{{url('/')}}";
          var admin_loader = {{ $gs->is_admin_loader }};
          var whole_sell = {{ $gs->wholesell }};
        </script>

        <!-- Dashboard Core -->
        <!-- <script src="{{asset('assets/admin/js/vendors/jquery-1.12.4.min.js')}}"></script>
        <script src="{{asset('assets/admin/js/vendors/bootstrap.min.js')}}"></script> -->
        <script src="{{asset('assets/admin/js/common-bundle-script.js')}}"></script>
        <script src="{{asset('assets/admin/js/jqueryui.min.js')}}"></script>
        <!-- Fullside-menu Js-->
        <script src="{{asset('assets/admin/plugins/fullside-menu/jquery.slimscroll.min.js')}}"></script>
        <script src="{{asset('assets/admin/plugins/fullside-menu/waves.min.js')}}"></script>

        <script src="{{asset('assets/admin/js/plugin.js')}}"></script>
        <script src="{{asset('assets/admin/js/Chart.min.js')}}"></script>
        <script src="{{asset('assets/admin/js/tag-it.js')}}"></script>
        <script src="{{asset('assets/admin/js/nicEdit.js')}}"></script>
        <script src="{{asset('assets/admin/js/bootstrap-colorpicker.min.js') }}"></script>
        <script src="{{asset('assets/admin/js/notify.js') }}"></script>

        <script src="{{asset('assets/admin/js/jquery.canvasjs.min.js')}}"></script>
        
        <script src="{{asset('assets/admin/js/load.js')}}"></script>
        <!-- Custom Js-->
        <script src="{{asset('assets/admin/js/custom.js')}}"></script>
        <!-- AJAX Js-->
        <script src="{{asset('assets/admin/js/myscript.js')}}"></script>
        <script src="{{asset('assets/admin/js/chosen.jquery.js')}}"></script>
        
        <script src="{{asset('assets/admin/js/script.js')}}"></script>
        <script src="{{asset('assets/admin/js/sidebar.large.script.js')}}"></script>
        @yield('scripts')

@if($gs->is_admin_loader == 0)
<style>
    div#geniustable_processing {
        display: none !important;
    }
</style>
@endif
<script type="text/javascript">
    
/**** Choose Multiple Option ******/
document.getElementById('output').innerHTML = location.search;
$(".chosen-select").chosen()

$(".chosen-select").change(function(event){
    if(event.target == this){
        //alert($(this).val());
        var value = $(this).val();
        var name = $(this).attr("id");
        //alert(name);
        $("input[name='"+name+"']").val(value);
        
     }
});

</script>
    </body>

</html>