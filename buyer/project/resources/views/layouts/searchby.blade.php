<section class="product-details-page">
  <div class="container">
        <div class="row">
	        <div class="col-lg-3 col-md-4 pl-0">
            <div class="product-filter-wrapper">
              <div class="product-filter-title">
                <h2> Filters </h2>
              </div>
              <div class="product-filter-box">
                <div class="product-filter-widget_list pro-filter-block">
                   <input type="hidden" name="category_id" value="{{$category->id}}" id="cat_id" >
                 
                    <h2>{{$filterby}}</h2>

                    <ul class="pro-filter-content cd-filters">
                      @if(!empty($searchBy))
                      @foreach($searchBy as $brandss)
                        <li>
                            <div class="form-group">
                              <input name="brands" class="form-check-input attribute-input" type="checkbox"  id="{{$brandss->name}}" value="{{$brandss->id}}">
                              <label class="form-check-label" for="{{$brandss->name}}"> {{$brandss->name}} </label>
                              <input type="hidden" name="filterBy" id="filterBy" value="{{$filterby}}" name="">
                            </div>
                        </li>
                      @endforeach
                      @endif
                        
                    </ul>
                </div>

                <div class="product-filter-widget_list pro-filter-block">
                    <h2> Filter 2</h2>
                    <ul class="pro-filter-content cd-filters">
                        <li>
                            <div class="form-group">
                              <input name="color[]" class="form-check-input attribute-input" type="checkbox" id="option1">
                              <label class="form-check-label" for="option1"> Option 1 </label>
                            </div>
                        </li>

                        <li>
                            <div class="form-group">
                              <input name="color[]" class="form-check-input attribute-input" type="checkbox" id="option2">
                              <label class="form-check-label" for="option2"> Option 2 </label>
                            </div>
                        </li>

                        <li>
                            <div class="form-group">
                              <input name="color[]" class="form-check-input attribute-input" type="checkbox" id="option3">
                              <label class="form-check-label" for="option3"> Option 3 </label>
                            </div>
                        </li>

                        <li>
                            <div class="form-group">
                              <input name="color[]" class="form-check-input attribute-input" type="checkbox" id="option4">
                              <label class="form-check-label" for="option4"> Option 4 </label>
                            </div>
                        </li>
                    </ul>
                </div>

                <div class="product-filter-widget_list pro-filter-block">
                    <h2> Filter 3</h2>
                    <ul class="pro-filter-content cd-filters">
                        <li>
                            <div class="form-group">
                              <input name="color[]" class="form-check-input attribute-input" type="checkbox" id="option1">
                              <label class="form-check-label" for="option1"> Option 1 </label>
                            </div>
                        </li>

                        <li>
                            <div class="form-group">
                              <input name="color[]" class="form-check-input attribute-input" type="checkbox" id="option2">
                              <label class="form-check-label" for="option2"> Option 2 </label>
                            </div>
                        </li>

                        <li>
                            <div class="form-group">
                              <input name="color[]" class="form-check-input attribute-input" type="checkbox" id="option3">
                              <label class="form-check-label" for="option3"> Option 3 </label>
                            </div>
                        </li>

                        <li>
                            <div class="form-group">
                              <input name="color[]" class="form-check-input attribute-input" type="checkbox" id="option4">
                              <label class="form-check-label" for="option4"> Option 4 </label>
                            </div>
                        </li>
                    </ul>
                </div>

                <div class="product-filter-widget_list pro-filter-block">
                    <h2> Filter 4</h2>
                    <ul class="pro-filter-content cd-filters">
                        <li>
                            <div class="form-group">
                              <input name="color[]" class="form-check-input attribute-input" type="checkbox" id="option1">
                              <label class="form-check-label" for="option1"> Option 1 </label>
                            </div>
                        </li>

                        <li>
                            <div class="form-group">
                              <input name="color[]" class="form-check-input attribute-input" type="checkbox" id="option2">
                              <label class="form-check-label" for="option2"> Option 2 </label>
                            </div>
                        </li>

                        <li>
                            <div class="form-group">
                              <input name="color[]" class="form-check-input attribute-input" type="checkbox" id="option3">
                              <label class="form-check-label" for="option3"> Option 3 </label>
                            </div>
                        </li>

                        <li>
                            <div class="form-group">
                              <input name="color[]" class="form-check-input attribute-input" type="checkbox" id="option4">
                              <label class="form-check-label" for="option4"> Option 4 </label>
                            </div>
                        </li>
                    </ul>
                </div>
              </div>
            </div>
  	    	</div>

          <div class="col-lg-9 p-lg-0">
            <div class="product-search-box">
              <form class="search-form" method="GET">
                <div class="input-group">
                  <input type="hidden" id="category_id" name="category_id" value="0">
                    <input type="text" class="form-control" id="prod_name" name="search" placeholder="Search" value="" required="" autocomplete="off">
                    <div class="input-group-append">
                        <button type="submit" class="input-group-text"><i class="fa fa-search" aria-hidden="true"></i></button>
                    </div>
                  </div>
              </form>
            </div>
          
