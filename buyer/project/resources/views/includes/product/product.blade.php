
									<div class="col-lg-3 col-md-4 col-6">


										<a href="{{ route('front.product', $prod->slug) }}" class="item">
											<div class="product-listing-grid">
												<div class="item-img product-listing-img">
													@if(!empty($prod->features))
														<div class="sell-area">
														@foreach($prod->features as $key => $data1)
															<span class="sale" style="background-color:{{ $prod->colors[$key] }}">{{ $prod->features[$key] }}</span>
															@endforeach 
														</div>
													@endif
														<!-- <div class="extra-list">
															<ul>
																<li>
																	@if(Auth::guard('web')->check())

																	<span class="add-to-wish" data-href="{{ route('user-wishlist-add',$prod->id) }}" data-toggle="tooltip" data-placement="right" title="{{ $langg->lang54 }}" data-placement="right"><i class="icofont-heart-alt" ></i>
																	</span>

																	@else 

																	<span rel-toggle="tooltip" title="{{ $langg->lang54 }}" data-toggle="modal" id="wish-btn" data-target="#comment-log-reg" data-placement="right">
																		<i class="icofont-heart-alt"></i>
																	</span>

																	@endif
																</li>
																<li>
																<span class="quick-view" rel-toggle="tooltip" title="{{ $langg->lang55 }}" href="javascript:;" data-href="{{ route('product.quick',$prod->id) }}" data-toggle="modal" data-target="#quickview" data-placement="right"> <i class="icofont-eye"></i>
																</span>
																</li>
																<li>
																	<span class="add-to-compare" data-href="{{ route('product.compare.add',$prod->id) }}"  data-toggle="tooltip" data-placement="right" title="{{ $langg->lang57 }}" data-placement="right">
																		<i class="icofont-exchange"></i>
																	</span>
																</li>
															</ul>
														</div> -->
													<img class="img-fluid m-auto d-block" src="{{ $prod->photo ? asset('assets/images/thumbnails/'.$prod->thumbnail):asset('assets/images/noimage.png') }}" alt="">
												</div>
												<div class="info prodct-grid-content">
													<!-- <div class="stars">
	                                                  <div class="ratings">
	                                                      <div class="empty-stars"></div>
	                                                      <div class="full-stars" style="width:{{App\Models\Rating::ratings($prod->id)}}%"></div>
	                                                  </div>
													</div> -->
													<!-- <h4 class="price">{{ $prod->setCurrency() }} <del><small>{{ $prod->showPreviousPrice() }}</small></del></h4> -->
															<h5 class="name productName">{{ $prod->showName() }}</h5>
															<p class="proVendor-name"> Placeholder Vendor Name Goes Here </p>

															<form action="">
																<div class="addToCompare-wrap">
																    <div class="custom-control custom-checkbox addToCompare-box">
																      <input type="checkbox" class="custom-control-input" id="customCheck" name="example1">
																      <label class="custom-control-label mt-0" for="customCheck">Add to comparison</label>
																    </div>
																</div>
														  	</form>
															<!-- <div class="item-cart-area">
																@if($prod->product_type == "affiliate")
																	<span class="add-to-cart-btn affilate-btn" data-href="{{ route('affiliate.product', $prod->slug) }}"><i class="icofont-cart"></i> {{ $langg->lang251 }}
																	</span>
																@else
																	<span class="add-to-cart add-to-cart-btn" data-href="{{ route('product.cart.add',$prod->id) }}">
																		<i class="icofont-cart"></i> {{ $langg->lang56 }}
																	</span>
																	<span class="add-to-cart-quick add-to-cart-btn" data-href="{{ route('product.cart.quickadd',$prod->id) }}">
																		<i class="icofont-cart"></i> {{ $langg->lang251 }}
																	</span>
																@endif

															</div> -->
												</div>
											</div>
										</a>

									</div>
