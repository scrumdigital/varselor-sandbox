
                                    @php $catProducts = CatalogController::getSearchCatProducts($category->id); 
                                    
                                    @endphp
                                     @foreach($catProducts as $item)
              <div class="col-lg-3 col-md-6 col-12">
                  <div class="product-listing-grid agencyPro-grid-wrap">
                    <div class="item-img product-listing-img">
                       <a href="{{ url('item/' . $item->slug)}}">
                        <img class="img-fluid m-auto d-block" src="{{asset('assets/images/thumbnails/')}}/{{$item->thumbnail}}" alt="">
                      </a>
                    </div>
                    <div class="info prodct-grid-content agency-pro-grid-content">
                          <h5 class="name productName"><a href="{{ url('item/' . $item->slug) }}"> {{$item->name}}</a></h5>
                        <div class="agency-seeOffer-btn addon-category-button">
                          <a href="javascript:;" class="seeOfferBtn  btn btn-hover add-to-compare" data-href="{{ route('product.compare.add',$item->id) }}" > Add to Comparison</a>
                        </div>
                    </div>
                  </div>
              </div>
              @endforeach