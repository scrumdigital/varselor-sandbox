@extends('layouts.admin') 
@section('content')  
<input type="hidden" id="headerdata" value="{{ __('CATEGORY') }}">
<div class="content-area">
   <div class="mr-breadcrumb">
      <div class="row">
         <div class="col-lg-12">
            <h4 class="heading">{{ __('Main Categories') }}</h4>
            <ul class="links">
               <li>
                  <a href="{{ route('admin.dashboard') }}">{{ __('Dashboard') }} </a>
               </li>
               <li><a href="javascript:;">{{ __('Manage Categories') }}</a></li>
               <li>
                  <a href="{{ route('admin-cat-index') }}">{{ __('Main Categories') }}</a>
               </li>
            </ul>
         </div>
      </div>
   </div>
   <div class="product-area">
      <div class="allproduct">
         @include('includes.admin.form-success')  
         <div class="table-responsiv categories-proTable">
            <table id="geniustable" class="table table-hover dt-responsive table-bordered table-striped" cellspacing="0" width="100%">
               <thead class="thead-dark">
                  <tr>
                     <th scope="col" width="5%">{{ __("S.No") }}</th>
                     <th scope="col" width="30%">{{ __("Name") }}</th>
                     <th scope="col">{{ __('Slug') }}</th>
                     <th scope="col" >{{ __("Status") }}</th>
                     <th scope="col" >{{ __("Actions") }}</th>
                  </tr>
               </thead>
            </table>
         </div>
      </div>
   </div>
</div>
{{-- ADD / EDIT MODAL --}}
<div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="modal1" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="submit-loader">
            <img  src="{{asset('assets/images/'.$gs->admin_loader)}}" alt="">
         </div>
         <div class="modal-header">
            <h5 class="modal-title"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __('Close') }}</button>
         </div>
      </div>
   </div>
</div>
{{-- ADD / EDIT MODAL ENDS --}}
{{-- DELETE MODAL --}}
<div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="modal1" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title">{{ __('Confirm Delete') }}</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>

         <div class="modal-body">
            <div class="delete-icon-box"> <span class="fal fa-times-circle"></span> </div>
            <h5> {{ __("Are you sure?") }}</h5>
            <p> {{ __('You are about to delete this Category. Everything under this category will be deleted') }}. </p>
            <!-- <p class="text-center">{{ __('You are about to delete this Category. Everything under this category will be deleted') }}.</p>
            <p class="text-center">{{ __('Do you want to proceed?') }}</p> -->

            <!-- Modal footer -->
             <div class="delete-action-box">
                <button type="button" class="btn btn-default delete-acitonBtn" data-dismiss="modal">{{ __("Cancel") }}</button>
                <a class="btn btn-danger btn-ok delete-acitonBtn">{{ __("Delete") }}</a>
             </div>
         </div>
      </div>
   </div>
</div>
{{-- DELETE MODAL ENDS --}}
@endsection    
@section('scripts')
{{-- DATA TABLE --}}
<script type="text/javascript">
   var table = $('#geniustable').DataTable({
   	   ordering: false,
                processing: true,
                serverSide: true,
                ajax: '{{ route('admin-cat-datatables') }}',
                columns: [
                			{ data: 's.no', name: 's.no' },
                         { data: 'name', name: 'name' },
                         { data: 'slug', name: 'slug' },
                         { data: 'status', searchable: false, orderable: false},
             			{ data: 'action', searchable: false, orderable: false }
   
                      ],
                 language : {
                 	processing: '<img src="{{asset('assets/images/'.$gs->admin_loader)}}">'
                 },
   		drawCallback : function( settings ) {
      				$('.select').niceSelect();	
   		}
             });
   
       	$(function() {
         $(".btn-area").append('<div class="col-sm-4 table-contents">'+
         	'<a class="add-btn btn btn-success add-row" data-href="{{route('admin-cat-create')}}" id="add-data" data-toggle="modal" data-target="#modal1">'+
           '<i class="nav-icon i-Add"></i> <span class="remove-mobile"> Add New Category</span>'+
           '</a>'+
           '</div>');
       });											
   							
   {{-- DATA TABLE ENDS--}}
   
</script>
@endsection