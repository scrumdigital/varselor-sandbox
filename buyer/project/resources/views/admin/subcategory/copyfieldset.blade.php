<!-- 
                       
                         <div class="col-lg-3">
                            <div class="right-area">
                              <h4 class="heading">{{ __("FieldName") }} *</h4>
                                <p class="sub-heading">(In Any Language)</p>
                           
                          </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="right-area">
                              <h4 class="heading">{{ __("FieldName") }} *</h4>
                                <p class="sub-heading">(In Any Language)</p>
                           
                          </div>
                        </div>
                        <div class="col-lg-5">
                            <div class="right-area">
                              <h4 class="heading">{{ __("FieldName") }} *</h4>
                                <p class="sub-heading">(In Any Language)</p>
                           
                          </div>
                        </div>
                      @foreach($data as $rows)
                        <div class="col-lg-3">
                            <div class="right-area">
                               <input type="text" class="form-control" name="fieldname[]" placeholder="{{ __("Enter Name") }}" required="" value="{{$rows->fieldname}}">
                          </div>
                        </div>
                       <div class="col-lg-3">
                            <div class="right-area">

                            <select name="fieldtype[]">
                              @if($rows->fieldtype=="text")
                              <option value="text" selected="selected"  >Text</option>
                              <option value="dropdown" >Dropdown</option>
                              <option value="checkbox">checkbox</option>
                              <option value="radio">Radio</option>
                              @endif
                              @if($rows->fieldtype=="dropdown")
                              <option value="dropdown" >Text</option>
                              <option value="dropdown" selected="selected">Dropdown</option>
                              <option value="checkbox">checkbox</option>
                              <option value="radio">Radio</option>
                              @endif
                              @if($rows->fieldtype=="checkbox")
                              <option value="dropdown" >Text</option>
                              <option value="dropdown" >Dropdown</option>
                              <option value="checkbox" selected="selected">checkbox</option>
                              <option value="radio">Radio</option>
                              @endif
                              @if($rows->fieldtype=="radio")
                              <option value="dropdown" >Text</option>
                              <option value="dropdown" >Dropdown</option>
                              <option value="checkbox">checkbox</option>
                              <option value="radio" selected="selected">Radio</option>
                              @endif
                            </select>
                          
                          </div>
                        </div>
                        <div class="col-lg-3">
                            <div class="right-area">
                            <input type="text" class="form-control" name="fieldvalue[]" placeholder="{{ __("Enter value") }}" required="" value="{{$rows->fieldvalue}}">
                          </div>
                        </div>
                         <div class="col-lg-2">
                            <div class="right-area">
                            <button type="button"   class="btn btn-danger btn_remove">X</button>
                           </div>
                        </div>
                        @endforeach

                         <div class="col-lg-12">
                            <div class="right-area">
                             <button type="button" name="add" id="add" class="btn btn-success">Add More</button>
                           </div>
                        </div>
                           
  <script src="{{asset('assets/admin/js/jquery.Jcrop.js')}}"></script>
    <script src="{{asset('assets/admin/js/jquery.SimpleCropper.js')}}"></script>

    <script>
 $(document).ready(function(){      
      var i=1;  


      $('#add').click(function(){  
           i++;  
           $('#copy_field').append('<din id="row'+i+'" class="row"><div  class="col-lg-4"><div class="right-area"><input type="hidden" name="count" value='+i+' /><input type="text" class="form-control" name="fieldname[]" placeholder="{{ __("Enter Name") }}" required="" value=""></div></div><div class="col-lg-3"><div class="right-area"><select name="fieldtype[]"><option value="text">Text</option><option value="dropdown">Dropdown</option> <option value="checkbox">Checkbox</option><option value="radio">Radio</option> </select></div> </div> <div class="col-lg-3"><div class="right-area"><input type="text" class="form-control" name="fieldvalue[]" placeholder="{{ __("Enter value") }}" required="" value=""> </div></div><div class="col-lg-2"><div class="right-area"><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></div></div></div>');  
      });  


      $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      });

   });
</script>              
     -->


<div class="product-area">
   <div class="allproduct">
      <div class="table-responsiv categories-proTable" id="dynamic_field">
         <table id="geniustable" class="table table-hover dt-responsive table-bordered table-striped" cellspacing="0" width="100%">
            <thead class="thead-dark">
               <tr>
                  <th scope="col">{{ __("FieldName") }} *</th>
                  <th scope="col">{{ __("FieldType") }} *</th>
                  <th scope="col" >{{ __("FieldValue") }} *</th>
                  <th scope="col" >{{ __("Actions") }}</th>
               </tr>
            </thead>

            <tbody>
               @foreach($data as $rows)
               <tr>
                  <td> <input type="text" class="form-control" name="fieldname[]" placeholder="{{ __("Enter Name") }}" required="" value="{{$rows->fieldname}}"></td>

                  <td>
                     <select name="fieldtype[]" class="form-control">
                        @if($rows->fieldtype=="text")
                        <option value="text" selected="selected">Text</option>
                        <option value="dropdown">Dropdown</option>
                        <option value="checkbox">checkbox</option>
                        <option value="radio">Radio</option>
                        @endif
                        @if($rows->fieldtype=="dropdown")
                        <option value="dropdown" >Text</option>
                        <option value="dropdown" selected="selected">Dropdown</option>
                        <option value="checkbox">checkbox</option>
                        <option value="radio">Radio</option>
                        @endif
                        @if($rows->fieldtype=="checkbox")
                        <option value="dropdown" >Text</option>
                        <option value="dropdown" >Dropdown</option>
                        <option value="checkbox" selected="selected">checkbox</option>
                        <option value="radio">Radio</option>
                        @endif
                        @if($rows->fieldtype=="radio")
                        <option value="dropdown" >Text</option>
                        <option value="dropdown" >Dropdown</option>
                        <option value="checkbox">checkbox</option>
                        <option value="radio" selected="selected">Radio</option>
                        @endif
                     </select>
                  </td>

                  <td>
                     <input type="text" class="form-control" name="fieldvalue[]" placeholder="{{ __("Enter value") }}" required="" value="{{$rows->fieldvalue}}">
                  </td>

                  <td>
                     <button type="button"   class="btn btn-danger btn_remove"> <i class="nav-icon i-Close-Window "></i> </button></a>
                  </td>
               </tr>
               @endforeach
            </tbody>
         </table>
      </div>
   </div>
</div>
<div class="row">
  <div class="col-12">
    <div class="addvarientrow-btn-box">
      <button type="button" class="btn btn-success add-row addCF" id="add" name="add">
      <i class="nav-icon i-Add "></i> Add More
      </button>
    </div>
  </div>
</div>

<script>
 $(document).ready(function(){      
      var i=1;  


      $('#add').click(function(){  
           i++;  
           $('#copy_field').append('<din id="row'+i+'" class="row"><div  class="col-lg-4"><div class="right-area"><input type="hidden" name="count" value='+i+' /><input type="text" class="form-control" name="fieldname[]" placeholder="{{ __("Enter Name") }}" required="" value=""></div></div><div class="col-lg-3"><div class="right-area"><select name="fieldtype[]"><option value="text">Text</option><option value="dropdown">Dropdown</option> <option value="checkbox">Checkbox</option><option value="radio">Radio</option> </select></div> </div> <div class="col-lg-3"><div class="right-area"><input type="text" class="form-control" name="fieldvalue[]" placeholder="{{ __("Enter value") }}" required="" value=""> </div></div><div class="col-lg-2"><div class="right-area"><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></div></div></div>');  
      });  


      $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      });

   });
</script>