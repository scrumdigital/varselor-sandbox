@extends('layouts.admin') 
@section('content')  
<input type="hidden" id="headerdata" value="{{ __("SUB CATEGORY") }}">
<div class="content-area">
   <div class="mr-breadcrumb">
      <div class="row">
         <div class="col-lg-12">
            <h4 class="heading">{{ $data->name }}</h4>
            <ul class="links">
               <li>
                  <a href="{{ route('admin.dashboard') }}">{{ __("Dashboard") }} </a>
               </li>
               <li><a href="javascript:;">{{ __("Manage Subcategory Features") }}</a></li>
               <li>
                  <a href="{{ route('admin-subcat-features-index',$data->id) }}">{{ __("Feature List") }}</a>
               </li>
            </ul>
         </div>
      </div>
   </div>
   <!-- <div class="product-area">
      <div class="row">
         <div class="col-lg-12">
            <div class="mr-table allproduct">
               @include('includes.admin.form-success')  
               <div class="table-responsiv">
                  <table id="geniustable" class="table table-hover dt-responsive" cellspacing="0" width="100%">
                     <thead>
                        <tr>
                           <th>{{ __("Name") }}</th>
                           <th>{{ __("Status") }}</th>
                           <th>{{ __("Actions") }}</th>
                        </tr>
                     </thead>
                     <tbody>
                        @foreach($featureLists as $featureList)
                        <tr>
                           <td>
                              {{$featureList->feature_name}}
                           </td>
                           <td>
                              <div class="action-list">
                                 <select class="process select droplinks '.$class.'">
                                 <option data-val="1" value="">Activated</option>
                                 <option data-val="0" value="">Deactivated</option>
                                 /select>
                              </div>
                           </td>
                           <td>
                              <div class="action-list"><a href="{{route('admin-subcat-view-features',$featureList->id)}}" >View</a><a href="javascript:;" data-href="" data-toggle="modal" data-target="#confirm-delete" class="delete"><i class="fas fa-trash-alt"></i></a></div>
                           </td>
                        </tr>
                        @endforeach
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div> -->
   <div class="row">
       <div class="col-lg-12 table-contents addBrand-btn-box">
          <a class="add-btn btn btn-success add-row" href="{{route('admin-subcat-features',[$data->id])}}" >
          <i class="nav-icon i-Add"></i> <span class="remove-mobile"> {{ __("Add New Feature List") }} </span>
          </a>
       </div>

       <div class="col-lg-12">
         <div class="product-area">
            <div class="allproduct">
               @include('includes.admin.form-success')  
               <div class="table-responsiv">
                  <table id="geniustable" class="table table-hover dt-responsive table-bordered table-striped" cellspacing="0" width="100%">
                     <thead class="thead-dark">
                        <tr>
                          <th scope="col" width="5%">{{ __("S.No") }}</th>
                          <th scope="col">{{ __("Name") }}</th>
                          <!-- <th scope="col">{{ __("Status") }}</th> -->
                          <th scope="col" width="15%">{{ __("Actions") }}</th>
                        </tr>
                     </thead>

                     <tbody>
                        @php
                        $i=1;
                        @endphp
                        @foreach($featureLists as $featureList)
                        <tr>
                          <td>{{$i}}</td>
                           <td>
                              {{$featureList->feature_name}}
                           </td>
                           <!-- <td>
                              <div class="action-list switch-active-deactive-box">
                                 <select class="process select droplinks '.$class.'">
                                    <option data-val="1" value="">Activated</option>
                                    <option data-val="0" value="">Deactivated</option>
                                 </select>
                              </div>
                           </td> -->
                           <td>
                              <div class="godropdown">
                                 <button class="go-dropdown-toggle"> Actions<span class="fal fa-chevron-down"></span></button>

                                 <div class="action-list product-list-action" >

                                    <a class="btn product-table-btn" href="{{route('admin-subcat-view-features',$featureList->id)}}" ><span class="fal fa-eye"></span> View </a>


                                    <a href="javascript:;" data-href="" data-toggle="modal" data-target="#confirm-delete" class="delete btn product-table-btn"><span class="fal fa-trash-alt"></span>Delete</a>
                                  </div>
                              </div>
                           </td>
                        </tr>
                        @php
                        $i++;
                        @endphp
                        @endforeach
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
       </div>
   </div>
</div>

{{-- ADD / EDIT MODAL --}}
<div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="modal1" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
         <div class="submit-loader">
            <img  src="{{asset('assets/images/'.$gs->admin_loader)}}" alt="">
         </div>
         <div class="modal-header">
            <h5 class="modal-title"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __("Close") }}</button>
         </div>
      </div>
   </div>
</div>
{{-- ADD / EDIT MODAL ENDS --}}
{{-- DELETE MODAL --}}
<div class="modal fade confirm-delete-modal" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="modal1" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title">{{ __("Confirm Delet") }}e</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>

         <!-- Modal body -->
         <div class="modal-body">
            <div class="delete-icon-box"> <span class="fal fa-times-circle"></span> </div>
            <h5> {{ __("Are you sure?") }}</h5>
            <p class="text-center">{{ __("You are about to delete this Category. Everything under this category will be deleted.") }}</p>
            <!-- <p class="text-center">{{ __("You are about to delete this Category. Everything under this category will be deleted.") }}</p>
            <p class="text-center">{{ __("Do you want to proceed?") }}</p> -->

            <!-- Modal footer -->
             <div class="delete-action-box">
                <button type="button" class="btn btn-default delete-acitonBtn" data-dismiss="modal">{{ __("Cancel") }}</button>
                <a class="btn btn-danger btn-ok delete-acitonBtn">{{ __("Delete") }}</a>
             </div>
         </div>
      </div>
   </div>
</div>
{{-- DELETE MODAL ENDS --}}
@endsection    
@section('scripts')
{{-- DATA TABLE --}}
@endsection