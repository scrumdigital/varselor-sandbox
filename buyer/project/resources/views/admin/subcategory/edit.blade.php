@extends('layouts.load')
@section('content')
<div class="content-area">
   <div class="add-product-content editBrand-modal-box">
      <div class="product-description">
         <div class="body-area p-0">
            @include('includes.admin.form-error')  
            <form id="geniusformdata" action="{{route('admin-subcat-update',$data->id)}}" method="POST" enctype="multipart/form-data">
               {{csrf_field()}}
               <div class="row">
                  <div class="col-lg-4">
                     <div class="left-area">
                        <h4 class="heading">{{ __("Category") }}*</h4>
                     </div>
                  </div>
                  <div class="col-lg-7">
                     <select name="category_id" class="form-control" required="">
                        <option value="">{{ __("Select Category") }}</option>
                        @foreach($cats as $cat)
                        <option value="{{ $cat->id }}" {{ $data->category_id == $cat->id ? 'selected' :'' }}>{{ $cat->name }}</option>
                        @endforeach
                     </select>
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-4">
                     <div class="left-area">
                        <h4 class="heading">{{ __("Name") }} *</h4>
                        <p class="sub-heading">(In Any Language)</p>
                     </div>
                  </div>
                  <div class="col-lg-7">
                     <input type="text" class="form-control" name="name" placeholder="{{ __("Enter Name") }}" required="" value="{{$data->name}}">
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-4">
                     <div class="left-area">
                        <h4 class="heading">{{ __("Slug") }} *</h4>
                        <p class="sub-heading">(In English)</p>
                     </div>
                  </div>
                  <div class="col-lg-7">
                     <input type="text" class="form-control" name="slug" placeholder="{{ __("Enter Slug") }}" required="" value="{{$data->slug}}">
                  </div>
               </div>
               <div class="row mb-0">
                  <div class="col-lg-12">
                     <div class="modal-footer pb-0">
                        <button class="editBrand-save-btn btn btn-secondary" id="save_custom" type="submit"> {{ __("Save") }} </button>
                        <button type="button" class="btn btn-secondary close_custom modal-closeBtn" data-dismiss="modal">Close</button>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
@endsection