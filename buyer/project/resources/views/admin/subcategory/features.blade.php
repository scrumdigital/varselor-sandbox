@extends('layouts.admin')

@section('content')

<div class="content-area">
            <div class="mr-breadcrumb">
              <div class="row">
                <div class="col-lg-12">
                    <h4 class="heading">{{ $cat->name }}</h4>
                    <ul class="links">
                      <li>
                        <a href="{{ route('admin.dashboard') }}">{{ __("Dashboard") }}</a>
                      </li>
                      <li>
                        <a href="javascript:;">{{ __("subcategory") }} </a>
                      </li>
                    
                      <li>
                        <a href="{{ route('admin-prod-types') }}">{{ __("Add Feature") }}</a>
                      </li>
                    </ul>
                </div>
              </div>
            </div>
            <div class="add-product-content">
              <div class="row">
                <div class="col-lg-12">
                  <div class="product-description">
                    <div class="heading-area">
                      <h2 class="title">
                          {{ __("Add Feature") }}
                      </h2>
                    </div>
                  </div>
                </div>
              </div>
           
              <div class="ap-product-categories">
                <div class="row">
                  <div class="col-lg-12">
                      <div class="product-description">
                      <div class="body-area">
                     <form method="post" action="{{route('admin-subcat-create-features',$cat->id)}}">
                        @if(isset($msg))
                        <span style="color:red">{{$msg}}</span>

                        @endif
                          {{csrf_field()}}

                     <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                                <h4 class="heading">{{ __("Feature Name") }}*</h4>
                            </div>
                          </div>
                          <div class="col-lg-7">
                            <input type="text" name="feature_name">
                          </div>
                        </div>
                       <div class="row">
                          <div class="col-lg-4">
                            <div class="left-area">
                             </div>
                          </div>
                          <div class="col-lg-2 text-center">
                            <button class="addProductSubmit-btn" id="AddModule" type="button">Add Module</button>
                          </div>
                          <div class="col-lg-3 text-center">
                            <button class="addProductSubmit-btn" type="submit">Save</button>
                          </div>
                        </div>
                    
                      <div id="dynamic_fields">
                      </div>     
                    </form>
                  </div>

                </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
             
   @endsection
              @section('scripts')

    <script src="{{asset('assets/admin/js/jquery.Jcrop.js')}}"></script>
    <script src="{{asset('assets/admin/js/jquery.SimpleCropper.js')}}"></script>

<script>
  $(document).ready(function(){    

    
   
    $('#addFunction').click(function(){  
          $('#functionArea').show();
      }); 

      var i=0;  


      $('#AddModule').click(function(){  
           i++;  
           $('#dynamic_fields').append(' <div class="row" id="moduleArea'+i+'" ><div class="col-lg-3"><div class="left-area"><h4 class="heading">{{ __("Module") }} '+i+'</h4></div></div> <div class="col-lg-4"> <input type="text" name="modulename[]"></div><div class="col-lg-4"><div class="right-area"><button type="button"   id="addSection'+i+'" class="btn btn-success">Add Section</button></div>');
          $('#addSection'+i+'').click(function(){        
           $('#dynamic_fields').append(' <div class="row" id="sectionArea'+i+'" ><div class="col-lg-3"><div class="left-area"><h4 class="heading">{{ __("section") }} '+i+'</h4></div></div> <div class="col-lg-4"> <input type="text" name="sectionName'+i+'[]"></div><div class="col-lg-4"><div class="right-area"><button type="button"  id="addMore'+i+'" class="btn btn-success">Add Function</button></div></div>');  

          $('#addMore'+i+'').click(function(){    
            $('#dynamic_fields').append(' <div class="row" id="functionArea'+i+'" ><div class="col-lg-3"><div class="left-area"><h4 class="heading">{{ __("Function") }} '+i+'</h4></div></div> <div class="col-lg-4"> <input type="text" name="functionName'+i+'[]"></div>'); 

            }); 

           }); 
          
      });  


      $(document).on('click', '.btn_remove', function(){  
           var button_id = $(this).attr("id");   
           $('#row'+button_id+'').remove();  
      });  
    });
</script>
@endsection