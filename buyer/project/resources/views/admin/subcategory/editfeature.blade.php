@extends('layouts.admin')
@section('styles')
<link href="{{asset('assets/admin/css/product.css')}}" rel="stylesheet"/>
@endsection
@section('content')
<div class="content-area">
   <div class="mr-breadcrumb">
      <div class="row">
         <div class="col-lg-12">
            <h4 class="heading">{{$featureLists->feature_name}}</h4>
            <ul class="links">
               <li>
                  <a href="{{ route('admin.dashboard') }}">{{ __("Dashboard") }} </a>
               </li>
               <li>
                  <a href="javascript:;">{{ __("Products") }} </a>
               </li>
               <li>
                  <a href="{{ route('admin-prod-index') }}">{{ __("All Products") }}</a>
               </li>
               <li>
                  <a href="{{ route('admin-prod-import') }}">{{ __("Bulk Upload") }}</a>
               </li>
            </ul>
         </div>
      </div>
   </div>
   

  <div class="accordion" id="featureListAccordion">
     @foreach($moduleData as $modules)
     <div class="card ">
        <div class="card-header header-elements-inline">
           <h6 class="card-title ul-collapse__icon--size ul-collapse__right-icon mb-0">
              <a data-toggle="collapse" class="text-default collapsed" href=" @php echo '#'.str_replace(' ', '-',preg_replace('/[^A-Za-z0-9\-]/', '', $modules->module_name)); @endphp"
                 aria-expanded="false"> {{htmlspecialchars($modules->module_name)}} </a>
           </h6>
        </div>
        <div id="@php echo str_replace(' ', '-',preg_replace('/[^A-Za-z0-9\-]/', '',$modules->module_name)); @endphp" class="collapse" data-parent="#featureListAccordion" style="">
           <div class="card-body"> 
              @php

              $sectionName = SubCategoryController::getSectionName($modules->module_name,$featureid);
              @endphp 
              @foreach($sectionName as $key=>$val)
              @foreach(json_decode($val) as $sec)
              <div class="featureList-wrap-list">
                <div class="featureList-wrap">
                   <h5 class="mb-4"><strong>{{htmlspecialchars($sec->section_name)}}:- <input type="hidden"  name="section_name[]" value="{{htmlspecialchars($sec->section_name)}}" ></strong></h5>

                   <ul>
                     @php  $fName = SubCategoryController::getSFName($modules->module_name,htmlspecialchars($sec->section_name),$featureid); @endphp
                     @foreach($fName as $key1=>$val1)
                     @foreach(json_decode($val1) as $fun)

                     <li>{{htmlspecialchars($fun->function_name)}}</li>

                     @endforeach
                     @endforeach
                   </ul>
                </div>
              </div>
              @endforeach
              @endforeach
           </div>
        </div>
     </div>
     @endforeach
  </div>
</div>
@endsection
@section('scripts')
<script src="{{asset('assets/admin/js/product.js')}}"></script>
<script>
   var coll = document.getElementsByClassName("collapsible");
   var i;
   
   for (i = 0; i < coll.length; i++) {
     coll[i].addEventListener("click", function() {
       this.classList.toggle("active");
       var content = this.nextElementSibling;
       if (content.style.maxHeight){
         content.style.maxHeight = null;
       } else {
         content.style.maxHeight = content.scrollHeight + "px";
       } 
     });
   }
</script>
@endsection