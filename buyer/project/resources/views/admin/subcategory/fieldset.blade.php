@extends('layouts.admin')
@section('styles')
@endsection
@section('content')
<div class="content-area">
   <div class="mr-breadcrumb">
      <div class="row">
         <div class="col-lg-12">
            <div class="content-area">
               <div class="mr-breadcrumb">
                  <div class="row">
                     <div class="col-lg-12">
                        <h4 class="heading">{{ $data->name }} <a class="add-btn" href="{{ route('admin-prod-types') }}"><i class="fas fa-arrow-left"></i> {{ __('Back') }}</a></h4>
                        <ul class="links">
                           <li>
                              <a href="{{ route('admin.dashboard') }}">{{ __('Dashboard') }} </a>
                           </li>
                           <li>
                              <a href="javascript:;">{{ __('Subcatecory') }} </a>
                           </li>
                           <li>
                              <a href="{{ route('admin-prod-types') }}">{{ __('Add FieldSet') }}</a>
                           </li>
                           <li>
                              <a href="{{ route('admin-prod-physical-create') }}">{{ __('Fieldset') }}</a>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-12">
                     <div class="row">
                        <div class="left-area col-lg-7">
                           <p class="sub-heading">For Copy fieldset from any categoey click on copy button</p>
                        </div>
                     </div>
                  </div>
               </div>
               <form  action="{{route('admin-subcat-fields',$data->id)}}" id="fieldsetForm" method="POST" enctype="multipart/form-data">
                 
                  <div class="product-area">
                     <div class="allproduct">
                        <div class="row mb-5">
                           <div class="col-lg-3">
                              <select name="subcategory" id="subcategory_id" class="form-control">
                                 <option value="">Select Sub Category</option>
                                 @foreach($subcats as $subcategory)
                                 <option value="{{$subcategory->id}}">{{$subcategory->name}}</option>
                                 @endforeach
                              </select>
                           </div>
                           <div class="col-lg-4 pl-0">
                              <button type="button" name="copy" id="copy" class="btn btn-success btn-subcategory-copy">Copy</button>
                           </div>
                        </div>
                        @if(isset($msg))
                        <span style="color:red">{{$msg}}</span>
                        @endif
                        {{ csrf_field() }}
                        <div class="table-responsiv categories-proTable" id="dynamic_field">
                           <table id="geniustable" class="table table-hover dt-responsive table-bordered table-striped" cellspacing="0" width="100%">
                              <thead class="thead-dark">
                                 <tr>
                                    <th scope="col">{{ __("FieldName") }} *</th>
                                    <th scope="col">{{ __("FieldType") }} *</th>
                                    <th scope="col" >{{ __("FieldValue") }} *</th>
                                    <th scope="col" >{{ __("Actions") }}</th>
                                 </tr>
                              </thead>
                              <tbody>
                                 <tr>
                                    <td> <input type="text" class="form-control" name="fieldname[]" placeholder="{{ __("Enter Name") }}" required="" value=""></td>
                                    <td>
                                       <select name="fieldtype[]" class="form-control">
                                          <option value="text">Text</option>
                                          <option value="text-area">Text Area</option>
                                          <option value="dropdown">Dropdown</option>
                                          <option value="checkbox">Checkbox</option>
                                          <option value="radio">Radio</option> 
                                          <option value="Multiselect">Multiselect</option>                                         
                                       </select>
                                    </td>
                                    <td>
                                       <input type="text" class="form-control" name="fieldvalue[]" placeholder="{{ __("Enter value") }}" required="" value="">
                                    </td>
                                    <td>
                                       
                                    </td>
                                 </tr>
                              </tbody>
                           </table>
                        </div>
                     </div>
                  </div>
                 
                  <div class="addvarientrow-btn-box">
                     <button type="button" class="btn btn-success add-row addCF" id="add" name="add">
                     <i class="nav-icon i-Add "></i> Add More
                     </button>
                  </div>
                  <button class="addPro-next-btn" type="submit">{{ __("Save") }}</button>
               </form>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('scripts')
<script src="{{asset('assets/admin/js/jquery.Jcrop.js')}}"></script>
<script src="{{asset('assets/admin/js/jquery.SimpleCropper.js')}}"></script>
<!-- <script>
   $(document).ready(function(){      
        var i=1;  
   
   
        $('#add').click(function(){  
             i++;  
             $('#dynamic_field').append('<din id="row'+i+'" class="row"><div  class="col-lg-4"><div class="right-area"><input type="hidden" name="count" value='+i+' /><input type="text" class="form-control" name="fieldname[]" placeholder="{{ __("Enter Name") }}" required="" value=""></div></div><div class="col-lg-3"><div class="right-area"><select name="fieldtype[]"><option value="text">Text</option><option value="dropdown">Dropdown</option> <option value="checkbox">Checkbox</option><option value="radio">Radio</option> </select></div> </div> <div class="col-lg-3"><div class="right-area"><input type="text" class="form-control" name="fieldvalue[]" placeholder="{{ __("Enter value") }}" required="" value=""> </div></div><div class="col-lg-2"><div class="right-area"><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></div></div></div>');  
        });  
   
   
        $(document).on('click', '.btn_remove', function(){  
             var button_id = $(this).attr("id");   
             $('#row'+button_id+'').remove();  
        });
   
         $('#copy').click(function(){ 
              var subcat = $('#subcategory_id').val();
              if(subcat == ''){
                alert('Please select Category for copy data');
              }else{
                $.ajax({
                        url: "../copyfieldset/"+subcat,
                       type: "get",
                      success: function (data) {
                      $('#dynamic_field').html('');
                      $('#copy_field').html(data);
                       $("#fieldsetForm").submit();
                    }
                }); 
             } 
            
          });  
      });
</script> -->


<script>
   $(document).ready(function(){      
        var i=1;
        $('#add').click(function(){  
             i++;  
             $('#dynamic_field table').append('<tr id="editfield-row'+i+'"> <td> <input type="hidden" name="count" value='+i+' /> <input type="text" class="form-control" name="fieldname[]" placeholder="{{__("Enter Name")}}" required="" value=""></td><td> <select name="fieldtype[]" class="form-control"> <option value="text">Text</option><option value="dropdown">Dropdown</option><option value="checkbox">Checkbox</option><option value="radio">Radio</option><option value="Multiselect">Multiselect</option></select> </td><td> <input type="text" class="form-control" name="fieldvalue[]" placeholder="{{__("Enter value")}}" required="" value=""> </td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove"> <i class="nav-icon i-Close-Window "></i> </button></a> </td></tr>');  
        });  
   
   
        // $(document).on('click', '.btn_remove', function(){  
        //      var button_id = $(this).attr("id");   
        //      $('#editfield-row'+button_id+'').remove();  
        // }); 

        $("#dynamic_field table").on('click','.btn_remove',function(){
           $(this).parent().parent().remove();
       });

        $('#copy').click(function(){ 
              var subcat = $('#subcategory_id').val();
              if(subcat == ''){
                alert('Please select Category for copy data');
              }else{
               
                       $("#fieldsetForm").submit();
                
             } 
            
          });
      });
</script>               
@endsection