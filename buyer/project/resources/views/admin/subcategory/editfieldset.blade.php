@extends('layouts.admin')
@section('styles')
@endsection
@section('content')
<div class="content-area">
<div class="mr-breadcrumb">
<div class="row">
   <div class="col-lg-12">
      <div class="content-area">
         <div class="mr-breadcrumb">
            <div class="row">
               <div class="col-lg-12">
                  <h4 class="heading">{{ $cats->name }} <a class="add-btn" href="{{ route('admin-prod-types') }}"><i class="fas fa-arrow-left"></i> {{ __('Back') }}</a></h4>
                  <ul class="links">
                     <li>
                        <a href="{{ route('admin.dashboard') }}">{{ __('Dashboard') }} </a>
                     </li>
                     <li>
                        <a href="javascript:;">{{ __('Subcatecory') }} </a>
                     </li>
                     <li>
                        <a href="{{ route('admin-prod-types') }}">{{ __('Add FieldSet') }}</a>
                     </li>
                     <li>
                        <a href="{{ route('admin-prod-physical-create') }}">{{ __('Fieldset') }}</a>
                     </li>
                  </ul>
               </div>
            </div>
         </div>
         <form  action="{{route('admin-subcat-edifields',$cats->id)}}" method="POST" enctype="multipart/form-data">
            <!-- <div class="add-product-content">
               <div class="row">
                  <div class="col-lg-12">
                     <div class="product-description">
                        <div class="body-area">
                           @if(isset($msg))
                           <span style="color:red">{{$msg}}</span>
                           @endif
                           <div class="row" id="dynamic_field" >
                              {{ csrf_field() }}
                              <div class="col-lg-3">
                                 <div class="right-area">
                                    <h4 class="heading">{{ __("FieldName") }} *</h4>
                                    <p class="sub-heading">(In Any Language)</p>
                                 </div>
                              </div>
                              <div class="col-lg-3">
                                 <div class="right-area">
                                    <h4 class="heading">{{ __("FieldName") }} *</h4>
                                    <p class="sub-heading">(In Any Language)</p>
                                 </div>
                              </div>
                              <div class="col-lg-5">
                                 <div class="right-area">
                                    <h4 class="heading">{{ __("FieldName") }} *</h4>
                                    <p class="sub-heading">(In Any Language)</p>
                                 </div>
                              </div>
                              @foreach($data as $rows)
                              <div class="col-lg-3">
                                 <div class="right-area">
                                    <input type="text" class="form-control" name="fieldname[]" placeholder="{{ __("Enter Name") }}" required="" value="{{$rows->fieldname}}">
                                 </div>
                              </div>
                              <div class="col-lg-3">
                                 <div class="right-area">
                                    <select name="fieldtype[]">
                                       @if($rows->fieldtype=="text")
                                       <option value="text" selected="selected"  >Text</option>
                                       <option value="dropdown" >Dropdown</option>
                                       <option value="checkbox">checkbox</option>
                                       <option value="radio">Radio</option>
                                       @endif
                                       @if($rows->fieldtype=="dropdown")
                                       <option value="dropdown" >Text</option>
                                       <option value="dropdown" selected="selected">Dropdown</option>
                                       <option value="checkbox">checkbox</option>
                                       <option value="radio">Radio</option>
                                       @endif
                                       @if($rows->fieldtype=="checkbox")
                                       <option value="dropdown" >Text</option>
                                       <option value="dropdown" >Dropdown</option>
                                       <option value="checkbox" selected="selected">checkbox</option>
                                       <option value="radio">Radio</option>
                                       @endif
                                       @if($rows->fieldtype=="radio")
                                       <option value="dropdown" >Text</option>
                                       <option value="dropdown" >Dropdown</option>
                                       <option value="checkbox">checkbox</option>
                                       <option value="radio" selected="selected">Radio</option>
                                       @endif
                                    </select>
                                 </div>
                              </div>
                              <div class="col-lg-3">
                                 <div class="right-area">
                                    <input type="text" class="form-control" name="fieldvalue[]" placeholder="{{ __("Enter value") }}" required="" value="{{$rows->fieldvalue}}">
                                 </div>
                              </div>
                              <div class="col-lg-2">
                                 <div class="right-area">
                                    <a href="{{route('admin-subcat-delfields',['id1' => $rows->id, 'id2' => $rows->cat_id])}}"> <button type="button"   class="btn btn-danger btn_remove">X</button></a>
                                 </div>
                              </div>
                              @endforeach
                              <div class="col-lg-12">
                                 <div class="right-area">
                                    <button type="button" name="add" id="add" class="btn btn-success">Add More</button>
                                 </div>
                              </div>
                           </div>
                           <div class="col-lg-7">
                              <button class="addProductSubmit-btn" type="submit">{{ __("Save") }}</button>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div> -->
            {{ csrf_field() }}
            @if(isset($msg))
               <span style="color:red">{{$msg}}</span>
            @endif
            <div class="product-area">
               <div class="allproduct"> 
                  <div class="table-responsiv categories-proTable" id="dynamic_field">
                     <table id="geniustable" class="table table-hover dt-responsive table-bordered table-striped" cellspacing="0" width="100%">
                        <thead class="thead-dark">
                           <tr>
                              <th scope="col">{{ __("Field Name") }} *</th>
                              <th scope="col">{{ __("Field Type") }} *</th>
                              <th scope="col" >{{ __("Values") }} *</th>
                              <th scope="col" >{{ __("Actions") }}</th>
                           </tr>
                        </thead>

                        <tbody>
                           @foreach($data as $rows)
                           <tr>
                              <td> <input type="text" class="form-control" name="fieldname[]" placeholder="{{ __("Enter Name") }}" required="" value="{{$rows->fieldname}}"></td>

                              <td>
                                 <select name="fieldtype[]" class="form-control">
                                    @if($rows->fieldtype=="Multiselect")
                                    <option value="Multiselect" selected="selected"  >Multiselect</option>
                                    <option value="text-area">Text Area</option>
                                    <option value="text-area">Text Area</option>
                                    <option value="dropdown" >Dropdown</option>
                                    <option value="checkbox">checkbox</option>
                                    <option value="radio">Radio</option>
                                    @endif
                                    @if($rows->fieldtype=="text")
                                    <option value="Multiselect">Multiselect</option>
                                    <option value="text" selected="selected"  >Text</option>
                                    <option value="text-area">Text Area</option>
                                    <option value="dropdown" >Dropdown</option>
                                    <option value="checkbox">checkbox</option>
                                    <option value="radio">Radio</option>
                                    @endif
                                    @if($rows->fieldtype=="dropdown")
                                    <option value="Multiselect">Multiselect</option>
                                    <option value="dropdown" >Text</option>
                                    <option value="text-area">Text Area</option>
                                    <option value="dropdown" selected="selected">Dropdown</option>
                                    <option value="checkbox">checkbox</option>
                                    <option value="radio">Radio</option>
                                    @endif
                                    @if($rows->fieldtype=="checkbox")
                                    <option value="Multiselect">Multiselect</option>
                                    <option value="dropdown" >Text</option>
                                    <option value="text-area">Text Area</option>
                                    <option value="dropdown" >Dropdown</option>
                                    <option value="checkbox" selected="selected">checkbox</option>
                                    <option value="radio">Radio</option>
                                    @endif
                                    @if($rows->fieldtype=="radio")
                                    <option value="Multiselect">Multiselect</option>
                                    <option value="dropdown" >Text</option>
                                    <option value="text-area">Text Area</option>
                                    <option value="dropdown" >Dropdown</option>
                                    <option value="checkbox">checkbox</option>
                                    <option value="radio" selected="selected">Radio</option>
                                    @endif
                                    @if($rows->fieldtype=="text-area")
                                    <option value="Multiselect">Multiselect</option>
                                    <option value="dropdown" >Text</option>
                                    <option value="text-area" selected="selected">Text Area</option>
                                    <option value="dropdown" >Dropdown</option>
                                    <option value="checkbox">checkbox</option>
                                    <option value="radio">Radio</option>
                                    @endif
                                     
                                 </select>
                              </td>

                              <td>
                                 <input type="text" class="form-control" name="fieldvalue[]" placeholder="{{ __("Enter value") }}" required="" value="{{$rows->fieldvalue}}">
                              </td>

                              <td>
                                 <a href="{{route('admin-subcat-delfields',['id1' => $rows->id, 'id2' => $rows->cat_id])}}"> <button type="button"   class="btn btn-danger btn_remove"> <i class="nav-icon i-Close-Window "></i> </button></a>
                              </td>
                           </tr>
                           @endforeach
                        </tbody>
                     </table>
                  </div>
               </div>
            </div>
            <div class="addvarientrow-btn-box">
              <button type="button" class="btn btn-success add-row addCF" id="add" name="add">
              <i class="nav-icon i-Add "></i> Add More
              </button>
            </div>
            <!-- <button type="button"   class="btn btn-success">Add More</button> -->
            <button class="addPro-next-btn" type="submit">{{ __("Save") }}</button>
         </form>
      </div>
   </div>
</div>
@endsection
@section('scripts')
<script src="{{asset('assets/admin/js/jquery.Jcrop.js')}}"></script>
<script src="{{asset('assets/admin/js/jquery.SimpleCropper.js')}}"></script>
<script>
   $(document).ready(function(){      
        var i=1;  
   
   // <div id="row'+i+'" class="row"><div  class="col-lg-4"><div class="right-area"><input type="hidden" name="count" value='+i+' /><input type="text" class="form-control" name="fieldname[]" placeholder="{{ __("Enter Name") }}" required="" value=""></div></div><div class="col-lg-4"><div class="right-area"><select name="fieldtype[]"><option value="text">Text</option><option value="dropdown">Dropdown</option><option value="checkbox">Checkbox</option><option value="radio">Radio</option></select></div> </div> <div class="col-lg-3"><div class="right-area"><input type="text" class="form-control" name="fieldvalue[]" placeholder="{{ __("Enter value") }}" required="" value=""> </div></div><div class="col-lg-1"><div class="right-area"><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></div></div></div>


        $('#add').click(function(){  
             i++;  
             $('#dynamic_field table').append('<tr id="editfield-row'+i+'"> <td> <input type="hidden" name="count" value='+i+' /> <input type="text" class="form-control" name="fieldname[]" placeholder="{{__("Enter Name")}}" required="" value=""></td><td> <select name="fieldtype[]" class="form-control"> <option value="text">Text</option><option value="dropdown">Dropdown</option><option value="checkbox">Checkbox</option><option value="radio">Radio</option><option value="Multiselect">Multiselect</option></select> </td><td> <input type="text" class="form-control" name="fieldvalue[]" placeholder="{{__("Enter value")}}" required="" value=""> </td><td><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove"> <i class="nav-icon i-Close-Window "></i> </button></a> </td></tr>');  
        });  
   
   
        // $(document).on('click', '.btn_remove', function(){  
        //      var button_id = $(this).attr("id");   
        //      $('#editfield-row'+button_id+'').remove();  
        // }); 

        $("#dynamic_field table").on('click','.btn_remove',function(){
           $(this).parent().parent().remove();
       }); 
      });
</script>               
@endsection