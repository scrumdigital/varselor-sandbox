@extends('layouts.admin')
@section('styles')
<link href="{{asset('assets/admin/css/product.css')}}" rel="stylesheet"/>
<link href="{{asset('assets/admin/css/jquery.Jcrop.css')}}" rel="stylesheet"/>
<link href="{{asset('assets/admin/css/Jcrop-style.css')}}" rel="stylesheet"/>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="{{asset('assets/admin/css/jquery.tagsinput-revisited.css')}}" />
@endsection
@section('content')
<div class="content-area">
   <div class="mr-breadcrumb">
      <div class="row">
         <div class="col-lg-12">
            <h4 class="heading">@if(!empty($cats)){{$cats->name}}@endif<a class="add-btn" href="{{ route('admin-prod-types') }}"><i class="fas fa-arrow-left"></i> {{ __('Back') }}</a></h4>
            <ul class="links">
               <li>
                  <a href="{{ route('admin.dashboard') }}">{{ __('Dashboard') }} </a>
               </li>
               <li>
                  <a href="javascript:;">{{ __('Products') }} </a>
               </li>
               <li>
                  <a href="{{ route('admin-prod-index') }}">{{ __('All Products') }}</a>
               </li>
               <li>
                  <a href="{{ route('admin-prod-create') }}">{{ __('Add Product') }}</a>
               </li>
            </ul>
         </div>
      </div>
   </div>
   <div class="add-product-content">
	  	<div class="row ">
	     	<div class="col-lg-2">
	         	<div class="addProduct-tabs-wrap">
		            @if(!empty($cats) && $cats->name=='Services')

		            @if($selectedTab =='genral')
		            <button class="tablink  addPro-tabLink" onclick="openPage('Genral', this, 'active')" id="defaultOpen">General Information</button>
		            @else
		            <button class="tablink  addPro-tabLink" onclick="openPage('Genral', this, 'active')" id="genralTab">General Information</button>
		            @endif

		            @if($selectedTab =='varient')
		            <button class="tablink  addPro-tabLink" onclick="openPage('Variations', this, 'active')" id="defaultOpen">Variations</button>
		            @else
		            <button class="tablink  addPro-tabLink" onclick="openPage('Variations', this, 'active')" id="variationsTab">Variations</button>
		            @endif



		            @else
		            @if($selectedTab =='genral')
		            <button class="tablink  addPro-tabLink" onclick="openPage('Genral', this, 'active')" id="defaultOpen">General Information</button>
		            @else
		            <button class="tablink  addPro-tabLink" onclick="openPage('Genral', this, 'active')" id="genralTab">General Information</button>
		            @endif

		            @if($selectedTab =='overview')
		            <button class="tablink  addPro-tabLink" onclick="openPage('Overview', this, 'active')" id="defaultOpen">Overview</button>
		            @else
		            <button class="tablink  addPro-tabLink" onclick="openPage('Overview', this, 'active')" id="overviewTab">Overview</button>
		            @endif

		            @if($selectedTab =='feature')
		            <button class="tablink  addPro-tabLink" onclick="openPage('Features', this, 'active')" id="defaultOpen">Features</button>
		            @else
		            <button class="tablink  addPro-tabLink" onclick="openPage('Features', this, 'active')" id="featureTab">Features</button>
		            @endif

		            @if($selectedTab =='varient')
		            <button class="tablink  addPro-tabLink" onclick="openPage('Variations', this, 'active')" id="defaultOpen">Variations</button>
		            @else
		            <button class="tablink  addPro-tabLink" onclick="openPage('Variations', this, 'active')" id="variationsTab">Variations</button>
		            @endif

		            @if($selectedTab =='price')
		            <button class="tablink  addPro-tabLink" onclick="openPage('Pricing', this, 'active')" id="defaultOpen">Pricing</button>
		            @else
		            <button class="tablink  addPro-tabLink" onclick="openPage('Pricing', this, 'active')" id="pricingTab">Pricing</button>
		            @endif
		            @endif
		            <button class="tablink" style="display: none" onclick="openPage('Implementation', this, 'blue')" id="implementationTab">Implementation</button>
					<button class="tablink" style="display: none" onclick="openPage('Support', this, 'blue')" id="supportTab">Support</button>
		        </div>
	     	</div>

	         <div class="col-lg-10">

	            <div class="product-description">

	               <div class="body-area" id="Genral">

	                  <form id="geniusform" action="{{route('admin-prod-store')}}" method="POST" enctype="multipart/form-data">
	                     {{csrf_field()}}
	                     @include('includes.admin.form-both')  
	                     @if($cats->name == 'Add-ons')

	                     <div class="row">
	                        <div class="col-lg-3">
	                           <div class="left-area">
	                              <h4 class="heading">{{ __('Add-on Name') }}* </h4>
	                              <p class="sub-heading">(In Any Language)</p>
	                           </div>
	                        </div>
	                        <div class="col-lg-8">
	                           <input type="text" class="form-control" placeholder="{{ __('Enter Product Name') }}" name="name" required="" value="<?php if (!empty($prod)): echo $prod->name; ?>
	                              <?php endif ?>">
	                        </div>
	                     </div>
	                     <div class="row">
	                        <div class="col-lg-3">
	                           <div class="left-area">
	                              <h4 class="heading">{{ __('Select Products') }}* </h4>
	                              <p class="sub-heading">(In Any Language)</p>
	                           </div>
	                        </div>
	                        <div class="col-lg-8">
	                           <input id="form-tags-4"  type="text" value="" placeholder="Add a Product">
	                           <input id="addon_products" name="addon_products" type="hidden" value="" >
	                        </div>
	                     </div>
	                     @else
	                     <div class="row">
	                        <div class="col-lg-3">
	                           <div class="left-area">
	                              <h4 class="heading">{{ __('Product Name') }}* </h4>
	                              <p class="sub-heading">(In Any Language)</p>
	                           </div>
	                        </div>
	                        <div class="col-lg-8">
	                           <input type="text" class="form-control" placeholder="{{ __('Enter Product Name') }}" name="name" required="" value="<?php if (!empty($prod)): echo $prod->name; ?>
	                              <?php endif ?>">
	                        </div>
	                     </div>
	                     @endif
	                     <div class="row">
	                        <div class="col-lg-3">
	                           <div class="left-area">
	                              <h4 class="heading">{{ __('Country') }}* </h4>
	                              <p class="sub-heading">(In Any Language)</p>
	                           </div>
	                        </div>
	                        <div class="col-lg-8">
	                           <select class="form-control" name="country_code">
	                              <option value="">{{ __('Select Country') }}</option>
	                              @foreach($countries as $country)
	                              <option value="{{$country->id}}" <?php if (!empty($prod) && $prod->country_code== $country->id ):  ?>selected="selected"
	                                 <?php endif ?>>{{$country->country_name}}</option>
	                              @endforeach
	                           </select>
	                        </div>
	                     </div>
	                     <div class="row">
	                        <div class="col-lg-3">
	                           <div class="left-area">
	                              <h4 class="heading">{{ __('Brand') }}* </h4>
	                              <p class="sub-heading">(In Any Language)</p>
	                           </div>
	                        </div>
	                        <div class="col-lg-8">
	                           <select class="form-control" name="brand_id">
	                              <option value="">{{ __('Select Brand') }}</option>
	                              @foreach($brands as $brand)
	                              <option value="{{$brand->id}}" <?php if (!empty($prod) && $prod->brand_id== $brand->id ):  ?>selected="selected"
	                                 <?php endif ?>>{{$brand->name}}</option>
	                              @endforeach
	                           </select>
	                        </div>
	                     </div>
	                     <div class="row">
	                        <div class="col-lg-3">
	                           <div class="left-area">
	                              <h4 class="heading">{{ __('Vendor') }}* </h4>
	                              <p class="sub-heading">(In Any Language)</p>
	                           </div>
	                        </div>
	                        <div class="col-lg-8">
	                           <select class="form-control" name="vendor_id">
	                              <option value="">{{ __('Select Vendor') }}</option>
	                              @foreach($VendorList as $vendor)
	                              <option value="{{$vendor->id}}" <?php if (!empty($prod) && $prod->user_id== $vendor->id ):  ?>selected="selected"
	                                 <?php endif ?> >{{$vendor->name}}</option>
	                              @endforeach
	                           </select>
	                        </div>
	                     </div>
	                     <div class="row">
	                        <div class="col-lg-3">
	                           <div class="left-area">
	                              <h4 class="heading">{{ __('Overview') }} </h4>
	                           </div>
	                        </div>
	                        <div class="col-lg-8">
	                           <!-- <input type="text" class="form-control" placeholder="{{ __('Enter Overview of Product') }}" name="overview" required="" value="<?php if (!empty($prod)): echo $prod->name; ?>
	                              <?php endif ?>" maxlength="255"> -->
	                           <textarea rows="5" cols="50" class="form-control" placeholder="{{ __('Enter Overview of Product') }}" name="overview" required=""><?php if (!empty($prod)): echo $prod->overview; ?>
															<?php endif ?></textarea>
	                        </div>
	                     </div>
	                     <div class="row">
	                        <div class="col-lg-3">
	                           <div class="left-area">
	                              <h4 class="heading">{{ __('Additional Information') }} </h4>
	                           </div>
	                        </div>
	                        <div class="col-lg-8">
	                           <!-- <input type="text" class="form-control" placeholder="{{ __('Enter Additional Information') }}" name="additional_information" required="" value="<?php if (!empty($prod)): echo $prod->name; ?>
	                              <?php endif ?>" maxlength="255"> -->
	                           <textarea rows="5" cols="50" class="form-control" placeholder="{{ __('Enter Additional Information') }}" name="additional_information" required=""><?php if (!empty($prod)): echo $prod->additional_information; ?>
															<?php endif ?></textarea>
	                        </div>
	                     </div>
	                     <div class="row">
	                        <div class="col-lg-3">
	                           <div class="left-area">
	                              <h4 class="heading">{{ __('Price Starts From') }} </h4>
	                           </div>
	                        </div>
	                        <div class="col-lg-8">
	                           <input type="text" class="form-control" placeholder="{{ __('Price Starts From') }}" name="price_start_range" required="" value="<?php if (!empty($prod)): echo $prod->price_start_range; ?>
	                              <?php endif ?>" maxlength="255">
	                        </div>
	                     </div>
	                     @if($cats->name !='Services')
	                     <div class="row">
	                        <div class="col-lg-3">
	                           <div class="left-area">
	                              <h4 class="heading">{{ __('Product Includes') }}* </h4>
	                              <p class="sub-heading">(In Any Language)</p>
	                           </div>
	                        </div>
	                        <div class="col-lg-8">
	                        	<label class="checkbox checkbox-outline-secondary checkbox-inline mr-4">
	                                <input type="checkbox" name="product_type" id="implementation" class="checkclick" value="implementation">
	                                <span>Implementation</span>
	                                <span class="checkmark"></span>
	                            </label>

	                            <label class="checkbox checkbox-outline-secondary checkbox-inline">
	                                <input type="checkbox" name="product_type" id="support" class="checkclick" value="support">
	                                <span>Support</span>
	                                <span class="checkmark"></span>
	                            </label>

	                           <!-- <input type="checkbox" name="product_type" id="implementation" class="checkclick" value="implementation"> Implementation
	                           <input type="checkbox" name="product_type" id="support" class="checkclick" value="support"> Support -->
	                        </div>
	                     </div>
	                     @endif
	                     <div class="row">
	                        <div class="col-lg-3">
	                           <div class="left-area">
	                              <h4 class="heading">{{ __('Feature Image') }} *</h4>
	                           </div>
	                        </div>
	                        <div class="col-lg-8">
								<div class="panel panel-body mb-3">
									<div class="span4 cropme text-center" id="landscape" style="width: 400px; height: 400px; border: 1px dashed black;">
									</div>
								</div>
	                           <a href="javascript:;" id="crop-image" class="d-inline-block mybtn1">
	                           <i class="icofont-upload-alt"></i> {{ __('Upload Image Here') }}
	                           </a>
	                        </div>
	                     </div>
	                     <input type="hidden" id="feature_photo" name="photo" value="">
	                     <input type="file" name="gallery[]" class="hidden" id="uploadgallery" accept="image/*" multiple>
	                     <input type="hidden" name="selectedTab" class="hidden" value="Features">
	                     <div class="row">
	                        <div class="col-lg-3">
	                           <div class="left-area">
	                              <h4 class="heading">
	                                 {{ __('Product Gallery Images') }} *
	                              </h4>
	                           </div>
	                        </div>
	                        <div class="col-lg-8">
	                           <a href="#" class="set-gallery"  data-toggle="modal" data-target="#setgallery">
	                           <i class="icofont-plus"></i> {{ __('Set Gallery') }}
	                           </a>
	                        </div>
	                     </div>
                     	<div class="row">
	                        <div class="col-lg-3">
	                           <div class="left-area">
	                              <h4 class="heading">{{ __('Youtube Video URL') }}*</h4>
	                              <p class="sub-heading">{{ __('(Optional)') }}</p>
	                           </div>
	                        </div>
	                        <div class="col-lg-8">
	                           <input  name="youtube" type="text" class="form-control" value="" placeholder="{{ __('Enter Youtube Video URL') }}">
	                           <div class="checkbox-wrapper">
	                              <input type="checkbox" name="seo_check" value="1" class="checkclick" id="allowProductSEO" value="1"
	                                 >
	                              <input type="hidden" name="subcat_id" value="{{$subcats}}" >
	                              <input type="hidden" name="cat_id" value="{{$catId}}" >
	                              <input type="hidden" name="prod_id" id="prod_id" value="@if(!empty($prod) ){{$prod->id}}@endif" >
	                              <label for="allowProductSEO">{{ __('Allow Product SEO') }}</label>
	                           </div>
	                        </div>
                     	</div>
	                     <!-- <div class="showbox">
	                        <div class="row">
	                           <div class="col-lg-3">
	                              <div class="left-area">
	                                 <h4 class="heading">{{ __('Meta Tags') }} *</h4>
	                              </div>
	                           </div>
	                           <div class="col-lg-8">
	                              <ul id="metatags" class="myTags">
	                              </ul>
	                           </div>
	                        </div>
	                        <div class="row">
	                           <div class="col-lg-3">
	                              <div class="left-area">
	                                 <h4 class="heading">
	                                    {{ __('Meta Description') }} *
	                                 </h4>
	                              </div>
	                           </div>
	                           <div class="col-lg-8">
	                              <div class="text-editor">
	                                 <textarea name="meta_description" rows="5" class="form-control" placeholder="{{ __('Meta Description') }}"></textarea> 
	                              </div>
	                           </div>
	                        </div>
	                     </div> -->
	                     <!-- <div class="row">
	                        <div class="col-lg-3">
	                           <div class="left-area">
	                              <h4 class="heading">{{ __('Tags') }} *</h4>
	                           </div>
	                        </div>
	                        <div class="col-lg-8">
	                           <ul id="tags" class="myTags">
	                           </ul>
	                        </div>
	                     </div> -->
	                     <div class="row">
	                        <div class="col-lg-3">
	                           <div class="left-area">
	                           </div>
	                        </div>
	                        <div class="col-lg-8 text-right">
	                           <button class="tablink addPro-next-btn" >Next</button>
	                        </div>
	                     </div>
	               </div>
	               </form>
	               <form id="geniusform" action="{{route('admin-prod-overview-store')}}" method="POST" enctype="multipart/form-data">
	                  {{csrf_field()}}
	                  <div id="Overview" class="body-area">
	                     {!!$fieldsetdata!!}
	                     <input type="hidden" name="prod_id" value="">
	                     <input type="hidden" name="subcat_id" value="{{$subcats}}" >
	                     <input type="hidden" name="cat_id" value="@if(!empty($cats)){{$catId}}@endif" >
	                     <input type="hidden" name="prod_id" id="prod_id" value="@if(!empty($prod) ){{$prod->id}}@endif" >
	                     <div class="row">
	                        <div class="col-lg-3">
	                           <div class="left-area">
	                           </div>
	                        </div>
	                        <div class="col-lg-8 text-center">
	                           <button class="tablink" >Next</button>
	                        </div>
	                     </div>
	                  </div>
	               </form>
	               <form id="geniusform" action="{{route('admin-prod-store-features')}}" method="POST" enctype="multipart/form-data">
	                  {{csrf_field()}}
	                  <div id="Features" class="body-area">
	                     <div class="row">
	                        <div class="col-lg-3">
	                           <div class="left-area">
	                              <h4 class="heading">{{ __('Feature List') }}* </h4>
	                              <p class="sub-heading">(In Any Language)</p>
	                           </div>
	                        </div>
	                        <div class="col-lg-8">
	                           <input type="hidden" name="subcat_id" value="{{$subcats}}" >
	                           <input type="hidden" name="cat_id" value="@if(!empty($cats)){{$cats->id}}@endif" >
	                           <input type="hidden" name="prod_id" id="prod_id" value="@if(!empty($prod) ){{$prod->id}}@endif" >
	                           <select class="form-control" name="featureId" id="featureId" >
	                              <option value="">{{ __('Select Feature') }}</option>
	                              @foreach($featureLists as $feature)
	                              <option value="{{ $feature->id }}">{{ $feature->feature_name }}</option>
	                              @endforeach
	                           </select>
	                        </div>
	                     </div>
	                     <div class="" id="FeatureModules">
	                     </div>
	                     <div class="row">
	                        <div class="col-lg-3">
	                           <div class="left-area">
	                           </div>
	                        </div>
	                        <div class="col-lg-8 text-center">
	                           <button class="tablink" type="submit" >Next</button>
	                        </div>
	                        <!-- <div class="col-lg-8 text-center">
	                           <button class="tablink" type="submit" >Create Product</button>
	                           
	                           </div> -->
	                     </div>
	                  </div>
	               </form>
	               <form id="geniusform" action="{{route('admin-prod-store-varient')}}" method="POST" enctype="multipart/form-data">
	                  {{csrf_field()}}
	                  <div id="Variations" class="body-area">
	                     <div class="row">
	                        <div class="col-lg-3">
	                           Number of Variants
	                        </div>
	                        <div class="col-lg-1">
	                           <select class="form-control" name="numberOfvar" id="numberOfvar">
	                              <option value="1" selected="selected" >1</option>
	                              <option value="2">2</option>
	                              <option value="3">3</option>
	                           </select>
	                        </div>
	                     </div>
	                     <input type="hidden" name="variationCount" id="variationCount" value="">
	                     <div class="row" id="variationHeading">
	                        <div class="col-lg-3">
	                           Varient Name	
	                        </div>
	                        <div class="col-lg-3" id="varienthead1">
	                           Variation 1	
	                        </div>
	                        <div class="col-lg-3" id="varienthead2" style="display: none"  >
	                           Variation 2
	                        </div>
	                        <div class="col-lg-3" id="varienthead3" style="display: none" >
	                           Variation 3	
	                        </div>
	                     </div>
	                     <div class="row col-lg-12" id="dynamic_field">
	                        <input type="hidden" name="prod_id" id="prod_id" value="@if(!empty($prod) ){{$prod->id}}@endif" >
	                        <div class="row">
	                           <div class="col-lg-3">
	                              <input type="text" name="variant_name[]"   class="form-control" value="Variation Name" placeholder="Feature">
	                           </div>
	                           <div class="col-lg-3 varient1" >
	                              <input type="text" class="form-control" name="varient1[]" >
	                           </div>
	                           <div class="col-lg-3 varient2"  style="display: none">
	                              <input type="text" class="form-control" name="varient2[]">
	                           </div>
	                           <div class="col-lg-3 varient3"  style="display: none">
	                              <input type="text" class="form-control" name="varient3[]">
	                           </div>
	                        </div>
	                        @if($cats->name=='Services')
	                        <div class="row">
	                           <div class="col-lg-3">
	                              <input type="text" name="variant_name[]"   class="form-control" value="Description" placeholder="Feature">	
	                           </div>
	                           <div class="col-lg-3 varient1" >
	                              <input type="text" class="form-control" name="varient1[]" >
	                           </div>
	                           <div class="col-lg-3 varient2"  style="display: none">
	                              <input type="text" class="form-control" name="varient2[]">
	                           </div>
	                           <div class="col-lg-3 varient3"  style="display: none">
	                              <input type="text" class="form-control" name="varient3[]">
	                           </div>
	                        </div>
	                        <div class="row">
	                           <div class="col-lg-3">
	                              <input type="text" name="variant_name[]"   class="form-control" value="Price" placeholder="Feature">	
	                           </div>
	                           <div class="col-lg-3 varient1" >
	                              <input type="text" class="form-control" name="varient1[]" >
	                           </div>
	                           <div class="col-lg-3 varient2"  style="display: none">
	                              <input type="text" class="form-control" name="varient2[]">
	                           </div>
	                           <div class="col-lg-3 varient3"  style="display: none">
	                              <input type="text" class="form-control" name="varient3[]">
	                           </div>
	                        </div>
	                        @endif
	                        <div class="col-lg-3">
	                           <input type="text" name="variant_name[]"   class="form-control" value="" placeholder="Feature">	
	                        </div>
	                        <div class="col-lg-3 varient1" >
	                           <input type="text" class="form-control" name="varient1[]" >
	                        </div>
	                        <div class="col-lg-3 varient2"  style="display: none">
	                           <input type="text" class="form-control" name="varient2[]">
	                        </div>
	                        <div class="col-lg-3 varient3"  style="display: none">
	                           <input type="text" class="form-control" name="varient3[]">
	                        </div>
	                     </div>
	                     <div class="col-lg-3"><button type="button" name="add" id="add" class="btn btn-success">Add More</button>
	                     </div>
	                     @if($cats->name=='Services')
	                     <button class="tablink" >Save Service</button>
	                     @else
	                     <button class="tablink" >Next</button>
	                     @endif
	                  </div>
	               </form>
	            </div>
	            <div id="Support" class="body-area">
	               <div class="row">
	                  <div class="col-lg-3">
	                     Number of Variants
	                  </div>
	                  <div class="col-lg-1">
	                     <select class="form-control" name="" id="s_numberOfvar">
	                        <option value="1" selected="selected" >1</option>
	                        <option value="2">2</option>
	                        <option value="3">3</option>
	                     </select>
	                  </div>
	               </div>
	               <div class="row" id="s_variationHeading">
	                  <div class="col-lg-3">
	                     Varient Name	
	                  </div>
	                  <div class="col-lg-3" id="s_varienthead1">
	                     Variation 1	
	                  </div>
	                  <div class="col-lg-3" id="s_varienthead2" style="display: none"  >
	                     Variation 2
	                  </div>
	                  <div class="col-lg-3" id="i_varienthead3" style="display: none" >
	                     Variation 3	
	                  </div>
	               </div>
	               <div class="row col-lg-12" id="s_dynamic_field">
	                  <input type="hidden" name="prod_id" id="prod_id" value="@if(!empty($prod) ){{$prod->id}}@endif" >
	                  <div class="col-lg-3">
	                     <input type="text" name="variant_name[]"   class="form-control" value="" placeholder="Feature">	
	                  </div>
	                  <div class="col-lg-3 s_varient1" >
	                     <input type="text" class="form-control" name="varient1[]" >
	                  </div>
	                  <div class="col-lg-3 s_varient2"  style="display: none">
	                     <input type="text" class="form-control" name="varient2[]">
	                  </div>
	                  <div class="col-lg-3 s_varient3"  style="display: none">
	                     <input type="text" class="form-control" name="varient3[]">
	                  </div>
	               </div>
	               <div class="col-lg-3"><button type="button" name="add" id="addSup" class="btn btn-success">Add More</button>
	               </div>
	               <button class="tablink" >Next</button>
	            </div>
	         </div>
	         <div id="Implementation" class="body-area">
	            <div class="row">
	               <div class="col-lg-3">
	                  Number of Variants
	               </div>
	               <div class="col-lg-1">
	                  <select class="form-control" name="" id="i_numberOfvar">
	                     <option value="1" selected="selected" >1</option>
	                     <option value="2">2</option>
	                     <option value="3">3</option>
	                  </select>
	               </div>
	            </div>
	            <div class="row" id="i_variationHeading">
	               <div class="col-lg-2">
	                  Varient Name	
	               </div>
	               <div class="col-lg-3" id="i_varienthead1">
	                  Variation 1	
	               </div>
	               <div class="col-lg-3" id="i_varienthead2" style="display: none"  >
	                  Variation 2
	               </div>
	               <div class="col-lg-3" id="i_varienthead3" style="display: none" >
	                  Variation 3	
	               </div>
	            </div>
	            <div class="row col-lg-12" id="i_dynamic_field">
	               <input type="hidden" name="prod_id" id="prod_id" value="@if(!empty($prod) ){{$prod->id}}@endif" >
	               <div class="col-lg-3">
	                  <input type="text" name="variant_name[]"   class="form-control" value="" placeholder="Feature">	
	               </div>
	               <div class="col-lg-3 i_varient1" >
	                  <input type="text" class="form-control" name="varient1[]" >
	               </div>
	               <div class="col-lg-3 i_varient2"  style="display: none">
	                  <input type="text" class="form-control" name="varient2[]">
	               </div>
	               <div class="col-lg-3 i_varient3"  style="display: none">
	                  <input type="text" class="form-control" name="varient3[]">
	               </div>
	            </div>
	            <div class="col-lg-3"><button type="button" name="add" id="addImp" class="btn btn-success">Add More</button>
	            </div>
	            <button class="tablink" >Next</button>
	         </div>
	         </form>
	      </div>
	      @if(!empty($prod) )
	      <form id="pricing_form" action="{{route('save-pricing',$prod->id)}}" method="POST" enctype="multipart/form-data">
	         @else
	      <form id="pricing_form" action="{{route('save-pricing','999')}}" method="POST" enctype="multipart/form-data">
	         @endif
	         {{csrf_field()}}
	         <div id="Pricing" class="body-area">
	            <!-- <div class="row">
	               <div class="col-lg-3">
	               	Number of Variants
	               </div>
	               <div class="col-lg-1">
	               	<select class="form-control" name="" id="numberOfvar">
	               			<option value="1" selected="selected" >1</option>
	               			<option value="2">2</option>	
	               			<option value="3">3</option>	
	               	</select>	
	               </div>
	               
	               </div> -->
	            <input type="hidden" name="prod_id" id="prod_id" value="@if(!empty($prod) ){{$prod->id}}@endif" >
	            <input type="hidden" name="variation_count" id="variation_count" value="@if($variation_count != ''){{$variation_count}}@endif">
	            <div class="row">
	               <div class="col-lg-3">
	                  <label for="fixed_price_checkbox">{{ __('Fixed Price') }}</label>
	                  <input type="checkbox" name="fixed_price_checkbox" class="fixed_price_checkbox" id="fixed_price_checkbox">
	               </div>
	               <div class="col-lg-3">
	                  <label for="user_slab_checkbox">{{ __('User Slab') }}</label>
	                  <input type="checkbox" name="user_slab_checkbox" class="user_slab_checkbox" id="user_slab_checkbox">
	               </div>
	               <div class="col-lg-3">
	                  <label for="module_price_checkbox">{{ __('Module Price') }}</label>
	                  <input type="checkbox" name="module_price_checkbox" class="module_price_checkbox" id="module_price_checkbox">
	               </div>
	            </div>
	            <div class="row" id="pricingHeading">
	               <div class="col-lg-2">
	               </div>
	               <div class="col-lg-3" id="varienthead1">
	                  Variation 1	
	               </div>
	               <div class="col-lg-3" id="varienthead2">
	                  Variation 2
	               </div>
	               <div class="col-lg-3" id="varienthead3">
	                  Variation 3	
	               </div>
	            </div>
	            <!-- Fixed Price Section (Sambhav) -->
	            <div class="row" id="fixed_price_section" style="display: none;">
	               <div class="col-lg-12">
	                  <h4 class="heading">{{ __('---------------Fixed Price-------------') }} </h4>
	               </div>
	               <div class="col-lg-2">
	                  <div class="left-area">
	                     <h4 class="heading">{{ __('Fixed Price') }}* </h4>
	                  </div>
	               </div>
	               <div class="col-lg-1">
	                  <label for="fixed_price_user_limit_checkbox">{{ __('User Limts') }}</label>
	                  <input type="checkbox" name="fixed_price_user_limit_checkbox" class="fixed_price_user_limit_checkbox" id="fixed_price_user_limit_checkbox">
	               </div>
	               <div class="col-lg-1" id="variation_fixed1" style="display: none;">
	                  <input type="text" class="form-control" placeholder="{{ __('Enter Price') }}" name="fixed_price_variation1">
	               </div>
	               <div class="col-lg-1" id="variation_fixed2" style="display: none;">
	                  <input type="text" class="form-control" placeholder="{{ __('Enter Price') }}" name="fixed_price_variation2">
	               </div>
	               <div class="col-lg-1" id="variation_fixed3" style="display: none;">
	                  <input type="text" class="form-control" placeholder="{{ __('Enter Price') }}" name="fixed_price_variation3">
	               </div>
	               <div class="col-lg-1">
	                  <input type="text" class="form-control" placeholder="{{ __('Lower Limit') }}" name="fixed_price_lower_limit">
	               </div>
	               <div class="col-lg-1">
	                  <input type="text" class="form-control" placeholder="{{ __('Upper Limit') }}" name="fixed_price_upper_limit">
	               </div>
	            </div>
	            <!-- Module Price Section (Sambhav) -->
	            <div class="row" id="module_price_section" style="display: none;">
	               <div class="col-lg-12">
	                  <h4 class="heading">{{ __('---------------Module Price-----------------') }} </h4>
	               </div>
	               <div class="col-lg-3">
	                  <input type="text" class="form-control" placeholder="{{ __('Enter Module Name') }}" name="module_name[]">
	               </div>
	               <div class="col-lg-2" id="variation_module1" style="display: none;">
	                  <input type="text" class="form-control" placeholder="{{ __('Enter Price') }}" name="module_price_variation1[]">
	               </div>
	               <div class="col-lg-2" id="variation_module2" style="display: none;">
	                  <input type="text" class="form-control" placeholder="{{ __('Enter Price') }}" name="module_price_variation2[]">
	               </div>
	               <div class="col-lg-2" id="variation_module3" style="display: none;">
	                  <input type="text" class="form-control" placeholder="{{ __('Enter Price') }}" name="module_price_variation3[]">
	               </div>
	               <div class="col-lg-1">
	                  <div class="right-area">
	                     <button type="button" name="add_module_price" id="add_module_price" class="btn btn-success">+</button>
	                  </div>
	               </div>
	               <div class="col-lg-1"><button type="button" name="remove" class="btn btn-danger btn_remove_module_price">X</button></div>
	            </div>
	            <!-- User Slab Section (Sambhav) -->
	            <div class="row" id="user_slab_section" style="display: none;">
	               <div class="col-lg-12">
	                  <h4 class="heading">{{ __('---------------User Price-----------------') }} </h4>
	               </div>
	               <div class="col-lg-1">
	                  <input type="text" class="form-control" placeholder="{{ __('Lower Limit') }}" name="user_slab_lower_limit[]">
	               </div>
	               <div class="col-lg-1">
	                  <input type="text" class="form-control" placeholder="{{ __('Upper Limit') }}" name="user_slab_upper_limit[]">
	               </div>
	               <div class="col-lg-1" id="variation_user_slab1" style="display: none;">
	                  <input type="text" class="form-control" placeholder="{{ __('Enter Price') }}" name="user_slab_variation1[]">
	               </div>
	               <div class="col-lg-1" id="variation_user_slab2" style="display: none;">
	                  <input type="text" class="form-control" placeholder="{{ __('Enter Price') }}" name="user_slab_variation2[]">
	               </div>
	               <div class="col-lg-1" id="variation_user_slab3" style="display: none;">
	                  <input type="text" class="form-control" placeholder="{{ __('Enter Price') }}" name="user_slab_variation3[]]">
	               </div>
	               <div class="col-lg-1">
	                  <div class="right-area">
	                     <button type="button" name="add_user_slab" id="add_user_slab" class="btn btn-success">+</button>
	                  </div>
	               </div>
	               <div class="col-lg-1">
	                  <button type="button" name="remove" class="btn btn-danger btn_remove_user_slab">X</button>
	               </div>
	            </div>
	            <!-- <div class="col-lg-2">
	               <div class="right-area">
	               	<button type="button" name="add" id="add" class="btn btn-success">Add More</button>
	               	</div>
	                  </div> -->
	            <!-- <button class="addProductSubmit-btn"  type="submit">Save Product</button> -->
	            <div class="col-lg-8">
	               <button class="addProductSubmit-btn" type="submit">{{ __("Save Product") }}</button>
	            </div>
	         </div>
	      </form>
	   	</div>
	</div>
</div>
</div>
<div class="modal fade" id="setgallery" tabindex="-1" role="dialog" aria-labelledby="setgallery" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered  modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitle">{{ __('Image Gallery') }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="top-area">
               <div class="row">
                  <div class="col-sm-6 text-right">
                     <div class="upload-img-btn">
                        <label for="image-upload" id="prod_gallery"><i class="icofont-upload-alt"></i>{{ __('Upload File') }}</label>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <a href="javascript:;" class="upload-done" data-dismiss="modal"> <i class="fas fa-check"></i> {{ __('Done') }}</a>
                  </div>
                  <div class="col-sm-12 text-center">( <small>{{ __('You can upload multiple Images.') }}</small> )</div>
               </div>
            </div>
            <div class="gallery-images">
               <div class="selected-image">
                  <div class="row">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<input type="hidden" value="{{$product_name}}" id="pstring">
@endsection
@section('scripts')
<script src="{{asset('assets/admin/js/jquery.Jcrop.js')}}"></script>
<script src="{{asset('assets/admin/js/jquery.SimpleCropper.js')}}"></script>
<script src="{{asset('assets/admin/js/jquery.tagsinput-revisited.js')}}"></script>
<script type="text/javascript">
   //var pstring = array();
   var pstring = $("#pstring").val();
   var prArray = pstring.split(",");
   //alert(pstring);
   // Gallery Section Insert
   $('#form-tags-4').tagsInput({
   				'autocomplete': {
   					source: prArray
   				}
   			});
   
    $(document).on('click', '.remove-img' ,function() {
      var id = $(this).find('input[type=hidden]').val();
      $('#galval'+id).remove();
      $(this).parent().parent().remove();
    });
   
    $(document).on('click', '#prod_gallery' ,function() {
      $('#uploadgallery').click();
       $('.selected-image .row').html('');
      $('#geniusform').find('.removegal').val(0);
    });
   
    $("#featureId").change(function() {
       var featureId  = $(this).val();
      $.ajax({
          url: "../../products/featureModules/"+featureId,
          type: "get",
         // data: $(this).val(),
         // dataType: 'JSON',
          success: function (data) {
              $('#FeatureModules').html(data);
   
              //alert(data)
   
          }
   }); 
    });            
      ////// display subcategory fields section
           
    $("#uploadgallery").change(function(){
       var total_file=document.getElementById("uploadgallery").files.length;
       for(var i=0;i<total_file;i++)
       {
        $('.selected-image .row').append('<div class="col-sm-6">'+
                                          '<div class="img gallery-img">'+
                                              '<span class="remove-img"><i class="fas fa-times"></i>'+
                                              '<input type="hidden" value="'+i+'">'+
                                              '</span>'+
                                              '<a href="'+URL.createObjectURL(event.target.files[i])+'" target="_blank">'+
                                              '<img src="'+URL.createObjectURL(event.target.files[i])+'" alt="gallery image">'+
                                              '</a>'+
                                          '</div>'+
                                    '</div> '
                                        );
        $('#geniusform').append('<input type="hidden" name="galval[]" id="galval'+i+'" class="removegal" value="'+i+'">')
       }
   
    });
   
    $('#numberOfvar').change(function(){
    	var count = $(this).val();
    	$('#variationCount').val(count);
   
     	  var no = $(this).val();
     	  if(no == 1){
     	  	$('.varient2').hide();
          $('#varienthead2').hide();
     	  	$('.varient3').hide();
          $('#varienthead3').hide();
     	  } if(no == 2){
     	  		$('.varient2').show();
          $('#varienthead2').show();
     	  	$('.varient3').hide();
          $('#varienthead3').hide();
     	  } if(no == 3){
     	  	$('.varient2').show();
          $('#varienthead2').show();
     	  	$('.varient3').show();
          $('#varienthead3').show();
     	  }
      
   });
   
    $('#i_numberOfvar').change(function(){
   
     	  var no = $(this).val();
     	  if(no == 1){
     	  	$('.i_varient2').hide();
          $('#i_varienthead2').hide();
     	  	$('.i_varient3').hide();
          $('#i_varienthead3').hide();
     	  } if(no == 2){
     	  		$('.i_varient2').show();
          $('#i_varienthead2').show();
     	  	$('.i_varient3').hide();
          $('#i_varienthead3').hide();
     	  } if(no == 3){
     	  	$('.i_varient2').show();
          $('#i_varienthead2').show();
     	  	$('.i_varient3').show();
          $('#i_varienthead3').show();
     	  }
      
   });
   
    $('#s_numberOfvar').change(function(){
   
     	  var no = $(this).val();
     	  if(no == 1){
     	  	$('.s_varient2').hide();
          $('#s_varienthead2').hide();
     	  	$('.s_varient3').hide();
          $('#s_varienthead3').hide();
     	  } if(no == 2){
     	  		$('.s_varient2').show();
          $('#s_varienthead2').show();
     	  	$('.s_varient3').hide();
          $('#s_varienthead3').hide();
     	  } if(no == 3){
     	  	$('.s_varient2').show();
          $('#s_varienthead2').show();
     	  	$('.s_varient3').show();
          $('#s_varienthead3').show();
     	  }
      
   });
   var i=1; 
    
        $('#add').click(function(){  
             i++;  
             $('#dynamic_field').append('<din id="row'+i+'" class="row col-lg-12"><div  class="col-lg-3"><div class="right-area"><input type="text" name="variant_name[]" class="form-control" value="" /></div></div><div  class="col-lg-3"><div class="right-area"><input type="hidden" name="count" value='+i+' /><input type="text" class="form-control" name="varient1[]" ></div></div><div class="col-lg-3 varient2" style="display: none"><div class="right-area"><input type="text" class="form-control" name="varient2[]" ></div> </div> <div class="col-lg-3 varient3" style="display: none"><div class="right-area"><input type="text" class="form-control" name="varient3[]" ></div></div><div class="col-lg-2"><div class="right-area"><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></div></div></div>');  
        });  
   
   
       
   
   
        $('#addImp').click(function(){  
             i++;  
             $('#i_dynamic_field').append('<din id="row'+i+'" class="row col-lg-12"><div  class="col-lg-3"><div class="right-area"><input type="text" name="variant_name[]" class="form-control" value="" /></div></div><div  class="col-lg-3"><div class="right-area"><input type="hidden" name="count" value='+i+' /><input type="text" class="form-control" name="varient1[]" ></div></div><div class="col-lg-3 varient2" style="display: none"><div class="right-area"><input type="text" class="form-control" name="varient2[]" ></div> </div> <div class="col-lg-3 varient3" style="display: none"><div class="right-area"><input type="text" class="form-control" name="varient3[]" ></div></div><div class="col-lg-2"><div class="right-area"><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></div></div></div>');  
        });  
   
   
      
   
   
   		 $('#addSup').click(function(){  
             i++;  
             $('#s_dynamic_field').append('<din id="row'+i+'" class="row col-lg-12"><div  class="col-lg-3"><div class="right-area"><input type="text" name="variant_name[]" class="form-control" value="" /></div></div><div  class="col-lg-3"><div class="right-area"><input type="hidden" name="count" value='+i+' /><input type="text" class="form-control" name="varient1[]" ></div></div><div class="col-lg-3 varient2" style="display: none"><div class="right-area"><input type="text" class="form-control" name="varient2[]" ></div> </div> <div class="col-lg-3 varient3" style="display: none"><div class="right-area"><input type="text" class="form-control" name="varient3[]" ></div></div><div class="col-lg-2"><div class="right-area"><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></div></div></div>');  
        });  
   
   
        $(document).on('click', '.btn_remove', function(){  
             var button_id = $(this).attr("id");   
             $('#row'+button_id+'').remove();  
        });
   //------------------Add implemntaion and support tab ------------------//
   $('#implementation').click(function(){
            var checked_status = this.checked;
            if(checked_status == true) {
                $('#implementationTab').show();
            }
            else { $('#implementationTab').hide();
            }
        }); 
    $('#support').click(function(){
            var checked_status = this.checked;
            if(checked_status == true) {
                $('#supportTab').show();
            }
            else { $('#supportTab').hide();
            }
        }); 

   
        //-----------add module price section through js (Sambhav)--------------//
      $(document).ready(function(){  
      	var i=1;
   
       $(document).on('click', '#add_module_price', function(){
       	i++;
       	var variation_count = $('#variation_count').val();
   
       	if(variation_count == '1')
       	{
       		$('#module_price_section').append('<div id="row'+i+'" class="row"><div class="col-lg-3"><input type="text" class="form-control" placeholder="Enter Module Name" name="module_name[]"></div><div class="col-lg-2" id="variation_module1"><input type="text" class="form-control" placeholder="Enter Price" name="module_price_variation1[]"></div><div class="col-lg-2" id="variation_module2" style="display: none;"><input type="text" class="form-control" placeholder="Enter Price" name="module_price_variation2[]"></div><div class="col-lg-2" id="variation_module3" style="display: none;"><input type="text" class="form-control" placeholder="Enter Price" name="module_price_variation3[]"></div><div class="col-lg-1"><div class="right-area"><button type="button" name="add_module_price" id="add_module_price" class="btn btn-success">+</button></div></div><div class="col-lg-1"><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove_module_price">X</button></div></div>');  
       	}
       	else if(variation_count == '2')
       	{
       		$('#module_price_section').append('<div id="row'+i+'" class="row"><div class="col-lg-3"><input type="text" class="form-control" placeholder="Enter Module Name" name="module_name[]"></div><div class="col-lg-2" id="variation_module1"><input type="text" class="form-control" placeholder="Enter Price" name="module_price_variation1[]"></div><div class="col-lg-2" id="variation_module2"><input type="text" class="form-control" placeholder="Enter Price" name="module_price_variation2[]"></div><div class="col-lg-2" id="variation_module3" style="display: none;"><input type="text" class="form-control" placeholder="Enter Price" name="module_price_variation3[]"></div><div class="col-lg-1"><div class="right-area"><button type="button" name="add_module_price" id="add_module_price" class="btn btn-success">+</button></div></div><div class="col-lg-1"><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove_module_price">X</button></div></div>');  
       	}
       	else if(variation_count == '3')
       	{
       		$('#module_price_section').append('<div id="row'+i+'" class="row"><div class="col-lg-3"><input type="text" class="form-control" placeholder="Enter Module Name" name="module_name[]"></div><div class="col-lg-2" id="variation_module1"><input type="text" class="form-control" placeholder="Enter Price" name="module_price_variation1[]"></div><div class="col-lg-2" id="variation_module2"><input type="text" class="form-control" placeholder="Enter Price" name="module_price_variation2[]"></div><div class="col-lg-2" id="variation_module3"><input type="text" class="form-control" placeholder="Enter Price" name="module_price_variation3[]"></div><div class="col-lg-1"><div class="right-area"><button type="button" name="add_module_price" id="add_module_price" class="btn btn-success">+</button></div></div><div class="col-lg-1"><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove_module_price">X</button></div></div>');  
       	}
           
         });  
   
   
       $(document).on('click', '.btn_remove_module_price', function(){  
          var button_id = $(this).attr("id");   
          $('#row'+button_id+'').remove();  
       }); 
   
   });
   
      //---------------End -------------------------------// 
   
      //-----------add user slab section through js (Sambhav)--------------//
      $(document).ready(function(){  
      	var i=1;
   
       $(document).on('click', '#add_user_slab', function(){
       	i++;
       	var variation_user_slab_count = $('#variation_count').val();
   
       	if(variation_user_slab_count == '1')
       	{
       		$('#user_slab_section').append('<div id="row'+i+'" class="row"><div class="col-lg-1"><input type="text" class="form-control" placeholder="{{ __('Lower Limit') }}" name="user_slab_lower_limit[]"></div><div class="col-lg-1"><input type="text" class="form-control" placeholder="{{ __('Upper Limit') }}" name="user_slab_upper_limit[]"></div><div class="col-lg-1" id="variation_user_slab1"><input type="text" class="form-control" placeholder="{{ __('Enter Price') }}" name="user_slab_variation1[]"></div><div class="col-lg-1" id="variation_user_slab2" style="display: none;"><input type="text" class="form-control" placeholder="{{ __('Enter Price') }}" name="user_slab_variation2[]"></div><div class="col-lg-1" id="variation_user_slab3" style="display: none;"><input type="text" class="form-control" placeholder="{{ __('Enter Price') }}" name="user_slab_variation3[]]"></div><div class="col-lg-1"><div class="right-area"><button type="button" name="add_user_slab" id="add_user_slab" class="btn btn-success">+</button></div></div><div class="col-lg-1"><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove_user_slab">X</button></div></div>');
       	}
       	else if(variation_user_slab_count == '2')
       	{
       		$('#user_slab_section').append('<div id="row'+i+'" class="row"><div class="col-lg-1"><input type="text" class="form-control" placeholder="{{ __('Lower Limit') }}" name="user_slab_lower_limit[]"></div><div class="col-lg-1"><input type="text" class="form-control" placeholder="{{ __('Upper Limit') }}" name="user_slab_upper_limit[]"></div><div class="col-lg-1" id="variation_user_slab1"><input type="text" class="form-control" placeholder="{{ __('Enter Price') }}" name="user_slab_variation1[]"></div><div class="col-lg-1" id="variation_user_slab2"><input type="text" class="form-control" placeholder="{{ __('Enter Price') }}" name="user_slab_variation2[]"></div><div class="col-lg-1" id="variation_user_slab3" style="display: none;"><input type="text" class="form-control" placeholder="{{ __('Enter Price') }}" name="user_slab_variation3[]]"></div><div class="col-lg-1"><div class="right-area"><button type="button" name="add_user_slab" id="add_user_slab" class="btn btn-success">+</button></div></div><div class="col-lg-1"><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove_user_slab">X</button></div></div>');
       	}
       	else if(variation_user_slab_count == '3')
       	{
       		$('#user_slab_section').append('<div id="row'+i+'" class="row"><div class="col-lg-1"><input type="text" class="form-control" placeholder="{{ __('Lower Limit') }}" name="user_slab_lower_limit[]"></div><div class="col-lg-1"><input type="text" class="form-control" placeholder="{{ __('Upper Limit') }}" name="user_slab_upper_limit[]"></div><div class="col-lg-1" id="variation_user_slab1"><input type="text" class="form-control" placeholder="{{ __('Enter Price') }}" name="user_slab_variation1[]"></div><div class="col-lg-1" id="variation_user_slab2"><input type="text" class="form-control" placeholder="{{ __('Enter Price') }}" name="user_slab_variation2[]"></div><div class="col-lg-1" id="variation_user_slab3"><input type="text" class="form-control" placeholder="{{ __('Enter Price') }}" name="user_slab_variation3[]]"></div><div class="col-lg-1"><div class="right-area"><button type="button" name="add_user_slab" id="add_user_slab" class="btn btn-success">+</button></div></div><div class="col-lg-1"><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove_user_slab">X</button></div></div>');
       	}
   
             
       });  
   
       $(document).on('click', '.btn_remove_user_slab', function(){  
          var button_id = $(this).attr("id");   
          $('#row'+button_id+'').remove();  
       }); 
   
   });
   
      //---------------End -------------------------------// 
   
      //-------Manage variation column number (Sambhav)-----------------//
   
      $(document).ready(function(){  
   
      	var count = $('#variation_count').val();
   
      	if(count == '1')
      	{
      		$('#variation_fixed1').show();
      		$('#variation_module1').show();
      		$('#variation_user_slab1').show();
      	}
      	else if(count == '2')
      	{
      		$('#variation_fixed1').show();
      		$('#variation_module1').show();
      		$('#variation_user_slab1').show();
      		$('#variation_fixed2').show();
      		$('#variation_module2').show();
      		$('#variation_user_slab2').show();
      	}
      	else if(count == '3')
      	{
      		$('#variation_fixed1').show();
      		$('#variation_module1').show();
      		$('#variation_user_slab1').show();
      		$('#variation_fixed2').show();
      		$('#variation_module2').show();
      		$('#variation_user_slab2').show();
      		$('#variation_fixed3').show();
      		$('#variation_module3').show();
      		$('#variation_user_slab3').show();
      	}
      });
   
      //-------End -------------------------//
   
      $(document).on('change', '#fixed_price_checkbox', function(){  
   
      	if($(this).prop("checked") == true){
              $('#fixed_price_section').show();
          }
          else if($(this).prop("checked") == false){
              $('#fixed_price_section').hide();
          }
   
      });
   
      $(document).on('change', '#user_slab_checkbox', function(){  
   
      	if($(this).prop("checked") == true){
              $('#user_slab_section').show();
   
          }
          else if($(this).prop("checked") == false){
              $('#user_slab_section').hide();
          }
   
      });
   
      $(document).on('change', '#module_price_checkbox', function(){  
   
      	if($(this).prop("checked") == true){
              $('#module_price_section').show();
   
          }
          else if($(this).prop("checked") == false){
              $('#module_price_section').hide();
          }
   
      });   
   
   // Gallery Section Insert Ends	
   
</script>
<script type="text/javascript">
   $('.cropme').simpleCropper();
   $('#crop-image').on('click',function(){
   $('.cropme').click();
   });
</script>
<script>

	$('.addProduct-tabs-wrap button').on('click', function(){
    	$('.addProduct-tabs-wrap button.active').removeClass('active');
    	$(this).addClass('active');
	});
   function openPage(pageName,elmnt,color) {
   	//alert(pageName)id="prod_id"
     //var pro_id = document.getElementById("prod_id").value;
     //alert(pro_id)
     var i, tabcontent, tablinks;
     tabcontent = document.getElementsByClassName("body-area");
     for (i = 0; i < tabcontent.length; i++) {
       tabcontent[i].style.display = "none";
   
     }
     // tablinks = document.getElementsByClassName("tablink");
     // for (i = 0; i < tablinks.length; i++) {
     //   tablinks[i].style.backgroundColor = "";
   
     // }
     document.getElementById(pageName).style.display = "block";
     // elmnt.className += " " + color;
     // if(pro_id !=''){
     // 		if(pageName == 'Overview'){
     // 			document.getElementById("genralTab").disabled = true;
     // 			document.getElementById("genralTab").style.pointerEvents = "none";
     // 			document.getElementById("genralTab").style.backgroundColor = "lightgray";
     // 		}if(pageName == 'Features'){
     // 			document.getElementById("genralTab").disabled = true;
     // 			document.getElementById("genralTab").style.pointerEvents = "none";
     // 			document.getElementById("genralTab").style.backgroundColor = "lightgray";
     // 			document.getElementById("overviewTab").disabled = true;
     // 			document.getElementById("overviewTab").style.pointerEvents = "none";
     // 			document.getElementById("overviewTab").style.backgroundColor = "lightgray";
     // 		}if(pageName == 'Variations'){
     // 			document.getElementById("genralTab").disabled = true;
     // 			document.getElementById("genralTab").style.pointerEvents = "none";
     // 			document.getElementById("genralTab").style.backgroundColor = "lightgray";
     // 			document.getElementById("overviewTab").disabled = true;
     // 			document.getElementById("overviewTab").style.pointerEvents = "none";
     // 			document.getElementById("overviewTab").style.backgroundColor = "lightgray";
     // 			document.getElementById("featureTab").disabled = true;
     // 			document.getElementById("featureTab").style.pointerEvents = "none";
     // 			document.getElementById("featureTab").style.backgroundColor = "lightgray";
     // 		}
   
     // }
   }
   
   function openNextPage(pageName,elmnt,color) {
   	//alert(elmnt)
     var i, tabcontent, tablinks;
     tabcontent = document.getElementsByClassName("body-area");
     for (i = 0; i < tabcontent.length; i++) {
       tabcontent[i].style.display = "none";
   
     }
     tablinks = document.getElementsByClassName("tablink");
     for (i = 0; i < tablinks.length; i++) {
       tablinks[i].style.backgroundColor = "";
   
     }
     document.getElementById(pageName).style.display = "block";
     document.getElementById(elmnt).style.backgroundColor = color;
     //if()
   }
   
   
   // Get the element with id="defaultOpen" and click on it
   document.getElementById("defaultOpen").click();
</script>
<script src="{{asset('assets/admin/js/product.js')}}"></script>
<!----------------------------- Add row ------------------->
@endsection