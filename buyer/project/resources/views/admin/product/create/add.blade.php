@extends('layouts.admin')
@section('styles')
<link href="{{asset('assets/admin/css/product.css')}}" rel="stylesheet"/>
<link href="{{asset('assets/admin/css/jquery.Jcrop.css')}}" rel="stylesheet"/>
<link href="{{asset('assets/admin/css/Jcrop-style.css')}}" rel="stylesheet"/>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" href="{{asset('assets/admin/css/jquery.tagsinput-revisited.css')}}" />
@endsection
@section('content')
<div class="content-area">
   <div class="mr-breadcrumb">
      <div class="row">
         <div class="col-lg-12">
            <h4 class="heading">@if(!empty($cats)){{$cats->name}}@endif<a class="add-btn" href="{{ route('admin-prod-types') }}"><i class="fas fa-arrow-left"></i> {{ __('Back') }}</a></h4>
            <ul class="links">
               <li>
                  <a href="{{ route('admin.dashboard') }}">{{ __('Dashboard') }} </a>
               </li>
               <li>
                  <a href="javascript:;">{{ __('Products') }} </a>
               </li>
               <li>
                  <a href="{{ route('admin-prod-index') }}">{{ __('All Products') }}</a>
               </li>
               <li>
                  <a href="{{ route('admin-prod-create') }}">{{ __('Add Product') }}</a>
               </li>
            </ul>
         </div>
      </div>
      <div class="row">
         <div class="col-lg-12">
            
               <h4>Category {{$cats->name}} > {{$subcatname->name}}</h5>
            
         </div>
      </div>
   </div>
   <div class="add-product-content">
      <div class="row ">
         <div class="col-lg-2">
            <div class="addProduct-tabs-wrap">
               @if($cats->id == '22' || $cats->id == '24')
               @if($selectedTab =='genral')
               <button class="tablink  addPro-tabLink" onclick="openPage('Genral', this, 'active')" id="defaultOpen">General Information</button>
               <button class="tablink  addPro-tabLink" disabled="" id="overviewTab">Overview</button>  
               <button class="tablink  addPro-tabLink" disabled="" id="featureTab">Features</button>
               <button class="tablink  addPro-tabLink" disabled="" id="variationsTab">Variations</button>
               <button class="tablink  addPro-tabLink" disabled=""  id="pricingTab">Pricing</button>
               @endif
               @if($selectedTab =='overview')
               <button class="tablink  addPro-tabLink" disabled="" id="genralTab">General Information</button>
               <button class="tablink  addPro-tabLink" onclick="openPage('Overview', this, 'active')" id="defaultOpen">Overview</button>
               <button class="tablink  addPro-tabLink" disabled="" id="featureTab">Features</button>
               <button class="tablink  addPro-tabLink" disabled=""  id="variationsTab">Variations</button>
               <button class="tablink  addPro-tabLink" disabled="" id="pricingTab">Pricing</button>
               @endif
               @if($selectedTab =='feature')
               <button class="tablink  addPro-tabLink" disabled="" id="genralTab">General Information</button>
               <button class="tablink  addPro-tabLink" disabled="" id="overviewTab">Overview</button>
               <button class="tablink  addPro-tabLink" onclick="openPage('Features', this, 'active')" id="defaultOpen">Features</button>
               <button class="tablink  addPro-tabLink" disabled=""  id="variationsTab">Variations</button>
               <button class="tablink  addPro-tabLink" disabled="" id="pricingTab">Pricing</button>
               @endif
               @if($selectedTab =='varient')
               <button class="tablink  addPro-tabLink" disabled="" id="genralTab">General Information</button>
               <button class="tablink  addPro-tabLink" disabled="" id="overviewTab">Overview</button>
               <button class="tablink  addPro-tabLink" disabled="" id="featureTab">Features</button>
               <button class="tablink  addPro-tabLink"  onclick="openPage('Variations', this, 'active')" id="defaultOpen">Variations</button>
               <button class="tablink  addPro-tabLink" disabled="">Pricing</button>
               @endif
               @if($selectedTab =='price')
               <button class="tablink  addPro-tabLink" disabled="" id="genralTab">General Information</button>
               <button class="tablink  addPro-tabLink" disabled="" id="overviewTab">Overview</button>
               <button class="tablink  addPro-tabLink" disabled="" id="featureTab">Features</button>
               <button class="tablink  addPro-tabLink" disabled="">Variations</button>
               <button class="tablink  addPro-tabLink" onclick="openPage('Pricing', this, 'active')" id="defaultOpen">Pricing</button>
               @endif
               @endif
               @if($cats->id == '23')
               @if($selectedTab =='genral')
               <button class="tablink  addPro-tabLink" onclick="openPage('Genral', this, 'active')" id="defaultOpen">General Information</button>
               <button class="tablink  addPro-tabLink"  id="variationsTab">Variations</button>

               @endif
               @if($selectedTab =='varient')
               <button class="tablink  addPro-tabLink" disabled="" id="genralTab">General Information</button>
               <button class="tablink  addPro-tabLink"  onclick="openPage('Variations', this, 'active')" id="defaultOpen">Variations</button>
               
               @endif
               
               @endif
               @if(!empty($prod) && $prod->product_implementation =='yes')
               @if($selectedTab =='Ivarient')
               <button class="tablink  addPro-tabLink" disabled="" id="genralTab">General Information</button>
               <button class="tablink  addPro-tabLink" disabled="" id="overviewTab">Overview</button>
               <button class="tablink  addPro-tabLink" disabled="" id="featureTab">Features</button>
               <button class="tablink  addPro-tabLink" disabled=""  id="variationsTab">Variations</button>
               <button class="tablink  addPro-tabLink" disabled="" id="pricingTab">Pricing</button>
               <button class="tablink addPro-tabLink"  onclick="openPage('Implementation', this, 'active')" id="defaultOpen">Implementation</button>
               @else
               <button class="tablink addPro-tabLink" disabled="" onclick="openPage('Implementation', this, 'active')" id="implementationTab">Implementation</button>
               @endif
               @else
               <button class="tablink addPro-tabLink" disabled="" style="display: none" onclick="openPage('Implementation', this, 'active')" id="implementationTab">Implementation</button>
               @endif
               @if(!empty($prod) && $prod->product_support =='yes')
               @if($selectedTab =='Svarient')
               <button class="tablink  addPro-tabLink" disabled="" id="genralTab">General Information</button>
               <button class="tablink  addPro-tabLink" disabled="" id="overviewTab">Overview</button>
               <button class="tablink  addPro-tabLink" disabled="" id="featureTab">Features</button>
               <button class="tablink  addPro-tabLink" disabled=""  id="variationsTab">Variations</button>
               <button class="tablink  addPro-tabLink" disabled="" id="pricingTab">Pricing</button>
               <button class="tablink addPro-tabLink"  disabled="" id="implementationTab">Implementation</button>
               <button class="tablink addPro-tabLink"  onclick="openPage('Support', this, 'active')" id="defaultOpen">Support</button>
               @else
               <button class="tablink addPro-tabLink" disabled="" onclick="openPage('Support', this, 'active')" id="supportTab">Support</button>
               @endif  
               @else
               <button class="tablink addPro-tabLink" disabled="" style="display: none" onclick="openPage('Support', this, 'active')" id="supportTab">Support</button>
               @endif
            </div>
         </div>
         <div class="col-lg-10">
            <div class="product-description">
               <div class="body-area" id="Genral">
                  <form id="geniusform" action="{{route('admin-prod-store')}}" method="POST" enctype="multipart/form-data">
                     {{csrf_field()}}
                     @include('includes.admin.form-both')  
                     @if($cats->name == 'Add-ons' || $cats->name == 'Services')
                     <div class="row">
                        <div class="col-lg-3">
                           <div class="left-area">
                              <h4 class="heading">
                                 @if($cats->name == 'Add-ons'){{ __('Add-on Name') }}* @endif
                                 @if($cats->name == 'Services'){{ __('Service Name') }}* @endif
                              </h4>
                              <p class="sub-heading">(In Any Language)</p>
                           </div>
                        </div>
                        <div class="col-lg-8">
                           <input type="text" class="form-control" placeholder="{{ __('Enter  Name') }}" name="name" required="" value="<?php if (!empty($prod)): echo $prod->name; ?>
                              <?php endif ?>">
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-lg-3">
                           <div class="left-area">
                              <h4 class="heading">{{ __('Tag Products') }}* </h4>
                              <p class="sub-heading">(Tag related products where this addon/service is applicable.)</p>
                           </div>
                        </div>
                        
                        <div class="col-lg-8">
                        <?php
                        	
                        	$prodd = array();
                        	$prodd = explode(",",$product_name);
                        	
                        ?>	
                        	<div id="prodlist"></div>
                                <select class="form-control chosen-product" id="addon_products" multiple="true">
                                	@foreach ($prodd as $prodd1)
                                    <option>{{$prodd1}}</option>
                                    
                                    @endforeach
                                </select>
                                <input type="hidden" name="addon_products" value="">
                           <!-- <input id="form-tags-4"  type="text" value="" placeholder="Add a Product">
                           <input id="addon_products" name="addon_products" type="hidden" value="" > -->
                        </div>
                     </div>
                     @else
                     <div class="row">
                        <div class="col-lg-3">
                           <div class="left-area">
                              <h4 class="heading">{{ __('Product Name') }}* </h4>
                              <p class="sub-heading">(In Any Language)</p>
                           </div>
                        </div>
                        <div class="col-lg-8">
                           <input type="text" class="form-control" placeholder="{{ __('Enter Product Name') }}" name="name" required="" value="<?php if (!empty($prod)): echo $prod->name; ?>
                              <?php endif ?>">
                        </div>
                     </div>
                     @endif
                     <div class="row">
                        <div class="col-lg-3">
                           <div class="left-area">
                              <h4 class="heading">{{ __('Country') }}* </h4>
                              <p class="sub-heading">(In Any Language)</p>
                           </div>
                        </div>
                        <div class="col-lg-8">
                           <select class="form-control" name="country_code">
                              <option value="">{{ __('Select Country') }}</option>
                              @foreach($countries as $country)
                              <option value="{{$country->id}}" <?php if (!empty($prod) && $prod->country_code== $country->id ):  ?>selected="selected"
                                 <?php endif ?>>{{$country->country_name}}</option>
                              @endforeach
                           </select>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-lg-3">
                           <div class="left-area">
                              <h4 class="heading">{{ __('Brand') }}* </h4>
                              <p class="sub-heading">(In Any Language)</p>
                           </div>
                        </div>
                        <div class="col-lg-8">
                           <select class="form-control" name="brand_id" required="">
                              <option value="">{{ __('Select Brand') }}</option>
                              @foreach($brands as $brand)
                              <option value="{{$brand->id}}" <?php if (!empty($prod) && $prod->brand_id== $brand->id ):  ?>selected="selected"
                                 <?php endif ?>>{{$brand->name}}</option>
                              @endforeach
                           </select>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-lg-3">
                           <div class="left-area">
                              <h4 class="heading">{{ __('Vendor') }}* </h4>
                              <p class="sub-heading">(In Any Language)</p>
                           </div>
                        </div>
                        <div class="col-lg-8">
                           <select class="form-control" name="vendor_id">
                              <option value="">{{ __('Select Vendor') }}</option>
                              @foreach($VendorList as $vendor)
                              <option value="{{$vendor->id}}" <?php if (!empty($prod) && $prod->user_id== $vendor->id ):  ?>selected="selected"
                                 <?php endif ?> >{{$vendor->name}}</option>
                              @endforeach
                           </select>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-lg-3">
                           <div class="left-area">
                              <h4 class="heading">{{ __('Overview') }} </h4>
                           </div>
                        </div>
                        <div class="col-lg-8">
                           <!-- <input type="text" class="form-control" placeholder="{{ __('Enter Overview of Product') }}" name="overview" required="" value="<?php if (!empty($prod)): echo $prod->name; ?>
                              <?php endif ?>" maxlength="255"> -->
                           <textarea rows="5" cols="50" class="form-control" placeholder="{{ __('Enter Overview of Product') }}" name="overview" required=""><?php if (!empty($prod)): echo $prod->overview; ?>
																<?php endif ?></textarea>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-lg-3">
                           <div class="left-area">
                              <h4 class="heading">{{ __('Additional Information') }} </h4>
                           </div>
                        </div>
                        <div class="col-lg-8">
                           <!-- <input type="text" class="form-control" placeholder="{{ __('Enter Additional Information') }}" name="additional_information" required="" value="<?php if (!empty($prod)): echo $prod->name; ?>
                              <?php endif ?>" maxlength="255"> -->
                           <textarea rows="5" cols="50" class="form-control" placeholder="{{ __('Enter Additional Information') }}" name="additional_information" required=""><?php if (!empty($prod)): echo $prod->additional_information; ?>
																<?php endif ?></textarea>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-lg-3">
                           <div class="left-area">
                              <h4 class="heading">{{ __('Price Starts From') }} </h4>
                           </div>
                        </div>
                        <div class="col-lg-8">
                           <input type="text" class="form-control" placeholder="{{ __('Price Starts From') }}" name="price_start_range" required="" value="<?php if (!empty($prod)): echo $prod->price_start_range; ?>
                              <?php endif ?>" maxlength="255">
                        </div>
                     </div>
                     <div class="row" id="upload_product_spec_sheet">
                        <div class="col-lg-3">
                           <div class="left-area">
                              <h4 class="heading">{{ __("Upload Product Specification Sheet") }}*</h4>
                           </div>
                        </div>
                        <div class="col-lg8">
                           <input type="file" name="upload_product_sheet" id="upload_product_sheet">
                        </div>
                     </div>
                     @if($cats->name !='Services')
                     <div class="row">
                        <div class="col-lg-3">
                           <div class="left-area">
                              <h4 class="heading">{{ __('Product Includes') }}* </h4>
                              <p class="sub-heading">(In Any Language)</p>
                           </div>
                        </div>
                        <div class="col-lg-8">
                           <label class="checkbox checkbox-outline-secondary checkbox-inline mr-4">
                           <input type="checkbox" name="product_implementation" id="implementation" class="checkclick" value="yes">
                           <span>Implementation</span>
                           <span class="checkmark"></span>
                           </label>
                           <label class="checkbox checkbox-outline-secondary checkbox-inline">
                           <input type="checkbox" name="product_support" id="support" class="checkclick" value="yes">
                           <span>Support</span>
                           <span class="checkmark"></span>
                           </label>
                           <!-- <input type="checkbox" name="product_type" id="implementation" class="checkclick" value="implementation"> Implementation
                              <input type="checkbox" name="product_type" id="support" class="checkclick" value="support"> Support -->
                        </div>
                     </div>
                     @endif
                     <div class="row">
                        <div class="col-lg-3">
                           <div class="left-area">
                              <h4 class="heading">{{ __('Feature Image') }} *</h4>
                           </div>
                        </div>
                        <div class="col-lg-8">
                           <div class="panel panel-body mb-3">
                              <div class="span4 cropme text-center" id="landscape" style="width: 400px; height: 400px; border: 1px dashed black;">
                              </div>
                           </div>
                           <a href="javascript:;" id="crop-image" class="d-inline-block mybtn1">
                           <i class="icofont-upload-alt"></i> {{ __('Upload Image Here') }}
                           </a>
                        </div>
                     </div>
                     <input type="hidden" id="feature_photo" name="photo" value="">
                     <input type="file" name="gallery[]" class="hidden" id="uploadgallery" accept="image/*" multiple>
                     <input type="hidden" name="selectedTab" class="hidden" value="Features">
                     <div class="row">
                        <div class="col-lg-3">
                           <div class="left-area">
                              <h4 class="heading">
                                 {{ __('Product Gallery Images') }} *
                              </h4>
                           </div>
                        </div>
                        <div class="col-lg-8">
                           <a href="#" class="set-gallery"  data-toggle="modal" data-target="#setgallery">
                           <i class="icofont-plus"></i> {{ __('Set Gallery') }}
                           </a>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-lg-3">
                           <div class="left-area">
                              <h4 class="heading">{{ __('Youtube Video URL') }}*</h4>
                              <p class="sub-heading">{{ __('(Optional)') }}</p>
                           </div>
                        </div>
                        <div class="col-lg-8">
                           <input  name="youtube" type="text" class="form-control" value="" placeholder="{{ __('Enter Youtube Video URL') }}">
                           <div class="checkbox-wrapper">
                              <input type="checkbox" name="seo_check" value="1" class="checkclick" id="allowProductSEO" value="1"
                                 >
                              <input type="hidden" name="subcat_id" value="{{$subcats}}" >
                              <input type="hidden" name="cat_id" value="{{$catId}}" >
                              <input type="hidden" name="prod_id" id="prod_id" value="@if(!empty($prod) ){{$prod->id}}@endif" >
                              <label for="allowProductSEO">{{ __('Allow Product SEO') }}</label>
                           </div>
                        </div>
                     </div>
                     <!-- <div class="showbox">
                        <div class="row">
                           <div class="col-lg-3">
                              <div class="left-area">
                                 <h4 class="heading">{{ __('Meta Tags') }} *</h4>
                              </div>
                           </div>
                           <div class="col-lg-8">
                              <ul id="metatags" class="myTags">
                              </ul>
                           </div>
                        </div>
                        <div class="row">
                           <div class="col-lg-3">
                              <div class="left-area">
                                 <h4 class="heading">
                                    {{ __('Meta Description') }} *
                                 </h4>
                              </div>
                           </div>
                           <div class="col-lg-8">
                              <div class="text-editor">
                                 <textarea name="meta_description" rows="5" class="form-control" placeholder="{{ __('Meta Description') }}"></textarea> 
                              </div>
                           </div>
                        </div>
                        </div> -->
                     <!-- <div class="row">
                        <div class="col-lg-3">
                           <div class="left-area">
                              <h4 class="heading">{{ __('Tags') }} *</h4>
                           </div>
                        </div>
                        <div class="col-lg-8">
                           <ul id="tags" class="myTags">
                           </ul>
                        </div>
                        </div> -->
                     <div class="row">
                        <div class="col-lg-3">
                           <div class="left-area">
                           </div>
                        </div>
                        <div class="col-lg-8 text-center">
                           <button class="tablink addPro-next-btn" >Next</button>
                        </div>
                     </div>
                  </form>
               </div>
               <form id="geniusform" action="{{route('admin-prod-overview-store')}}" method="POST" enctype="multipart/form-data">
                  {{csrf_field()}}
                  <div id="Overview" class="body-area">
                     {!!$fieldsetdata!!}
                     <input type="hidden" name="prod_id" value="">
                     <input type="hidden" name="subcat_id" value="{{$subcats}}" >
                     <input type="hidden" name="cat_id" value="@if(!empty($cats)){{$catId}}@endif" >
                     <input type="hidden" name="prod_id" id="prod_id" value="@if(!empty($prod) ){{$prod->id}}@endif" >
                     <div class="row">
                        <div class="col-lg-3">
                           <div class="left-area">
                           </div>
                        </div>
                        <div class="col-lg-8 text-center">
                           <button class="tablink addPro-next-btn" >Next</button>
                        </div>
                     </div>
                  </div>
               </form>
               <form id="geniusform" action="{{route('admin-prod-store-features')}}" method="POST" enctype="multipart/form-data">
                  {{csrf_field()}}
                  <div id="Features" class="body-area">
                     <div class="row">
                        <div class="col-lg-3">
                           <div class="left-area">
                              <h4 class="heading">{{ __('Feature List') }}* </h4>
                              <p class="sub-heading">(In Any Language)</p>
                           </div>
                        </div>
                        <div class="col-lg-8">
                           <input type="hidden" name="subcat_id" value="{{$subcats}}" >
                           <input type="hidden" name="cat_id" value="@if(!empty($cats)){{$cats->id}}@endif" >
                           <input type="hidden" name="prod_id" id="prod_id" value="@if(!empty($prod) ){{$prod->id}}@endif" >
                           <select class="form-control" name="featureId" id="featureId" >
                              <option value="">{{ __('Select Feature') }}</option>
                              @foreach($featureLists as $feature)
                              <option value="{{ $feature->id }}">{{ $feature->feature_name }}</option>
                              @endforeach
                           </select>
                        </div>
                     </div>
                     <div class="" id="FeatureModules">
                     </div>
                     <div class="row" >
                        <div class="col-lg-12 text-center">
                        </div>
                        <!-- <div class="col-lg-8 text-center">
                           <button class="tablink" type="submit" >Create Product</button>
                           
                           </div> -->
                        <button class="tablink addPro-next-btn" type="submit" >Next</button>
                        <button class="tablink  addPro-tabLink"  onclick="openPage('Variations', this, 'active')" id="defaultOpen">Skip</button>
                     </div>
                  </div>
               </form>
               <form id="geniusform" action="{{route('admin-prod-store-varient')}}" method="POST" enctype="multipart/form-data">
                  {{csrf_field()}}
                  <div id="Variations" class="body-area">
                     <div class="row align-items-center">
                        <div class="col-lg-3">
                           <div class="left-area">
                              <h4 class="heading">Number of Variants</h4>
                           </div>
                        </div>
                        <div class="col-lg-8">
                           <select class="form-control" name="numberOfvar" id="numberOfvar">
                              <option value="1" >1</option>
                              <option value="2">2</option>
                              <option value="3" selected="selected" >3</option>
                           </select>
                        </div>
                     </div>
                     <input type="hidden" name="variationCount" id="variationCount" value="3">
                     <input type="hidden" name="prod_id" id="prod_id" value="@if(!empty($prod) ){{$prod->id}}@endif" >
                     <div class="row">
                        <div class="col-12">
                           <div class="addvarientrow-btn-box">
                              <button type="button" class="btn btn-success add-row addCF">
                              <i class="nav-icon i-Add "></i> Add More Features
                              </button>
                           </div>
                        </div>
                        <div class="col-12">
                           <div class="table-responsive addProductVariation-table">
                              <table class="table" id="variations-table-wrap">
                                 <thead>
                                    <tr>
                                       <th scope="col">Varient Name</th>
                                       <th scope="col" class="varient1">Variation 1	</th>
                                       <th scope="col" class="varient2">Variation 2</th>
                                       <th scope="col" class="varient3"> Variation 3</th>
                                       <th scope="col">Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td><input type="text" name="variant_name[]"   class="form-control" value="Variation Name" readonly=""></td>
                                       <td class="varient1"><input type="text" class="form-control" name="varient1[]" ></td>
                                       <td class="varient2"><input type="text" class="form-control" name="varient2[]" ></td>
                                       <td class="varient3"><input type="text" class="form-control" name="varient3[]" ></td>
                                       <td></td>
                                    </tr>
                                    @if($cats->name=='Services')
                                    <tr>
                                       <td><input type="text" name="variant_name[]"   class="form-control" value="Description" readonly=""></td>
                                       <td class="varient1"><input type="text" class="form-control" name="varient1[]" ></td>
                                       <td class="varient2"><input type="text" class="form-control" name="varient2[]" ></td>
                                       <td class="varient3"><input type="text" class="form-control" name="varient3[]" ></td>
                                       <td></td>
                                    </tr>
                                    <tr>
                                       <td><input type="text" name="variant_name[]"   class="form-control" value="Price" readonly=""></td>
                                       <td class="varient1"><input type="text" class="form-control" name="varient1[]" ></td>
                                       <td class="varient2"><input type="text" class="form-control" name="varient2[]" ></td>
                                       <td class="varient3"><input type="text" class="form-control" name="varient3[]" ></td>
                                       <td></td>
                                    </tr>
                                    @endif
                                    <tr>
                                       <td><input type="text" name="variant_name[]"   class="form-control" value="" placeholder="Feature"></td>
                                       <td class="varient1"><input type="text" class="form-control" name="varient1[]" ></td>
                                       <td class="varient2"><input type="text" class="form-control" name="varient2[]" ></td>
                                       <td class="varient3"><input type="text" class="form-control" name="varient3[]" ></td>
                                       <td>                                                
                                          <button type="button" class="btn btn-danger remCF">
                                          <i class="nav-icon i-Close-Window "></i>
                                          </button>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                        @if($cats->name=='Services')
                        <div class="col-12 text-center"><button class="tablink addPro-next-btn" >Save Service</button></div>
                        @else
                        <div class="col-12 text-center"><button class="tablink addPro-next-btn" >Next</button> </div>
                        @endif
                     </div>
                  </div>
               </form>
               <form id="geniusform" action="{{route('admin-prod-store-support-varient')}}" method="POST" enctype="multipart/form-data">
                  {{csrf_field()}}   
                  <div id="Support" class="body-area">
                     <div class="row align-items-center">
                        <div class="col-lg-3">
                           <div class="left-area">
                              <h4 class="heading"> Variants</h4>
                           </div>
                        </div>
                        <div class="col-lg-8">
                           <select class="form-control" name="variationCount" id="s_numberOfvar">
                              <option value="1" >1</option>
                              <option value="2">2</option>
                              <option value="3" selected="selected" >3</option>
                           </select>
                        </div>
                     </div>
                     <div class="row">
                        <input type="hidden" name="prod_id" id="prod_id" value="@if(!empty($prod) ){{$prod->id}}@endif" >
                        <div class="col-12">
                           <div class="addvarientrow-btn-box">
                              <button type="button" class="btn btn-success add-row addCF-support">
                              <i class="nav-icon i-Add "></i> Add Row
                              </button>
                           </div>
                        </div>
                        <div class="col-12">
                           <div class="table-responsive addProductVariation-table" id="supportTable-wrap">
                              <table class="table" id="customFields">
                                 <thead>
                                    <tr>
                                       <th scope="col">Varient Name</th>
                                       <th scope="col" class="s_varient1">Variation 1 </th>
                                       <th scope="col" class="s_varient2">Variation 2</th>
                                       <th scope="col" class="s_varient3"> Variation 3</th>
                                       <th scope="col">Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td><input type="text" name="variant_name_support[]"   class="form-control" value="Variation Name" placeholder="Support Variation Name" readonly=""></td>
                                       <td class="s_varient1"><input type="text" class="form-control" name="s_varient1[]" ></td>
                                       <td class="s_varient2"><input type="text" class="form-control" name="s_varient2[]" ></td>
                                       <td class="s_varient3"><input type="text" class="form-control" name="s_varient3[]" ></td>
                                       <td>                                                
                                          <button type="button" class="btn btn-danger remCF-impementation">
                                          <i class="nav-icon i-Close-Window "></i>
                                          </button>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td><input type="text" name="variant_name_support[]"   class="form-control" value="Support Price" placeholder="Support Price" readonly=""></td>
                                       <td class="i_varient1"><input type="number" class="form-control" name="s_varient1[]" ></td>
                                       <td class="i_varient2"><input type="number" class="form-control" name="s_varient2[]" ></td>
                                       <td class="i_varient3"><input type="number" class="form-control" name="s_varient3[]" ></td>
                                       <td>                                                
                                          <button type="button" class="btn btn-danger remCF-impementation">
                                          <i class="nav-icon i-Close-Window "></i>
                                          </button>
                                       </td>
                                    </tr>
                                    <!-- <tr>
                                       <td><input type="text" name="variant_name_support[]"   class="form-control" value="" placeholder="Feature"></td>
                                       <td class="s_varient1"><input type="text" class="form-control" name="s_varient1[]"></td>
                                       <td class="s_varient2"><input type="text" class="form-control" name="s_varient2[]"></td>
                                       <td class="s_varient3"><input type="text" class="form-control" name="s_varient3[]"></td>
                                       <td>                                                
                                          <button type="button" class="btn btn-danger remCF-support">
                                          <i class="nav-icon i-Close-Window "></i>
                                          </button>
                                       </td>
                                    </tr> -->
                                 </tbody>
                              </table>
                           </div>
                        </div>
                        <div class="col-12 text-center">
                           <button class="tablink addPro-next-btn">Save & Close</button>
                        </div>
                     </div>
                  </div>
               </form>
               <form id="geniusform" action="{{route('admin-prod-store-implementation-varient')}}" method="POST" enctype="multipart/form-data">
                  <div id="Implementation" class="body-area">
                     {{csrf_field()}}  
                     <div class="row align-items-center">
                        <div class="col-lg-3">
                           <div class="left-area">
                              <h4 class="heading">Number of Variants</h4>
                           </div>
                        </div>
                        <div class="col-lg-8">
                           <select class="form-control" name="variationCount" id="i_numberOfvar">
                              <option value="1"  >1</option>
                              <option value="2">2</option>
                              <option value="3" selected="selected">3</option>
                           </select>
                        </div>
                     </div>
                     <div class="row">
                        <input type="hidden" name="prod_id" id="prod_id" value="@if(!empty($prod) ){{$prod->id}}@endif" >
                        <div class="col-12">
                           <div class="addvarientrow-btn-box">
                              <button type="button" class="btn btn-success add-row addCF-implementation">
                              <i class="nav-icon i-Add "></i> Add Row
                              </button>
                           </div>
                        </div>
                        <div class="col-12">
                           <div class="table-responsive addProductVariation-table" id="implementation-table-wrapper">
                              <table class="table" id="customFields">
                                 <thead>
                                    <tr>
                                       <th scope="col">Varient Name</th>
                                       <th scope="col" class="i_varient1">Variation 1	</th>
                                       <th scope="col" class="i_varient2">Variation 2</th>
                                       <th scope="col" class="i_varient3"> Variation 3</th>
                                       <th scope="col">Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td><input type="text" name="variant_name[]"   class="form-control" value="Variation Name" placeholder="Implementation Variation Name" readonly=""></td>
                                       <td class="i_varient1"><input type="text" class="form-control" name="i_varient1[]" ></td>
                                       <td class="i_varient2"><input type="text" class="form-control" name="i_varient2[]" ></td>
                                       <td class="i_varient3"><input type="text" class="form-control" name="i_varient3[]" ></td>
                                       <td>                                                
                                          <button type="button" class="btn btn-danger remCF-impementation">
                                          <i class="nav-icon i-Close-Window "></i>
                                          </button>
                                       </td>
                                    </tr>
                                    <tr>
                                       <td><input type="text" name="variant_name[]"   class="form-control" value="Implementation Price" placeholder="Implementation Price" readonly=""></td>
                                       <td class="i_varient1"><input type="number" class="form-control" name="i_varient1[]" ></td>
                                       <td class="i_varient2"><input type="number" class="form-control" name="i_varient2[]" ></td>
                                       <td class="i_varient3"><input type="number" class="form-control" name="i_varient3[]" ></td>
                                       <td>                                                
                                          <button type="button" class="btn btn-danger remCF-impementation">
                                          <i class="nav-icon i-Close-Window "></i>
                                          </button>
                                       </td>
                                    </tr>
                                    <!-- <tr>
                                       <td><input type="text" name="variant_name[]"   class="form-control" value="" placeholder="Feature"></td>
                                       <td class="i_varient1"><input type="text" class="form-control" name="i_varient1[]" ></td>
                                       <td class="i_varient2"><input type="text" class="form-control" name="i_varient2[]" ></td>
                                       <td class="i_varient3"><input type="text" class="form-control" name="i_varient3[]" ></td>
                                       <td>                                                
                                          <button type="button" class="btn btn-danger remCF-impementation">
                                          <i class="nav-icon i-Close-Window "></i>
                                          </button>
                                       </td>
                                    </tr> -->
                                 </tbody>
                              </table>
                           </div>
                        </div>
                        <div class="col-12 text-center">
                           <button class="tablink addPro-next-btn">Next</button>
                        </div>
                     </div>
                  </div>
               </form>
            </div>
            @if(!empty($prod) )
            <form id="pricing_form" action="{{route('save-pricing',$prod->id)}}" method="POST" enctype="multipart/form-data">
               @else
            <form id="pricing_form" action="{{route('save-pricing','999')}}" method="POST" enctype="multipart/form-data">
               @endif
               {{csrf_field()}}
               <div id="Pricing" class="body-area">
                  <!-- <div class="row">
                     <div class="col-lg-3">
                     	Number of Variants
                     </div>
                     <div class="col-lg-1">
                     	<select class="form-control" name="" id="numberOfvar">
                     			<option value="1" selected="selected" >1</option>
                     			<option value="2">2</option>	
                     			<option value="3">3</option>	
                     	</select>	
                     </div>
                     
                     </div> -->
                  <input type="hidden" name="prod_id" id="prod_id" value="@if(!empty($prod) ){{$prod->id}}@endif" >
                  <input type="hidden" name="variation_count" id="variation_count" value="@if($variation_count != ''){{$variation_count}}@endif">
                  <div class="row">
                     <div class="col-lg-3">
                        <label class="checkbox checkbox-primary">
                        <input type="checkbox" name="fixed_price_checkbox" class="fixed_price_checkbox" id="fixed_price_checkbox" checked="">
                        <span>{{ __('Fixed Price') }}</span>
                        <span class="checkmark"></span>
                        </label>
                     </div>
                     <div class="col-lg-3">
                        <label class="checkbox checkbox-primary">
                        <input type="checkbox" name="user_slab_checkbox" class="user_slab_checkbox" id="user_slab_checkbox">
                        <span>{{ __('User Slab') }}</span>
                        <span class="checkmark"></span>
                        </label>
                     </div>
                     <div class="col-lg-3">
                        <label class="checkbox checkbox-primary">
                        <input type="checkbox" name="module_price_checkbox" class="module_price_checkbox" id="module_price_checkbox">
                        <span>{{ __('Module Price') }}</span>
                        <span class="checkmark"></span>
                        </label>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-12">
                        <div class="table-responsive addProductVariation-table addPricing-table">
                           <div id="fixed_price_section">
                              <h4 class="pricing-table-head">{{ __('Fixed Price') }}:- </h4>
                              <table class="table table-bordered">
                                 <thead>
                                    <tr>
                                       <th scope="col" width="20%"> Particular </th>
                                       <th class="fixed_variation_price1" scope="col"> Variation 1 </th>
                                       <th class="fixed_variation_price2" scope="col"> Variation 2</th>
                                       <th class="fixed_variation_price3" scope="col"> Variation 3</th>
                                       <th scope="col"> Licence Type</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td><input type="text" class="form-control" value="{{ __('Fixed Price') }}" name="" disabled></td>
                                       <td class="fixed_variation_price1"> <input type="text" class="form-control fixed_variation_price1" placeholder="{{ __('Enter Price') }}" name="fixed_price_variation1"></td>
                                       <td class="fixed_variation_price2"> <input type="text" class="form-control fixed_variation_price2" placeholder="{{ __('Enter Price') }}" name="fixed_price_variation2"> </td>
                                       <td class="fixed_variation_price3"> <input type="text" class="form-control fixed_variation_price3" placeholder="{{ __('Enter Price') }}" name="fixed_price_variation3"> </td>
                                       <td>    
                                          <label class="checkbox checkbox-primary">    
                                             <input type="checkbox" name="fixed_pricing_type" class="fixed_pricing_type" id="fixed_pricing_type">     
                                             <span>{{ __('User Based') }}</span>    
                                             <span class="checkmark"></span>     
                                          </label>       
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                           <div id="user_slab_section" class="mt-5" style="display: none;">
                              <h4 class="pricing-table-head">{{ __('User Slab') }}:- </h4>
                              <table class="table table-bordered">
                                 <thead>
                                    <tr>
                                       <th scope="col" width="20%"> Particular </th>
                                       <th class="user_slab_variation_price1" scope="col"> Variation 1	</th>
                                       <th class="user_slab_variation_price2" scope="col"> Variation 2</th>
                                       <th class="user_slab_variation_price3" scope="col"> Variation 3</th>
                                       <th scope="col"> Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td><input type="text" class="form-control" value="{{ __('User Slab') }}" name="" disabled></td>
                                       <td class="user_slab_variation_price1">
                                          <div class="min-max-priceCol">
                                             <input type="text" class="form-control user_slab_variation_price1" placeholder="{{ __('Lower Limit') }}" name="user_slab_lower_limit[]">
                                             <input type="text" class="form-control user_slab_variation_price1" placeholder="{{ __('Upper Limit') }}" name="user_slab_upper_limit[]">
                                          </div>
                                          <input type="text" class="form-control mt-2 user_slab_variation_price1" name="user_slab_variation1[]" placeholder="{{ __('Price') }}">
                                       </td>
                                       <td class="user_slab_variation_price2">
                                          <div class="min-max-priceCol">
                                             <input type="text" class="form-control user_slab_variation_price2" placeholder="{{ __('Lower Limit') }}" disabled>
                                             <input type="text" class="form-control user_slab_variation_price2" placeholder="{{ __('Upper Limit') }}" disabled>
                                          </div>
                                          <input type="text" class="form-control mt-2 user_slab_variation_price2" name="user_slab_variation2[]" placeholder="{{ __('Price') }}">
                                       </td>
                                       <td class="user_slab_variation_price3">
                                          <div class="min-max-priceCol">
                                             <input type="text" class="form-control user_slab_variation_price3" placeholder="{{ __('Lower Limit') }}" disabled>
                                             <input type="text" class="form-control user_slab_variation_price3" placeholder="{{ __('Upper Limit') }}" disabled>
                                          </div>
                                          <input type="text" class="form-control mt-2 user_slab_variation_price3" name="user_slab_variation3[]" placeholder="{{ __('Price') }}">
                                       </td>
                                       <td>                                                
                                          <button type="button" class="btn btn-success addCF-userSlab">
                                          <i class="nav-icon i-Add "></i>
                                          </button>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                           <div id="module_price_section" class="mt-5" style="display: none;">
                              <h4 class="pricing-table-head">{{ __('Module Price') }}:- </h4>
                              <table class="table table-bordered">
                                 <thead>
                                    <tr>
                                       <th scope="col" width="20%"> Particular </th>
                                       <th class="module_variation_price1" scope="col"> Variation 1	</th>
                                       <th class="module_variation_price2" scope="col"> Variation 2</th>
                                       <th class="module_variation_price3" scope="col"> Variation 3</th>
                                       <th scope="col"> Licence Type</th>
                                       <th scope="col"> Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td> <input type="text" name="module_name[]" class="form-control" placeholder="{{ __('Enter Module Name') }}" name=""></td>
                                       <td class="module_variation_price1"><input type="text" class="form-control" placeholder="{{ __('Module Price') }}" name="module_price_variation1[]"></td>
                                       <td class="module_variation_price2"><input type="text" class="form-control" placeholder="{{ __('Module Price') }}" name="module_price_variation2[]"></td>
                                       <td class="module_variation_price3"><input type="text" class="form-control" placeholder="{{ __('Module Price') }}" name="module_price_variation3[]"></td>
                                       <td>    
                                          <label class="checkbox checkbox-primary">    
                                             <input type="checkbox" name="pricing_type[]" class="pricing_type" id="pricing_type[]">     
                                             <span>{{ __('User Based') }}</span>    
                                             <span class="checkmark"></span>     
                                          </label>       
                                       </td>
                                       <td>                                                 
                                          <button type="button" class="btn btn-success addCF-pricing">
                                          <i class="nav-icon i-Add "></i>
                                          </button>
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-12">
                     <div class="text-center">
                        <button class="tablink addPro-next-btn" type="submit">{{ __("Next") }}</button>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
</div>
</div>
<div class="modal fade" id="setgallery" tabindex="-1" role="dialog" aria-labelledby="setgallery" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered  modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitle">{{ __('Image Gallery') }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="top-area">
               <div class="row">
                  <div class="col-sm-6 text-right">
                     <div class="upload-img-btn">
                        <label for="image-upload" id="prod_gallery"><i class="icofont-upload-alt"></i>{{ __('Upload File') }}</label>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <a href="javascript:;" class="upload-done" data-dismiss="modal"> <i class="fas fa-check"></i> {{ __('Done') }}</a>
                  </div>
                  <div class="col-sm-12 text-center">( <small>{{ __('You can upload multiple Images.') }}</small> )</div>
               </div>
            </div>
            <div class="gallery-images">
               <div class="selected-image">
                  <div class="row">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
<input type="hidden" value="{{$product_name}}" id="pstring">
@endsection
@section('scripts')
<script src="{{asset('assets/admin/js/jquery.Jcrop.js')}}"></script>
<script src="{{asset('assets/admin/js/jquery.SimpleCropper.js')}}"></script>
<script src="{{asset('assets/admin/js/jquery.tagsinput-revisited.js')}}"></script>
<script type="text/javascript">
 
   //var pstring = array();
   var pstring = $("#pstring").val();
   var prArray = pstring.split(",");
   //alert(pstring);
   // Gallery Section Insert
   $('#form-tags-4').tagsInput({
   				'autocomplete': {
   					source: prArray
   				}
   			});
   
    $(document).on('click', '.remove-img' ,function() {
      var id = $(this).find('input[type=hidden]').val();
      $('#galval'+id).remove();
      $(this).parent().parent().remove();
    });
   
    $(document).on('click', '#prod_gallery' ,function() {
      $('#uploadgallery').click();
       $('.selected-image .row').html('');
      $('#geniusform').find('.removegal').val(0);
    });
   
    $("#featureId").change(function() {
   
       var featureId  = $(this).val();
     // document.getElementById('hide_next_btn').style.display = 'block';
      $.ajax({
   
          url: "../featureModules/"+featureId,
          type: "get",
         // data: $(this).val(),
         // dataType: 'JSON',
          success: function (data) {
              $('#FeatureModules').html(data);
   
              //alert(data)
   
          }
   }); 
    });            
      ////// display subcategory fields section
           
    $("#uploadgallery").change(function(){
       var total_file=document.getElementById("uploadgallery").files.length;
       for(var i=0;i<total_file;i++)
       {
        $('.selected-image .row').append('<div class="col-sm-6">'+
                                          '<div class="img gallery-img">'+
                                              '<span class="remove-img"><i class="fas fa-times"></i>'+
                                              '<input type="hidden" value="'+i+'">'+
                                              '</span>'+
                                              '<a href="'+URL.createObjectURL(event.target.files[i])+'" target="_blank">'+
                                              '<img src="'+URL.createObjectURL(event.target.files[i])+'" alt="gallery image">'+
                                              '</a>'+
                                          '</div>'+
                                    '</div> '
                                        );
        $('#geniusform').append('<input type="hidden" name="galval[]" id="galval'+i+'" class="removegal" value="'+i+'">')
       }
   
    });
   
   //  $('#numberOfvar').change(function(){
   //  	var count = $(this).val();
   //  	$('#variationCount').val(count);
   
   //   	  var no = $(this).val();
   //   	  if(no == 1){
   //   	  	$('.varient2').hide();
   //        $('#varienthead2').hide();
   //   	  	$('.varient3').hide();
   //        $('#varienthead3').hide();
   //   	  } if(no == 2){
   //   	  		$('.varient2').show();
   //        $('#varienthead2').show();
   //   	  	$('.varient3').hide();
   //        $('#varienthead3').hide();
   //   	  } if(no == 3){
   //   	  	$('.varient2').show();
   //        $('#varienthead2').show();
   //   	  	$('.varient3').show();
   //        $('#varienthead3').show();
   //   	  }
      
   // });
   
   //  $('#i_numberOfvar').change(function(){
   
   //   	  var no = $(this).val();
   //   	  if(no == 1){
   //   	  	$('.i_varient2').hide();
   //        $('#i_varienthead2').hide();
   //   	  	$('.i_varient3').hide();
   //        $('#i_varienthead3').hide();
   //   	  } if(no == 2){
   //   	  		$('.i_varient2').show();
   //        $('#i_varienthead2').show();
   //   	  	$('.i_varient3').hide();
   //        $('#i_varienthead3').hide();
   //   	  } if(no == 3){
   //   	  	$('.i_varient2').show();
   //        $('#i_varienthead2').show();
   //   	  	$('.i_varient3').show();
   //        $('#i_varienthead3').show();
   //   	  }
      
   // });
   
   //  $('#s_numberOfvar').change(function(){
   
   //   	  var no = $(this).val();
   //   	  if(no == 1){
   //   	  	$('.s_varient2').hide();
   //        $('#s_varienthead2').hide();
   //   	  	$('.s_varient3').hide();
   //        $('#s_varienthead3').hide();
   //   	  } if(no == 2){
   //   	  		$('.s_varient2').show();
   //        $('#s_varienthead2').show();
   //   	  	$('.s_varient3').hide();
   //        $('#s_varienthead3').hide();
   //   	  } if(no == 3){
   //   	  	$('.s_varient2').show();
   //        $('#s_varienthead2').show();
   //   	  	$('.s_varient3').show();
   //        $('#s_varienthead3').show();
   //   	  }
      
   // });
  // var i=1; 
    
        /*$('#add').click(function(){  
             i++;  
             $('#dynamic_field').append('<din id="row'+i+'" class="row col-lg-12"><div  class="col-lg-3"><div class="right-area"><input type="text" name="variant_name[]" class="form-control" value="" /></div></div><div  class="col-lg-3"><div class="right-area"><input type="hidden" name="count" value='+i+' /><input type="text" class="form-control" name="varient1[]" ></div></div><div class="col-lg-3 varient2" style="display: none"><div class="right-area"><input type="text" class="form-control" name="varient2[]" ></div> </div> <div class="col-lg-3 varient3" style="display: none"><div class="right-area"><input type="text" class="form-control" name="varient3[]" ></div></div><div class="col-lg-2"><div class="right-area"><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></div></div></div>');  
        });  
   
   
       
   
   
        $('#addImp').click(function(){  
             i++;  
             $('#i_dynamic_field').append('<din id="row'+i+'" class="row col-lg-12"><div  class="col-lg-3"><div class="right-area"><input type="text" name="variant_name[]" class="form-control" value="" /></div></div><div  class="col-lg-3"><div class="right-area"><input type="hidden" name="count" value='+i+' /><input type="text" class="form-control" name="varient1[]" ></div></div><div class="col-lg-3 varient2" style="display: none"><div class="right-area"><input type="text" class="form-control" name="varient2[]" ></div> </div> <div class="col-lg-3 varient3" style="display: none"><div class="right-area"><input type="text" class="form-control" name="varient3[]" ></div></div><div class="col-lg-2"><div class="right-area"><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></div></div></div>');  
        });  
   
   
      
   
   
   		 $('#addSup').click(function(){  
             i++;  
             $('#s_dynamic_field').append('<din id="row'+i+'" class="row col-lg-12"><div  class="col-lg-3"><div class="right-area"><input type="text" name="variant_name[]" class="form-control" value="" /></div></div><div  class="col-lg-3"><div class="right-area"><input type="hidden" name="count" value='+i+' /><input type="text" class="form-control" name="varient1[]" ></div></div><div class="col-lg-3 varient2" style="display: none"><div class="right-area"><input type="text" class="form-control" name="varient2[]" ></div> </div> <div class="col-lg-3 varient3" style="display: none"><div class="right-area"><input type="text" class="form-control" name="varient3[]" ></div></div><div class="col-lg-2"><div class="right-area"><button type="button" name="remove" id="'+i+'" class="btn btn-danger btn_remove">X</button></div></div></div>');  
        });  */
   
   
        /*$(document).on('click', '.btn_remove', function(){  
             var button_id = $(this).attr("id");   
             $('#row'+button_id+'').remove();  
        });*/
   
   
    //------------------Add implemntaion and support tab ------------------//
   $('#implementation').click(function(){
            var checked_status = this.checked;
            if(checked_status == true) {
                $('#implementationTab').show();
            }
            else { $('#implementationTab').hide();
            }
        }); 
    $('#support').click(function(){
            var checked_status = this.checked;
            if(checked_status == true) {
                $('#supportTab').show();
            }
            else { $('#supportTab').hide();
            }
        }); 
                   
        //-----------add module price section through js (Sambhav)--------------//
      $(document).ready(function(){  
      	var i=1;
   
       $(document).on('click', '.addCF-pricing', function(){
       	var variation_count = $('#variation_count').val();
   
       	if(variation_count == '1')
       	{
   			$("#module_price_section table").append('<tr> <td> <input type="text" class="form-control" placeholder="{{__('Enter Module Nam')}}" name="module_name[]"></td><td class="module_variation_price1"><input type="text" class="form-control 1" placeholder="{{__('Module Price')}}" name="module_price_variation1[]"></td><td class="module_variation_price2" style="display:none;"><input type="text" class="form-control"  placeholder="{{__('Module Price')}}" name="module_price_variation2[]"></td><td class="module_variation_price3" style="display:none;"><input type="text" class="form-control"  placeholder="{{__('Module Price')}}" name="module_price_variation3[]"></td><td><label class="checkbox checkbox-primary"><input type="checkbox" name="pricing_type[]" class="pricing_type" id="pricing_type[]"><span>{{ __('User Based') }}</span><span class="checkmark"></span></label></td><td> <button type="button" class="btn btn-danger remCF-pricing"> <i class="nav-icon i-Close-Window "></i> </button> </td></tr>');
       	}
       	else if(variation_count == '2')
       	{
       		$("#module_price_section table").append('<tr> <td> <input type="text" class="form-control" placeholder="{{__('Enter Module Nam')}}" name="module_name[]"></td><td class="module_variation_price1"><input type="text" class="form-control" placeholder="{{__('Module Price')}}" name="module_price_variation1[]"></td><td class="module_variation_price2"><input type="text" class="form-control" placeholder="{{__('Module Price')}}" name="module_price_variation2[]"></td><td class="module_variation_price3" style="display:none;"><input type="text" class="form-control" placeholder="{{__('Module Price')}}" name="module_price_variation3[]"></td><td><label class="checkbox checkbox-primary"><input type="checkbox" name="pricing_type[]" class="pricing_type" id="pricing_type[]"><span>{{ __('User Based') }}</span><span class="checkmark"></span></label></td><td> <button type="button" class="btn btn-danger remCF-pricing"> <i class="nav-icon i-Close-Window "></i></button></td></tr>');
       	}
       	else if(variation_count == '3')
       	{
       		$("#module_price_section table").append('<tr> <td> <input type="text" class="form-control" placeholder="{{__('Enter Module Nam')}}" name="module_name[]"></td><td class="module_variation_price1"><input type="text" class="form-control" placeholder="{{__('Module Price')}}" name="module_price_variation1[]"></td><td class="module_variation_price2"><input type="text" class="form-control" placeholder="{{__('Module Price')}}" name="module_price_variation2[]"></td><td class="module_variation_price3"><input type="text" class="form-control" placeholder="{{__('Module Price')}}" name="module_price_variation3[]"></td><td><label class="checkbox checkbox-primary"><input type="checkbox" name="pricing_type[]" class="pricing_type" id="pricing_type[]"><span>{{ __('User Based') }}</span><span class="checkmark"></span></label></td><td> <button type="button" class="btn btn-danger remCF-pricing"> <i class="nav-icon i-Close-Window "></i> </button> </td></tr>');  
       	}
   
   		$("#module_price_section table").on('click','.remCF-pricing',function(){
           $(this).parent().parent().remove();
       });
           
        });  
   
   
       $(document).on('click', '.btn_remove_module_price', function(){  
          var button_id = $(this).attr("id");   
          $('#row'+button_id+'').remove();  
       }); 
   
   });
   
      //---------------End -------------------------------// 
   
      //-----------add user slab section through js (Sambhav)--------------//
      $(document).ready(function(){  
      	var i=1;
   
       	$(document).on('click', '.addCF-userSlab', function(){
   
       	var variation_user_slab_count = $('#variation_count').val();
   		// alert(variation_user_slab_count)
       	if(variation_user_slab_count == '1')
       	{
   			$("#user_slab_section table").append('<tr><td><input type="text" class="form-control" value="{{__('User Slab')}}" name="" disabled></td><td class="user_slab_variation_price1"> <div class="min-max-priceCol"> <input type="text" class="form-control" placeholder="{{__('Lower Limit')}}" name="user_slab_lower_limit[]"> <input type="text" class="form-control" placeholder="{{__('Upper Limit')}}" name="user_slab_upper_limit[]"> </div><input type="text" class="form-control mt-2 user_slab_variation_price1" name="user_slab_variation1[]"  placeholder="{{__('Price')}}"> </td><td class="user_slab_variation_price2" style="display:none;"> <div class="min-max-priceCol"> <input type="text" class="form-control" placeholder="{{__('Lower Limit')}}" style="display:none;"> <input type="text" class="form-control" placeholder="{{__('Upper Limit')}}" style="display:none;"> </div><input type="text" class="form-control mt-2 user_slab_variation_price2" name="user_slab_variation2[]" style="display:none;" placeholder="{{__('Price')}}"> </td><td class="user_slab_variation_price3" style="display:none;"> <div class="min-max-priceCol"> <input type="text" class="form-control" placeholder="{{__('Lower Limit')}}" style="display:none;"> <input type="text" class="form-control" placeholder="{{__('Upper Limit')}}" style="display:none;"> </div><input type="text" class="form-control mt-2 user_slab_variation_price3" name="user_slab_variation3[]" style="display:none;" placeholder="{{__('Price')}}"> </td><td> <button type="button" class="btn btn-danger remCF-userSlab"> <i class="nav-icon i-Close-Window "></i> </button> </td></tr>');
   			
       	}
       	else if(variation_user_slab_count == '2')
       	{
       		$("#user_slab_section table").append('<tr><td><input type="text" class="form-control" value="{{__('User Slab')}}" name="" disabled></td><td class="user_slab_variation_price1"> <div class="min-max-priceCol"> <input type="text" class="form-control" placeholder="{{__('Lower Limit')}}" name="user_slab_lower_limit[]"> <input type="text" class="form-control" placeholder="{{__('Upper Limit')}}" name="user_slab_upper_limit[]"> </div><input type="text" class="form-control mt-2 user_slab_variation_price1" name="user_slab_variation1[]"  placeholder="{{__('Price')}}"> </td><td class="user_slab_variation_price2"> <div class="min-max-priceCol"> <input type="text" class="form-control" placeholder="{{__('Lower Limit')}}" disabled> <input type="text" class="form-control" placeholder="{{__('Upper Limit')}}" disabled> </div><input type="text" class="form-control mt-2 user_slab_variation_price2" name="user_slab_variation2[]" placeholder="{{__('Price')}}"> </td><td class="user_slab_variation_price3" style="display:none;"> <div class="min-max-priceCol"> <input type="text" class="form-control" placeholder="{{__('Lower Limit')}}" style="display:none;"> <input type="text" class="form-control" placeholder="{{__('Upper Limit')}}" style="display:none;"> </div><input type="text" class="form-control mt-2 user_slab_variation_price3" name="user_slab_variation3[]" style="display:none;" placeholder="{{__('Price')}}"> </td><td> <button type="button" class="btn btn-danger remCF-userSlab"> <i class="nav-icon i-Close-Window "></i> </button> </td></tr>');
       	}
       	else if(variation_user_slab_count == '3')
       	{
       		$("#user_slab_section table").append('<tr><td><input type="text" class="form-control" value="{{__('User Slab')}}" name="" disabled></td><td class="user_slab_variation_price1"> <div class="min-max-priceCol"> <input type="text" class="form-control" placeholder="{{__('Lower Limit')}}" name="user_slab_lower_limit[]"> <input type="text" class="form-control" placeholder="{{__('Upper Limit')}}" name="user_slab_upper_limit[]"> </div><input type="text" class="form-control mt-2 user_slab_variation_price1" name="user_slab_variation1[]"  placeholder="{{__('Price')}}"> </td><td class="user_slab_variation_price2"> <div class="min-max-priceCol"> <input type="text" class="form-control" placeholder="{{__('Lower Limit')}}" "> <input type="text" class="form-control" placeholder="{{__('Upper Limit')}}"> </div><input type="text" class="form-control mt-2 user_slab_variation_price2" name="user_slab_variation2[]" placeholder="{{__('Price')}}"> </td><td class="user_slab_variation_price3"> <div class="min-max-priceCol"> <input type="text" class="form-control" placeholder="{{__('Lower Limit')}}"> <input type="text" class="form-control" placeholder="{{__('Upper Limit')}}"> </div><input type="text" class="form-control mt-2 user_slab_variation_price3" name="user_slab_variation3[]" placeholder="{{__('Price')}}"> </td><td> <button type="button" class="btn btn-danger remCF-userSlab"> <i class="nav-icon i-Close-Window "></i> </button> </td></tr>');
       	}
   
             
       });  
   
       $("#user_slab_section table").on('click','.remCF-userSlab',function(){
           $(this).parent().parent().remove();
       	});
   
   });
   
      //---------------End -------------------------------// 
   
      //-------Manage variation column number (Sambhav)-----------------//
   
      $(document).ready(function(){  
   
      	var count = $('#variation_count').val();

      	if(count == '1')
      	{
      		$('.fixed_variation_price2').hide();
      		$('.fixed_variation_price3').hide();
      		$('.user_slab_variation_price2').hide();
      		$('.user_slab_variation_price3').hide();
      		$('.module_variation_price2').hide();
      		$('.module_variation_price3').hide();
      	}
      	else if(count == '2')
      	{
      		$('.fixed_variation_price3').hide();
      		$('.user_slab_variation_price3').hide();
      		$('.module_variation_price3').hide();
   
      	}
      	else if(count == '3')
      	{
      		//$('.fixed_variation_price1').attr('disabled', 'false');
      		// $('.variation_module1').attr('disabled', 'false');
      		// $('.variation_user_slab1').attr('disabled', 'false');
      		//$('.fixed_variation_price2').attr('disabled', 'false');
      		// $('.variation_module2').attr('disabled', 'false');
      		// $('.variation_user_slab2').attr('disabled', 'false');
      		//$('.fixed_variation_price3').attr('disabled', 'false');
      		// $('.variation_module3').attr('disabled', 'false');
      		// $('.variation_user_slab3').attr('disabled', 'false');
      	}
      });
   
   

   // Add variation start //
   
   $(document).ready(function(){
   		$("#numberOfvar").change(function(){
   			var count = $('#numberOfvar').val();
            // alert("count is"+count)
         $('#variationCount').val(count);
       	if(count == '1')
       	{
    		$(".varient2 input").attr("disabled","disabled");
    		$(".varient3 input").attr("disabled","disabled");
       	}
       	else if(count == '2')
       	{       		
       		$(".varient2 input").removeAttr("disabled");
       		$(".varient3 input").attr("disabled","disabled");
   
       	}
       	else if(count == '3')
       	{
       		$(".varient2 input").removeAttr("disabled");
       		$(".varient3 input").removeAttr("disabled");
       		
       	}
      	});
      });
   

   	$(document).ready(function(){  
      	var i=1;
   
       	$(document).on('click', '.addCF', function(){
   			var count = $('#numberOfvar').val();
       	// var variation_user_slab_count = $('#variation_count').val();
   		//alert(variation_user_slab_count)
       	if(count == '1')
       	{

   			$("#variations-table-wrap").append('<tr> <td><input type="text" name="variant_name[]" class="form-control" value="" placeholder="Feature"></td><td class="varient1"><input type="text" class="form-control" name="varient1[]"></td><td class="varient2"><input type="text" class="form-control" name="varient2[]" disabled="disabled"></td><td class="varient3"><input type="text" class="form-control" name="varient3[]" disabled="disabled"></td><td> <button type="button" class="btn btn-danger remCF"> <i class="nav-icon i-Close-Window "></i> </button> </td></tr>');
   			
       	}
       	else if(count == '2')
       	{
       		$("#variations-table-wrap").append('<tr> <td><input type="text" name="variant_name[]" class="form-control" value="" placeholder="Feature"></td><td class="varient1"><input type="text" class="form-control" name="varient1[]"></td><td class="varient2"><input type="text" class="form-control" name="varient2[]"></td><td class="varient3"><input type="text" class="form-control" name="varient3[]" disabled="disabled"></td><td> <button type="button" class="btn btn-danger remCF"> <i class="nav-icon i-Close-Window "></i> </button> </td></tr>');
       	}
       	else if(count == '3')
       	{
       		$("#variations-table-wrap").append('<tr> <td><input type="text" name="variant_name[]" class="form-control" value="" placeholder="Feature"></td><td class="varient1"><input type="text" class="form-control" name="varient1[]"></td><td class="varient2"><input type="text" class="form-control" name="varient2[]"></td><td class="varient3"><input type="text" class="form-control" name="varient3[]"></td><td> <button type="button" class="btn btn-danger remCF"> <i class="nav-icon i-Close-Window "></i> </button> </td></tr>');
       	}
   
             
       });  
   
       $("#variations-table-wrap").on('click','.remCF',function(){
           $(this).parent().parent().remove();
       	});
   
   	});

   	// Add variation End //


   	   // Add implementation start //
   
   $(document).ready(function(){
   		$("#i_numberOfvar").change(function(){
   			var count = $('#i_numberOfvar').val();
       	if(count == '1')
       	{
    		$(".i_varient2 input").attr("disabled","disabled");
    		$(".i_varient3 input").attr("disabled","disabled");
       	}
       	else if(count == '2')
       	{       		
       		$(".i_varient2 input").removeAttr("disabled");
       		$(".i_varient3 input").attr("disabled","disabled");
   
       	}
       	else if(count == '3')
       	{
       		$(".i_varient2 input").removeAttr("disabled");
       		$(".i_varient3 input").removeAttr("disabled");
       		
       	}
      	});
      });
   

   	$(document).ready(function(){  
      	var i=1;
   
       	$(document).on('click', '.addCF-implementation', function(){
   			var implementationCount = $('#i_numberOfvar').val();
       	// var variation_user_slab_count = $('#variation_count').val();
   		// alert(implementationCount)
       	if(implementationCount == '1')
       	{

   			$("#implementation-table-wrapper table").append('<tr><td><input type="text" name="variant_name[]" class="form-control" value="" placeholder="Feature"></td><td class="i_varient1"><input type="text" class="form-control" name="i_varient1[]"></td><td class="i_varient2"><input type="text" class="form-control" name="i_varient2[]" disabled="disabled"></td><td class="i_varient3"><input type="text" class="form-control" name="i_varient3[]" disabled="disabled"></td><td> <button type="button" class="btn btn-danger remCF-impementation"> <i class="nav-icon i-Close-Window "></i> </button> </td></tr>');
   			
       	}
       	else if(implementationCount == '2')
       	{
       		$("#implementation-table-wrapper table").append('<tr> <td><input type="text" name="variant_name[]" class="form-control" value="" placeholder="Feature"></td><td class="i_varient1"><input type="text" class="form-control" name="i_varient1[]"></td><td class="i_varient2"><input type="text" class="form-control" name="i_varient2[]"></td><td class="i_varient3" disabled="disabled"><input type="text" class="form-control" name="i_varient3[]"></td><td> <button type="button" class="btn btn-danger remCF-impementation"> <i class="nav-icon i-Close-Window "></i> </button> </td></tr>');
       	}
       	else if(implementationCount == '3')
       	{
       		$("#implementation-table-wrapper table").append('<tr> <td><input type="text" name="variant_name[]" class="form-control" value="" placeholder="Feature"></td><td class="i_varient1"><input type="text" class="form-control" name="i_varient1[]"></td><td class="i_varient2"><input type="text" class="form-control" name="i_varient2[]"></td><td class="i_varient3"><input type="text" class="form-control" name="i_varient3[]"></td><td> <button type="button" class="btn btn-danger remCF-impementation"> <i class="nav-icon i-Close-Window "></i> </button> </td></tr>');
       	}
   
             
       });  
   
       $("#implementation-table-wrapper table").on('click','.remCF-impementation',function(){
           $(this).parent().parent().remove();
       	});
   
   	});

   	// Add implementation End //



   	// Add Support start //
   
   $(document).ready(function(){
   		$("#s_numberOfvar").change(function(){
   			var count = $('#s_numberOfvar').val();
       	if(count == '1')
       	{
    		$(".s_varient2 input").attr("disabled","disabled");
    		$(".s_varient3 input").attr("disabled","disabled");
       	}
       	else if(count == '2')
       	{       		
       		$(".s_varient2 input").removeAttr("disabled");
       		$(".s_varient3 input").attr("disabled","disabled");
   
       	}
       	else if(count == '3')
       	{
       		$(".s_varient2 input").removeAttr("disabled");
       		$(".s_varient3 input").removeAttr("disabled");
       		
       	}
      	});
      });
   

   	$(document).ready(function(){  
      	var i=1;
   
       	$(document).on('click', '.addCF-support', function(){
   			var supportCount = $('#i_numberOfvar').val();
       	// var variation_user_slab_count = $('#variation_count').val();
   		//alert(supportCount)
       	if(supportCount == '1')
       	{

   			$("#supportTable-wrap table").append('<tr> <td><input type="text" name="variant_name_support[]" class="form-control" value="" placeholder="Feature"></td><td class="s_varient1"><input type="text" class="form-control" name="s_varient1[]"></td><td class="s_varient2"><input type="text" class="form-control" name="s_varient2[]" disabled="disabled"></td><td class="s_varient3"><input type="text" class="form-control" name="s_varient3[]" disabled="disabled"></td><td> <button type="button" class="btn btn-danger remCF-support"> <i class="nav-icon i-Close-Window "></i> </button> </td></tr>');
   			
       	}
       	else if(supportCount == '2')
       	{
       		$("#supportTable-wrap table").append('<tr> <td><input type="text" name="variant_name_support[]" class="form-control" value="" placeholder="Feature"></td><td class="s_varient1"><input type="text" class="form-control" name="s_varient1[]"></td><td class="s_varient2"><input type="text" class="form-control" name="s_varient2[]"></td><td class="s_varient3"><input type="text" class="form-control" name="s_varient3[]" disabled="disabled"></td><td> <button type="button" class="btn btn-danger remCF-support"> <i class="nav-icon i-Close-Window "></i> </button> </td></tr>');
       	}
       	else if(supportCount == '3')
       	{
       		$("#supportTable-wrap table").append('<tr> <td><input type="text" name="variant_name_support[]" class="form-control" value="" placeholder="Feature"></td><td class="s_varient1"><input type="text" class="form-control" name="s_varient1[]"></td><td class="s_varient2"><input type="text" class="form-control" name="s_varient2[]"></td><td class="s_varient3"><input type="text" class="form-control" name="s_varient3[]"></td><td> <button type="button" class="btn btn-danger remCF-support"> <i class="nav-icon i-Close-Window "></i> </button> </td></tr>');
       	}
   
             
       });  
   
       $("#supportTable-wrap table").on('click','.remCF-support',function(){
           $(this).parent().parent().remove();
         });
   
   	});

   	// Add Support End //





      //-------End -------------------------//
   
      $(document).on('change', '#fixed_price_checkbox', function(){  
   
      	if($(this).prop("checked") == true){
              $('#fixed_price_section').show();
          }
          else if($(this).prop("checked") == false){
              $('#fixed_price_section').hide();
          }
   
      });
   
      $(document).on('change', '#user_slab_checkbox', function(){  
   
      	if($(this).prop("checked") == true){
              $('#user_slab_section').show();
   
          }
          else if($(this).prop("checked") == false){
              $('#user_slab_section').hide();
          }
   
      });
   
      $(document).on('change', '#module_price_checkbox', function(){  
   
      	if($(this).prop("checked") == true){
              $('#module_price_section').show();
   
          }
          else if($(this).prop("checked") == false){
              $('#module_price_section').hide();
          }
   
      });  
   
      $(document).ready(function() {
      	 document.getElementById('hide_next_btn').style.display = 'none';
   
      }); 
   
   // Gallery Section Insert Ends	
   
</script>
<script type="text/javascript">
   $('.cropme').simpleCropper();
   $('#crop-image').on('click',function(){
   $('.cropme').click();
   });
</script>
<script>
   $('.addProduct-tabs-wrap button').on('click', function(){
      	$('.addProduct-tabs-wrap button.active').removeClass('active');
      	$(this).addClass('active');
   });
     function openPage(pageName,elmnt,color) {
     	//alert(pageName)id="prod_id"
       //var pro_id = document.getElementById("prod_id").value;
       //alert(pro_id)
       var i, tabcontent, tablinks;
       tabcontent = document.getElementsByClassName("body-area");
       for (i = 0; i < tabcontent.length; i++) {
         tabcontent[i].style.display = "none";
     
       }
       // tablinks = document.getElementsByClassName("tablink");
       // for (i = 0; i < tablinks.length; i++) {
       //   tablinks[i].style.backgroundColor = "";
     
       // }
       document.getElementById(pageName).style.display = "block";
       // elmnt.className += " " + color;
       // if(pro_id !=''){
       // 		if(pageName == 'Overview'){
       // 			document.getElementById("genralTab").disabled = true;
       // 			document.getElementById("genralTab").style.pointerEvents = "none";
       // 			document.getElementById("genralTab").style.backgroundColor = "lightgray";
       // 		}if(pageName == 'Features'){
       // 			document.getElementById("genralTab").disabled = true;
       // 			document.getElementById("genralTab").style.pointerEvents = "none";
       // 			document.getElementById("genralTab").style.backgroundColor = "lightgray";
       // 			document.getElementById("overviewTab").disabled = true;
       // 			document.getElementById("overviewTab").style.pointerEvents = "none";
       // 			document.getElementById("overviewTab").style.backgroundColor = "lightgray";
       // 		}if(pageName == 'Variations'){
       // 			document.getElementById("genralTab").disabled = true;
       // 			document.getElementById("genralTab").style.pointerEvents = "none";
       // 			document.getElementById("genralTab").style.backgroundColor = "lightgray";
       // 			document.getElementById("overviewTab").disabled = true;
       // 			document.getElementById("overviewTab").style.pointerEvents = "none";
       // 			document.getElementById("overviewTab").style.backgroundColor = "lightgray";
       // 			document.getElementById("featureTab").disabled = true;
       // 			document.getElementById("featureTab").style.pointerEvents = "none";
       // 			document.getElementById("featureTab").style.backgroundColor = "lightgray";
       // 		}
     
       // }
     }
     
     function openNextPage(pageName,elmnt,color) {
     	//alert(elmnt)
       var i, tabcontent, tablinks;
       tabcontent = document.getElementsByClassName("body-area");
       for (i = 0; i < tabcontent.length; i++) {
         tabcontent[i].style.display = "none";
     
       }
       tablinks = document.getElementsByClassName("tablink");
       for (i = 0; i < tablinks.length; i++) {
         tablinks[i].style.backgroundColor = "";
     
       }
       document.getElementById(pageName).style.display = "block";
       document.getElementById(elmnt).style.backgroundColor = color;
       //if()
     }
     
     
     // Get the element with id="defaultOpen" and click on it
     document.getElementById("defaultOpen").click();
</script>
<script>
   // $(document).ready(function(){
   // 	$(".addCF").click(function(){
   // 		$("#customFields").append('<tr> <td><input type="text" name="variant_name[]" class="form-control" value="" placeholder="Feature"></td><td><input type="text" class="form-control" name="varient1[]" ></td><td><input type="text" class="form-control" name="varient2[]" ></td><td><input type="text" class="form-control" name="varient3[]" ></td><td><button type="button" class="btn btn-danger delete-row remCF"> <i class="nav-icon i-Close-Window "></i> </button> </td></tr>');
   // 	});
   
   // 	$(".addCF-support").click(function(){
   // 		$("#supportTable-wrap table").append('<tr> <td><input type="text" name="variant_name_support[]" class="form-control" value="" placeholder="Feature"></td><td><input type="text" class="form-control" name="s_varient1[]" ></td><td><input type="text" class="form-control" name="s_varient2[]" ></td><td><input type="text" class="form-control" name="s_varient3[]" ></td><td><button type="button" class="btn btn-danger delete-row remCF-support"> <i class="nav-icon i-Close-Window "></i> </button> </td></tr>');
   // 	});
   // 	$("#supportTable-wrap table").on('click','.remCF-support',function(){
   //         $(this).parent().parent().remove();
   //     });
   	// $(".addCF-implementation").click(function(){
   	// 	$("#implementation-table-wrapper table").append('<tr> <td><input type="text" name="variant_name[]" class="form-control" value="" placeholder="Feature"></td><td><input type="text" class="form-control" name="i_varient1[]" ></td><td><input type="text" class="form-control" name="i_varient2[]" ></td><td><input type="text" class="form-control" name="i_varient3[]" ></td><td><button type="button" class="btn btn-danger delete-row remCF-implementation"> <i class="nav-icon i-Close-Window "></i> </button> </td></tr>');
   	// });
    //    $("#implementation-table-wrapper table").on('click','.remCF-implementation',function(){
    //        $(this).parent().parent().remove();
    //    });
   
   
   // });   
</script>
<script src="{{asset('assets/admin/js/chosen.jquery.js')}}"></script>
<script>
	document.getElementById('prodlist').innerHTML = location.search;
	$(".chosen-product").chosen();
</script>
<script>
	$('#addon_products').change(function(event){
    if(event.target == this){
        var value = $(this).val();
        $("input[name='addon_products']").val(value);
     }
});
</script>

<script src="{{asset('assets/admin/js/product.js')}}"></script>
<!----------------------------- Add row ------------------->
@endsection