<div class="accordion" id="featureListAccordion">
   @foreach($moduleData as $modules)
   <div class="card ">
      <div class="card-header header-elements-inline">
         <h6 class="card-title ul-collapse__icon--size ul-collapse__right-icon mb-0">
            <a data-toggle="collapse" class="text-default collapsed" href=" @php echo '#'.str_replace(' ', '-',preg_replace('/[^A-Za-z0-9\-]/', '',$modules->module_name)); @endphp"
               aria-expanded="false"> {{$modules->module_name}} </a>
         </h6>
      </div>
      <div id="@php echo str_replace(' ', '-',preg_replace('/[^A-Za-z0-9\-]/', '',$modules->module_name)); @endphp" class="collapse" data-parent="#featureListAccordion" style="">
         <div class="card-body">
            @php
            $sectionName = SubCategoryController::getSectionName($modules->module_name,$id);
            @endphp 
            @foreach($sectionName as $key=>$val)
            @foreach(json_decode($val) as $sec)
            <div class="featureList-wrap">
               <h5 class="mb-4"><strong>{{htmlspecialchars($sec->section_name)}}:- <input type="hidden"  name="section_name[]" value="{{htmlspecialchars($sec->section_name)}}" ></strong></h5>
               @php  $fName = SubCategoryController::getSFName($modules->module_name,$sec->section_name,$id); @endphp
               <input type="hidden"  name="section_name[]" value="{{htmlspecialchars($sec->section_name)}}" >
               @foreach($fName as $key1=>$val1)
               @foreach(json_decode($val1) as $fun)
               <label class="checkbox checkbox-primary checkbox-inline-block">
               <input type="checkbox" class="checkclick" name="function_val[{{htmlspecialchars($modules->module_name)}}_{{htmlspecialchars($sec->section_name)}}_{{htmlspecialchars($fun->function_name)}}]"  value="yes">
               <span>{{htmlspecialchars($fun->function_name)}}</span>
               <span class="checkmark"></span>
               </label>
               <input type="hidden"  name="function_name[]" value="{{htmlspecialchars($fun->function_name)}}">
               <input type="hidden"  name="module_name[]" val="{{$modules->module_name}}" >
               <input type="hidden"  name="feature_id" value="{{$id}}" >
               @endforeach
               @endforeach
            </div>
            @endforeach
            @endforeach
         </div>
      </div>
   </div>
   @endforeach
</div>