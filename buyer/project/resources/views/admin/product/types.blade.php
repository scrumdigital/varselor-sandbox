@extends('layouts.admin')

@section('content')

<div class="content-area">
            <div class="mr-breadcrumb">
              <div class="row">
                <div class="col-lg-12">
                    <h4 class="heading">{{ __("Add Product") }}</h4>
                    <ul class="links">
                      <li>
                        <a href="{{ route('admin.dashboard') }}">{{ __("Dashboard") }}</a>
                      </li>
                      <li>
                        <a href="javascript:;">{{ __("Products") }} </a>
                      </li>
                      <li>
                        <a href="{{ route('admin-prod-index') }}">{{ __("All Products") }}</a>
                      </li>
                      <li>
                        <a href="{{ route('admin-prod-types') }}">{{ __("Add Product") }}</a>
                      </li>
                    </ul>
                </div>
              </div>
            </div>
            <!-- <div class="add-product-content">
              <div class="row">
                <div class="col-lg-12">
                  <div class="product-description">
                    <div class="heading-area">
                      <h2 class="title">
                          {{ __("Product Types") }}
                      </h2>
                    </div>
                  </div>
                </div>
              </div> -->
              <div class="ap-product-categories">
                <div class="row">
                  <div class="col-lg-12">
                      <div class="product-description">
                      <div class="body-area">
                        <div class="row">
                          <div class="col-lg-12 offset-lg-0 col-md-10 offset-md-1">
                            <div class="product-category-column">
                              <div class="row">
                                
                                @foreach($cats as $cat)
                                  
                                  <div class="col-md-4">
                                    <div class="product-category-item">
                                        <div class="product-category-icon">
                                          @if($cat->name == "Products")
                                          <img src="{{asset('assets/images/products/product.svg')}}" alt="Product">
                                          @elseif ($cat->name == "Services")
                                          <img src="{{asset('assets/images/products/services.svg')}}" alt="Product">
                                          @elseif ($cat->name == "Add-ons")
                                          <img src="{{asset('assets/images/products/addon.svg')}}" alt="Product">
                                          @else
                                          <img src="{{asset('assets/images/products/product.svg')}}" alt="Product">
                                          @endif
                                            <h3>{{$cat->name}}</h3>
                                        </div>
                                        @php
                                          $subcats = ProductController::getSubcats($cat->id);

                                          @endphp                  
                                      <ul class="product-subcat-list">
                                        @php $i = 0; @endphp
                                        @foreach($subcats as $subcat)
                                        
                                        @if ($i < 5)
                                          @php $i++; @endphp
                                          @php $subcatfields = ProductController::getsubcatfields($subcat->id);
                                          @endphp
                                          @if($subcatfields == 0 && $cat->name != 'Services')
                                           
                                          <li class="clearfix">
                                            <button type="button" onclick="alertThis()" class="product_cat-btn">
                                              <span class="product_cat_title">{{$subcat->name}} ({{ProductController::noOfProducts($subcat->id)}})</span>
                                         
                                          
                                              <span class="fas fa-plus product_cat_go" aria-hidden="true"></span></button>
                                          </li>


                                         @else
                                          
                                          <li class="clearfix">
                                            <form name="product_cat" action="{{ route('admin-prod-create') }}" method="post">
                                          
                                            {{csrf_field()}}
                                            <button type="submit" class="product_cat-btn">
                                              <span class="product_cat_title">{{$subcat->name}}({{ProductController::noOfProducts($subcat->id)}}) </span> 
                                         
                                              <span class="fas fa-plus product_cat_go" aria-hidden="true"></span></button>
                                              <input type="hidden" id="cat" name="category_id" value="{{$cat->id}}">
                                              <input type="hidden" id="subcat" name="subcategory_id" value="{{$subcat->id}}">
                                            </form>
                                          </li>
                                          
                                          @endif
                                        @endif
                                        @endforeach
                                      
                                      </ul>
                                      <div class="proCat-read-more text-center">
                                        @if ($cat->name == "Products")
                                        <a href="javascript:void(0);" class="btn btn-red" data-toggle="modal" data-target="#filterpopup1"> View all </a>
                                        @elseif ($cat->name == "Services")
                                        <a href="javascript:void(0);" class="btn btn-red" data-toggle="modal" data-target="#filterpopup2"> View all </a>
                                        @elseif ($cat->name == "Add-ons")
                                        <a href="javascript:void(0);" class="btn btn-red" data-toggle="modal" data-target="#filterpopup3"> View all </a>
                                        @endif
                                      </div>
                                  </div>
                                </div>
                                 
                                                   
                                @endforeach
                              </div>
                            </div>
                          </div>
                        </div>
                  </div>
                </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

<!-- Prodcucts Pop-up -->          
<div class="modal category-filterModal in" id="filterpopup1" role="dialog">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Products</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">X</button>        
         </div>
         <div class="modal-body">
            <div class="filterTabs">
               <ul class="subcat_list">
                @php
                  $subcats = ProductController::getSubcats('22');                  
                @endphp

                @foreach($subcats as $subcat)
                @php $subcatfields = ProductController::getsubcatfields($subcat->id); @endphp
                  @if($subcatfields == 0 && $cat->name != 'Services')



                    <li>

                      <button  type="button" class="product_cat-btn" onclick="alertThis()">
                        <span class="product_cat_title">{{$subcat->name}} ({{ProductController::noOfProducts($subcat->id)}})  </span> 
                        
                        <span class="fas fa-plus product_cat_go" aria-hidden="true"></span>
                      </button>
                    </li>
                   @else
                    <li>
                      <form name="product_cat" action="{{ route('admin-prod-create') }}" method="post">
                      {{csrf_field()}}
                        <button type="submit" class="product_cat-btn">
                          <span class="product_cat_title">{{$subcat->name}} ({{ProductController::noOfProducts($subcat->id)}}) </span> 
                          
                          <span class="fas fa-plus product_cat_go" aria-hidden="true"></span>
                        </button>
                        <input type="hidden" id="cat" name="category_id" value="22">
                        <input type="hidden" id="subcat" name="subcategory_id" value="{{$subcat->id}}">
                      </form>
                    </li>
                    
                    @endif
                  
                  @endforeach
                  
               </ul>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Prodcucts Pop-up End-->

<!-- Services Pop-up -->          
<div class="modal category-filterModal in" id="filterpopup2" role="dialog">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Services</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">X</button>        
         </div>
         <div class="modal-body">
            <div class="filterTabs">
               <ul class="subcat_list">
                @php
                  $subcats = ProductController::getSubcats('23');                  
                @endphp

                @foreach($subcats as $subcat)
                @php $subcatfields = ProductController::getsubcatfields($subcat->id); @endphp
                  @if($subcatfields == 0 && $cat->name != 'Services')
                    <li>

                      <button  type="button" class="product_cat-btn" onclick="alertThis()">
                        <span class="product_cat_title">{{$subcat->name}} ({{ProductController::noOfProducts($subcat->id)}})  </span> 
                        
                        <span class="fas fa-plus product_cat_go" aria-hidden="true"></span>
                      </button>
                    </li>
                   @else
                    <li>
                      <form name="product_cat" action="{{ route('admin-prod-create') }}" method="post">
                      {{csrf_field()}}
                        <button type="submit" class="product_cat-btn">
                          <span class="product_cat_title">{{$subcat->name}} ({{ProductController::noOfProducts($subcat->id)}}) </span> 
                          
                          <span class="fas fa-plus product_cat_go" aria-hidden="true"></span>
                        </button>
                        <input type="hidden" id="cat" name="category_id" value="23">
                        <input type="hidden" id="subcat" name="subcategory_id" value="{{$subcat->id}}">
                      </form>
                    </li>
                    
                    @endif
                  
                  @endforeach
                  
               </ul>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Services Pop-up End-->

<!-- Add-ons Pop-up -->          
<div class="modal category-filterModal in" id="filterpopup3" role="dialog">
   <div class="modal-dialog modal-lg">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title" id="myModalLabel">Products</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">X</button>        
         </div>
         <div class="modal-body">
            <div class="filterTabs">
               <ul class="subcat_list">
                @php
                  $subcats = ProductController::getSubcats('24');                  
                @endphp

                @foreach($subcats as $subcat)
                @php $subcatfields = ProductController::getsubcatfields($subcat->id); @endphp
                  @if($subcatfields == 0 && $cat->name != 'Services')
                    <li>

                      <button  type="button" class="product_cat-btn" onclick="alertThis()">
                        <span class="product_cat_title">{{$subcat->name}} ({{ProductController::noOfProducts($subcat->id)}})  </span> 
                        
                        <span class="fas fa-plus product_cat_go" aria-hidden="true"></span>
                      </button>
                    </li>
                   @else
                    <li>
                      <form name="product_cat" action="{{ route('admin-prod-create') }}" method="post">
                      {{csrf_field()}}
                        <button type="submit" class="product_cat-btn">
                          <span class="product_cat_title">{{$subcat->name}} ({{ProductController::noOfProducts($subcat->id)}}) </span> 
                          
                          <span class="fas fa-plus product_cat_go" aria-hidden="true"></span>
                        </button>
                        <input type="hidden" id="cat" name="category_id" value="24">
                        <input type="hidden" id="subcat" name="subcategory_id" value="{{$subcat->id}}">
                      </form>
                    </li>
                    
                    @endif
                  
                  @endforeach
                  
               </ul>
            </div>
         </div>
      </div>
   </div>
</div>
<!-- Add-ons Pop-up End-->

<script type="text/javascript">
  function alertThis(){
    alert('Please add category fieldset first. Go to Product Management->Manage Categories-> Sub Category');
  }
</script>

@endsection