@extends('layouts.admin')
@section('styles')
<link href="{{asset('assets/admin/css/product.css')}}" rel="stylesheet"/>
<link href="{{asset('assets/admin/css/jquery.Jcrop.css')}}" rel="stylesheet"/>
<link href="{{asset('assets/admin/css/Jcrop-style.css')}}" rel="stylesheet"/>
<link rel="stylesheet" href="{{asset('assets/admin/css/jquery-ui-new.css')}}" />
<link rel="stylesheet" href="{{asset('assets/admin/css/jquery.tagsinput-revisited.css')}}" />
@endsection
@section('content')
<div class="content-area">
   <div class="mr-breadcrumb">
      <div class="row">
         <div class="col-lg-12">
            <h4 class="heading"><a class="add-btn" href="{{ route('admin-prod-types') }}"><i class="fas fa-arrow-left"></i> {{ __('Back') }}</a></h4>
            <ul class="links">
               <li>
                  <a href="{{ route('admin.dashboard') }}">{{ __('Dashboard') }} </a>
               </li>
               <li>
                  <a href="javascript:;">{{ __('Products') }} </a>
               </li>
               <li>
                  <a href="{{ route('admin-prod-index') }}">{{ __('All Products') }}</a>
               </li>
               <li>
                  <a href="{{ route('admin-prod-create') }}">{{ __('Edit Product') }}</a>
               </li>
            </ul>
         </div>
      </div>
      <div class="row">
         <div class="col-lg-12">
            @if ($data->category_id == 22)
               <h3><b>Product: {{ $data->name }}</b></h3>
               <h5>Category: {{$subcatname->name}}</h5>
            @elseif ($data->category_id == 23)
               <h3><b>Service: {{ $data->name }}</b></h3>
               <h5>Category: {{$subcatname->name}}</h5>
            @else
               <h3><b>Add-on: {{ $data->name }}</b></h3>
               <h5>Category: {{$subcatname->name}}</h5>
            @endif
         </div>
      </div>
   </div>
   <div class="add-product-content">
      <div class="row ">
         <div class="col-lg-2">
            <div class="addProduct-tabs-wrap">
               @if ($data->category_id==23)
                  @if($selectedTab =='genral')
                  <button class="tablink addPro-tabLink" onclick="openPage('Genral', this, 'blue')" id="defaultOpen">Genral Information</button>
                  @else
                  <button class="tablink addPro-tabLink" onclick="openPage('Genral', this, 'blue')" id="genralTab">Genral Information</button>
                  @endif
                  @if($selectedTab =='varient')
                  <button class="tablink addPro-tabLink" onclick="openPage('Variations', this, 'blue')" id="defaultOpen">Variations & Pricing</button>
                  @else
                  <button class="tablink addPro-tabLink" onclick="openPage('Variations', this, 'blue')" id="variationsTab">Variations & Pricing</button>
                  @endif
               @else
               @if($selectedTab =='genral')
               <button class="tablink addPro-tabLink" onclick="openPage('Genral', this, 'blue')" id="defaultOpen">Genral Information</button>
               @else
               <button class="tablink addPro-tabLink" onclick="openPage('Genral', this, 'blue')" id="genralTab">Genral Information</button>
               @endif
               @if($selectedTab =='overview')
               <button class="tablink addPro-tabLink" onclick="openPage('Overview', this, 'blue')" id="defaultOpen">Overview</button>
               @else
               <button class="tablink addPro-tabLink" onclick="openPage('Overview', this, 'blue')" id="overviewTab">Overview</button>
               @endif
               @if($selectedTab =='feature')
               <button class="tablink addPro-tabLink" onclick="openPage('Features', this, 'blue')" id="defaultOpen">Features</button>
               @else
               <button class="tablink addPro-tabLink" onclick="openPage('Features', this, 'blue')" id="featureTab">Features</button>
               @endif
               @if($selectedTab =='varient')
               <button class="tablink addPro-tabLink" onclick="openPage('Variations', this, 'blue')" id="defaultOpen">Variations</button>
               @else
               <button class="tablink addPro-tabLink" onclick="openPage('Variations', this, 'blue')" id="variationsTab">Variations</button>
               @endif
               @if($selectedTab =='price')
               <button class="tablink addPro-tabLink" onclick="openPage('Pricing', this, 'blue')" id="defaultOpen">Pricing</button>
               @else
               <button class="tablink addPro-tabLink" onclick="openPage('Pricing', this, 'blue')" id="pricingTab">Pricing</button>
               @endif
               @endif
              @if(!empty($data) && $data->product_implementation =='yes')
                  @if($selectedTab =='Ivarient')
                  <button class="tablink addPro-tabLink"  onclick="openPage('Implementation', this, 'active')" id="defaultOpen">Implementation</button>
                  @else
                  <button class="tablink addPro-tabLink"  onclick="openPage('Implementation', this, 'active')" id="implementationTab">Implementation</button>
                  @endif
              @endif
               @if(!empty($data) && $data->product_support =='yes')
                  @if($selectedTab =='Svarient')
                   <button class="tablink addPro-tabLink"  onclick="openPage('Support', this, 'active')" id="defaultOpen">Support</button>
                  @else
                  <button class="tablink addPro-tabLink"  onclick="openPage('Support', this, 'active')" id="supportTab">Support</button>
                  @endif  
              
               @endif
               
            </div>
         </div>
         <div class="col-lg-10">
            <div class="product-description ">
               <div class="body-area" id="Genral">
                  <form id="geniusform" action="{{route('admin-prod-update',$id)}}" method="POST" enctype="multipart/form-data">
                     {{csrf_field()}}
                     @include('includes.admin.form-both') 
                     <div class="row">
                        <div class="col-lg-3">
                           <div class="left-area">
                              <h4 class="heading">{{ __('Name') }}* </h4>
                              <p class="sub-heading">{{ __('(In Any Language)') }}</p>
                           </div>
                        </div>
                        <div class="col-lg-8">
                           <input type="text" class="form-control" placeholder="{{ __('Enter Product Name') }}" name="name" required="" value="{{ $data->name }}">
                        </div>
                     </div>
                     @if ($data->category_id == '23' || $data->category_id == '24')
                     <div class="row">
                        <div class="col-lg-3">
                           <div class="left-area">
                              <h4 class="heading">{{ __('Tagged Products') }}* </h4>
                              <p class="sub-heading">(Tag related products where this addon/service is applicable.)</p>
                           </div>
                        </div>
                        
                        <div class="col-lg-8">
                        <?php
                           
                           $prodd = array();
                           $prodd = explode(",",$data->addon_products);
                           
                        ?> 
                           <div id="prodlist"></div>
                                <select class="form-control chosen-product" id="addon_products" multiple="true">
                                 @foreach ($prodlist as $prodlist1)
                                    @if (in_array($prodlist1->name,$prodd))
                                       <option selected="">{{$prodlist1->name}}</option>
                                    @else
                                       <option>{{$prodlist1->name}}</option>
                                    @endif
                                    @endforeach
                                </select>
                                <input type="hidden" name="addon_products" value="{{$data->addon_products}}">
                           <!-- <input id="form-tags-4"  type="text" value="" placeholder="Add a Product">
                           <input id="addon_products" name="addon_products" type="hidden" value="" > -->
                        </div>
                     </div>
                     @endif
                     <div class="row">
                        <div class="col-lg-3">
                           <div class="left-area">
                              <h4 class="heading">{{ __('Country') }}* </h4>
                              <p class="sub-heading">(In Any Language)</p>
                           </div>
                        </div>
                        <div class="col-lg-8">
                           <select class="form-control" name="country_code">
                              <option value="">{{ __('Select Country') }}</option>
                              @foreach($countries as $country)
                              <option value="{{$country->id}}" <?php if (!empty($data) && $data->country_code== $country->id ):  ?>selected="selected"
                                 <?php endif ?>>{{$country->country_name}}</option>
                              @endforeach
                           </select>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-lg-3">
                           <div class="left-area">
                              <h4 class="heading">{{ __('Brand') }}* </h4>
                              <p class="sub-heading">(In Any Language)</p>
                           </div>
                        </div>
                        <div class="col-lg-8">
                           <select class="form-control" name="brand_id">
                              <option value="">{{ __('Select Brand') }}</option>
                              @foreach($brands as $brand)
                              <option value="{{$brand->id}}" <?php if (!empty($data) && $data->brand_id== $brand->id ):  ?>selected="selected"
                                 <?php endif ?>>{{$brand->name}}</option>
                              @endforeach
                           </select>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-lg-3">
                           <div class="left-area">
                              <h4 class="heading">{{ __('Vendor') }}* </h4>
                              <p class="sub-heading">(In Any Language)</p>
                           </div>
                        </div>
                        <div class="col-lg-8">
                           <select class="form-control" name="vendor_id">
                              <option value="">{{ __('Select Vendor') }}</option>
                              @foreach($VendorList as $vendor)
                              <option value="{{$vendor->id}}" <?php if (!empty($data) && $data->user_id== $vendor->id ):  ?>selected="selected"
                                 <?php endif ?> >{{$vendor->name}}</option>
                              @endforeach
                           </select>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-lg-3">
                           <div class="left-area">
                              <h4 class="heading">{{ __('Overview') }} </h4>
                           </div>
                        </div>
                        <div class="col-lg-8">
                           <!-- <input type="text" class="form-control" placeholder="{{ __('Enter Overview of Product') }}" name="overview" required="" value="<?php if (!empty($prod)): echo $prod->name; ?>
                              <?php endif ?>" maxlength="255"> -->
                           <textarea rows="5" cols="50" class="form-control" placeholder="{{ __('Enter Overview of Product') }}" name="overview" required="">@php echo str_replace("<<ENTER>>","\n",$data->overview); @endphp</textarea>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-lg-3">
                           <div class="left-area">
                              <h4 class="heading">{{ __('Additional Information') }} </h4>
                           </div>
                        </div>
                        <div class="col-lg-8">
                           <!-- <input type="text" class="form-control" placeholder="{{ __('Enter Additional Information') }}" name="additional_information" required="" value="<?php if (!empty($prod)): echo $prod->name; ?>
                              <?php endif ?>" maxlength="255"> -->
                           <textarea rows="5" cols="50" class="form-control" placeholder="{{ __('Enter Additional Information') }}" name="additional_information" required="">@php echo str_replace("<<ENTER>>","\n",$data->additional_information); @endphp</textarea>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-lg-3">
                           <div class="left-area">
                              <h4 class="heading">{{ __('Price Starts From') }} </h4>
                           </div>
                        </div>
                        <div class="col-lg-8">
                           <input type="text" class="form-control" placeholder="{{ __('Price Starts From') }}" name="price_start_range" required="" value="{{ $data->price_start_range}}" maxlength="255">
                        </div>
                     </div>
                     <div class="row" id="upload_product_spec_sheet">
                        <div class="col-lg-3">
                           <div class="left-area">
                              <h4 class="heading">{{ __("Upload Product Specification Sheet") }}*</h4>
                           </div>
                        </div>
                        <div class="col-lg8">
                           <span><a target="_blank" href="{{asset('assets/product_sheets/'.$data->product_spec_sheet) }}">Uploaded Spec Sheet</a><br></span>
                           <input type="file" name="upload_product_sheet" id="upload_product_sheet">
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-lg-3">
                           <div class="left-area">
                              <h4 class="heading">{{ __('Product Includes') }}* </h4>
                              <p class="sub-heading">(In Any Language)</p>
                           </div>
                        </div>
                        <div class="col-lg-8">
	                    	<label class="checkbox checkbox-outline-secondary checkbox-inline mr-4">
	                        <input type="checkbox" name="product_implementation" id="implementation" class="checkclick" value="yes" <?php if (!empty($data) && $data->product_implementation== 'yes' ):  ?>checked <?php endif ?>>
	                        <span>Implementation</span>
	                        <span class="checkmark"></span>
	                        </label>
	                        <label class="checkbox checkbox-outline-secondary checkbox-inline">
	                        <input type="checkbox" name="product_support" id="support" class="checkclick" value="yes" <?php if (!empty($data) && $data->product_support== 'yes' ):  ?>checked <?php endif ?>>
	                        <span>Support</span>
	                        <span class="checkmark"></span>
	                        </label>
                           
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-lg-3">
                           <div class="left-area">
                              <h4 class="heading">{{ __('Feature Image') }} *</h4>
                           </div>
                        </div>
                        <div class="col-lg-8">
                           <div class="row">
                              <div class="panel panel-body">
                                 <div class="span4 cropme text-center" id="landscape" style="width: 400px; height: 400px; border: 1px dashed black;">
                                 </div>
                              </div>
                           </div>
                           <a href="javascript:;" id="crop-image" class="d-inline-block mybtn1">
                           <i class="icofont-upload-alt"></i> {{ __('Upload Image Here') }}
                           </a>
                        </div>
                     </div>
                     <input type="hidden" id="feature_photo" name="photo" value="{{ $data->photo }}" accept="image/*">
                     <input type="hidden" id="feature_photo2" name="photo2" value="{{ $data->photo }}" accept="image/*">
                     <div class="row">
                        <div class="col-lg-3">
                           <div class="left-area">
                              <h4 class="heading">
                                 {{ __('Product Gallery Images') }} *
                              </h4>
                           </div>
                        </div>
                        <div class="col-lg-8">
                           <a href="javascript" class="set-gallery"  data-toggle="modal" data-target="#setgallery">
                           <input type="hidden" value="{{$data->id}}">
                           <i class="icofont-plus"></i> {{ __('Set Gallery') }}
                           </a>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-lg-3">
                           <div class="left-area">
                              <h4 class="heading">{{ __('Youtube Video URL') }}*</h4>
                              <p class="sub-heading">{{ __('(Optional)') }}</p>
                           </div>
                        </div>
                        <div class="col-lg-8">
                           <input  name="youtube" type="text" class="form-control" placeholder="Enter Youtube Video URL" value="{{$data->youtube}}">
                           <!-- <div class="checkbox-wrapper">
                              <input type="checkbox" name="seo_check" value="1" class="checkclick" id="allowProductSEO" {{ ($data->meta_tag != null || strip_tags($data->meta_description) != null) ? 'checked':'' }}>
                              <label for="allowProductSEO">{{ __('Allow Product SEO') }}</label>
                              </div> -->
                        </div>
                     </div>
                     <!-- <div class="row">
                        <div class="{{ ($data->meta_tag == null && strip_tags($data->meta_description) == null) ? "showbox":"" }}"> </div>
                        </div> -->
                     <!-- <div class="row">
                        <div class="col-lg-3">
                           <div class="left-area">
                              <h4 class="heading">{{ __('Meta Tags') }} *</h4>
                           </div>
                        </div>
                        <div class="col-lg-8">
                           <ul id="metatags" class="myTags">
                              @if(!empty($data->meta_tag))
                              @foreach ($data->meta_tag as $element)
                              <li>{{  $element }}</li>
                              @endforeach
                              @endif
                           </ul>
                        </div>
                        </div>
                        <div class="row">
                        <div class="col-lg-3">
                           <div class="left-area">
                              <h4 class="heading">
                                 {{ __('Meta Description') }} *
                              </h4>
                           </div>
                        </div>
                        <div class="col-lg-8">
                           <div class="text-editor">
                              <textarea name="meta_description" class="form-control" placeholder="{{ __('Details') }}">{{ $data->meta_description }}</textarea> 
                           </div>
                        </div>
                        </div>
                        <div class="row">
                        <div class="col-lg-3">
                           <div class="left-area">
                              <h4 class="heading">{{ __('Tags') }} *</h4>
                           </div>
                        </div>
                        <div class="col-lg-8">
                           <ul id="tags" class="myTags">
                           </ul>
                        </div>
                        </div>
                        <div class="row">
                        <div class="col-lg-3">
                           <div class="left-area">
                              <h4 class="heading">{{ __('Product Sheet') }} *</h4>
                           </div>
                        </div>
                        <div class="col-lg-3">
                           <div class="left-area">
                              @if(!empty($data->product_spec_sheet))
                              <a href="{{('../../../assets/product_sheets')}}/{{ $data->product_spec_sheet }}" target="_blank">{{ $data->product_spec_sheet }}</a>
                              @endif
                              <input type="file" name="upload_product_sheet">
                           </div>
                        </div>
                        </div> -->
                     <div class="row">
                        <div class="col-lg-3">
                           <div class="left-area">
                           </div>
                        </div>
                        <div class="col-lg-8 text-center">
                           <button class="tablink addPro-next-btn">Update</button>
                        </div>
                     </div>
                  </form>
               </div>
               <form id="geniusform" name="editOverview" action="{{route('admin-prod-update-overview',$id)}}" method="POST" enctype="multipart/form-data">
                  {{csrf_field()}}
                  <div id="Overview" class="body-area">
                     {!!$fieldsetdata!!}
                     <input type="hidden" name="prod_id" id="prod_id" value="@if(!empty($data) ){{$data->id}}@endif" >
                     <div class="row">
                        <div class="col-lg-3">
                           <div class="left-area">
                           </div>
                        </div>
                        <div class="col-lg-8 text-center">
                           <button class="tablink addPro-next-btn" >Update</button>
                        </div>
                     </div>
                  </div>
               </form>
               <form id="geniusform" action="{{route('admin-prod-update-feature',$id)}}" method="POST" enctype="multipart/form-data">
                  {{csrf_field()}}
                  <div id="Features" class="body-area">
                     <div class="row">
                        <div class="col-lg-3">
                           <div class="left-area">
                              <h4 class="heading">{{ __('Feature List') }}* </h4>
                              <p class="sub-heading">(In Any Language)</p>
                           </div>
                        </div>
                        <div class="col-lg-8">
                           <select class="form-control" name="featureId" id="featureId" >
                              <option value="">{{ __('Select Feature') }}</option>
                              @if(!empty($featureLists))
                              @foreach($featureLists as $feature)
                              @if(!empty($featureId) && $featureId== $feature->id)
                              <option value="{{ $feature->id }}" selected="selected"  >{{ $feature->feature_name }}</option>
                              @else
                              @if(empty($featureId))
                              <option value="{{ $feature->id }}">{{ $feature->feature_name }}</option>
                              @endif
                              @endif
                              @endforeach
                              @endif
                           </select>
                        </div>
                     </div>
                     <div class="" id="FeatureModules">
                       

						<div class="accordion" id="featureListAccordion">
						   @foreach($moduleData as $modules)
						   <div class="card ">
						      <div class="card-header header-elements-inline">
						         <h6 class="card-title ul-collapse__icon--size ul-collapse__right-icon mb-0">
						            <a data-toggle="collapse" class="text-default collapsed" href=" @php echo '#'.str_replace(' ', '-',preg_replace('/[^A-Za-z0-9\-]/', '',$modules->module_name)); @endphp"
						               aria-expanded="false"> {{$modules->module_name}} </a>
						         </h6>
						      </div>
						      <div id="@php echo str_replace(' ', '-',preg_replace('/[^A-Za-z0-9\-]/', '',$modules->module_name)); @endphp" class="collapse" data-parent="#featureListAccordion" style="">
						         <div class="card-body">
						            @php
						           $sectionName = SubCategoryController::getSectionNameFront($modules->module_name,$id);
						           @endphp 
						           @foreach($sectionName as $key=>$val)
						           @foreach(json_decode($val) as $sec)

						            <div class="featureList-wrap">
						               <h5 class="mb-4"><strong>{{htmlspecialchars($sec->section_name)}}:- <input type="hidden"  name="section_name[]" value="{{htmlspecialchars($sec->section_name)}}" ></strong></h5>
						               @php  $fName = SubCategoryController::getSFNameFront($modules->module_name,$sec->section_name,$id); @endphp
						               <input type="hidden"  name="section_name[]" value="{{htmlspecialchars($sec->section_name)}}" >
						   				@php $i= 0;@endphp
						              	@foreach($fName as $key1=>$val1)
						              	@foreach(json_decode($val1) as $fun)
						              	@php  $fvalue = SubCategoryController::getFValFront($sec->section_name,$fun->function_name,$id); @endphp
						               <label class="checkbox checkbox-primary checkbox-inline-block">

						               	@if(!empty($fvalue[0]) && $fvalue[0]->function_value == 'yes')    
						              	<input type="checkbox" checked  class="checkclick" name="function_val[{{$modules->module_name}}_{{$sec->section_name}}_{{htmlspecialchars($fun->function_name)}}]"  value="yes">
						              	@else
						              	<input type="checkbox"  class="checkclick" name="function_val[{{$modules->module_name}}_{{$sec->section_name}}_{{htmlspecialchars($fun->function_name)}}]"  value="yes">
						              	@endif
						               <span>{{htmlspecialchars($fun->function_name)}}</span>
						               <span class="checkmark"></span>
						               </label>
						               <input type="hidden"  name="function_name[]" value="{{htmlspecialchars($fun->function_name)}}">
						               <input type="hidden"  name="module_name[]" val="{{$modules->module_name}}" >
						               <input type="hidden"  name="feature_id" value="{{$id}}" >
						               @endforeach
						               @endforeach
						            </div>
						            @endforeach
						            @endforeach
						         </div>
						      </div>
						   </div>
						   @endforeach
						</div>


                     </div>
                     <div class="row">
                        <div class="col-lg-12 text-center">
                           <button class="tablink addPro-next-btn" type="submit" >Update</button>
                        </div>
                        <!-- <div class="col-lg-8 text-center">
                           <button class="tablink" type="submit" >Create Product</button>
                           
                           </div> -->
                     </div>
                  </div>
               </form>
               <form id="geniusform" action="{{route('admin-prod-update-varient',$id)}}" method="POST" enctype="multipart/form-data">
                  {{csrf_field()}}
                  <div id="Variations" class="body-area">
                      <div class="row align-items-center">
                        <div class="col-lg-3">
                           <div class="left-area">
                              <h4 class="heading">Number of Variants</h4>
                           </div>
                        </div>
                        <div class="col-lg-8">
                           <select class="form-control" name="numberOfvar" id="numberOfvar">
                              <option value="1" >1</option>
                              <option value="2">2</option>
                              <option value="3" selected="selected" >3</option>
                           </select>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-12">
                           <div class="addvarientrow-btn-box">
                              <button type="button" class="btn btn-success add-row addCF">
                              <i class="nav-icon i-Add "></i> Add More Features
                              </button>
                           </div>
                        </div>
                        <div class="col-12">
                           <div class="table-responsive addProductVariation-table">
                              <table class="table" id="variations-table-wrap">
                                 <thead>
                                    <tr>
                                       <th scope="col">Varient Name</th>
                                       <th scope="col" class="varient1">Variation 1 </th>
                                       <th scope="col" class="varient2">Variation 2</th>
                                       <th scope="col" class="varient3"> Variation 3</th>
                                       <th scope="col">Action</th>
                                    </tr>
                                 </thead>
                            
	                             <tbody>
                                   
                                 @if(!empty($product_varient))
                                 @php $i = 1; @endphp;
                                
	                             	@foreach($product_varient as $varients)
                                  @php
                                    $disabled= '';
                                    $type = 'text';
                                    if($i == 1){
                                       $disabled= 'readonly=""';
                                       
                                     }
                                      if($i == 2){
                                        $type = 'number';
                                       
                                     }

                                   @endphp
	                                <tr>
	                                   <td><input type="text" name="variant_name[]"   class="form-control" value="{{$varients->varient_name}}"  {{$disabled}}></td>
	                                   <td class="varient1"><input type="text" class="form-control" name="varient1[]" value="{{$varients->varient1}}" ></td>
	                                   <td class="varient2"><input type="text" class="form-control" name="varient2[]" value="{{$varients->varient2}}"></td>
	                                   <td class="varient3"><input type="text" class="form-control" name="varient3[]" value="{{$varients->varient3}}"></td>
                                       <td>                                                
                                          <button type="button" class="btn btn-danger remCF">
                                          <i class="nav-icon i-Close-Window "></i>
                                          </button>
                                       </td>
	                                </tr>
                                    @php $i++; @endphp
                                 
	                              @endforeach
                               
                                  @endif
	                             </tbody>
	                          </table>
	                       </div>
	                    </div>
	                    <div class="col-12 text-center"><button class="tablink addPro-next-btn" type="submit">Update</button></div>
                    </div>
                  </div>
               </form>
            </div>
            <form id="geniusform" action="{{route('admin-prod-update-svarient',$id)}}" method="POST" enctype="multipart/form-data">
                  {{csrf_field()}}   
                  <div id="Support" class="body-area">
                     <div class="row align-items-center">
                        <div class="col-lg-3">
                           <div class="left-area">
                              <h4 class="heading"> Variants</h4>
                           </div>
                        </div>
                        <div class="col-lg-8">
                           <select class="form-control" name="variationCount" id="s_numberOfvar">
                              <option value="1" >1</option>
                              <option value="2">2</option>
                              <option value="3" selected="selected" >3</option>
                           </select>
                        </div>
                     </div>
                     <div class="row">
                        <input type="hidden" name="prod_id" id="prod_id" value="@if(!empty($prod) ){{$prod->id}}@endif" >
                        <div class="col-12">
                           <div class="addvarientrow-btn-box">
                              <button type="button" class="btn btn-success add-row addCF-support">
                              <i class="nav-icon i-Add "></i> Add Row
                              </button>
                           </div>
                        </div>
                        <div class="col-12">
                           <div class="table-responsive addProductVariation-table" id="supportTable-wrap">
                              <table class="table" id="customFields">
                                 <thead>
                                    <tr>
                                       <th scope="col">Varient Name</th>
                                       <th scope="col" class="s_varient1">Variation 1 </th>
                                       <th scope="col" class="s_varient2">Variation 2</th>
                                       <th scope="col" class="s_varient3"> Variation 3</th>
                                       <th scope="col">Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @if(!empty($product_s_varient))
                                    @php $k = 1; @endphp;
                                 @foreach($product_s_varient as $svarients)
                                   @php
                                  $stype = 'number';
                                    $sdisabled ='';
                                    $sdisableV2 = '';
                                    $sdisableV3 = '';
                                    if($k == 1){
                                       $sdisabled= 'readonly=""';
                                       $stype = "text";
                                     } 
                                    if($svarients->variation_count == 1 ){
                                          $sdisableV2 = 'disabled=""';
                                   
                                          $disableV3 = 'disabled=""';
                                    } if($svarients->variation_count == 2){
                                          $sdisableV3 = 'disabled=""';
                                    }
                                   @endphp
                                   <tr>
                                      <td><input type="text" name="variant_name_support[]"   class="form-control" value="{{$svarients->varient_name}}" @php echo $sdisabled; @endphp></td>
                                      <td class="s_varient1"><input type="{{$stype}}" class="form-control" name="s_varient1[]" value="{{$svarients->varient1}}" ></td>
                                      <td class="s_varient2"><input type="{{$stype}}" {{$sdisableV2}} class="form-control" name="s_varient2[]" value="{{$svarients->varient2}}"></td>
                                      <td class="s_varient3"><input type="{{$stype}}" {{$sdisableV3}} class="form-control" name="s_varient3[]" value="{{$svarients->varient3}}"></td>
                                        <td>                                                
                                          <button type="button" class="btn btn-danger remCF-impementation">
                                          <i class="nav-icon i-Close-Window "></i>
                                          </button>
                                       </td>
                                   </tr>
                                   @php $i++; @endphp
                                   @endforeach
                                  
                                    @endif
                                    <!-- <tr>
                                       <td><input type="text" name="variant_name_support[]"   class="form-control" value="" placeholder="Feature"></td>
                                       <td class="s_varient1"><input type="text" class="form-control" name="s_varient1[]"></td>
                                       <td class="s_varient2"><input type="text" class="form-control" name="s_varient2[]"></td>
                                       <td class="s_varient3"><input type="text" class="form-control" name="s_varient3[]"></td>
                                       <td>                                                
                                          <button type="button" class="btn btn-danger remCF-support">
                                          <i class="nav-icon i-Close-Window "></i>
                                          </button>
                                       </td>
                                    </tr> -->
                                 </tbody>
                              </table>
                           </div>
                        </div>
                        <div class="col-12 text-center">
                           <button class="tablink addPro-next-btn">Update</button>
                        </div>
                     </div>
                  </div>
               </form>
               <form id="geniusform" action="{{route('admin-prod-update-ivarient',$id)}}" method="POST" enctype="multipart/form-data">
                  <div id="Implementation" class="body-area">
                     {{csrf_field()}}  
                     <div class="row align-items-center">
                        <div class="col-lg-3">
                           <div class="left-area">
                              <h4 class="heading">Number of Variants</h4>
                           </div>
                        </div>
                        <div class="col-lg-8">
                           <select class="form-control" name="variationCount" id="i_numberOfvar">
                              <option value="1"  >1</option>
                              <option value="2">2</option>
                              <option value="3" selected="selected">3</option>
                           </select>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-12">
                           <div class="addvarientrow-btn-box">
                              <button type="button" class="btn btn-success add-row addCF-implementation">
                              <i class="nav-icon i-Add "></i> Add Row
                              </button>
                           </div>
                        </div>
                        <div class="col-12">
                           <div class="table-responsive addProductVariation-table" id="implementation-table-wrapper">
                              <table class="table" id="customFields">
                                 <thead>
                                    <tr>
                                       <th scope="col">Varient Name</th>
                                       <th scope="col" class="i_varient1">Variation 1  </th>
                                       <th scope="col" class="i_varient2">Variation 2</th>
                                       <th scope="col" class="i_varient3"> Variation 3</th>
                                       <th scope="col">Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>

                                  
                                    @php $j = 1; @endphp
                                    @if (!empty($product_i_varient))
                                 @foreach($product_i_varient as $ivarients)
                                  @php
                                    $type = 'number';
                                    $disabled ='';
                                    $disableV2 = '';
                                    $disableV3 = '';
                                    if($j == 1){
                                       $disabled= 'readonly=""';
                                       $type = "text";
                                     } 
                                    if($ivarients->variation_count == 1){
                                          $disableV2 = 'disabled=""';
                                   
                                          $disableV3 = 'disabled=""';
                                    } if($ivarients->variation_count == 2){
                                          $disableV3 = 'disabled=""';
                                    }
                                   
                                   @endphp

                                   <tr>
                                      <td><input type="text" name="i_variant_name[]"   class="form-control" value="{{$ivarients->varient_name}}" @php echo $disabled; @endphp></td>
                                      <td class="i_varient1"><input type="{{$type}}" class="form-control" name="i_varient1[]" value="{{$ivarients->varient1}}" ></td>
                                      <td class="i_varient2"><input type="{{$type}}" {{$disableV2}} class="form-control" name="i_varient2[]" value="{{$ivarients->varient2}}"></td>
                                      <td class="i_varient3"><input type="{{$type}}" {{$disableV3}} class="form-control" name="i_varient3[]" value="{{$ivarients->varient3}}"></td>
                                       <td>                                                
                                          <button type="button" class="btn btn-danger remCF-impementation">
                                          <i class="nav-icon i-Close-Window "></i>
                                          </button>
                                       </td>
                                   </tr>
                                   @php $j++; @endphp;
                                   @endforeach
                                    @endif

                                 
                                    
                                 
                                 </tbody>
                              </table>
                           </div>
                        </div>
                        <div class="col-12 text-center">
                           <button class="tablink addPro-next-btn">Update</button>
                        </div>
                     </div>
                  </div>
               </form>


            <form id="pricing_form" action="{{route('edit-pricing')}}" method="POST" enctype="multipart/form-data">
               {{csrf_field()}}
               <div id="Pricing" class="body-area">
                  <!-- <div class="row">
                     <div class="col-lg-3">
                        Number of Variants
                     </div>
                     <div class="col-lg-1">
                        <select class="form-control" name="" id="numberOfvar">
                              <option value="1" selected="selected" >1</option>
                              <option value="2">2</option>  
                              <option value="3">3</option>  
                        </select>   
                     </div>
                     
                     </div> -->
                     
                  <input type="hidden" name="prod_id" id="prod_id" value="@if(!empty($id) ){{$id}}@endif" >
                  <input type="hidden" name="variation_count" id="variation_count" value="@if (!empty($varientcount)) {{$varientcount->variation_count}} @else 3 @endif">
                  <div class="row">
                     <div class="col-lg-3">
                        <label class="checkbox checkbox-primary">
                        <input type="checkbox" name="fixed_price_checkbox" class="fixed_price_checkbox" id="fixed_price_checkbox" @if (!empty($product_fixed_price))
                        checked=""
                        @endif
                        >
                        <span>{{ __('Fixed Price') }}</span>
                        <span class="checkmark"></span>
                        </label>
                     </div>
                     <div class="col-lg-3">
                        <label class="checkbox checkbox-primary">
                        <input type="checkbox" name="user_slab_checkbox" class="user_slab_checkbox" id="user_slab_checkbox" @if (!count($product_user_slab)==0)
                        checked=""
                        @endif>
                        <span>{{ __('User Slab') }}</span>
                        <span class="checkmark"></span>
                        </label>
                     </div>
                     <div class="col-lg-3">
                        <label class="checkbox checkbox-primary">
                        <input type="checkbox" name="module_price_checkbox" class="module_price_checkbox" id="module_price_checkbox" @if (!count($product_modules)==0)
                        checked=""
                        @endif>
                        <span>{{ __('Module Price') }}</span>
                        <span class="checkmark"></span>
                        </label>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-12">
                        <div class="table-responsive addProductVariation-table addPricing-table">
                           <div id="fixed_price_section" class="mt-5" style="@if (empty($product_fixed_price))
                        display: none;
                        @endif">
                              <h4 class="pricing-table-head">{{ __('Fixed Price') }}:- </h4>
                              <table class="table table-bordered">
                                 <thead>
                                    <tr>
                                       <th scope="col" width="20%"> Particular </th>
                                       <th class="fixed_variation_price1" scope="col"> Variation 1 </th>
                                       <th class="fixed_variation_price2" scope="col"> Variation 2</th>
                                       <th class="fixed_variation_price3" scope="col"> Variation 3</th>
                                       <th scope="col"> Licence Type</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    <tr>
                                       <td><input type="text" class="form-control" value="{{ __('Fixed Price') }}" name="" disabled></td>
                                       <td class="fixed_variation_price1"> <input type="text" class="form-control fixed_variation_price1" placeholder="{{ __('Enter Price') }}" name="fixed_price_variation1" value ="@if(!empty($product_fixed_price)){{$product_fixed_price->variation1}}@endif"></td>
                                       <td class="fixed_variation_price2"> <input type="text" class="form-control fixed_variation_price2" placeholder="{{ __('Enter Price') }}" name="fixed_price_variation2" value ="@if(!empty($product_fixed_price)){{$product_fixed_price->variation2}}@endif"> </td>
                                       <td class="fixed_variation_price3"> <input type="text" class="form-control fixed_variation_price3" placeholder="{{ __('Enter Price') }}" name="fixed_price_variation3" value ="@if(!empty($product_fixed_price)){{$product_fixed_price->variation3}}@endif"> </td>
                                       <td>    
                                          <label class="checkbox checkbox-primary">    
                                             <input type="checkbox" name="fixed_pricing_type" class="fixed_pricing_type" id="fixed_pricing_type" @if(!empty($product_fixed_price) && $product_fixed_price->user_based_pricing == '1') checked="" @endif>     
                                             <span>{{ __('User Based') }}</span>    
                                             <span class="checkmark"></span>     
                                          </label>       
                                       </td>
                                    </tr>
                                 </tbody>
                              </table>
                           </div>

                           <div id="user_slab_section" class="mt-5" style="@if (count($product_user_slab)==0)
                        display: none;
                        @endif">
                              <h4 class="pricing-table-head">{{ __('User Slab') }}:- </h4>
                              <table class="table table-bordered">
                                 <thead>
                                    <tr>
                                       <th scope="col" width="20%"> Particular </th>
                                       <th class="user_slab_variation_price1" scope="col"> Variation 1   </th>
                                       <th class="user_slab_variation_price2" scope="col"> Variation 2</th>
                                       <th class="user_slab_variation_price3" scope="col"> Variation 3</th>
                                       <th scope="col"> Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @if(!count($product_user_slab)==0)
                                    @foreach($product_user_slab as $userslabs)
                                    <tr>
                                       <td><input type="text" class="form-control" value="{{ __('User Slab') }}" name="" disabled></td>
                                       <td class="user_slab_variation_price1">
                                          <div class="min-max-priceCol">
                                             <input type="text" class="form-control user_slab_variation_price1" placeholder="{{ __('Lower Limit') }}" name="user_slab_lower_limit[]" value="{{$userslabs->lower_limit}}">
                                             <input type="text" class="form-control user_slab_variation_price1" placeholder="{{ __('Upper Limit') }}" name="user_slab_upper_limit[]" value="{{$userslabs->upper_limit}}">
                                          </div>
                                          <input type="text" class="form-control mt-2 user_slab_variation_price1" name="user_slab_variation1[]" placeholder="{{ __('Price') }}" value="{{$userslabs->variation1}}">
                                       </td>
                                       <td class="user_slab_variation_price2">
                                          <div class="min-max-priceCol">
                                             <input type="text" class="form-control user_slab_variation_price2" placeholder="{{ __('Lower Limit') }}" disabled >
                                             <input type="text" class="form-control user_slab_variation_price2" placeholder="{{ __('Upper Limit') }}" disabled >
                                          </div>
                                          <input type="text" class="form-control mt-2 user_slab_variation_price2" name="user_slab_variation2[]" placeholder="{{ __('Price') }}" value="{{$userslabs->variation2}}">
                                       </td>
                                       <td class="user_slab_variation_price3">
                                          <div class="min-max-priceCol">
                                             <input type="text" class="form-control user_slab_variation_price3" placeholder="{{ __('Lower Limit') }}" disabled>
                                             <input type="text" class="form-control user_slab_variation_price3" placeholder="{{ __('Upper Limit') }}" disabled>
                                          </div>
                                          <input type="text" class="form-control mt-2 user_slab_variation_price3" name="user_slab_variation3[]" placeholder="{{ __('Price') }}" value="{{$userslabs->variation3}}">
                                       </td>
                                       <td>                                                
                                          <button type="button" class="btn btn-success addCF-userSlab">
                                          <i class="nav-icon i-Add "></i>
                                          </button>
                                       </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                 </tbody>
                              </table>
                           </div>
                           <div id="module_price_section" class="mt-5" style="@if (count($product_modules)==0)
                        display: none;
                        @endif">
                              <h4 class="pricing-table-head">{{ __('Module Price') }}:- </h4>
                              <table class="table table-bordered">
                                 <thead>
                                    <tr>
                                       <th scope="col" width="20%"> Particular </th>
                                       <th class="module_variation_price1" scope="col"> Variation 1   </th>
                                       <th class="module_variation_price2" scope="col"> Variation 2</th>
                                       <th class="module_variation_price3" scope="col"> Variation 3</th>
                                       <th scope="col"> Licence Type</th>
                                       <th scope="col"> Action</th>
                                    </tr>
                                 </thead>
                                 <tbody>
                                    @if(!count($product_modules)==0)
                                    @foreach($product_modules as $modules)
                                    <tr>
                                       <td> <input type="text" name="module_name[]" class="form-control" placeholder="{{ __('Enter Module Name') }}" name="" value="{{$modules->moduleName}}"></td>
                                       <td class="module_variation_price1"><input type="text" class="form-control" placeholder="{{ __('Module Price') }}" name="module_price_variation1[]" value="{{$modules->variation1}}"></td>
                                       <td class="module_variation_price2"><input type="text" class="form-control" placeholder="{{ __('Module Price') }}" name="module_price_variation2[]" value="{{$modules->variation2}}"></td>
                                       <td class="module_variation_price3"><input type="text" class="form-control" placeholder="{{ __('Module Price') }}" name="module_price_variation3[]" value="{{$modules->variation3}}"></td>
                                       <td>  
                                       <div class="sambhav">
                                          <label class="checkbox checkbox-primary">    
                                             <input type="checkbox" class="pricing_type" id="pricing_type[]" @if($modules->user_based_pricing == '1') checked="" @endif>     
                                             <span>{{ __('User Based') }}</span>    
                                             <span class="checkmark"></span>   
                                             <input type="hidden" class="abc" name="pricing_type[]" value="@if($modules->user_based_pricing == '1') 1 @else 0 @endif">  
                                          </label>
                                       </div>  
                                                 
                                       </td>
                                       <td>                                                 
                                          <button type="button" class="btn btn-success addCF-pricing">
                                          <i class="nav-icon i-Add "></i>
                                          </button>
                                       </td>
                                    </tr>
                                    @endforeach
                                    @endif
                                 </tbody>
                              </table>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="col-lg-12">
                     <div class="text-center">
                        <button class="tablink addPro-next-btn" type="submit">{{ __("Update") }}</button>
                     </div>
                  </div>
               </div>
            </form>

         </div>
      </div>
   </div>
</div>
<div class="modal fade" id="setgallery" tabindex="-1" role="dialog" aria-labelledby="setgallery" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered  modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title" id="exampleModalCenterTitle">{{ __('Image Gallery') }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
            </button>
         </div>
         <div class="modal-body">
            <div class="top-area">
               <div class="row">
                  <div class="col-sm-6 text-right">
                     <div class="upload-img-btn">
                        <label for="image-upload" id="prod_gallery"><i class="icofont-upload-alt"></i>{{ __('Upload File') }}</label>
                     </div>
                  </div>
                  <div class="col-sm-6">
                     <a href="javascript:;" class="upload-done" data-dismiss="modal"> <i class="fas fa-check"></i> {{ __('Done') }}</a>
                  </div>
                  <div class="col-sm-12 text-center">( <small>{{ __('You can upload multiple Images.') }}</small> )</div>
               </div>
            </div>
            <div class="gallery-images">
               <div class="selected-image">
                  <div class="row">
                  </div>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
@endsection
@section('scripts')
@section('scripts')
<script src="{{asset('assets/admin/js/jquery.Jcrop.js')}}"></script>
<script src="{{asset('assets/admin/js/jquery.SimpleCropper.js')}}"></script>
<script src="{{asset('assets/admin/js/jquery.tagsinput-revisited.js')}}"></script>

<script type="text/javascript">
   $(document).ready(function(){
         //$('form.editOverview .chosen-select').val();
        
         $("#i_numberOfvar").change(function(){
            var count = $('#i_numberOfvar').val();
         if(count == '1')
         {
         $(".i_varient2 input").attr("disabled","disabled");
         $(".i_varient3 input").attr("disabled","disabled");
         }
         else if(count == '2')
         {           
            $(".i_varient2 input").removeAttr("disabled");
            $(".i_varient3 input").attr("disabled","disabled");
   
         }
         else if(count == '3')
         {
            $(".i_varient2 input").removeAttr("disabled");
            $(".i_varient3 input").removeAttr("disabled");
            
         }
         });
      });
   
$(document).ready(function(){  
         var i=1;
   
         $(document).on('click', '.addCF-implementation', function(){
            var implementationCount = $('#i_numberOfvar').val();
         // var variation_user_slab_count = $('#variation_count').val();
         // alert(implementationCount)
         if(implementationCount == '1')
         {

            $("#implementation-table-wrapper table").append('<tr><td><input type="text" name="i_variant_name[]" class="form-control" value="" placeholder="Feature"></td><td class="i_varient1"><input type="text" class="form-control" name="i_varient1[]"></td><td class="i_varient2"><input type="text" class="form-control" name="i_varient2[]" disabled="disabled"></td><td class="i_varient3"><input type="text" class="form-control" name="i_varient3[]" disabled="disabled"></td><td> <button type="button" class="btn btn-danger remCF-impementation"> <i class="nav-icon i-Close-Window "></i> </button> </td></tr>');
            
         }
         else if(implementationCount == '2')
         {
            $("#implementation-table-wrapper table").append('<tr> <td><input type="text" name="i_variant_name[]" class="form-control" value="" placeholder="Feature"></td><td class="i_varient1"><input type="number" class="form-control" name="i_varient1[]"></td><td class="i_varient2"><input type="number" class="form-control" name="i_varient2[]"></td><td class="i_varient3" disabled="disabled"><input type="number" class="form-control" name="i_varient3[]"></td><td> <button type="button" class="btn btn-danger remCF-impementation"> <i class="nav-icon i-Close-Window "></i> </button> </td></tr>');
         }
         else if(implementationCount == '3')
         {
            $("#implementation-table-wrapper table").append('<tr> <td><input type="text" name="i_variant_name[]" class="form-control" value="" placeholder="Feature"></td><td class="i_varient1"><input type="number" class="form-control" name="i_varient1[]"></td><td class="i_varient2"><input type="number" class="form-control" name="i_varient2[]"></td><td class="i_varient3"><input type="number" class="form-control" name="i_varient3[]"></td><td> <button type="button" class="btn btn-danger remCF-impementation"> <i class="nav-icon i-Close-Window "></i> </button> </td></tr>');
         }
   
             
       });  
   
       $("#implementation-table-wrapper table").on('click','.remCF-impementation',function(){
           $(this).parent().parent().remove();
         });
   
      });

      // Add implementation End //



      // Add Support start //
   
   $(document).ready(function(){
         $("#s_numberOfvar").change(function(){
            var count = $('#s_numberOfvar').val();
         if(count == '1')
         {
         $(".s_varient2 input").attr("disabled","disabled");
         $(".s_varient3 input").attr("disabled","disabled");
         }
         else if(count == '2')
         {           
            $(".s_varient2 input").removeAttr("disabled");
            $(".s_varient3 input").attr("disabled","disabled");
   
         }
         else if(count == '3')
         {
            $(".s_varient2 input").removeAttr("disabled");
            $(".s_varient3 input").removeAttr("disabled");
            
         }
         });
      });
   

      $(document).ready(function(){  
         var i=1;
   
         $(document).on('click', '.addCF-support', function(){
            var supportCount = $('#i_numberOfvar').val();
         // var variation_user_slab_count = $('#variation_count').val();
         //alert(supportCount)
         if(supportCount == '1')
         {

            $("#supportTable-wrap table").append('<tr> <td><input type="text" name="variant_name_support[]" class="form-control" value="" placeholder="Feature"></td><td class="s_varient1"><input type="text" class="form-control" name="s_varient1[]"></td><td class="s_varient2"><input type="text" class="form-control" name="s_varient2[]" disabled="disabled"></td><td class="s_varient3"><input type="text" class="form-control" name="s_varient3[]" disabled="disabled"></td><td> <button type="button" class="btn btn-danger remCF-support"> <i class="nav-icon i-Close-Window "></i> </button> </td></tr>');
            
         }
         else if(supportCount == '2')
         {
            $("#supportTable-wrap table").append('<tr> <td><input type="text" name="variant_name_support[]" class="form-control" value="" placeholder="Feature"></td><td class="s_varient1"><input type="text" class="form-control" name="s_varient1[]"></td><td class="s_varient2"><input type="text" class="form-control" name="s_varient2[]"></td><td class="s_varient3"><input type="text" class="form-control" name="s_varient3[]" disabled="disabled"></td><td> <button type="button" class="btn btn-danger remCF-support"> <i class="nav-icon i-Close-Window "></i> </button> </td></tr>');
         }
         else if(supportCount == '3')
         {
            $("#supportTable-wrap table").append('<tr> <td><input type="text" name="variant_name_support[]" class="form-control" value="" placeholder="Feature"></td><td class="s_varient1"><input type="text" class="form-control" name="s_varient1[]"></td><td class="s_varient2"><input type="text" class="form-control" name="s_varient2[]"></td><td class="s_varient3"><input type="text" class="form-control" name="s_varient3[]"></td><td> <button type="button" class="btn btn-danger remCF-support"> <i class="nav-icon i-Close-Window "></i> </button> </td></tr>');
         }
   
             
       });  
   
       $("#supportTable-wrap table").on('click','.remCF-support',function(){
           $(this).parent().parent().remove();
         });
   
      });

   // Gallery Section Insert
    $(document).on("click", ".set-gallery" , function(){
           var pid = $(this).find('input[type=hidden]').val();
           $('#pid').val(pid);
           $('.selected-image .row').html('');
               $.ajax({
                       type: "GET",
                       url:"{{ route('admin-gallery-show') }}",
                       data:{id:pid},
                       success:function(data){
                         if(data[0] == 0)
                         {
   	                    $('.selected-image .row').addClass('justify-content-center');
   	      				$('.selected-image .row').html('<h3>{{ __('No Images Found.') }}</h3>');
        				  }
                         else {
   	                    $('.selected-image .row').removeClass('justify-content-center');
   	      				$('.selected-image .row h3').remove();      
                             var arr = $.map(data[1], function(el) {
                             return el });
   
                             for(var k in arr)
                             {
           				$('.selected-image .row').append('<div class="col-sm-6">'+
                                           '<div class="img gallery-img">'+
                                               '<span class="remove-img"><i class="fas fa-times"></i>'+
                                               '<input type="hidden" value="'+arr[k]['id']+'">'+
                                               '</span>'+
                                               '<a href="'+'{{asset('assets/images/galleries').'/'}}'+arr[k]['photo']+'" target="_blank">'+
                                               '<img src="'+'{{asset('assets/images/galleries').'/'}}'+arr[k]['photo']+'" alt="gallery image">'+
                                               '</a>'+
                                           '</div>'+
                                     	'</div>');
                             }                         
                          }
    
                       }
                     });
         });
   
    $(document).ready(function() {
   
       let html = `<img src="{{ empty($data->photo) ? asset('assets/images/noimage.png') : filter_var($data->photo, FILTER_VALIDATE_URL) ? $data->photo : asset('assets/images/products/'.$data->photo) }}" alt="">`;
       $(".span4.cropme").html(html);
   
       $.ajaxSetup({
           headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
           }
       });
   
     });
   
     $(document).on('click', '.remove-img' ,function() {
       var id = $(this).find('input[type=hidden]').val();
       $('#galval'+id).remove();
       $(this).parent().parent().remove();
     });
   
     $(document).on('click', '#prod_gallery' ,function() {
       $('#uploadgallery').click();
        $('.selected-image .row').html('');
       $('#geniusform').find('.removegal').val(0);
     });
   
     $("#featureId").change(function() {
        var featureId  = $(this).val();
       $.ajax({
           url: "../../products/featureModules/"+featureId,
           type: "get",
          // data: $(this).val(),
          // dataType: 'JSON',
           success: function (data) {
               $('#FeatureModules').html(data);
   
               //alert(data)
   
           }
   	}); 
     });            
       ////// display subcategory fields section
            
     $("#uploadgallery").change(function(){
        var total_file=document.getElementById("uploadgallery").files.length;
        for(var i=0;i<total_file;i++)
        {
         $('.selected-image .row').append('<div class="col-sm-6">'+
                                           '<div class="img gallery-img">'+
                                               '<span class="remove-img"><i class="fas fa-times"></i>'+
                                               '<input type="hidden" value="'+i+'">'+
                                               '</span>'+
                                               '<a href="'+URL.createObjectURL(event.target.files[i])+'" target="_blank">'+
                                               '<img src="'+URL.createObjectURL(event.target.files[i])+'" alt="gallery image">'+
                                               '</a>'+
                                           '</div>'+
                                     '</div> '
                                         );
         $('#geniusform').append('<input type="hidden" name="galval[]" id="galval'+i+'" class="removegal" value="'+i+'">')
        }
   
     });
   
  ///////add varient//////////// 
      $(document).ready(function(){  
         var i=1;
   
         $(document).on('click', '.addCF', function(){
            var count = $('#numberOfvar').val();
         // var variation_user_slab_count = $('#variation_count').val();
         //alert(variation_user_slab_count)
         if(count == '1')
         {

            $("#variations-table-wrap").append('<tr> <td><input type="text" name="variant_name[]" class="form-control" value="" placeholder="Feature"></td><td class="varient1"><input type="text" class="form-control" name="varient1[]"></td><td class="varient2"><input type="text" class="form-control" name="varient2[]" disabled="disabled"></td><td class="varient3"><input type="text" class="form-control" name="varient3[]" disabled="disabled"></td><td> <button type="button" class="btn btn-danger remCF"> <i class="nav-icon i-Close-Window "></i> </button> </td></tr>');
            
         }
         else if(count == '2')
         {
            $("#variations-table-wrap").append('<tr> <td><input type="text" name="variant_name[]" class="form-control" value="" placeholder="Feature"></td><td class="varient1"><input type="text" class="form-control" name="varient1[]"></td><td class="varient2"><input type="text" class="form-control" name="varient2[]"></td><td class="varient3"><input type="text" class="form-control" name="varient3[]" disabled="disabled"></td><td> <button type="button" class="btn btn-danger remCF"> <i class="nav-icon i-Close-Window "></i> </button> </td></tr>');
         }
         else if(count == '3')
         {
            $("#variations-table-wrap").append('<tr> <td><input type="text" name="variant_name[]" class="form-control" value="" placeholder="Feature"></td><td class="varient1"><input type="text" class="form-control" name="varient1[]"></td><td class="varient2"><input type="text" class="form-control" name="varient2[]"></td><td class="varient3"><input type="text" class="form-control" name="varient3[]"></td><td> <button type="button" class="btn btn-danger remCF"> <i class="nav-icon i-Close-Window "></i> </button> </td></tr>');
         }
   
             
       });  
   
       $("#variations-table-wrap").on('click','.remCF',function(){
           $(this).parent().parent().remove();
         });
   
      });

      // Add variation End //
      
   
   // Gallery Section Insert Ends	
   
</script>
<script type="text/javascript">
   $('.cropme').simpleCropper();
   $('#crop-image').on('click',function(){
   $('.cropme').click();
   });
</script><script>
   $('.addProduct-tabs-wrap button').on('click', function(){
     	$('.addProduct-tabs-wrap button.active').removeClass('active');
     	$(this).addClass('active');
   });
   function openPage(pageName,elmnt,color) {
   	//alert(pageName)id="prod_id"
     //var pro_id = document.getElementById("prod_id").value;
     //alert(pro_id)
     var i, tabcontent, tablinks;
     tabcontent = document.getElementsByClassName("body-area");
     for (i = 0; i < tabcontent.length; i++) {
       tabcontent[i].style.display = "none";
     }
   
     // tablinks = document.getElementsByClassName("tablink");
     // for (i = 0; i < tablinks.length; i++) {
     //   tablinks[i].style.backgroundColor = "";
   
     // }
     document.getElementById(pageName).style.display = "block";
     // elmnt.style.backgroundColor = color;
     // if(pro_id !=''){
     // 		if(pageName == 'Overview'){
     // 			document.getElementById("genralTab").disabled = true;
     // 			document.getElementById("genralTab").style.pointerEvents = "none";
     // 			document.getElementById("genralTab").style.backgroundColor = "lightgray";
     // 		}if(pageName == 'Features'){
     // 			document.getElementById("genralTab").disabled = true;
     // 			document.getElementById("genralTab").style.pointerEvents = "none";
     // 			document.getElementById("genralTab").style.backgroundColor = "lightgray";
     // 			document.getElementById("overviewTab").disabled = true;
     // 			document.getElementById("overviewTab").style.pointerEvents = "none";
     // 			document.getElementById("overviewTab").style.backgroundColor = "lightgray";
     // 		}if(pageName == 'Variations'){
     // 			document.getElementById("genralTab").disabled = true;
     // 			document.getElementById("genralTab").style.pointerEvents = "none";
     // 			document.getElementById("genralTab").style.backgroundColor = "lightgray";
     // 			document.getElementById("overviewTab").disabled = true;
     // 			document.getElementById("overviewTab").style.pointerEvents = "none";
     // 			document.getElementById("overviewTab").style.backgroundColor = "lightgray";
     // 			document.getElementById("featureTab").disabled = true;
     // 			document.getElementById("featureTab").style.pointerEvents = "none";
     // 			document.getElementById("featureTab").style.backgroundColor = "lightgray";
     // 		}
   
     // }
   }
   
   function openNextPage(pageName,elmnt,color) {
   	//alert(elmnt)
     var i, tabcontent, tablinks;
     tabcontent = document.getElementsByClassName("body-area");
     for (i = 0; i < tabcontent.length; i++) {
       tabcontent[i].style.display = "none";
   
     }
     tablinks = document.getElementsByClassName("tablink");
     for (i = 0; i < tablinks.length; i++) {
       tablinks[i].style.backgroundColor = "";
   
     }
     document.getElementById(pageName).style.display = "block";
     document.getElementById(elmnt).style.backgroundColor = color;
     //if()
   }
   
   
   // Get the element with id="defaultOpen" and click on it
   document.getElementById("defaultOpen").click();
</script>
<script>
   var coll = document.getElementsByClassName("collapsible");
   var i;
   
   for (i = 0; i < coll.length; i++) {
     coll[i].addEventListener("click", function() {
       this.classList.toggle("active");
       var content = this.nextElementSibling;
       if (content.style.maxHeight){
         content.style.maxHeight = null;
       } else {
         content.style.maxHeight = content.scrollHeight + "px";
       } 
     });
   }
</script>
<script src="{{asset('assets/admin/js/product.js')}}"></script>
<script>
        $(document).ready(function(){  
         var i=1;
   
       $(document).on('click', '.addCF-pricing', function(){
         var variation_count = $('#variation_count').val();
   
         if(variation_count == '1')
         {
            $("#module_price_section table").append('<tr> <td> <input type="text" class="form-control" placeholder="{{__('Enter Module Nam')}}" name="module_name[]"></td><td class="module_variation_price1"><input type="text" class="form-control 1" placeholder="{{__('Module Price')}}" name="module_price_variation1[]"></td><td class="module_variation_price2" style="display:none;"><input type="text" class="form-control"  placeholder="{{__('Module Price')}}" name="module_price_variation2[]"></td><td class="module_variation_price3" style="display:none;"><input type="text" class="form-control"  placeholder="{{__('Module Price')}}" name="module_price_variation3[]"></td><td><label class="checkbox checkbox-primary"><input type="checkbox" name="pricing_type[]" class="pricing_type" id="pricing_type[]"><span>{{ __('User Based') }}</span><span class="checkmark"></span></label></td><td> <button type="button" class="btn btn-danger remCF-pricing"> <i class="nav-icon i-Close-Window "></i> </button> </td></tr>');
         }
         else if(variation_count == '2')
         {
            $("#module_price_section table").append('<tr> <td> <input type="text" class="form-control" placeholder="{{__('Enter Module Nam')}}" name="module_name[]"></td><td class="module_variation_price1"><input type="text" class="form-control" placeholder="{{__('Module Price')}}" name="module_price_variation1[]"></td><td class="module_variation_price2"><input type="text" class="form-control" placeholder="{{__('Module Price')}}" name="module_price_variation2[]"></td><td class="module_variation_price3" style="display:none;"><input type="text" class="form-control" placeholder="{{__('Module Price')}}" name="module_price_variation3[]"></td><td><label class="checkbox checkbox-primary"><input type="checkbox" name="pricing_type[]" class="pricing_type" id="pricing_type[]"><span>{{ __('User Based') }}</span><span class="checkmark"></span></label></td><td> <button type="button" class="btn btn-danger remCF-pricing"> <i class="nav-icon i-Close-Window "></i></button></td></tr>');
         }
         else if(variation_count == '3')
         {
            $("#module_price_section table").append('<tr> <td> <input type="text" class="form-control" placeholder="{{__('Enter Module Nam')}}" name="module_name[]"></td><td class="module_variation_price1"><input type="text" class="form-control" placeholder="{{__('Module Price')}}" name="module_price_variation1[]"></td><td class="module_variation_price2"><input type="text" class="form-control" placeholder="{{__('Module Price')}}" name="module_price_variation2[]"></td><td class="module_variation_price3"><input type="text" class="form-control" placeholder="{{__('Module Price')}}" name="module_price_variation3[]"></td><td><label class="checkbox checkbox-primary"><input type="checkbox" name="pricing_type[]" class="pricing_type" id="pricing_type[]"><span>{{ __('User Based') }}</span><span class="checkmark"></span></label></td><td> <button type="button" class="btn btn-danger remCF-pricing"> <i class="nav-icon i-Close-Window "></i> </button> </td></tr>');  
         }
   
         $("#module_price_section table").on('click','.remCF-pricing',function(){
           $(this).parent().parent().remove();
       });
           
        });  
   
   
       $(document).on('click', '.btn_remove_module_price', function(){  
          var button_id = $(this).attr("id");   
          $('#row'+button_id+'').remove();  
       }); 
   
   });
   
      //---------------End -------------------------------// 
   
      //-----------add user slab section through js (Sambhav)--------------//
      $(document).ready(function(){  
         var i=1;
   
         $(document).on('click', '.addCF-userSlab', function(){
   
         var variation_user_slab_count = $('#variation_count').val();
         
         // alert(variation_user_slab_count)
         if(variation_user_slab_count == '1')
         {
            $("#user_slab_section table").append('<tr><td><input type="text" class="form-control" value="{{__('User Slab')}}" name="" disabled></td><td class="user_slab_variation_price1"> <div class="min-max-priceCol"> <input type="text" class="form-control" placeholder="{{__('Lower Limit')}}" name="user_slab_lower_limit[]"> <input type="text" class="form-control" placeholder="{{__('Upper Limit')}}" name="user_slab_upper_limit[]"> </div><input type="text" class="form-control mt-2 user_slab_variation_price1" name="user_slab_variation1[]"  placeholder="{{__('Price')}}"> </td><td class="user_slab_variation_price2" style="display:none;"> <div class="min-max-priceCol"> <input type="text" class="form-control" placeholder="{{__('Lower Limit')}}" style="display:none;"> <input type="text" class="form-control" placeholder="{{__('Upper Limit')}}" style="display:none;"> </div><input type="text" class="form-control mt-2 user_slab_variation_price2" name="user_slab_variation2[]" style="display:none;" placeholder="{{__('Price')}}"> </td><td class="user_slab_variation_price3" style="display:none;"> <div class="min-max-priceCol"> <input type="text" class="form-control" placeholder="{{__('Lower Limit')}}" style="display:none;"> <input type="text" class="form-control" placeholder="{{__('Upper Limit')}}" style="display:none;"> </div><input type="text" class="form-control mt-2 user_slab_variation_price3" name="user_slab_variation3[]" style="display:none;" placeholder="{{__('Price')}}"> </td><td> <button type="button" class="btn btn-danger remCF-userSlab"> <i class="nav-icon i-Close-Window "></i> </button> </td></tr>');
            
         }
         else if(variation_user_slab_count == '2')
         {
            $("#user_slab_section table").append('<tr><td><input type="text" class="form-control" value="{{__('User Slab')}}" name="" disabled></td><td class="user_slab_variation_price1"> <div class="min-max-priceCol"> <input type="text" class="form-control" placeholder="{{__('Lower Limit')}}" name="user_slab_lower_limit[]"> <input type="text" class="form-control" placeholder="{{__('Upper Limit')}}" name="user_slab_upper_limit[]"> </div><input type="text" class="form-control mt-2 user_slab_variation_price1" name="user_slab_variation1[]"  placeholder="{{__('Price')}}"> </td><td class="user_slab_variation_price2"> <div class="min-max-priceCol"> <input type="text" class="form-control" placeholder="{{__('Lower Limit')}}" disabled> <input type="text" class="form-control" placeholder="{{__('Upper Limit')}}" disabled> </div><input type="text" class="form-control mt-2 user_slab_variation_price2" name="user_slab_variation2[]" placeholder="{{__('Price')}}"> </td><td class="user_slab_variation_price3" style="display:none;"> <div class="min-max-priceCol"> <input type="text" class="form-control" placeholder="{{__('Lower Limit')}}" style="display:none;"> <input type="text" class="form-control" placeholder="{{__('Upper Limit')}}" style="display:none;"> </div><input type="text" class="form-control mt-2 user_slab_variation_price3" name="user_slab_variation3[]" style="display:none;" placeholder="{{__('Price')}}"> </td><td> <button type="button" class="btn btn-danger remCF-userSlab"> <i class="nav-icon i-Close-Window "></i> </button> </td></tr>');
         }
         else if(variation_user_slab_count == '3')
         {
            $("#user_slab_section table").append('<tr><td><input type="text" class="form-control" value="{{__('User Slab')}}" name="" disabled></td><td class="user_slab_variation_price1"> <div class="min-max-priceCol"> <input type="text" class="form-control" placeholder="{{__('Lower Limit')}}" name="user_slab_lower_limit[]"> <input type="text" class="form-control" placeholder="{{__('Upper Limit')}}" name="user_slab_upper_limit[]"> </div><input type="text" class="form-control mt-2 user_slab_variation_price1" name="user_slab_variation1[]"  placeholder="{{__('Price')}}"> </td><td class="user_slab_variation_price2"> <div class="min-max-priceCol"> <input type="text" class="form-control" placeholder="{{__('Lower Limit')}}" "> <input type="text" class="form-control" placeholder="{{__('Upper Limit')}}"> </div><input type="text" class="form-control mt-2 user_slab_variation_price2" name="user_slab_variation2[]" placeholder="{{__('Price')}}"> </td><td class="user_slab_variation_price3"> <div class="min-max-priceCol"> <input type="text" class="form-control" placeholder="{{__('Lower Limit')}}"> <input type="text" class="form-control" placeholder="{{__('Upper Limit')}}"> </div><input type="text" class="form-control mt-2 user_slab_variation_price3" name="user_slab_variation3[]" placeholder="{{__('Price')}}"> </td><td> <button type="button" class="btn btn-danger remCF-userSlab"> <i class="nav-icon i-Close-Window "></i> </button> </td></tr>');
         }
   
             
       });  
   
       $("#user_slab_section table").on('click','.remCF-userSlab',function(){
           $(this).parent().parent().remove();
         });
   
   });
   
      //---------------End -------------------------------// 
   
      //-------Manage variation column number (Sambhav)-----------------//
   
      $(document).ready(function(){  
   
         var count = $('#variation_count').val();

         if(count == '1')
         {
            $('.fixed_variation_price2').hide();
            $('.fixed_variation_price3').hide();
            $('.user_slab_variation_price2').hide();
            $('.user_slab_variation_price3').hide();
            $('.module_variation_price2').hide();
            $('.module_variation_price3').hide();
         }
         else if(count == '2')
         {
            $('.fixed_variation_price3').hide();
            $('.user_slab_variation_price3').hide();
            $('.module_variation_price3').hide();
   
         }
         else if(count == '3')
         {
            //$('.fixed_variation_price1').attr('disabled', 'false');
            // $('.variation_module1').attr('disabled', 'false');
            // $('.variation_user_slab1').attr('disabled', 'false');
            //$('.fixed_variation_price2').attr('disabled', 'false');
            // $('.variation_module2').attr('disabled', 'false');
            // $('.variation_user_slab2').attr('disabled', 'false');
            //$('.fixed_variation_price3').attr('disabled', 'false');
            // $('.variation_module3').attr('disabled', 'false');
            // $('.variation_user_slab3').attr('disabled', 'false');
         }
      });
      $(document).on('change', '#fixed_price_checkbox', function(){  
   
         if($(this).prop("checked") == true){
              $('#fixed_price_section').show();
          }
          else if($(this).prop("checked") == false){
              $('#fixed_price_section').hide();
          }
   
      });
   
      $(document).on('change', '#user_slab_checkbox', function(){  
   
         if($(this).prop("checked") == true){
              $('#user_slab_section').show();
   
          }
          else if($(this).prop("checked") == false){
              $('#user_slab_section').hide();
          }
   
      });
   
      $(document).on('change', '#module_price_checkbox', function(){  
   
         if($(this).prop("checked") == true){
              $('#module_price_section').show();
   
          }
          else if($(this).prop("checked") == false){
              $('#module_price_section').hide();
          }
   
      });  
   $(".pricing_type").change(function(event){
         
      var name = $(this).attr("id"); 
       if ($(this).prop('checked')) {
            $(this).closest("div.sambhav").find("input[type=hidden]").val('1');
            
            
        }
        else {
            $(this).closest("div.sambhav").find("input[type=hidden]").val('0');
        }
        // var value = $(this).val();
        // var name = $(this).attr("id");
        // //alert(name);
        // $("input[name='"+name+"']").val(value);
        
     
});
</script>
<script src="{{asset('assets/admin/js/chosen.jquery.js')}}"></script>
<script>
   document.getElementById('prodlist').innerHTML = location.search;
   $(".chosen-product").chosen();
</script>
<script>
   $('#addon_products').change(function(event){
    if(event.target == this){
        var value = $(this).val();
        $("input[name='addon_products']").val(value);
     }
});
</script>
<!----------------------------- Add row ------------------->
@endsection