@extends('layouts.admin')

@section('content')

<div class="content-area">
            <div class="mr-breadcrumb">
              <div class="row">
                <div class="col-lg-12">
                    <h4 class="heading">{{ __("Add Brand") }}</h4>
                    <ul class="links">
                      <li>
                        <a href="{{ route('admin.dashboard') }}">{{ __("Dashboard") }}</a>
                      </li>
                      <!-- <li>
                        {{ __("Product Management") }}
                      </li> -->
                      <li>
                        <a href="{{ route('brands-view') }}">{{ __("All Brands") }}</a>
                      </li>
                      <li>
                        <a href="{{ route('brands-add') }}">{{ __("Add Brands") }}</a>
                      </li>
                    </ul>
                </div>
              </div>
            </div>
            <div class="add-product-content small-label">
              <!-- <div class="row">
                <div class="col-lg-12">
                  <div class="product-description">
                    <div class="heading-area">
                      <h2 class="title">
                          {{ __("Brands") }}
                      </h2>
                    </div>
                  </div>
                </div>
              </div> -->
              <div class="ap-product-categories">
                <div class="row">
                  <div class="col-lg-12">
                      <div class="product-description">
                      <div class="body-area">

                    <form name="add_brand" action="{{ route('brands-create') }}" method="post" enctype="multipart/form-data">
                          {{csrf_field()}}

                     <div class="row">
                          <div class="col-lg-3">
                            <div class="left-area">
                                <h4 class="heading">{{ __("Image") }}*</h4>
                            </div>
                          </div>
                          <div class="col-lg-8">
                              <input type="file" name="image" id="image">
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-lg-3">
                            <div class="left-area">
                                <h4 class="heading">{{ __("Name") }}*</h4>
                            </div>
                          </div>
                          <div class="col-lg-8">
                              <input type="text" class="form-control" name="name" id="name">
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-lg-3">
                            <div class="left-area">
                                <h4 class="heading">{{ __("Description") }}*</h4>
                            </div>
                          </div>
                          <div class="col-lg-8">
                              <textarea name="description" class="form-control" id="description" rows="4" cols="50"></textarea>
                              <!-- <input type="text" name="description" id="description" rows="4" cols="50"> -->
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-lg-3">
                            <div class="left-area">
                                <h4 class="heading">{{ __("Some information") }}*</h4>
                            </div>
                          </div>
                          <div class="col-lg-8">
                              <input type="text" class="form-control" name="information" id="information">
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-lg-3">
                            <div class="left-area">
                                <h4 class="heading">{{ __("Brand Specs") }}*</h4>
                            </div>
                          </div>
                          <div class="col-lg-8">
                              <input type="file" name="brand_specs" id="brand_specs">
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-lg-3">
                            <div class="left-area">
                                <h4 class="heading">{{ __("About the Company") }}*</h4>
                            </div>
                          </div>
                          <div class="col-lg-8">
                            <textarea name="company_information" class="form-control" id="company_information" rows="4" cols="50"></textarea>
                              <!-- <input type="text" name="company_information" id="company_information"> -->
                          </div>
                        </div>

                        <div class="row">
                          <div class="col-lg-3">
                            <div class="left-area">
                              
                            </div>
                          </div>
                          <div class="col-lg-8 text-center">
                            <button class="createBrand-btn btn btn-secondary" type="submit">Create Brand</button>
                          </div>
                        </div>

                    </form>
                  </div>
                </div>
                  </div>
                </div>
              </div>
            </div>
          </div>

@endsection