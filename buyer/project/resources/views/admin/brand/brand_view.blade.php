@extends('layouts.admin') 
@section('content')  
<input type="hidden" id="headerdata" value="{{ __("PRODUCT") }}">
<div class="content-area">
   <div class="mr-breadcrumb">
      <div class="row">
         <div class="col-lg-12">
            <h4 class="heading">Brands</h4>
            <ul class="links">
               <li>
                  <a href="{{ route('admin.dashboard') }}">{{ __("Dashboard") }} </a>
               </li>
               <li>
                  <a href="">Product Management</a>
               </li>
               <li>
                  <a href="{{ route('brands-view') }}">{{ __("Brands") }}</a>
               </li>
            </ul>
         </div>
      </div>
   </div>
   <div class="product-area">
      <div class="row">
         <div class="col-lg-12">
            <div class="allproduct">
               @include('includes.admin.form-success')  
               <div class="table-contents addBrand-btn-box">
                  <a class="add-btn btn btn-success add-row" href="{{route('brands-add')}}">
                  <i class="nav-icon i-Add "></i> <span class="remove-mobile">{{ __("Add New Brand") }}<span>
                  </a>
               </div>
               <!-- <div class="table-responsiv">
                  <table id="geniustable" class="table table-hover dt-responsive" cellspacing="0" width="100%">
                  	<thead>
                                         <tr>
                                                   <th>{{ __("Name") }}</th>
                                                   <th>{{ __("Image") }}</th>
                                                   <th>{{ __("Actions") }}</th>
                                         </tr>
                                     </thead>
                                     <tbody>
                                          @foreach($brands as $brand)
                                          <tr>
                  
                                            <td>
                                              {{$brand->name}}
                                            </td>
                                            	
                                            <td>
                                      		<img src="{{asset('assets/brand_images/'.$brand->image) }}" width="50px" height="50px">
                                            </td>
                                            <td>
                                                <div class="action-list"><a href="" data-href="{{ route('brands-edit',$brand->id) }}" class="edit" data-toggle="modal" data-target="#modal1"> <i class="fas fa-edit"></i>Edit</a><a href="" data-href="{{ route('brands-delete',$brand->id) }}" data-toggle="modal" data-target="#confirm-delete" class="delete"><i class="fas fa-trash-alt"></i></a></div>
                                            </td>
                  
                                          </tr>
                                         @endforeach
                  
                                     </tbody>
                  </table>
                  </div> -->
               <div class="table-responsive">
                  <table class="table table-bordered table-striped">
                     <thead class="thead-dark">
                        <tr>
                           <th width="5%"> {{ __("S.No") }}</th>
                           <th scope="col" width="35%">{{ __("Name") }}</th>
                           <th scope="col" width="30%">{{ __("Thumbnail") }}</th>
                           <th scope="col" width="30%">{{ __("Actions") }}</th>
                        </tr>
                     </thead>
                     <tbody>
                        @php
                        $i=1;
                        @endphp
                        @foreach($brands as $brand)
                        <tr>
                           <td>{{$i}}</td>
                           <td>{{$brand->name}}</td>
                           <td><img src="{{asset('assets/brand_images/'.$brand->image) }}" width="50px" height="50px"></td>
                           <td>
                              <div class="action-list brand-action-link">
                                 <a href="" data-href="{{ route('brands-edit',$brand->id) }}" class="edit edit-brand-btn" data-toggle="modal" data-target="#modal1" > <span class="fal fa-edit"></span> Edit</a>
                                 <a href="" data-href="{{ route('brands-delete',$brand->id) }}" data-toggle="modal" data-target="#confirm-delete" class="delete-brand-btn"><span class="fal fa-trash-alt"></span> Delete</a>
                              </div>
                           </td>
                        </tr>
                        @php
                        $i++;
                        @endphp
                        @endforeach
                     </tbody>
                  </table>
               </div>
            </div>
         </div>
      </div>
   </div>
</div>
{{-- HIGHLIGHT MODAL --}}
<div class="modal fade" id="modal2" tabindex="-1" role="dialog" aria-labelledby="modal2" aria-hidden="true">
   <div class="modal-dialog highlight" role="document">
      <div class="modal-content">
         <div class="submit-loader">
            <img  src="{{asset('assets/images/'.$gs->admin_loader)}}" alt="">
         </div>
         <div class="modal-header">
            <h5 class="modal-title"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
         </div>
         <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ __("Close") }}</button>
         </div>
      </div>
   </div>
</div>
{{-- HIGHLIGHT ENDS --}}
{{-- DELETE MODAL --}}
<div class="modal fade confirm-delete-modal" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="modal1" aria-hidden="true">
   <div class="modal-dialog">
      <div class="modal-content">
         <div class="modal-header">
            <h4 class="modal-title">{{ __("Confirm Delete") }}</h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <!-- Modal body -->
         <div class="modal-body">
            <div class="delete-icon-box"> <span class="fal fa-times-circle"></span> </div>
            <h5> {{ __("Are you sure?") }}</h5>
            <p> {{ __("Do You really want to delete these Product?. This process cannot be undone.") }} </p>
            <!-- <p class="text-center">{{ __("You are about to delete this Product.") }}</p>
            <p class="text-center">{{ __("Do you want to proceed?") }}</p> -->

            <div class="delete-action-box">
              <button type="button" class="btn btn-default delete-acitonBtn" data-dismiss="modal">{{ __("Cancel") }}</button>
              <a class="btn btn-danger btn-ok delete-acitonBtn" id="delete_custom">{{ __("Delete") }}</a>
            </div>
         </div>
      </div>
   </div>
</div>
{{-- DELETE MODAL ENDS --}}
{{-- ADD / EDIT MODAL --}}
<div class="modal fade" id="modal1" tabindex="-1" role="dialog" aria-labelledby="modal1" aria-hidden="true">
   <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
      <div class="modal-content">
         <div class="modal-header">
            <h5 class="modal-title"></h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
         </div>
         <div class="modal-body">
         </div>
      </div>
   </div>
</div>
{{-- ADD / EDIT MODAL ENDS --}}
@endsection    
@section('scripts')
<script>
   $('#delete_custom').click(function(){
      location.reload();
   });
</script>
@endsection