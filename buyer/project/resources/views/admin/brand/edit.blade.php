@extends('layouts.load')
@section('content')
<div class="content-area">
   <div class="add-product-content editBrand-modal-box">
      <div class="product-description">
         <div class="body-area p-0">
            @include('includes.admin.form-error')  
            <form id="geniusformdata" action="{{route('brands-update',$data->id)}}" method="POST" enctype="multipart/form-data">
               {{csrf_field()}}
               <div class="row">
                  <div class="col-lg-4">
                     <div class="left-area">
                        <h4 class="heading">{{ __("Image") }}*</h4>
                     </div>
                  </div>
                  <div class="col-lg-7">
                     <img src="{{asset('assets/brand_images/'.$data->image) }}" width="50px" height="50px">
                     <input type="file" name="image" id="image">
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-4">
                     <div class="left-area">
                        <h4 class="heading">{{ __("Name") }} *</h4>
                     </div>
                  </div>
                  <div class="col-lg-7">
                     <input type="text" class="form-control" name="name" placeholder="{{ __("Enter Name") }}" required="" value="{{$data->name}}">
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-4">
                     <div class="left-area">
                        <h4 class="heading">{{ __("Description") }}*</h4>
                     </div>
                  </div>
                  <div class="col-lg-7">
                     <textarea name="description" class="form-control" id="description" rows="4" cols="50">{{$data->description}}</textarea>
                     <!-- <input type="text" name="description" id="description" rows="4" cols="50"> -->
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-4">
                     <div class="left-area">
                        <h4 class="heading">{{ __("Some information") }}*</h4>
                     </div>
                  </div>
                  <div class="col-lg-7">
                     <input type="text" name="information" class="form-control" id="information" value="{{$data->information}}">
                  </div>
               </div>

               <div class="row">
                  <div class="col-lg-4">
                     <div class="left-area">
                        <h4 class="heading">{{ __("Brand Specs") }}*</h4>
                     </div>
                  </div>
                  <div class="col-lg-7">
                     <input type="file" name="brand_specs" id="brand_specs">
                  </div>
               </div>
               <div class="row">
                  <div class="col-lg-4">
                     <div class="left-area">
                        <h4 class="heading">{{ __("About the Company") }}*</h4>
                     </div>
                  </div>
                  <div class="col-lg-7">
                     <textarea name="company_information" class="form-control" id="company_information" rows="4" cols="50">{{$data->company_information}}</textarea>
                     <!-- <input type="text" name="company_information" id="company_information"> -->
                  </div>
               </div>
               <div class="row mb-0">
                  <div class="col-lg-12">
                     <div class="modal-footer">
                        <button class="editBrand-save-btn btn btn-secondary" id="save_custom" type="submit">{{ __("Save") }}</button>
                        <button type="button" class="btn btn-secondary close_custom modal-closeBtn" data-dismiss="modal">{{ __("Close") }}</button>
                     </div>
                  </div>
               </div>
            </form>
         </div>
      </div>
   </div>
</div>
@endsection
@section('scripts')
<script>
   $('#save_custom').click(function(){
     window.location.reload();
   });
</script>
@endsection